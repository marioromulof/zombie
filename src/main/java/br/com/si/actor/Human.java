package br.com.si.actor;

import br.com.si.solution.DoubleSolution;
import br.com.si.util.Ordering;

public class Human implements Comparable<Human> {

	private DoubleSolution solution;
	private Double fear;
	private double defenderRate;
	private boolean adventure;

	public Human(DoubleSolution solution, double alpha, double maxFear) {
		this.fear = alpha;// * maxFear;
		this.solution = solution;
		adventure = false;
	}

	public Human(DoubleSolution solution, double alpha, double maxFear, double defenderRate) {
		this.fear = alpha * maxFear;
		this.solution = solution;
		this.defenderRate = defenderRate;
	}

	public double[] getCoords() {
		double[] coords = new double[solution.getNumberOfVariables()];

		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			coords[i] = ((DoubleSolution) solution).getVariableValue(i).doubleValue();
		}

		return coords;
	}

	public DoubleSolution getSolution() {
		return solution;
	}

	public void setSolution(DoubleSolution solution) {
		this.solution = solution;
	}

	public Double getFear() {
		return fear;
	}

	public void setFear(Double fear) {
		this.fear = fear < 0.0 ? 0.0 : fear;
	}

	public double getDefenderRate() {
		return defenderRate;
	}

	public void updateAdventure() {
		adventure = !adventure;
	}

	public boolean isAdventure() {
		return adventure;
	}

	@Override
	public int compareTo(Human h) {
		int result, i;
		DoubleSolution a = getSolution();
		DoubleSolution o = h.getSolution();

		if (getSolution().getOrder() == Ordering.ASCENDING) {
			result = a.getObjective().compareTo(o.getObjective());
			i = 0;
			while (result == 0 && i < a.getNumberOfVariables()) {
				result = a.getVariableValue(i).compareTo(o.getVariableValue(i));
				i++;
			}
			return result;
		} else {
			result = o.getObjective().compareTo(a.getObjective());
			i = 0;
			while (result == 0 && i < a.getNumberOfVariables()) {
				result = o.getVariableValue(i).compareTo(a.getVariableValue(i));
				i++;
			}
			return result;
		}
	}

	@Override
	public String toString() {
		return fear + " " + solution.toString();
	}
}
