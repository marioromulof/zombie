package br.com.si.actor;

import br.com.si.solution.DoubleSolution;
import br.com.si.util.Ordering;

public class Zombie {

	private DoubleSolution solution;
	private double huntingRate;

	public Zombie(int numberOfVariables, double huntingRate) {
		this.solution = new DoubleSolution(numberOfVariables);
		this.huntingRate = huntingRate;
	}

	public Zombie(int numberOfVariables, int immobile, Ordering order) {
		this.solution = new DoubleSolution(numberOfVariables);
		this.solution.setObjective(Double.MAX_VALUE);
		this.solution.setOrder(order);
		this.huntingRate = immobile;
	}

	public Double[] getCoords() {
		return solution.getVariables();
	}

	public DoubleSolution getSolution() {
		return solution;
	}

	public void setSolution(Double[] solution) {
		this.solution.setVariables(solution);
	}

	public boolean isChaser() {
		return huntingRate != 0.0;
	}

	public void setChaser(double chaser) {
		huntingRate = chaser;
	}

	public double getHuntingRate() {
		return huntingRate;
	}

	@Override
	public String toString() {
		return huntingRate + " " + solution.toString();
	}
}
