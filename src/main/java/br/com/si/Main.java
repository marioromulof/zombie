package br.com.si;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import br.com.si.algorithm.Algorithm;
import br.com.si.algorithm.BatAlgorithm;
import br.com.si.algorithm.Complexity;
import br.com.si.algorithm.EdaOptimizer;
import br.com.si.algorithm.GA;
import br.com.si.algorithm.ICMAES;
import br.com.si.algorithm.MultiStart;
import br.com.si.algorithm.SPSO;
import br.com.si.algorithm.SimpleCmaEs;
import br.com.si.algorithm.ZombieEDA;
import br.com.si.params.BatParams;
import br.com.si.params.CmaEsParams;
import br.com.si.params.EdaParams;
import br.com.si.params.GaParams;
import br.com.si.params.MultiStartParams;
import br.com.si.params.ProblemParams;
import br.com.si.params.PsoParams;
import br.com.si.params.SimpleCmaEsParams;
import br.com.si.params.ZombieParams;
import br.com.si.problem.ProblemsCEC15;
import br.com.si.problem.Ackley;
import br.com.si.problem.Beale;
import br.com.si.problem.Booth;
import br.com.si.problem.DeJongN5;
import br.com.si.problem.DixonPrice;
import br.com.si.problem.DropWave;
import br.com.si.problem.Easom;
import br.com.si.problem.Eggholder;
import br.com.si.problem.GoldsteinPrice;
import br.com.si.problem.GoldsteinPriceScaled;
import br.com.si.problem.Griewanks;
import br.com.si.problem.Levy;
import br.com.si.problem.Matyas;
import br.com.si.problem.McCormick;
import br.com.si.problem.Michalewicz;
import br.com.si.problem.OptimizeParameters;
import br.com.si.problem.PermDB;
import br.com.si.problem.Problem;
import br.com.si.problem.Rastrigin;
import br.com.si.problem.Rosenbrocks;
import br.com.si.problem.RotatedHyperEllipsoid;
import br.com.si.problem.Schaffer;
import br.com.si.problem.Schwefels;
import br.com.si.problem.Shubert;
import br.com.si.problem.SixthBukin;
import br.com.si.problem.Sphere;
import br.com.si.problem.StyblinskiTang;
import br.com.si.problem.SumDifferentPowers;
import br.com.si.problem.SumSquares;
import br.com.si.problem.Trid;
import br.com.si.problem.Zakharov;
import br.com.si.util.DataReader;
import br.com.si.util.Experimenter;
import br.com.si.util.Ordering;
import br.com.si.util.Utils;

public class Main {

	private static final Logger logger = LogManager.getLogger(Main.class);

	// private static boolean runClonal = false;
	// private static boolean runElliptic = false;
	// private static boolean runCigar = false;
	// private static boolean runDiscus = false;
	// private static boolean runWeierstrass = false;
	// private static boolean runKatsuura = false;

	// 13 problems for 2-dimensions
	private static boolean runGoldsteinPrice = false;
	private static boolean runGoldsteinPriceScaled = false;
	private static boolean runBeale = false;
	private static boolean runEasom = false;
	private static boolean runDeJongN5 = false;
	private static boolean runMcCormick = false;
	private static boolean runMatyas = false;
	private static boolean runBooth = false;
	private static boolean runShubert = false;
	private static boolean runSchaffer = false;
	private static boolean runEggholder = false;
	private static boolean runDropWave = false; // muito pequeno
	private static boolean runSixthBukin = false;

	// 16 problems for n-dimensions
	private static boolean runStyblinskiTang = true;
	private static boolean runMichalewicz = false; // ruim
	private static boolean runRosenbrocks = true;
	private static boolean runDixonPrice = false; //
	private static boolean runZakharov = true;
	private static boolean runTrid = false; //
	private static boolean runSumSquares = false; //
	private static boolean runSumDifferentPowers = true;
	private static boolean runSphere = false; //
	private static boolean runRotatedHyperEllipsoid = true;
	private static boolean runPermDB = false; // apenas grandes dimensoes
	private static boolean runSchwefels = true;
	private static boolean runRastrigin = true;
	private static boolean runLevy = true;
	private static boolean runGriewanks = true;
	private static boolean runAckley = true;

	// Optimize parameters problems
	private static boolean runOPT = false;

	// CEC'15 problems
	private static boolean runCEC = true;

	// CEC'15 problems
	private static boolean runComplexity = false;

	private static int[] dimensions = { 10, 30, 50, 100 };//
	private static int[] functions = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	private static int[] algorithms = { 1, 2, 3, 4, 5, 6 };//

	private static boolean showSteps = true;
	private static int numThreds = 3;
	private static int numRuns = 51;
	private static boolean saveResultsOnDataBase = false;

	public static void main(String[] args) throws Exception {
		Ordering order = Ordering.ASCENDING;
		Problem problem;

		getParameters(args);

		/**
		 * CEC'15 [ 2 Dimensões: Funções 1, 2, 3, 4, 5, 9, 11, 12, 14 e 15; 10
		 * Dimensões: Funções 1 a 15; 30 Dimensões: Funções 1 a 15; 50
		 * Dimensões: Funções 1 a 15; 100 Dimensões: Funções 1 a 15; ]
		 **/
		if (runCEC) {
			if (runOPT) {
				for (int dim : dimensions) {
					Experimenter exp = new Experimenter(1, numThreds, saveResultsOnDataBase);

					if (dim == 2 && functions.length > 10) {
						functions = new int[] { 1, 2, 3, 4, 5, 9, 11, 12, 14, 15 };
					} else if (dim != 2 && functions.length == 10) {
						functions = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
					}

					for (int fun : functions) {
						problem = new OptimizeParameters(14, order, "double", fun, dim);
						addAlgorithm(exp, problem);
					}

					exp.run();
				}
			}

			for (int dim : dimensions) {
				Experimenter exp = new Experimenter(numRuns, numThreds, saveResultsOnDataBase);

				if (dim == 2 && functions.length > 10) {
					functions = new int[] { 1, 2, 3, 4, 5, 9, 11, 12, 14, 15 };
				} else if (dim != 2 && functions.length == 10) {
					functions = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
				}

				for (int fun : functions) {
					problem = new ProblemsCEC15(dim, order, "double", fun);
					addAlgorithm(exp, problem);
				}

				exp.run();
			}
		} else if (runComplexity) {
			double t0, t1;
			Date inicio, fim;
			ProblemParams params;
			String log;
			Algorithm alg;

			boolean isComplexity = false;
			int iterations = 200000;
			double[][] t2 = new double[algorithms.length][5];
			Complexity comp = new Complexity(null);

			inicio = new Date();
			comp.search();
			fim = new Date();
			t0 = fim.getTime() - inicio.getTime();

			for (int dim : dimensions) {
				problem = new ProblemsCEC15(dim, order, "double", 1);

				params = new GaParams(iterations, 100, 3, 0.95, 15.0, 15.0, 1.0e-14);
				do {
					alg = new GA(problem, (GaParams) params, false, false);
					alg.search();
					t1 = ((GA) alg).getTime();
				} while (alg.getIterations() < iterations);

				for (int j = 0; j < 5; j++) {
					// OK - Zombie
					if (containsValue(algorithms, 1)) {
						double[] p = DataReader.readParams(ZombieEDA.name, problem.getNumFunction(),
								problem.getNumberOfVariables(), 14);

						params = new ZombieParams(iterations, (int) p[0], (int) p[1], p[2], p[3], p[4], p[5],
								(int) p[6], (int) p[7], p[8], p[9], p[10], p[11], (int) p[12], p[13], -1);
						alg = new ZombieEDA(problem, (ZombieParams) params, false, false, false, false, isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[0][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[0][j] + " - It.: " + alg.getIterations());
					}

					// OK - EDAmvg
					if (containsValue(algorithms, 2)) {
						alg = new EdaOptimizer(problem, new EdaParams(iterations, 1000, 1.5, 0.2, 0.3), showSteps,
								false, isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[1][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[1][j] + " - It.: " + alg.getIterations());
					}

					// OK - SPSO 2011
					if (containsValue(algorithms, 3)) {
						alg = new SPSO(problem, new PsoParams(iterations, 40, 3.0), showSteps, false, isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[2][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[2][j] + " - It.: " + alg.getIterations());
					}

					// OK - iCMA-ES
					if (containsValue(algorithms, 4)) {
						alg = new ICMAES(problem,
								new CmaEsParams(iterations, 7.315, 3.776, 0.8297, 2.030, new BigDecimal("-8.104"),
										new BigDecimal("-6.688"), new BigDecimal("-13.85")),
								showSteps, false, null, false, isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[3][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[3][j] + " - It.: " + alg.getIterations());
					}

					// OK - C-GRASP
					if (containsValue(algorithms, 5)) {
						alg = new MultiStart(problem, new MultiStartParams(iterations, 1.0, 0.1, 0.3), showSteps, false,
								isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[4][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[4][j] + " - It.: " + alg.getIterations());
					}

					// OK - Bat
					if (containsValue(algorithms, 6)) {
						alg = new BatAlgorithm(problem, new BatParams(iterations, 100, 0.9, 0.9), showSteps, false,
								isComplexity);

						inicio = new Date();
						alg.search();
						fim = new Date();
						t2[5][j] = fim.getTime() - inicio.getTime();
						System.out.println(
								"Alg.: " + alg.getName() + " - Time: " + t2[5][j] + " - It.: " + alg.getIterations());
					}
				}

				for (int a = 0; a < algorithms.length; a++) {
					log = "Algorithm: " + algorithms[a] + " - Dimension: " + dim + " - T0: " + t0 + " - T1: " + t1
							+ " - T2: " + Utils.getStringVector(t2[a]) + " - (T2 - T1)/T0: "
							+ ((Utils.mean(t2[a]) - t1) / t0);
					DataReader.saveLog(inicio, comp.getName(), 0, log);
				}
			}
		} else {
			for (int dim : dimensions) {
				Experimenter experimenter = new Experimenter(numRuns, numThreds, saveResultsOnDataBase);

				if (dim == 2) {
					if (runGoldsteinPrice) {
						problem = new GoldsteinPrice(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runGoldsteinPriceScaled) {
						problem = new GoldsteinPriceScaled(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runBeale) {
						problem = new Beale(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runEasom) {
						problem = new Easom(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runDeJongN5) {
						problem = new DeJongN5(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runMcCormick) {
						problem = new McCormick(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runMatyas) {
						problem = new Matyas(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runBooth) {
						problem = new Booth(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runShubert) {
						// problem = new Shubert(dim, order, "double",false);
						// addAlgorithm(experimenter, problem);

						problem = new Shubert(dim, order, "double", true);
						addAlgorithm(experimenter, problem);
					}

					/** Schaffer's **/
					if (runSchaffer) {
						problem = new Schaffer(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					/** Eggholder **/
					if (runEggholder) {
						problem = new Eggholder(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}

					if (runDropWave) {
						problem = new DropWave(dim, order, "double", false);
						addAlgorithm(experimenter, problem);

						problem = new DropWave(dim, order, "double", true);
						addAlgorithm(experimenter, problem);
					}

					if (runSixthBukin) {
						problem = new SixthBukin(dim, order, "double");
						addAlgorithm(experimenter, problem);
					}
				}

				if (runStyblinskiTang) {
					problem = new StyblinskiTang(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runMichalewicz) {
					problem = new Michalewicz(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				/** Rosenbrocks **/
				if (runRosenbrocks) {
					problem = new Rosenbrocks(dim, order, "double", false);
					addAlgorithm(experimenter, problem);

					// problem = new Rosenbrocks(dim, order, "double",true);
					// addAlgorithm(experimenter, problem);
				}

				if (runDixonPrice) {
					problem = new DixonPrice(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runZakharov) {
					problem = new Zakharov(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runTrid) {
					problem = new Trid(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runSumSquares) {
					problem = new SumSquares(dim, order, "double", false);
					addAlgorithm(experimenter, problem);

					// problem = new SumSquares(dim, order, "double",true);
					// addAlgorithm(experimenter, problem);
				}

				if (runSumDifferentPowers) {
					problem = new SumDifferentPowers(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runSphere) {
					// problem = new Sphere(dim, order, "double",false);
					// addAlgorithm(experimenter, problem);

					problem = new Sphere(dim, order, "double", true);
					addAlgorithm(experimenter, problem);
				}

				if (runRotatedHyperEllipsoid) {
					problem = new RotatedHyperEllipsoid(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runPermDB) {
					problem = new PermDB(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				/** Schwefels **/
				if (runSchwefels) {
					problem = new Schwefels(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				/** Rastrigin **/
				if (runRastrigin) {
					problem = new Rastrigin(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				if (runLevy) {
					problem = new Levy(dim, order, "double");
					addAlgorithm(experimenter, problem);
				}

				/** Griewanks **/
				if (runGriewanks) {
					problem = new Griewanks(dim, order, "double", 1);
					addAlgorithm(experimenter, problem);

					problem = new Griewanks(dim, order, "double", 2);
					addAlgorithm(experimenter, problem);

					// problem = new Griewanks(dim, order, "double",3);
					// addAlgorithm(experimenter, problem);

					// problem = new Griewanks(dim, order, "double",4);
					// addAlgorithm(experimenter, problem);
				}

				/** Ackley **/
				if (runAckley) {
					problem = new Ackley(dim, order, "double", false);
					addAlgorithm(experimenter, problem);

					// problem = new Ackley(dim, order, "double",true);
					// addAlgorithm(experimenter, problem);
				}

				experimenter.run();
			}
		}

		System.exit(0);
	}

	private static void addAlgorithm(Experimenter experimenter, Problem problem) {
		double dim = problem.getNumberOfVariables();
		int iterations = (int) (10000 * dim);
		boolean isComplexity = false;

		// OK - Zombie
		if (containsValue(algorithms, 1)) {
			ZombieParams params = null;

			boolean hasNoise = problem.getName().contains("OptParam");
			if (hasNoise) {
				if (((OptimizeParameters) problem).getNumDim() < 11) {
					iterations = 6000;
				} else if (((OptimizeParameters) problem).getNumDim() == 30) {
					iterations = 2000;
				} else if (((OptimizeParameters) problem).getNumDim() == 50) {
					iterations = 1200;
				} else if (((OptimizeParameters) problem).getNumDim() == 100) {
					iterations = 600;
				}

				double[] p = DataReader.readParams(ZombieEDA.name + "_opt", problem.getNumFunction(),
						((OptimizeParameters) problem).getNumDim(), 16);

				if (p != null) {
					params = new ZombieParams(iterations, (int) p[1], (int) p[2], p[3], p[4], p[5], p[6], (int) p[7],
							(int) p[8], p[9], p[10], p[11], p[12], (int) p[13], p[14], (int) p[15]);
				} else {
					double sum = 0.0;
					for (int i = 0; i < dim; i++) {
						sum += problem.getUpperBound(i) - problem.getLowerBound(i);
					}
					double ln = Math.log((sum / dim) + 1.0);
					double sqrt = Math.sqrt(dim);
					double step = ln * sqrt;

					params = new ZombieParams(iterations, (int) (20 * dim), (int) dim, 0.5, 0.1, 0.5, 0.1, 1, (int) dim,
							0.5, 0.1, 0.5, 0.1, 1, step, -1);
				}
			} else {
				double[] p = DataReader.readParams(ZombieEDA.name, problem.getNumFunction(),
						problem.getNumberOfVariables(), 14);

				if (p != null) {
					params = new ZombieParams(iterations, (int) p[0], (int) p[1], p[2], p[3], p[4], p[5], (int) p[6],
							(int) p[7], p[8], p[9], p[10], p[11], (int) p[12], p[13], -1);
				} else {
					double sum = 0.0;
					for (int i = 0; i < dim; i++) {
						sum += problem.getUpperBound(i) - problem.getLowerBound(i);
					}
					double ln = Math.log((sum / dim) + 1.0);
					double sqrt = Math.sqrt(dim);
					double step = ln * sqrt;
					params = new ZombieParams(iterations, (int) (20 * dim), (int) dim, 0.5, 0.1, 0.5, 0.1, 1, (int) dim,
							0.5, 0.1, 0.5, 0.1, 1, step, -1);
				}
			}

			if (params != null) {
				experimenter.register(new ZombieEDA(problem, params, showSteps && !hasNoise, false, hasNoise, hasNoise,
						isComplexity));
			}
		}

		// OK - EDAmvg
		if (containsValue(algorithms, 2)) {
			experimenter.register(new EdaOptimizer(problem, new EdaParams(iterations, 1000, 1.5, 0.2, 0.3), showSteps,
					false, isComplexity));
		}

		// OK - SPSO 2011
		if (containsValue(algorithms, 3)) {
			experimenter
					.register(new SPSO(problem, new PsoParams(iterations, 40, 3.0), showSteps, false, isComplexity));
		}

		// OK - iCMA-ES
		if (containsValue(algorithms, 4)) {
			experimenter.register(new ICMAES(problem,
					new CmaEsParams(iterations, 7.315, 3.776, 0.8297, 2.030, new BigDecimal("-8.104"),
							new BigDecimal("-6.688"), new BigDecimal("-13.85")),
					showSteps, false, null, false, isComplexity));
		}

		// OK - C-GRASP
		if (containsValue(algorithms, 5)) {
			experimenter.register(new MultiStart(problem, new MultiStartParams(iterations, 1.0, 0.1, 0.3), showSteps,
					false, isComplexity));
		}

		// OK - Bat
		if (containsValue(algorithms, 6)) {
			experimenter.register(
					new BatAlgorithm(problem, new BatParams(iterations, 50, 0.9, 0.9), showSteps, false, isComplexity));
		}

		// nOK - GA
		if (containsValue(algorithms, 7)) {
			experimenter
					.register(new GA(problem, new GaParams(6000, 100, 3, 0.95, 15.0, 15.0, 1.0e-14), showSteps, false));
		}

		if (containsValue(algorithms, 8)) {
			experimenter.register(new SimpleCmaEs(problem,
					new SimpleCmaEsParams(iterations, 30, 3, 70.0 / 100.0, 1 / 30.0, 0.001), showSteps, false));
		}

	}

	public static void saveLog(String message) {
		System.out.println(message);
		logger.info(message);
	}

	public static boolean containsValue(int[] array, int myValue) {
		for (int value : array) {
			if (value == myValue) {
				return true;
			}
		}

		return false;
	}

	public static void getParameters(String[] args) {
		if (args.length < 2) {
			return;
		}

		String[] params = { "-t", "-d", "-f", "-r", "-a", "-s", "-c", "-o", "-x" };
		String[] data;
		int i, j;
		String arg;

		for (String param : params) {
			i = 0;
			do {
				arg = args[i];
				i++;
			} while (i < args.length && !param.equalsIgnoreCase(arg));

			if (i < args.length && !args[i].startsWith("-")) {
				switch (param) {
				case "-t":
					numThreds = Integer.parseInt(args[i]);
					break;

				case "-r":
					numRuns = Integer.parseInt(args[i]);
					break;

				case "-d":
					arg = args[i];
					j = 1;
					while ((i + j) < args.length && !args[i + j].startsWith("-")) {
						arg += "," + args[i + j];
						j++;
					}
					data = arg.split(",");
					dimensions = new int[data.length];
					j = 0;
					for (String info : data) {
						if (info.length() > 0) {
							dimensions[j] = Integer.parseInt(info);
							j++;
						}
					}
					break;

				case "-f":
					arg = args[i];
					j = 1;
					while ((i + j) < args.length && !args[i + j].startsWith("-")) {
						arg += "," + args[i + j];
						j++;
					}
					data = arg.split(",");
					functions = new int[data.length];
					j = 0;
					for (String info : data) {
						if (info.length() > 0) {
							functions[j] = Integer.parseInt(info);
							j++;
						}
					}
					break;

				case "-a":
					arg = args[i];
					j = 1;
					while ((i + j) < args.length && !args[i + j].startsWith("-")) {
						arg += "," + args[i + j];
						j++;
					}
					data = arg.split(",");
					algorithms = new int[data.length];
					j = 0;
					for (String info : data) {
						if (info.length() > 0) {
							algorithms[j] = Integer.parseInt(info);
							j++;
						}
					}
					break;

				case "-s":
					arg = args[i].toLowerCase();
					if (arg.length() > 0) {
						if (args[i].equals("t") || args[i].equals("true")) {
							showSteps = true;
						} else if (args[i].equals("f") || args[i].equals("false")) {
							showSteps = false;
						}
					}
					break;

				case "-c":
					arg = args[i].toLowerCase();
					if (arg.length() > 0) {
						if (args[i].equals("t") || args[i].equals("true")) {
							runCEC = true;
						} else if (args[i].equals("f") || args[i].equals("false")) {
							runCEC = false;
						}
					}
					break;

				case "-o":
					arg = args[i].toLowerCase();
					if (arg.length() > 0) {
						if (args[i].equals("t") || args[i].equals("true")) {
							runOPT = true;
						} else if (args[i].equals("f") || args[i].equals("false")) {
							runOPT = false;
						}
					}
					break;

				case "-x":
					arg = args[i].toLowerCase();
					if (arg.length() > 0) {
						if (args[i].equals("t") || args[i].equals("true")) {
							runComplexity = true;
						} else if (args[i].equals("f") || args[i].equals("false")) {
							runComplexity = false;
						}
					}
					break;

				default:
					break;
				}
			}
		}
	}
}
