package br.com.si.params;

public class MultiStartParams extends ProblemParams {
	public int _numSolutions;
	public double _hs;// discretization density
	public double _he; // discretization density
	public double _plo;// defines the probability that the returned solution
						// from the local improvement phase is an h-local
						// minimum;

	public MultiStartParams(int maxObjectiveFunctionCalls, double hs, double he, double plo) {
		super(maxObjectiveFunctionCalls);

		_hs = hs;
		_he = he;
		_plo = plo;
	}
}
