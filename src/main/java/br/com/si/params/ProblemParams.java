package br.com.si.params;

public class ProblemParams {
	public int _maxObjectiveFunctionCalls;

	public ProblemParams(int maxObjectiveFunctionCalls) {
		_maxObjectiveFunctionCalls = maxObjectiveFunctionCalls;
	}
}
