package br.com.si.params;

public class EdaParams extends ProblemParams {
	// public static int EDA_NUM_SOLUTIONS = 50;
	// public static int EDA_NUM_SUBINTERVALS = 50;
	// public static int EDA_NUM_PARTICLES = 30;

	public int _populationSize;
	public double _maxAmplification;
	public double _learningRate;
	public double _selectionRatio;

	public EdaParams(int maxObjectiveFunctionCalls, int populationSize, double maxAmplification, double learningRate,
			double selectionRatio) {
		super(maxObjectiveFunctionCalls);

		_populationSize = populationSize;
		_maxAmplification = maxAmplification;
		_learningRate = learningRate;
		_selectionRatio = selectionRatio;
	}
}
