package br.com.si.params;

import java.math.BigDecimal;

import br.com.si.util.Utils;

public class CmaEsParams extends ProblemParams {
	// public static int CMA_POP_SIZE = 20;
	// public static double CMA_SIGMA = 0.3;

	public double _popSizeFactor;
	public double _parentSizeFactor;
	public double _initStepSizeFactor;
	public double _iPOPFactor;
	public BigDecimal _stopTolFun;
	public BigDecimal _stopTolFunHist;
	public BigDecimal _stopTolX;
	public BigDecimal _stopTolXFactor;
	public int _populationSize;

	public CmaEsParams(int maxObjectiveFunctionCalls) {
		super(maxObjectiveFunctionCalls);
	}

	public CmaEsParams(int maxObjectiveFunctionCalls, double popSizeFactor, double parentSizeFactor,
			double initStepSizeFactor, double iPOPFactor, BigDecimal stopTolFunFactor, BigDecimal stopTolFunHistFactor,
			BigDecimal stopTolXFactor) {
		super(maxObjectiveFunctionCalls);

		_popSizeFactor = popSizeFactor;
		_parentSizeFactor = parentSizeFactor;
		_initStepSizeFactor = initStepSizeFactor;
		_iPOPFactor = iPOPFactor;
		
		_stopTolFun = Utils.powTen(stopTolFunFactor);
		_stopTolFunHist = Utils.powTen(stopTolFunHistFactor);
		_stopTolXFactor = stopTolXFactor;
		_stopTolX = Utils.powTen(stopTolXFactor);

		// _populationSize = populationSize;
		// _sigma = sigma;
	}
}
