package br.com.si.params;

public class PsoParams extends ProblemParams {

	// public static int PSO_SWARM_SIZE = 100;
	// public static double PSO_R1_MIN = 0.0;
	// public static double PSO_R1_MAX = 1.0;
	// public static double PSO_R2_MIN = 0.0;
	// public static double PSO_R2_MAX = 1.0;
	// public static double PSO_C1_MIN = 1.49168;
	// public static double PSO_C1_MAX = 1.49168;
	// public static double PSO_C2_MIN = 1.49168;
	// public static double PSO_C2_MAX = 1.49168;
	// public static double PSO_WEIGHT = 0.7298;
	// public static double PSO_SPEED_MIN = -0.5;
	// public static double PSO_SPEED_MAX = +0.5;

	public int _swarmSize;
	public double _k;
	public double _p;
	public double _w;
	public double _c;

	public PsoParams(int maxObjectiveFunctionCalls, int swarmSize, double k) {
		super(maxObjectiveFunctionCalls);

		_swarmSize = swarmSize;
		_k = k;
		_p = 1.0 - Math.pow(1.0 - (1.0 / _swarmSize), _k);
		_w = 1.0 / (2.0 * Math.log(2.0));
		_c = 0.5 + Math.log(2.0);
	}
}
