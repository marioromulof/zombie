package br.com.si.params;

public class ZombieParams extends ProblemParams {
	// Parameters Zombie
	// public static int ZMBPOPHUMANSIZE = 12;
	// public static int ZMBPOPZOMBIESIZE = 12;
	// public static double ZMBHITRATE = 0.7;
	// public static double ZMBCHASERRATE = 0.8;
	// public static int ZMBHUMANHEADSTART = 10;
	// public static int ZMBHUMANSTEPSNUMBER = 5;
	// public static double ZMBMAXFEAR = 1000.0;
	// public static double ZMBHITFEAR = 1.0;
	// public static double ZMBMAXDISTANCE = 50.0;
	// public static int ZMBPOWDISTANCE = -1;
	// public static double ZMBCHANGERATE = 0.5;
	// public static int ZMBPERIODMUTATION = 500;
	// public static double ZMBZOMBIECOVAR = 0.5;
	// public static double ZMBZOMBIEDISTTOTRANSFORM = 0.5;
	// public static double ZMBHUMANINITIALCOVAR = 10.0;
	// public static double ZMBHUMANCOVARREDUCTIONSTEP = 0.0005;
	// public static double ZMBHUMANMINCOVAR = 0.0001;

	// public double humanCovar;

	public int minHumanPop;
	public int maxHumanPop;
	// public int humanHeadStart;
	public int humanStepsPerZombieStep;
	public double humanMaxFear;
	public double humanMaxDistance;
	public double humanEscapeFactor;
	public double humanMinMutIdxFactor;
	public double humanMaxMutIdxFactor;
	public double hNinit;
	public double hNstd;
	public double hCinit;
	public double hCstd;
	public int hQ;
	// public double humanElope;

	public int powDistance;
	public double maxDistance;
	// public double covar;

	public int zombiePopSize;
	public double zNinit;
	public double zNstd;
	public double zCinit;
	public double zCstd;
	public int zQ;
	public int zombiePeriodMutation;
	public double zombieDistanceAttack;
	// public double zombieAvert;

	// public ZombieParams(int maxObjectiveFunctionCalls, int humanPopSize, int
	// zombiePopSize, int humanHeadStart, int numHumanStepsPerZombieStep, double
	// maxFear, double maxDistance, int powDistance, double zombieCovar, double
	// humanCovar, int periodMutation) {
	public ZombieParams(int maxObjectiveFunctionCalls, int maxHumanPop, int humanStepsPerZombieStep, double hNinit,
			double hNstd, double hCinit, double hCstd, int hQ, int zombiePopSize, double zNinit, double zNstd,
			double zCinit, double zCstd, int zQ, double maxDistance, int powDistance) {
		super(maxObjectiveFunctionCalls);

		this.minHumanPop = 10;
		this.maxHumanPop = maxHumanPop;
		this.humanStepsPerZombieStep = humanStepsPerZombieStep;
		this.hNinit = hNinit;
		this.hNstd = hNstd;
		this.hCinit = hCinit;
		this.hCstd = hCstd;
		this.hQ = hQ;
		// this.humanElope = humanElope;

		this.powDistance = powDistance;
		this.maxDistance = maxDistance;

		this.zombiePopSize = zombiePopSize;
		this.zNinit = zNinit;
		this.zNstd = zNstd;
		this.zCinit = zCinit;
		this.zCstd = zCstd;
		this.zQ = zQ;
		// this.zombieAvert = zombieAvert;
	}

	@Override
	public String toString() {
		return "ZombieParams: [maxHPop: " + maxHumanPop + "; hNinit: " + hNinit + "; hNstd: " + hNstd + "; hCinit: "
				+ hCinit + "; hCstd: " + hCstd + "; hQ: " + hQ + "; zPop: " + zombiePopSize + "; zNinit: " + zNinit
				+ "; zNstd: " + zNstd + "; zCinit: " + zCinit + "; zCstd: " + zCstd + "; zQ: " + zQ + "; maxDistance: "
				+ maxDistance + "]\n";
	}

	public String toStringClean() {
		return maxHumanPop + " " + humanStepsPerZombieStep + " " + hNinit + " " + hNstd + " " + hCinit + " " + hCstd
				+ " " + hQ + " " + zombiePopSize + " " + zNinit + " " + zNstd + " " + zCinit + " " + zCstd + " " + zQ
				+ " " + maxDistance + " " + powDistance;
	}
}
