package br.com.si.params;

public class SimpleCmaEsParams extends ProblemParams 
{
	public int _numSolutions;
	public double _initialCovar;
	public double _finalCovar;
	public double _intensificationCovar;
	public double _numParticlesToIntensificate;
	
	public SimpleCmaEsParams(int maxObjectiveFunctionCalls, int numSolutions, int numParticlesToIntensificate, 
			double initialCovar, double finalCovar, double intensificationCovar)
	{
		super(maxObjectiveFunctionCalls);
	
		_numSolutions = numSolutions;
		_initialCovar = initialCovar;
		_intensificationCovar = intensificationCovar;
		_numParticlesToIntensificate = numParticlesToIntensificate;

		_finalCovar = finalCovar;
	}
}
