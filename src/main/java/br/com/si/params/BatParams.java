package br.com.si.params;

public class BatParams extends ProblemParams
{
//	public static int BAT_BATS = 25;
//	public static double BAT_Q_MIN = 0.0;
//	public static double BAT_Q_MAX = 1.0;
//	public static double BAT_ALPHA = 0.9;
//	public static double BAT_GAMMA = 0.01;

	public int _numBats;
	public double _alpha; 
	public double _gamma;
	
	public BatParams(int maxObjectiveFunctionCalls, int numBats, double alpha, double gamma)
	{
		super(maxObjectiveFunctionCalls);
		
		_numBats = numBats;
		_alpha = alpha;
		_gamma = gamma;
	}
}
