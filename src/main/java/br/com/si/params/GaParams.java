package br.com.si.params;

public class GaParams extends ProblemParams {
	// public static int GA_POP_SIZE = 100;
	// public static int GA_TOURNAMENTS = 2;
	// public static double GA_CROSSOVER_PROBABILITY = 0.9;
	// public static double GA_DISTRIBUTION_INDEX_CROSSOVER = 20.0; // Between
	// 5-20
	// public static double GA_DISTRIBUTION_INDEX_MUTATION = 20.0; // Between
	// 5-20
	// public static double GA_EPS = 1.0e-14;

	public int _populationSize;
	public int _numTournaments;
	public double _crossoverProbability;
	public double _distributionCrossover;
	public double _distributionMutation;
	public double _eps;

	public GaParams(int maxObjectiveFunctionCalls, int populationSize, int numTournaments, double crossoverProbability,
			double distributionCrossover, double distributionMutation, double eps) {
		super(maxObjectiveFunctionCalls);

		_populationSize = populationSize;
		_numTournaments = numTournaments;
		_crossoverProbability = crossoverProbability;
		_distributionCrossover = distributionCrossover;
		_distributionMutation = distributionMutation;
		_eps = eps;
	}
}
