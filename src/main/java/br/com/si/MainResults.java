package br.com.si;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.inference.WilcoxonSignedRankTest;

import br.com.si.algorithm.BatAlgorithm;
import br.com.si.algorithm.EdaOptimizer;
import br.com.si.algorithm.ICMAES;
import br.com.si.algorithm.MultiStart;
import br.com.si.algorithm.SPSO;
import br.com.si.algorithm.ZombieEDA;
import br.com.si.problem.math.Constants;
import br.com.si.util.Utils;

public class MainResults {
	private static String[] algorithms = { ZombieEDA.name.toUpperCase(), "SPS_L_SHADE_EIG", SPSO.name.toUpperCase(),
			EdaOptimizer.name.toUpperCase(), ICMAES.name.toUpperCase(), MultiStart.name.toUpperCase(),
			BatAlgorithm.name.toUpperCase(), };

	private static String[] aL = { "ZS", "SPS-L-SHADE-EIG", "SPSO-2011", "EDA$_{mvg}$", "iCMA-ES", "C-GRASP", "BA", };

	private static int[] dimensions = { 10, 30, 50, 100 };

	private static String format = "%.2E";

	private static String[] types = { "Alg_DStat_17C", "Stat_Alg_9C" };

	private static Map<String, Map<String, Map<String, double[]>>> data = new HashMap<>();

	private static Map<String, Map<String, Map<String, Map<String, Double>>>> statisticData = new HashMap<>();

	public static void main(String[] args) throws IOException {
		// saveOptimizedParametersTables(1, true); // 0-ZS / 1-Shade
		loadData();
		//saveFreidmanStatistical(true); // false(without means) true(with
		// means)
		// saveWilcoxonStatistical();
		// generateMeanStdData();
		// generateStatisticData(types[1]);
		//saveCSVData();

		List<List<BigDecimal>> sorted = generateMeanSorted();
		// generateMeanStdData(sorted);

		// generateMeanFriedmanData(sorted);
		// generateBestWorstMedianZombie();
	}

	public static void loadData() {
		String fileName;
		String line = "";
		int limit, index;

		data.clear();
		statisticData.clear();
		for (int dimension : dimensions) {
			Map<String, Map<String, double[]>> algData = new HashMap<>();
			Map<String, Map<String, Map<String, Double>>> algStData = new HashMap<>();

			for (String algorithm : algorithms) {
				Map<String, double[]> funcData = new HashMap<>();
				Map<String, Map<String, Double>> funcStData = new HashMap<>();

				for (int f = 1; f < 16; f++) {
					if (dimension == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
						continue;
					}
					fileName = algorithm + "_" + f + "_" + dimension + ".txt";

					FileInputStream in;
					try {
						in = new FileInputStream("input_data/results/" + fileName);
						BufferedReader br = new BufferedReader(new InputStreamReader(in));

						if (!fileName.contains("SPS_L_SHADE_EIG")) {
							limit = 18;
						} else {
							limit = 17;
						}

						index = 0;
						while (index < limit) {
							line = br.readLine();
							index++;
						}
						br.close();
						in.close();

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						line = "";
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						line = "";
					}
					String[] values = line.split(" ");
					double[] maxfes = new double[51];
					index = 0;
					if (values.length > 1) {
						for (int i = 0; i < values.length; i++) {
							try {
								maxfes[index] = Double.parseDouble(values[i]);
								index++;
							} catch (NumberFormatException nfe) {
							}
						}
					} else {
						for (int i = 0; i < maxfes.length; i++) {
							maxfes[i] = Float.MAX_VALUE;
						}
					}

					Map<String, Double> stData = new HashMap<>();
					double[] copy = Arrays.copyOf(maxfes, maxfes.length);
					Arrays.sort(copy);
					stData.put("best", copy[0]);
					stData.put("worst", copy[copy.length - 1]);
					stData.put("median", Utils.median(copy));
					stData.put("mean", Utils.mean(maxfes));
					stData.put("std", Utils.standardDeviation(maxfes));

					funcData.put("f" + f, maxfes);
					funcStData.put("f" + f, stData);
				}
				algData.put(algorithm, funcData);
				algStData.put(algorithm, funcStData);
			}
			data.put("d" + dimension, algData);
			statisticData.put("d" + dimension, algStData);
		}
	}

	public static void saveFreidmanStatistical(boolean withMean) {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			for (int dimension : dimensions) {
				out.println("%  Freidman Statistical - " + dimension + " Dimensoes  %");
				for (int f = 1; f < 16; f++) {
					double rank;
					double[] means = new double[algorithms.length];
					String info = "f" + (f < 10 ? "0" + f : f);

					for (int a = 0; a < algorithms.length; a++) {
						means[a] = statisticData.get("d" + dimension).get(algorithms[a]).get("f" + f).get("mean");
					}
					Arrays.sort(means);

					for (String algorithm : algorithms) {
						double value = statisticData.get("d" + dimension).get(algorithm).get("f" + f).get("mean");
						int count = 0;
						while (Double.compare(value, means[count]) != 0) {
							count++;
						}

						int end = count + 1;
						if (end < means.length) {
							while (Double.compare(value, means[end]) == 0) {
								end++;
							}
						}

						if (end != (count + 1)) {
							count += 1;
							end += 1;
							rank = 0.0;
							for (int i = count; i < end; i++) {
								rank += i;
							}
							rank /= (end - count);
						} else {
							rank = count + 1;
						}

						BigDecimal aux = new BigDecimal(rank).setScale(0, RoundingMode.CEILING);
						if (withMean) {
							info += " & " + (rank == 1.0 || rank == 1.5 ? "\\bfseries " : "")
									+ String.format(format, value) + "(" + (rank == aux.doubleValue() ? aux : rank)
									+ ")";
						} else {
							info += " & " + (rank == 1.0 || rank == 1.5 ? "\\bfseries " : "")
									+ (rank == aux.doubleValue() ? aux : rank);
						}
					}

					info += " \\\\ ";
					if (f == 15) {
						info += "\\hline ";

					}
					out.println(info);
				}
				out.println();
				out.println();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private static void saveWilcoxonStatistical() {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		String info;

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.print("%  Wilcoxon Statistical - " + dimensions[0]);
			for (int d = 1; d < dimensions.length; d++) {
				out.print(", " + dimensions[d]);
			}
			out.println(" Dimensoes  %");

			for (int f = 1; f < 16; f++) {
				if (f == 1) {
					System.out.println(algorithms[0] + " vs ");
					System.out.printf("%10s | ", "Func.-Dim.");
					for (String algorithm : algorithms) {
						if (!algorithm.equalsIgnoreCase(algorithms[0])) {
							System.out.printf("%23s | ", algorithm);
						}
					}
					System.out.println();
				}
				info = " \\rule{0pt}{7pt} \\multirow{" + dimensions.length + "}{*}{f" + (f < 10 ? "0" + f : f) + "}";

				for (int dimension : dimensions) {
					System.out.printf("%10s | ", "F" + f + "-" + dimension);
					double meanZS = statisticData.get("d" + dimension).get(algorithms[0]).get("f" + f).get("mean");
					double[] dtZS = data.get("d" + dimension).get(algorithms[0]).get("f" + f);

					info += " & " + dimension;
					for (int a = 1; a < algorithms.length; a++) {
						double[] dtAlg = data.get("d" + dimension).get(algorithms[a]).get("f" + f);

						WilcoxonSignedRankTest wtest = new WilcoxonSignedRankTest();
						double value = wtest.wilcoxonSignedRankTest(dtZS, dtAlg, false);
						double mean = statisticData.get("d" + dimension).get(algorithms[a]).get("f" + f).get("mean");
						info += " & "
								+ (value < 0.00000001 ? String.format(format, BigDecimal.ZERO)
										: String.format(format, value))
								+ " (" + String.format(format, meanZS - mean) + ")";

						System.out.printf("%23s | ", String.format(format, value));
					}
					info += " \\\\ ";
					if (dimension == dimensions[dimensions.length - 1]) {
						info += "\\hline ";
					}
					out.println(info);
					info = "";
					System.out.println();
				}
			}
			out.println();
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void generateMeanStdData() {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		String results;

		for (int f = 1; f < 16; f++) {
			results = " \\multirow{" + dimensions.length + "}{*}{f" + (f < 10 ? "0" + f : f) + "}";
			for (int d = 0; d < dimensions.length; d++) {
				if (dimensions[d] == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
					continue;
				}

				results += " & " + dimensions[d];
				double[] means = new double[algorithms.length];
				for (int a = 0; a < algorithms.length; a++) {
					means[a] = statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f).get("mean");
				}
				Arrays.sort(means);

				for (String algorithm : algorithms) {
					double mean = statisticData.get("d" + dimensions[d]).get(algorithm).get("f" + f).get("mean");
					double std = statisticData.get("d" + dimensions[d]).get(algorithm).get("f" + f).get("std");

					results += " & ";
					if (mean == means[0]) {
						results += "\\bfseries ";
					}

					results += String.format(format, mean) + "(" + String.format(format, std) + ")";
				}
				results += " \\\\";

				if (d == (dimensions.length - 1)) {
					if (f == 15) {
						results += " \\\\[-10pt] \\hline";
					} else {
						results += " \\hline ";
					}
				}

				try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
					out.print(results);
					out.println();
					results = "";
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		}
	}

	private static void generateStatisticData(String type) {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		String info;

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			if (type.equals(types[0])) {
				out.println("\\hline");
				out.println(
						"\\multirow{2}{*}{\\bfseries Algorithm} & \\multirow{2}{*}{\\bfseries Func.} & \\multicolumn{5}{c}{\\bfseries \\textit{D}=10} & \\multicolumn{5}{|c}{\\bfseries \\textit{D}=30} & \\multicolumn{5}{|c}{\\bfseries \\textit{D}=50} \\\\");
				out.println("\\hline");
				out.println(
						"& & \\bfseries Best & \\bfseries Worst & \\bfseries Median & \\bfseries Mean & \\bfseries Std. & \\bfseries Best & \\bfseries Worst & \\bfseries Median & \\bfseries Mean & \\bfseries Std. & \\bfseries Best & \\bfseries Worst & \\bfseries Median & \\bfseries Mean & \\bfseries Std. \\\\");
				out.println("\\hline");
				for (int f = 1; f < 16; f++) {
					double[] best = new double[dimensions.length];
					for (int d = 0; d < dimensions.length; d++) {
						double[] means = new double[algorithms.length];
						for (int a = 0; a < algorithms.length; a++) {
							means[a] = statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f)
									.get("mean");
						}
						Arrays.sort(means);
						best[d] = means[0];
					}

					for (int a = 0; a < algorithms.length; a++) {
						info = algorithms[a].replace("_", "-");
						if (a == 0) {
							info += " & \\multirow{" + algorithms.length + "}{*}{f" + (f < 10 ? "0" + f : f) + "}";
						} else {
							info += " & ";
						}

						for (int d = 0; d < dimensions.length; d++) {
							String bold;
							double mean = statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f)
									.get("mean");
							if (mean == best[d]) {
								bold = "\\bfseries ";
							} else {
								bold = "";
							}

							info += " & " + bold + String.format(format,
									statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f).get("best"));
							info += " & " + bold + String.format(format, statisticData.get("d" + dimensions[d])
									.get(algorithms[a]).get("f" + f).get("worst"));
							info += " & " + bold + String.format(format, statisticData.get("d" + dimensions[d])
									.get(algorithms[a]).get("f" + f).get("median"));
							info += " & " + bold + String.format(format, mean);
							info += " & " + bold + String.format(format,
									statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f).get("std"));
						}
						info += " \\\\";

						if (a == algorithms.length - 1) {
							info += " \\hline";
						}

						out.println(info);
					}
				}
				out.println();
				out.println();

			} else if (type.equals(types[1])) {
				String[] statistics = { "Best", "Worst", "Median", "Mean", "Std" };
				for (int d = 0; d < dimensions.length; d++) {
					out.println("\\begin{longtable}[th]{@{}r@{\\space\\space\\space}"
							+ "c@{\\space\\space\\space}c@{\\space\\space\\space}"
							+ "c@{\\space\\space\\space\\space}c@{\\space\\space\\space\\space}"
							+ "c@{\\space\\space\\space}c@{\\space\\space\\space}c@{\\space\\space\\space}c@{}}");
					info = "\\caption{The results of ";
					for (String algorithm : aL) {
						if (algorithm.equals(aL[aL.length - 1])) {
							info = info.substring(0, info.length() - 2) + " and " + algorithm;
						} else {
							info += algorithm + ", ";
						}
					}
					info += " on the CEC'15 benchmark functions for $D$=" + dimensions[d] + ".}";
					out.println(info);
					out.println("\\label{tab:cecresults" + dimensions[d] + "}\\\\");
					out.println();
					out.println("\\toprulelt");
					info = " & Func.";
					for (String algorithm : aL) {
						info += " & " + algorithm;
					}
					out.println(info + " \\\\");
					out.println("\\colrulelt");
					out.println("\\endfirsthead");
					out.println();
					out.println("\\multicolumn{9}{@{}p{\\dimexpr0.82\\textwidth + 10\\tabcolsep\\relax}@{}}%");
					info = "{\\tablename\\ \\thetable{.} The results of ";
					for (String algorithm : aL) {
						if (algorithm.equals(aL[aL.length - 1])) {
							info = info.substring(0, info.length() - 2) + " and " + algorithm;
						} else {
							info += algorithm + ", ";
						}
					}
					info += " on the CEC'15 benchmark functions for $D$=";
					out.println(info + dimensions[d] + ". (\\textit{Continued}).} \\\\ [4pt]");
					out.println();
					out.println("\\toprulelt");
					info = " & Func.";
					for (String algorithm : aL) {
						info += " & " + algorithm;
					}
					out.println(info + " \\\\");
					out.println("\\colrulelt");
					out.println("\\endhead");
					out.println();
					out.println("\\botrulelt");
					out.println("\\endfoot");
					out.println("\\endlastfoot");
					out.println();

					for (int f = 1; f < 16; f++) {
						double[] means = new double[algorithms.length];
						for (int a = 0; a < algorithms.length; a++) {
							means[a] = statisticData.get("d" + dimensions[d]).get(algorithms[a]).get("f" + f)
									.get("mean");
						}
						String best = Utils.getBestOfVector(means);

						for (int s = 0; s < statistics.length; s++) {
							info = statistics[s];

							if (s == 0) {
								info += " & \\multirow{" + statistics.length + "}{*}{f" + (f < 10 ? "0" + f : f) + "}";
							} else {
								info += " & ";
							}

							for (int a = 0; a < algorithms.length; a++) {
								String bold;
								if (best.contains(Integer.toString(a))) {
									bold = "\\bfseries ";
								} else {
									bold = "";
								}

								if (statistics[s].toLowerCase().equals("mean")) {
									info += " & " + bold + String.format(format, means[a]);
								} else {
									info += " & " + bold + String.format(format, statisticData.get("d" + dimensions[d])
											.get(algorithms[a]).get("f" + f).get(statistics[s].toLowerCase()));
								}
							}
							info += " \\\\";
							out.println(info);
						}

						if (f != 15) {
							out.println("\\hline");
						}
					}
					out.println("\\botrulelt");
					out.println("\\end{longtable}");
					out.println();
				}
				out.println();
				out.println();
			} else {
				new Throwable("Opção Invalida");
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private static void saveCSVData() {
		String fileName;

		for (int dimension : dimensions) {
			for (int f = 1; f < 16; f++) {
				fileName = "input_data/results/Data_Exp" + f + "_" + dimension + ".csv";

				try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
					out.print("datasets");
					for (String algorithm : algorithms) {
						out.print("," + algorithm);
					}
					out.println();

					for (int i = 0; i < 51; i++) {
						out.print("f" + f + "-" + dimension);
						for (String algorithm : algorithms) {
							out.print("," + data.get("d" + dimension).get(algorithm).get("f" + f)[i]);
						}
						out.println();
					}
				} catch (Exception e) {
					System.err.println(e);
				}
			}

			fileName = "input_data/results/Data_Exp" + dimension + ".csv";
			try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
				out.print("datasets");
				for (String algorithm : algorithms) {
					out.print("," + algorithm);
				}
				out.println();

				for (int f = 1; f < 16; f++) {
					out.print("f" + f + "-" + dimension);
					for (String algorithm : algorithms) {
						out.print("," + statisticData.get("d" + dimension).get(algorithm).get("f" + f).get("mean"));
					}
					out.println();
				}
				out.println();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

	public static void generateBestWorstMedianZombie() {
		String fileName = "input_data/results/Results_BWM_Zombie.txt";
		String algorithm = "ZOMBIESEARCH";
		// int[] dimensions = { 10, 30, 50, 100 };
		String title = "";
		String best = "Best";
		String worst = "Worst";
		String median = "Median";

		for (int dimension : dimensions) {
			for (int f = 1; f < 16; f++) {
				if (dimension == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
					continue;
				}
				title += " & \\bfseries f" + (f < 10 ? "0" + f : f);

				List<String> bwm = getBestWorstMedianOfFile(algorithm + "_" + f + "_" + dimension + ".txt");

				best += " & " + bwm.get(0);
				worst += " & " + bwm.get(1);
				median += " & " + bwm.get(2);

				if (f % 5 == 0) {
					try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
						title += " \\\\ \\hline \\hline";
						out.print(title);
						out.println();
						title = "";

						best += " \\\\";
						out.print(best);
						out.println();
						best = "Best";

						worst += " \\\\";
						out.print(worst);
						out.println();
						worst = "Worst";

						median += " \\\\ \\hline";
						out.print(median);
						out.println();

						if (f == 15) {
							out.println();
							out.println();
						}

						median = "Median";
					} catch (Exception e) {
						System.err.println(e);
					}
				}
			}
		}
	}

	public static void generateMeanStdData(List<List<BigDecimal>> sorted) {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		// String[] legend = { "", "$^{\\Diamond}$", "$^{\\Box}$",
		// "$^{\\triangle}$", "$^{\\bigtriangledown}$", "$^{\\ominus}$",
		// "$^{\\otimes}$" };
		// int[] factors = { 1, 2, 0, 0, 2, 2, 0, 2, 0, 2, 1, 0, 1, 3, 0 };
		// int[] factors = { 6, 5, 0, 2, 3, 5, 1, 4, 2, 4, 2, 2, 2, 3, 0 }; //
		// 10d,
		// 30d
		// and
		// 50d
		// int[] factors = { 6, 5, 0, 2, 4, 6, 1, 5, 2, 4, 2, 1, 2, 3, 1 }; //
		// 100d
		String results;

		for (int f = 1; f < 16; f++) {
			results = " \\multirow{" + dimensions.length + "}{*}{f" + (f < 10 ? "0" + f : f) // +
																								// legend[factors[f
																								// -
																								// 1]]
					+ "}";
			for (int d = 0; d < dimensions.length; d++) {
				if (dimensions[d] == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
					continue;
				}

				results += " & " + dimensions[d];
				for (String algorithm : algorithms) {
					results += " & " + getMeanStdOfFile(sorted.get(d).get(f - 1), null,
							algorithm + "_" + f + "_" + dimensions[d] + ".txt");
				}
				results += " \\\\";

				if (d == (dimensions.length - 1)) {
					if (f == 15) {
						results += " \\\\[-10pt] \\hline";
						// results += " \\\\[-10pt] \\hline
						// \\multicolumn{8}{l}{$\\Diamond$ divided by 1.0E+1 \\
						// \\ $\\Box$ divided by 1.0E+2\\ \\ $\\triangle$
						// divided by 1.0E+3\\ \\ $\\bigtriangledown$ divided by
						// 1.0E+4\\ \\ $\\ominus$ divided by 1.0E+5\\ \\
						// $\\otimes$ divided by 1.0E+6}";
					} else {
						results += " \\hline ";
					}
				}

				try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
					out.print(results);
					out.println();
					results = "";
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		}
	}

	public static void generateMeanFriedmanData(List<List<BigDecimal>> sorted) {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		// int[] dimensions = { 10 };// , 30, 50, 100 };
		// String[] algorithms = { ZombieSearch.name.toUpperCase(),
		// ZombieEDA.name.toUpperCase(), ZombiePSO.name.toUpperCase(), };
		// int[] dimensions = { 2};//10, 30, 50, 100 };
		String results;

		for (int f = 1; f < 16; f++) {
			results = "f" + (f < 10 ? "0" + f : f);
			for (int d = 0; d < dimensions.length; d++) {
				if (dimensions[d] == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
					continue;
				}

				for (String algorithm : algorithms) {
					results += " & " + getMeanStdOfFile(sorted.get(d).get(f - 1), null,
							algorithm + "_" + f + "_" + dimensions[d] + ".txt");
				}
				results += " \\\\";

				if (d == (dimensions.length - 1)) {
					if (f == 15) {
						results += " \\\\[-10pt] \\hline";
					} else {
						results += " \\hline ";
					}
				}

				try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
					out.print(results);
					out.println();
					results = "";
				} catch (Exception e) {
					System.err.println(e);
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<List<BigDecimal>> generateMeanSorted() {
		Map<String, BigDecimal> results;
		Map<String, Map<String, double[]>> data;
		// Map<String, BigDecimal[]> data;
		// BigDecimal[] data;
		Map<String, Double> means;

		List<List<BigDecimal>> resultado = new ArrayList<>();
		data = new HashMap<>();
		for (int dimension : dimensions) {
			means = new HashMap<>();
			List<BigDecimal> values = new ArrayList<>();
			for (int f = 1; f < 16; f++) {
				if (dimension == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
					continue;
				}

				results = new HashMap<>();
				// data = new BigDecimal[algorithms.length];
				// int i = 0;
				Map<String, double[]> maxfess = new HashMap<>();
				for (String algorithm : algorithms) {
					results.put(algorithm, getMeanOfFile(algorithm + "_" + f + "_" + dimension + ".txt"));
					maxfess.put(algorithm, getMaxFESOfFileD(algorithm + "_" + f + "_" + dimension + ".txt"));
					// data[i] = getMeanOfFile(algorithm + "_" + f + "_" +
					// dimension + ".txt");
					// i++;
				}
				data.put(Integer.toString(f), maxfess);

				List list = new LinkedList(results.entrySet());
				Collections.sort(list, new Comparator() {
					public int compare(Object o1, Object o2) {
						return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
					}
				});

				values.add((BigDecimal) ((Map.Entry) list.get(0)).getValue());

				results = new LinkedHashMap<>();
				double count = 1;
				for (Iterator it = list.iterator(); it.hasNext();) {
					Map.Entry entry = (Map.Entry) it.next();
					results.put(entry.getKey().toString(), (BigDecimal) entry.getValue());

					if (!means.containsKey(entry.getKey().toString())) {
						means.put(entry.getKey().toString(), count);
					} else {
						means.put(entry.getKey().toString(), means.get(entry.getKey().toString()) + count);
					}
					count++;
				}

				saveResultsSorted(results, f, dimension);
				// saveCSVData(algorithms, f, dimension, data);
			}
			saveCSVData(data, dimension);
			saveWilcoxonStatistical(data, dimension);

			resultado.add(values);
			List list = new LinkedList(means.entrySet());
			Collections.sort(list, new Comparator() {

				public int compare(Object o1, Object o2) {
					return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
				}

			});
			saveMeanSorted((LinkedList) list, dimension);
		}

		return resultado;
	}

	private static void saveWilcoxonStatistical(Map<String, Map<String, double[]>> data, int dimension) {
		String fileName = "input_data/results/Results_MeanStd_Data.txt";
		String info = "";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.println("%  Wilcoxon Statistical - " + dimension + " Dimensoes%");
			for (int f = 1; f < 16; f++) {
				info += "\\rule{0pt}{7pt} f" + (f < 10 ? "0" + f : f);
				if (f == 1) {
					System.out.println(algorithms[0] + " vs ");
					System.out.printf("%10s | ", "Func.-Dim.");
					for (String algorithm : algorithms) {
						if (!algorithm.equalsIgnoreCase(algorithms[0])) {
							System.out.printf("%23s | ", algorithm);
						}
					}
					System.out.println();
				}
				System.out.printf("%10s | ", "F" + f);
				BigDecimal meanZS = getMeanOfFile(algorithms[0] + "_" + f + "_" + dimension + ".txt");
				double[] dtZS = data.get(Integer.toString(f)).get(algorithms[0]);
				for (int j = 1; j < algorithms.length; j++) {
					double[] dtAlg = data.get(Integer.toString(f)).get(algorithms[j]);
					WilcoxonSignedRankTest wtest = new WilcoxonSignedRankTest();
					BigDecimal value = new BigDecimal(Double.toString(wtest.wilcoxonSignedRankTest(dtZS, dtAlg, false)))
							.setScale(8, RoundingMode.CEILING);
					BigDecimal mean = getMeanOfFile(algorithms[j] + "_" + f + "_" + dimension + ".txt");
					info += " & "
							+ (value.compareTo(BigDecimal.valueOf(0.00000001)) == 0
									? String.format(format, BigDecimal.ZERO) : String.format(format, value))
							+ " (" + String.format(format, meanZS.subtract(mean)) + ")";

					System.out.printf("%23s | ", String.format(format, value));
				}
				info += " \\\\ ";
				if (f == 15) {
					info += "\\hline ";
				}
				out.print(info);
				out.println();
				info = "";
				System.out.println();
			}
			out.println();
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private static void saveCSVData(int f, int dimension, Map<String, BigDecimal[]> data) {
		String fileName = "input_data/results/Data_Exp" + f + ".csv";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.print("datasets");
			for (String algorithm : algorithms) {
				out.print("," + algorithm);
			}
			out.println();

			for (int i = 0; i < 51; i++) {
				out.print("f" + f + "-" + dimension);
				for (String algorithm : algorithms) {
					out.print("," + data.get(algorithm)[i]);
				}
				out.println();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private static void saveCSVData(Map<String, Map<String, double[]>> data, int dimension) {
		String fileName;

		for (int f = 1; f < 16; f++) {
			fileName = "input_data/results/Data_Exp" + f + ".csv";

			try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
				out.print("datasets");
				for (String algorithm : algorithms) {
					out.print("," + algorithm);
				}
				out.println();

				Map<String, double[]> func = data.get(String.valueOf(f));
				for (int i = 0; i < 51; i++) {
					out.print("f" + f + "-" + dimension);
					for (String algorithm : algorithms) {
						out.print("," + func.get(algorithm)[i]);
					}
					out.println();
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}

		fileName = "input_data/results/Data_Exp.csv";
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
			out.print("datasets");
			for (String algorithm : algorithms) {
				out.print("," + algorithm);
			}
			out.println();

			for (int f = 1; f < 16; f++) {
				Map<String, double[]> func = data.get(String.valueOf(f));
				out.print("f" + f + "-" + dimension);
				for (String algorithm : algorithms) {
					out.print("," + Utils.mean(func.get(algorithm)));
				}
				out.println();
			}
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private static void saveCSVData(int f, int dimension, BigDecimal[] data) {
		String fileName = "input_data/results/Data_Exp.csv";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			if (f == 1) {
				out.print("datasets");
				for (String algorithm : algorithms) {
					out.print("," + algorithm);
				}
				out.println();
			}

			out.print("f" + f + "-" + dimension);
			for (int i = 0; i < algorithms.length; i++) {
				out.print("," + data[i]);
			}
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	@SuppressWarnings("resource")
	public static BigDecimal getMeanOfFile(String fileName) {
		String result = "";
		String line = "";
		int index, limit;
		BigDecimal value = null;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			if (!fileName.contains("SPS_L_SHADE_EIG")) {
				limit = 21;
			} else {
				limit = 17;
			}
			index = 0;
			while (index < limit) {
				line = br.readLine();
				index++;
			}
			String values[] = line.split(" ");

			if (limit > 18) {
				index = values.length - 2;
				do {
					if (values[index].length() > 0) {
						result = values[index];
					}
					index--;
				} while (result.length() == 0);
			} else {
				value = BigDecimal.ZERO;
				limit = 0;
				for (index = 0; index < values.length; index++) {
					if (values[index].length() > 0) {
						value = value.add(new BigDecimal(values[index]));
						limit++;
					}
				}
				value = value.divide(BigDecimal.valueOf(limit), RoundingMode.CEILING);
				if (value.compareTo(Constants.SMALLER_VALUE) == -1) {
					value = BigDecimal.ZERO;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (result.length() == 0 && value == null) {
			result = String.valueOf(Double.MAX_VALUE);
		}

		if (value != null) {
			return value;
		}
		return new BigDecimal(result);
	}

	@SuppressWarnings("resource")
	public static BigDecimal getMedianOfFile(String fileName) {
		String result = "";
		String line = "";
		int index;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			index = 0;
			while (index < 21) {
				line = br.readLine();
				index++;
			}

			String values[] = line.split(" ");
			index = values.length - 2;
			boolean into = false;
			do {
				if (values[index].length() > 0) {
					if (!into) {
						into = true;
					} else {
						result = values[index];
					}
				}
				index--;
			} while (result.length() == 0);

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (result.length() == 0) {
			result = String.valueOf(Double.MAX_VALUE);
		}

		return new BigDecimal(result);
	}

	@SuppressWarnings("resource")
	public static BigDecimal[] getMaxFESOfFile(String fileName) {
		BigDecimal[] result = new BigDecimal[51];
		String line = "";
		int index;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			index = 0;
			while (index < 18) {
				line = br.readLine();
				index++;
			}

			String values[] = line.split(" ");
			int i = 0;
			for (String value : values) {
				if (!value.isEmpty() && !value.equals("MaxFES") && !value.equals("=")) {
					result[i] = new BigDecimal(value);
					i++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (result[0] == null) {
			for (int i = 0; i < 51; i++) {
				result[i] = new BigDecimal(String.valueOf(Double.MAX_VALUE));
			}
		}

		return result;
	}

	@SuppressWarnings("resource")
	public static double[] getMaxFESOfFileD(String fileName) {
		double[] result = new double[51];
		String line = "";
		int limit, index = 0;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			if (!fileName.contains("SPS_L_SHADE_EIG")) {
				limit = 18;
			} else {
				limit = 17;
			}

			while (index < limit) {
				line = br.readLine();
				index++;
			}

			String values[] = line.split(" ");
			index = 0;
			for (String value : values) {
				if (!value.isEmpty() && !value.equals("MaxFES") && !value.equals("=")) {
					result[index] = new BigDecimal(value).doubleValue();
					index++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (index == 0) {
			for (int i = 0; i < 51; i++) {
				result[i] = Double.MAX_VALUE;
			}
		}

		return result;
	}

	@SuppressWarnings("resource")
	public static String getMeanStdOfFile(BigDecimal best, BigDecimal factor, String fileName) {
		// MathContext mc = new MathContext(4);
		best.setScale(10, RoundingMode.CEILING);
		String result = "";
		String line = "";
		int index, limit;
		BigDecimal bd;

		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			if (!fileName.contains("SPS_L_SHADE_EIG")) {
				limit = 21;
			} else {
				limit = 17;
			}

			index = 0;
			while (index < limit) {
				line = br.readLine();
				index++;
			}

			String values[] = line.split(" ");
			if (limit > 18) {
				index = values.length - 2;
				do {
					if (values[index].length() > 0) {
						bd = new BigDecimal(values[index]).setScale(10, RoundingMode.CEILING);
						result = (bd.compareTo(best) == 0 ? "\\bfseries " : "");
						if (factor != null) {
							bd = bd.divide(factor);
						}

						result += String.format(format, bd);// bd.setScale(2,
															// RoundingMode.CEILING).toString();
					}
					index--;
				} while (result.length() == 0);

				bd = new BigDecimal(values[values.length - 1]);// new
																// BigDecimal(values[values.length
																// - 1], mc);
			} else {
				double[] aux = new double[51];
				limit = 0;
				for (index = 0; index < values.length; index++) {
					if (values[index].length() > 0) {
						aux[limit] = new BigDecimal(values[index]).doubleValue();
						limit++;
					}
				}
				bd = new BigDecimal(Utils.mean(aux)).setScale(10, RoundingMode.CEILING);
				result = (bd.compareTo(best) == 0 ? "\\bfseries " : "");
				if (factor != null) {
					bd = bd.divide(factor);
				}

				result += String.format(format, bd);// .setScale(2,
													// RoundingMode.CEILING).toString();

				bd = new BigDecimal(Utils.standardDeviation(aux));
			}

			if (factor != null) {
				bd = bd.divide(factor);
			}
			// if (bd.compareTo(new BigDecimal("0.01")) == -1) {
			// bd = bd.setScale(6, BigDecimal.ROUND_HALF_UP);
			// } else if (bd.compareTo(new BigDecimal("10000")) == -1) {
			// bd = new BigDecimal(values[values.length - 1], new
			// MathContext(6));
			// }
			result += "(" + String.format(format, bd) + ")";// .setScale(2,
															// RoundingMode.CEILING).toString()
															// + ")";
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	@SuppressWarnings("resource")
	public static List<String> getBestWorstMedianOfFile(String fileName) {
		MathContext mc = new MathContext(4);
		List<String> result = new ArrayList<String>();
		String line = "";
		int index, count;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			index = 0;
			while (index < 21) {
				line = br.readLine();
				index++;
			}

			String values[] = line.split(" ");
			index = 0;
			count = 0;
			do {
				if (values[index].length() > 0) {
					BigDecimal db = new BigDecimal(values[index], mc);
					if (db.compareTo(new BigDecimal("10000")) == -1) {
						result.add(new BigDecimal(values[index], new MathContext(7)).toString());
					} else {
						result.add(db.toString());
					}
					count++;
				}
				index++;
			} while (count < 3);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static void saveResultsSorted(Map<String, BigDecimal> results, int function, int dimension) {
		String fileName = "input_data/results/Results_Sorted.txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf("%10s | ", "F" + function + "-D" + dimension);
			int count = 1;
			int actual = -1;
			BigDecimal last = BigDecimal.valueOf(-1);
			for (Map.Entry<String, BigDecimal> entry : results.entrySet()) {
				if (last.compareTo(entry.getValue()) == 0) {
					out.print(actual + "-" + entry.getKey() + "(" + entry.getValue() + ") ");
				} else {
					out.print(count + "-" + entry.getKey() + "(" + entry.getValue() + ") ");
					actual = count;
				}
				count++;
				last = entry.getValue();
			}
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}

		if (!(dimension == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(function))) {
			fileName = "input_data/results/Results_MeanStd_Data.txt";
			String info = "f" + (function < 10 ? "0" + function : function);
			double rank;

			BigDecimal[] values = results.values().toArray(new BigDecimal[results.size()]);

			for (String algorithm : algorithms) {
				BigDecimal value = results.get(algorithm);
				int count = 0;
				while (value.compareTo(values[count]) != 0) {
					count++;
				}

				int end = count + 1;
				if (end < values.length) {
					while (value.compareTo(values[end]) == 0) {
						end++;
					}
				}

				if (end != (count + 1)) {
					count += 1;
					end += 1;
					rank = 0.0;
					for (int i = count; i < end; i++) {
						rank += i;
					}
					rank /= (end - count);
				} else {
					rank = count + 1;
				}

				BigDecimal aux = new BigDecimal(rank).setScale(0, RoundingMode.CEILING);

				info += " & " + (rank == 1.0 || rank == 1.5 ? "\\bfseries " : "") + String.format(format, value) + "("
						+ (rank == aux.doubleValue() ? aux : rank) + ")";
			}
			info += " \\\\ \\hline ";

			try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
				out.print(info);
				out.println();

				if (function == 15) {
					out.println();
					out.println();
				}
				info = "";
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public static void saveMeanSorted(LinkedList results, int dimension) {
		String fileName = "input_data/results/Results_Sorted.txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf("%10s | ", "Means-D" + dimension);
			for (Iterator it = results.iterator(); it.hasNext();) {
				Map.Entry entry = (Map.Entry) it.next();
				out.print(entry.getKey() + "(" + ((Double) entry.getValue() / (dimension > 2 ? 15.0 : 10.0)) + ") ");
			}
			out.println();
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	@SuppressWarnings("resource")
	public static BigDecimal[] getParametersOfFile(String fileName) throws ParseException {
		BigDecimal[] result;
		String line = "";
		int index;
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			line = br.readLine();
			String values[] = line.split(" ");
			result = new BigDecimal[values.length];

			for (index = 0; index < values.length; index++) {
				result[index] = new BigDecimal(values[index]);
			}

			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static BigDecimal getErrorParameters(int dim, int func, int opt) {
		BigDecimal result = BigDecimal.valueOf(func * 100);
		String find, line = "";

		if (dim == 10) {
			find = "OptParam:" + func + ":";
		} else {
			find = "OptParam-" + func + "-" + dim;
		}

		try {
			FileInputStream in = null;
			if (opt == 0) {
				in = new FileInputStream("input_data/results/log_ZOMBIEEDA_14.txt");
			} else {
				in = new FileInputStream("input_data/results/Results_Experiment_14.txt");
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			while (line != null && !line.contains(find)) {
				line = br.readLine();
			}
			br.close();
			in.close();

			if (line != null) {
				String[] values = null;
				if (opt == 0) {
					values = line.split("- Error: ");
				} else {
					values = line.split("\\) ");
				}
				values = values[1].split(" ");

				return result.add(new BigDecimal(values[0]));
			} else {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void saveOptimizedParametersTables(boolean inverter) {
		String fileName = "input_data/results/optParameters.txt";
		BigDecimal[][] data = new BigDecimal[15][];
		String bfseries = "\\bfseries{";
		String rule = "\\rule{0pt}{7pt} ";
		String multrow = "\\multirow{2}{*}{";
		String[] linhas = { "$n_{s}$", "$\\textit{step}$", "$\\textit{sN}_{\\text{init}}$", "$\\textit{wsn}$",
				"$\\textit{sC}_{\\text{init}}$", "$\\textit{wsc}$", "$\\textit{qs}$", "$n_{z}$",
				"$\\textit{zN}_{\\text{init}}$", "$\\textit{wzn}$", "$\\textit{zC}_{\\text{init}}$", "$\\textit{wzc}$",
				"$\\textit{qz}$", "$\\textit{d}_{\\textit{md}}$" };
		String[] colunas = { "f01", "f02", "f03", "f04", "f05", "f06", "f07", "f08", "f09", "f10", "f11", "f12", "f13",
				"f14", "f15", "Default" };
		String[] def = { "280", "14", "0.5", "0.1", "0.5", "0.1", "1", "14", "0.5", "0.1", "0.5", "0.1", "1", "17.36" };

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			if (inverter) {
				out.println("\\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c}");
			} else {
				out.println("\\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c}");
			}
			for (int dim : dimensions) {
				out.println("\\hline");
				if (inverter) {
					out.printf(multrow + bfseries + "\\textit{D = }%d}}", dim);
					for (String linha : linhas) {
						out.print(" & " + multrow + bfseries + linha + "}}");
					}
					out.println(" & " + bfseries + "\\textit{Solution}} \\\\");
					for (int i = 0; i < linhas.length; i++) {
						out.print(" & ");
					}
					out.println(" & " + bfseries + "\\textit{Error}}");
				} else {
					out.printf(rule + bfseries + "\\textit{D = }%d", dim);
					for (String coluna : colunas) {
						out.print(" & " + bfseries + coluna + "}");
					}
				}
				out.print(" \\\\ \\hline\n");
				for (int f = 1; f < 16; f++) {
					if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
						continue;
					}
					data[f - 1] = getParametersOfFile("PARAMS_ZOMBIEEDA_" + f + "_" + dim + ".txt");
				}

				if (inverter) {
					for (int l = 0; l < colunas.length - 1; l++) {
						out.print(rule + colunas[l]);
						for (int c = 0; c < linhas.length; c++) {
							out.print(" & " + (data[l] != null && data[l][c] != null ? (def[c].contains(".")
									? data[l][c].setScale(2, RoundingMode.CEILING).toString() : data[l][c].intValue())
									: "-"));
						}

						BigDecimal error = null;
						int count = 1;
						// while (error == null && count < 2) {
						error = getErrorParameters(dim, l + 1, count);
						// count++;
						// }
						out.println(" & " + (error != null ? String.format(format, error) : "-") + " \\\\");
					}
					out.println(" \\hline");
					out.print(rule + colunas[colunas.length - 1]);
					for (int c = 0; c < linhas.length; c++) {
						out.print(" & " + def[c]);
					}
					out.println(" & - \\\\");
				} else {
					for (int p = 0; p < data[0].length; p++) {
						out.print(rule + linhas[p]);
						for (int f = 1; f < 16; f++) {
							if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
								continue;
							}
							out.print(" & " + (data[f - 1][p] != null ? (def[p].contains(".")
									? data[f - 1][p].setScale(2, RoundingMode.CEILING).toString()
									: data[f - 1][p].intValue()) : "-"));
						}
						out.println(" & " + def[p] + "\\\\");
					}
				}
				out.println(" \\hline");

				if (inverter) {

				} else {
					out.print("\\textit{Solution}");
					for (int f = 1; f < 16; f++) {
						if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
							continue;
						}
						BigDecimal error = null;
						int count = 1;
						// while (error == null && count < 2) {
						error = getErrorParameters(dim, f, count);
						// count++;
						// }
						out.print(" & \\multirow{2}{*}{" + String.format(format, error) + "}");
					}
					out.println(" & \\multirow{2}{*}{-} \\\\");
					out.println("\\textit{Error} & & & & & & & & & & & & & & & & \\\\ \\hline");
				}
			}
			out.println("\\end{tabular}");
			out.println();
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveOptimizedParametersTables(int alg, boolean inverter) {
		BigDecimal[][] data = new BigDecimal[15][];
		String[] linhas, def;
		Map<Integer, String> def2 = null;

		String[] colunas = { "f01", "f02", "f03", "f04", "f05", "f06", "f07", "f08", "f09", "f10", "f11", "f12", "f13",
				"f14", "f15", "Padrão" };

		String fileName = "input_data/results/optParameters.txt";
		String bfseries = "\\bfseries{";
		String rule = "\\rule{0pt}{7pt} ";
		String multrow = "\\multirow{2}{*}{";
		String algorithm;

		if (alg == 0) {
			algorithm = "ZOMBIEEDA";
			linhas = new String[] { "$n_{s}$", "$\\textit{step}$", "$\\textit{sN}_{\\text{init}}$", "$\\textit{wsn}$",
					"$\\textit{sC}_{\\text{init}}$", "$\\textit{wsc}$", "$\\textit{qs}$", "$n_{z}$",
					"$\\textit{zN}_{\\text{init}}$", "$\\textit{wzn}$", "$\\textit{zC}_{\\text{init}}$",
					"$\\textit{wzc}$", "$\\textit{qz}$", "$\\textit{d}_{\\textit{md}}$" };
			def = new String[] { "280", "14", "0.5", "0.1", "0.5", "0.1", "1", "14", "0.5", "0.1", "0.5", "0.1", "1",
					"17.36" };
		} else {
			algorithm = aL[alg];
			linhas = new String[] { "$\\textit{NP}_{\\text{init}} - \\textit{NP}_{\\text{min}}$", "$F_{\\text{init}}$",
					"$\\textit{Cr}_{\\text{init}}$", "$\\textit{Er}_{\\text{init}}$", "$p$", "$n$", "$Q$",
					"$w_{_{\\text{EXT}}}$", "$\\alpha_{\\text{init}}$", "$w_{\\textit{Er}}$",
					"$\\textit{Cr}_{\\text{min}}$", "$\\textit{Cr}_{\\text{max}} - \\textit{Cr}_{\\text{min}}$",
					"$\\textit{NP}_{\\text{min}}$", "$w_{\\textit{Cr}}$", "$w_F$" };
			def = new String[] { "-", "0.5", "0.5", "1", "0.11", "6", "64", "2.6", "0.3", "0.2", "0.05", "0.25", "4",
					"0.1", "0.1" };

			def2 = new HashMap<>();
			def2.put(10, "186");
			def2.put(30, "566");
			def2.put(50, "946");
			def2.put(100, "1896");
		}

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			if (inverter) {
				out.println("\\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c}");
			} else {
				out.println("\\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c}");
			}
			for (int dim : dimensions) {
				out.println("\\hline");
				if (inverter) {
					out.printf(multrow + bfseries + "\\textit{D = }%d}}", dim);
					for (String linha : linhas) {
						out.print(" & " + multrow + bfseries + linha + "}}");
					}
					out.println(" & " + bfseries + "\\textit{Solution}} \\\\");
					for (int i = 0; i < linhas.length; i++) {
						out.print(" & ");
					}
					out.println(" & " + bfseries + "\\textit{Error}}");
				} else {
					out.printf(rule + bfseries + "\\textit{D = }%d", dim);
					for (String coluna : colunas) {
						out.print(" & " + bfseries + coluna + "}");
					}
				}
				out.print(" \\\\ \\hline\n");
				for (int f = 1; f < 16; f++) {
					if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
						continue;
					}
					data[f - 1] = getParametersOfFile("PARAMS_" + algorithm + "_" + f + "_" + dim + ".txt");
				}

				if (inverter) {
					for (int l = 0; l < colunas.length - 1; l++) {
						out.print(rule + colunas[l]);
						for (int c = 0; c < linhas.length; c++) {
							out.print(" & " + (data[l] != null && data[l][c] != null ? (def[c].contains(".")
									? data[l][c].setScale((alg == 0 ? 2 : 4), RoundingMode.CEILING).toString()
									: data[l][c].intValue()) : "-"));
						}

						BigDecimal error = null;
						if (alg == 0) {
							int count = 1;
							// while (error == null && count < 2) {
							error = getErrorParameters(dim, l + 1, count);
							// count++;
							// }
						} else {
							error = data[l][linhas.length];
						}
						out.println(" & " + (error != null ? String.format(format, error) : "-") + " \\\\");
					}
					out.println(" \\hline");
					out.print(rule + colunas[colunas.length - 1]);
					for (int c = 0; c < linhas.length; c++) {
						out.print(" & " + (def[c].equals("-") ? def2.get(dim) : def[c]));
					}
					out.println(" & - \\\\");
				} else {
					for (int p = 0; p < linhas.length; p++) {
						out.print(rule + linhas[p]);
						for (int f = 1; f < 16; f++) {
							if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
								continue;
							}
							out.print(" & " + (data[f - 1][p] != null ? (def[p].contains(".")
									? data[f - 1][p].setScale((alg == 0 ? 2 : 4), RoundingMode.CEILING).toString()
									: data[f - 1][p].intValue()) : "-"));
						}
						out.println(" & " + (def[p].equals("-") ? def2.get(dim) : def[p]) + "\\\\");
					}
				}
				out.println(" \\hline");

				if (inverter) {

				} else {
					out.print("\\textit{Solution}");
					for (int f = 1; f < 16; f++) {
						if (dim == 2 && Arrays.asList(6, 7, 8, 10, 13).contains(f)) {
							continue;
						}
						BigDecimal error = null;
						if (alg == 0) {
							int count = 1;
							// while (error == null && count < 2) {
							error = getErrorParameters(dim, f, count);
							// count++;
							// }
						} else {
							error = data[f - 1][linhas.length];
						}
						out.print(" & \\multirow{2}{*}{" + String.format(format, error) + "}");
					}
					out.println(" & \\multirow{2}{*}{-} \\\\");
					out.println("\\textit{Error} & & & & & & & & & & & & & & & & \\\\ \\hline");
				}
			}
			out.println("\\end{tabular}");
			out.println();
			out.println();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e);
		}
	}
}
