package br.com.si.solution;

import br.com.si.util.Ordering;

public class IntegerSolution extends Solution<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 409744405617458067L;

	public IntegerSolution(int numberOfVariables) {
		super(Integer[].class, numberOfVariables);
	}

	public IntegerSolution(int numberOfVariables, Ordering order) {
		super(Integer[].class, numberOfVariables, order);
	}

	@Override
	public Solution<Integer> copy() {
		int numberOfVariables = this.getNumberOfVariables();
		IntegerSolution copy = new IntegerSolution(numberOfVariables);
		copy.setVariables(getVariables().clone());
		copy.setObjective(this.getObjective());
		copy.setOrder(this.order);

		return copy;
	}

	@Override
	public int compareTo(Solution<Integer> o) {
		if (order == Ordering.ASCENDING) {
			return getObjective().compareTo(o.getObjective());
		} else {
			return o.getObjective().compareTo(getObjective());
		}
	}

}
