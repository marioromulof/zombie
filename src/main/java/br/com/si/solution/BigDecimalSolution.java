package br.com.si.solution;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import br.com.si.problem.Problem;
import br.com.si.util.Ordering;

public class BigDecimalSolution extends Solution<BigDecimal> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7372334044847667775L;

	public BigDecimalSolution(int numberOfVariables) {
		super(BigDecimal[].class, numberOfVariables);
	}

	public BigDecimalSolution(int numberOfVariables, Ordering order) {
		super(BigDecimal[].class, numberOfVariables, order);
	}

	@Override
	public Solution<BigDecimal> copy() {
		BigDecimalSolution copy = new BigDecimalSolution(this.getNumberOfVariables(), this.order);
		copy.setVariables(getVariables().clone());
		copy.setObjective(this.getObjective());

		return copy;
	}

	@Override
	public void setVariableValue(int index, BigDecimal value) {
		super.setVariableValue(index, value.setScale(20, RoundingMode.CEILING));
	}

	@Override
	public boolean equals(Object obj) {
		BigDecimalSolution sol = (BigDecimalSolution) obj;

		return this.getVariables().equals(sol.getVariables());
	}

	@Override
	public int compareTo(Solution<BigDecimal> o) {
		if (order == Ordering.ASCENDING) {
			return getObjective().compareTo(o.getObjective());
		} else {
			return o.getObjective().compareTo(getObjective());
		}
	}

	public double[] getArrayOfDouble() {
		int numberOfVariables = this.getNumberOfVariables();

		double[] result = new double[numberOfVariables];
		for (int i = 0; i < numberOfVariables; i++) {
			result[i] = this.getVariableValue(i).doubleValue();
		}

		return result;
	}

	public static BigDecimalSolution createSolution(Problem problem, Random random) {
		BigDecimalSolution solution = new BigDecimalSolution(problem.getNumberOfVariables(), problem.getOrder());

		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			solution.setVariableValue(i, BigDecimal.valueOf(problem.getLowerBound(i)
					+ random.nextDouble() * (problem.getUpperBound(i) - problem.getLowerBound(i))));
		}

		return solution;
	}

}
