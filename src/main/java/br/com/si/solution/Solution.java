package br.com.si.solution;

import java.io.Serializable;
import java.lang.reflect.Array;

import br.com.si.util.Ordering;

public abstract class Solution<T> implements Serializable, Comparable<Solution<T>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7905118709859267242L;

	private Double objetive;
	private T[] variables;
	protected Ordering order;

	public Solution(Class<T[]> type, int numberOfVariables) {
		this.variables = type.cast(Array.newInstance(type.getComponentType(), numberOfVariables));
		this.objetive = null;
	}

	public Solution(Class<T[]> type, int numberOfVariables, Ordering order) {
		this.variables = type.cast(Array.newInstance(type.getComponentType(), numberOfVariables));
		this.objetive = null;
		this.order = order;
	}

	public void setObjective(Double value) {
		this.objetive = value;
	}

	public Double getObjective() {
		return objetive;
	}

	public T getVariableValue(int index) {
		return variables[index];
	}

	public void setVariableValue(int index, T value) {
		this.variables[index] = value;
	}

	public int getNumberOfVariables() {
		return this.variables.length;
	}

	public T[] getVariables() {
		return this.variables;
	}

	public void setVariables(T[] variables) {
		this.variables = variables;
	}

	public abstract Solution<T> copy();

	public void setOrder(Ordering order) {
		this.order = order;
	}

	public Ordering getOrder() {
		return this.order;
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < getNumberOfVariables(); i++) {
			result += getVariableValue(i).toString() + " ";
		}
		result += objetive.toString() + " " + order.ordinal();

		return result;
	}
}
