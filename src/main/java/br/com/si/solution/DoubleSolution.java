package br.com.si.solution;

import java.util.Random;

import br.com.si.problem.Problem;
import br.com.si.util.Ordering;

public class DoubleSolution extends Solution<Double> {

	private static final long serialVersionUID = 4091741954646995205L;

	public DoubleSolution(int numberOfVariables) {
		super(Double[].class, numberOfVariables);
	}

	public DoubleSolution(int numberOfVariables, Ordering order) {
		super(Double[].class, numberOfVariables, order);
	}

	@Override
	public int compareTo(Solution<Double> o) {
		if (order == Ordering.ASCENDING) {
			return getObjective().compareTo(o.getObjective());
		} else {
			return o.getObjective().compareTo(getObjective());
		}
	}

	@Override
	public Solution<Double> copy() {
		DoubleSolution copy = new DoubleSolution(getNumberOfVariables(), order);
		copy.setVariables(getVariables().clone());
		copy.setObjective(getObjective());

		return copy;
	}

	@Override
	public boolean equals(Object obj) {
		DoubleSolution sol = (DoubleSolution) obj;

		return this.getVariables().equals(sol.getVariables());
	}

	public static DoubleSolution createSolution(Problem problem, Random random) {
		DoubleSolution solution = new DoubleSolution(problem.getNumberOfVariables(), problem.getOrder());

		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			solution.setVariableValue(i, problem.getLowerBound(i)
					+ random.nextDouble() * (problem.getUpperBound(i) - problem.getLowerBound(i)));
		}

		return solution;
	}
}
