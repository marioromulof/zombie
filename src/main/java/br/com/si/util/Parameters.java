package br.com.si.util;

public interface Parameters {
	public static double MIN_ERROR = 0.00000001;
	public static int MAX_FES = 10000;
	
	// Parameters Zombie
	public static int ZMB_POP_HUMAN_SIZE = 12;
	public static int ZMB_POP_ZOMBIE_SIZE = 12;
	public static double ZMB_HIT_RATE = 0.7;
	public static double ZMB_CHASER_RATE = 0.8;
	public static int ZMB_HUMAN_HEAD_START = 10;
	public static int ZMB_HUMAN_STEPS_NUMBER = 5;
	public static double ZMB_MAX_FEAR = 1000.0;
	public static double ZMB_HIT_FEAR = 1.0;
	public static double ZMB_MAX_DISTANCE = 50.0;
	public static int ZMB_POW_DISTANCE = -1;
	public static double ZMB_CHANGE_RATE = 0.5;
	public static int ZMB_PERIOD_MUTATION = 500;
	public static double ZMB_ZOMBIE_COVAR = 0.5;
	public static double ZMB_ZOMBIE_DIST_TO_TRANSFORM = 0.5;
	public static double ZMB_HUMAN_INITIAL_COVAR = 10.0;
	public static double ZMB_HUMAN_COVAR_REDUCTION_STEP = 0.0005;
	public static double ZMB_HUMAN_MIN_COVAR = 0.0001;

	// Parameters PSO
	public static int PSO_SWARM_SIZE = 100;
	public static double PSO_R1_MIN = 0.0;
	public static double PSO_R1_MAX = 1.0;
	public static double PSO_R2_MIN = 0.0;
	public static double PSO_R2_MAX = 1.0;
	public static double PSO_C1_MIN = 1.49168;
	public static double PSO_C1_MAX = 1.49168;
	public static double PSO_C2_MIN = 1.49168;
	public static double PSO_C2_MAX = 1.49168;
	public static double PSO_WEIGHT = 0.7298;
	public static double PSO_SPEED_MIN = -0.5;
	public static double PSO_SPEED_MAX = +0.5;

	// Parameters GA
	public static int GA_POP_SIZE = 100;
	public static int GA_TOURNAMENTS = 2;
	public static double GA_CROSSOVER_PROBABILITY = 0.9;
	public static double GA_DISTRIBUTION_INDEX_CROSSOVER = 20.0; // Between 5-20
	public static double GA_DISTRIBUTION_INDEX_MUTATION = 20.0; // Between 5-20
	public static double GA_EPS = 1.0e-14;

	// Parameters BAT
	public static int BAT_BATS = 25;
	public static double BAT_Q_MIN = 0.0;
	public static double BAT_Q_MAX = 1.0;
	public static double BAT_ALPHA = 0.9;
	public static double BAT_GAMMA = 0.01;

	// Parameters CMAES
	public static int CMA_POP_SIZE = 20;
	public static double CMA_SIGMA = 0.3;
	public static int CMA_LAMBDA = 1;

	// Parameters EDA
	public static int EDA_NUM_SOLUTIONS = 50;
	public static int EDA_NUM_SUBINTERVALS = 50;
	public static int EDA_NUM_PARTICLES = 30;
}
