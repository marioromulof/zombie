package br.com.si.util;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.util.FastMath;

import br.com.si.actor.Human;
import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.Solution;

public class Utils {
	private static final long MEGABYTE = 1024L * 1024L;

	public static double getEuclideanDistance(int[] modelA, int[] modelB) {
		if (modelA.length != modelB.length) {
			return -1;
		}

		double distance = 0;
		for (int i = 0; i < modelA.length; ++i) {
			distance += FastMath.pow(modelA[i] - modelB[i], 2.0);
		}

		return FastMath.sqrt(distance);
	}

	public static double getEuclideanDistance(Double[] modelA, Double[] modelB) {
		double distance = 0.0;
		for (int i = 0; i < modelA.length; ++i) {
			double diff = modelA[i] - modelB[i];
			distance += diff * diff;
		}

		return FastMath.sqrt(distance);
	}

	public static double getEuclideanDistance(BigDecimalSolution solution, BigDecimalSolution solution2) {
		double distance = 0.0;

		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			distance += ((BigDecimal) solution.getVariableValue(i)).subtract((BigDecimal) solution2.getVariableValue(i))
					.pow(2).doubleValue();
		}

		return FastMath.sqrt(distance);
	}

	public static double getEuclideanDistance(DoubleSolution solution, DoubleSolution solution2) {
		double distance = 0.0;

		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			double diff = solution.getVariableValue(i) - solution2.getVariableValue(i);
			distance += diff * diff;
		}

		return FastMath.sqrt(distance);
	}

	public static double getEuclideanDistance(BigDecimal[] solution, BigDecimalSolution solution2) {
		double distance = 0.0;

		for (int i = 0; i < solution.length; i++) {
			distance += solution[i].subtract(solution2.getVariableValue(i)).pow(2).doubleValue();
		}

		return FastMath.sqrt(distance);
	}

	public static double getManhattanDistance(BigDecimal[] var, BigDecimal[] var2) {
		BigDecimal distance = BigDecimal.ZERO;

		for (int i = 0; i < var.length; i++) {
			distance = distance.add(var[i].subtract(var2[i]).abs());
		}

		return BigDecimal.ONE.subtract(BigDecimal.ONE.divide(distance)).doubleValue();
	}

	public static DoubleSolution getRandomPoint(DoubleSolution gi, double radius, Random random) {
		/*
		 * ******* Random point in a hypersphere ******** Maurice Clerc
		 * 2003-07-11 Last update: 2011-01-01
		 * 
		 * Put a random point inside the hypersphere S(center 0, radius 1), or
		 * on its surface
		 */

		int j;
		double length;
		double r;
		double[] x = new double[gi.getNumberOfVariables()];

		// ----------------------------------- Step 1. Direction
		length = 0;
		for (j = 0; j < gi.getNumberOfVariables(); j++) {
			// Here a Gaussian distribution is needed
			double value = random.nextGaussian();
			x[j] = value;

			length = length + value * value;
		}

		length = FastMath.sqrt(length);
		// ----------------------------------- Step 2. Random radius

		r = random.nextDouble();

		for (j = 0; j < gi.getNumberOfVariables(); j++) {
			double value = (radius * r * x[j] / length) + gi.getVariableValue(j);
			gi.setVariableValue(j, value);
		}

		return gi;
	}

	@SuppressWarnings("rawtypes")
	public static Solution getBestSolution(Solution solution1, Solution solution2, Ordering order, Random random) {
		Solution result;
		int flag = compare(solution1, solution2, order);

		if (flag == -1)
			result = solution1;
		else {
			if (flag == 1)
				result = solution2;
			else {
				if (random.nextDouble() < 0.5)
					result = solution1;
				else
					result = solution2;
			}
		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	public static int compare(Solution solution1, Solution solution2, Ordering order) {
		int result;

		if (solution1 == null) {
			if (solution2 == null)
				result = 0;
			else
				result = +1;
		} else {
			if (solution2 == null)
				result = -1;
			else {
				Double objective1 = solution1.getObjective();
				Double objective2 = solution2.getObjective();

				if (order == Ordering.ASCENDING) {
					result = objective1.compareTo(objective2);
					int i = 0;
					while (result == 0 && i < solution1.getNumberOfVariables()) {
						result = ((Double) solution1.getVariableValue(i))
								.compareTo((Double) solution2.getVariableValue(i));
						i++;
					}
				} else {
					result = objective2.compareTo(objective1);
					int i = 0;
					while (result == 0 && i < solution1.getNumberOfVariables()) {
						result = ((Double) solution2.getVariableValue(i))
								.compareTo((Double) solution1.getVariableValue(i));
						i++;
					}
				}
			}
		}

		return result;
	}

	public static int compare(Double solution1, Double solution2, Ordering order) {
		int result;

		if (solution1 == null) {
			if (solution2 == null)
				result = 0;
			else
				result = +1;
		} else {
			if (solution2 == null)
				result = -1;
			else {
				if (order == Ordering.ASCENDING)
					result = Double.compare(solution1, solution2);
				else
					result = Double.compare(solution2, solution1);
			}
		}

		return result;
	}

	public static List<String> sortMap(Map<String, double[]> unsortMap) {

		// Convert Map to List
		List<Map.Entry<String, double[]>> list = new LinkedList<Map.Entry<String, double[]>>(unsortMap.entrySet());

		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, double[]>>() {
			public int compare(Map.Entry<String, double[]> o1, Map.Entry<String, double[]> o2) {
				int result = Double.compare(o1.getValue()[0], o2.getValue()[0]);
				if (result == 0) {
					result = Double.compare(o1.getValue()[1], o2.getValue()[1]);
				}
				return result;
			}
		});

		// Convert sorted map to a list
		List<String> sorted = new ArrayList<String>();
		for (Iterator<Map.Entry<String, double[]>> it = list.iterator(); it.hasNext();) {
			sorted.add(it.next().getKey());
		}
		return sorted;
	}

	/*
	 * Return random value from Cauchy distribution with mean "mu" and variance
	 * "gamma"
	 * http://www.sat.t.u-tokyo.ac.jp/~omi/random_variables_generation.html#
	 * Cauchy
	 */
	public static double cauchyG(double median, double scale, Random random, double ll, double ul) {
		double ret;
		do {
			ret = random.nextDouble();
			if (ret < 0 || ret > 1) {
				throw new OutOfRangeException(ret, 0, 1);
			} else if (ret == 0) {
				ret = Double.NEGATIVE_INFINITY;
			} else if (ret == 1) {
				ret = Double.POSITIVE_INFINITY;
			} else {
				ret = median + scale * FastMath.tan(FastMath.PI * (ret - .5));
			}
		} while (ret < ll || ret > ul);
		return ret;

		// result = mu + gamma * Math.tan(Math.PI * (random.nextDouble() -
		// 0.5));
	}

	public static double gauss(double mean, double sd, Random random) {
		return sd * random.nextGaussian() + mean;
		// return ((1 / sigma * Math.sqrt(2 * Math.PI)) * Math.exp((rand - mu) *
		// (rand - mu) / (-2 * sigma * sigma)));

		// return mu + sigma * rand;// mu + sigma * Math.sqrt(-2.0 *
		// Math.log(rand1)) *
		// Math.sin(2.0 * Math.PI *
		// rand2);
	}

	public static double[][] covariance(Double[][] matrix, double[] means, double divisor) {
		int rows = matrix.length;
		if (rows == 0) {
			return new double[0][0];
		}

		int cols = matrix[0].length;
		if (means.length != cols) {
			throw new IllegalArgumentException("Length of the mean vector should equal the number of columns");
		}

		double[][] cov = new double[cols][cols];
		for (int i = 0; i < cols; i++) {
			for (int j = i; j < cols; j++) {
				double s = 0.0;
				for (int k = 0; k < rows; k++) {
					s += (matrix[k][j] - means[j]) * (matrix[k][i] - means[i]);
				}
				s /= divisor;
				cov[i][j] = s;
				cov[j][i] = s;
			}
		}

		return cov;
	}

	public static double[][] choleskyDecomposition(double[][] A) {
		// Initialize.
		int n = A.length;
		double[][] L = new double[n][n];
		// boolean isspd = (A[0].length == n);

		// Main loop.
		for (int j = 0; j < n; j++) {
			double[] Lrowj = L[j];
			double d = 0.0;
			for (int k = 0; k < j; k++) {
				double[] Lrowk = L[k];
				double s = 0.0;
				for (int i = 0; i < k; i++) {
					s += Lrowk[i] * Lrowj[i];
				}

				if (L[k][k] != 0) {
					Lrowj[k] = s = (A[j][k] - s) / L[k][k];
				} else {
					Lrowj[k] = s = 0;
				}
				d = d + s * s;
				// isspd = isspd & (A[k][j] == A[j][k]);
			}
			d = A[j][j] - d;
			// isspd = isspd & (d > 0.0);
			L[j][j] = FastMath.sqrt(FastMath.max(d, 0.0));
			for (int k = j + 1; k < n; k++) {
				L[j][k] = 0.0;
			}
		}

		return L;
	}

	public static double[] mean(Double[][] matrix) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		double[] mean = new double[cols];

		// for each column
		for (int j = 0; j < cols; j++) {
			// for each row
			for (int i = 0; i < rows; i++) {
				mean[j] += matrix[i][j];
			}

			mean[j] /= rows;
		}

		return mean;
	}

	public static double mean(double[] list) {
		double sum = 0.0;
		for (double value : list) {
			sum += value;
		}
		return sum / (double) list.length;
	}

	public static double meanLehmer(double[] list) {
		double sum = 0.0;
		double sum2 = 0.0;
		for (double value : list) {
			sum += value * value;
			sum2 += value;
		}
		return sum / sum2;
	}

	public static double median(double[] list) {
		int middle = list.length / 2;
		if (list.length % 2 == 1) {
			return list[middle];
		} else {
			return (list[middle - 1] + list[middle]) / 2.0;
		}
	}

	public static double standardDeviation(double[] list) {
		double accum = 0.0;
		double accum2 = 0.0;
		double variance = 0.0;
		double mean = mean(list);

		for (double value : list) {
			accum += FastMath.pow(value - mean, 2.0);
			accum2 += (value - mean);
		}
		variance = (accum - (accum2 * accum2 / (double) list.length)) / (double) (list.length - 1.0);

		return FastMath.sqrt(variance);
	}

	public static double[] standardDeviation(Double[][] list) {
		double[] result = new double[list[0].length];
		double mean[] = mean(list);
		for (int i = 0; i < result.length; i++) {
			double accum = 0.0;
			double accum2 = 0.0;
			double variance = 0.0;

			for (int j = 0; j < list.length; j++) {
				accum += FastMath.pow(list[j][i] - mean[i], 2.0);
				accum2 += (list[j][i] - mean[i]);
			}
			variance = (accum - (accum2 * accum2 / (double) list.length)) / (double) (list.length - 1.0);
			result[i] = FastMath.sqrt(variance);
		}

		return result;
	}

	public static void deleteFiles(int dimension) {
		String[] folders = { "input_data/results/" };
		String endFileName = dimension + ".txt";
		String name;

		for (String folder : folders) {
			File dir = new File(folder);
			if (dir.isDirectory()) {
				for (File file : dir.listFiles()) {
					name = file.getName();
					if (name.endsWith(endFileName)) {
						file.delete();
					}
				}
			}
		}
	}

	public static BigDecimal powTen(BigDecimal value) {
		BigDecimal result, remainder, intPow, realPow;
		int sign = value.signum();

		value = value.multiply(new BigDecimal(sign));
		remainder = value.remainder(BigDecimal.ONE);
		intPow = BigDecimal.TEN.pow(value.subtract(remainder).intValueExact());
		realPow = new BigDecimal(FastMath.pow(BigDecimal.TEN.doubleValue(), remainder.doubleValue()));

		result = intPow.multiply(realPow);

		if (sign == -1) {
			result = BigDecimal.ONE.divide(result, 20, RoundingMode.HALF_UP);
		}

		return result;
	}

	/**
	 * Linear algebraic matrix multiplication, A * B
	 * 
	 * @return Matrix product, A * B
	 * @throws IllegalArgumentException
	 *             Matrix inner dimensions must agree.
	 */
	public static double[][] matrixTimes(double[][] A, double[][] B) {
		if (B.length != A[0].length) {
			throw new IllegalArgumentException("Matrix inner dimensions must agree.");
		}
		double[][] C = new double[A.length][B[0].length];
		double[] Bcolj = new double[A[0].length];
		for (int j = 0; j < B[0].length; j++) {
			for (int k = 0; k < A[0].length; k++) {
				Bcolj[k] = B[k][j];
			}
			for (int i = 0; i < A.length; i++) {
				double[] Arowi = A[i];
				double s = 0;
				for (int k = 0; k < A[0].length; k++) {
					s += Arowi[k] * Bcolj[k];
				}
				C[i][j] = s;
			}
		}
		return C;
	}

	/**
	 * Multiply a matrix by a scalar, C = s*A
	 * 
	 * @return s*A
	 */
	public static double[][] times(double s, double[][] A) {
		double[][] C = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				C[i][j] = s * A[i][j];
			}
		}
		return C;
	}

	/**
	 * divide a matrix by a scalar, C = A/s
	 * 
	 * @return A/s
	 */
	public static double[][] divide(double s, double[][] A) {
		double[][] C = new double[A.length][A[0].length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				C[i][j] = A[i][j] / s;
			}
		}
		return C;
	}

	/**
	 * Matrix transpose.
	 * 
	 * @return A'
	 */
	public static double[][] transpose(double[][] A) {
		double[][] C = new double[A[0].length][A.length];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[0].length; j++) {
				C[j][i] = A[i][j];
			}
		}
		return C;
	}

	public static void minFastSort(double[] x, int[] idx, int size) {
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (x[i] > x[j]) {
					double temp = x[i];
					int tempIdx = idx[i];
					x[i] = x[j];
					x[j] = temp;
					idx[i] = idx[j];
					idx[j] = tempIdx;
				} else if (x[i] == x[j]) {
					if (idx[i] > idx[j]) {
						double temp = x[i];
						int tempIdx = idx[i];
						x[i] = x[j];
						x[j] = temp;
						idx[i] = idx[j];
						idx[j] = tempIdx;
					}
				} // if
			}
		} // for
	} // minFastSort

	public static void tred2(int n, double V[][], double d[], double e[]) {
		// This is derived from the Algol procedures tred2 by
		// Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
		// Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
		// Fortran subroutine in EISPACK.
		for (int j = 0; j < n; j++) {
			d[j] = V[n - 1][j];
		}

		// Householder reduction to tridiagonal form.
		for (int i = n - 1; i > 0; i--) {
			// Scale to avoid under/overflow.
			double scale = 0.0;
			double h = 0.0;
			for (int k = 0; k < i; k++) {
				scale = scale + FastMath.abs(d[k]);
			}
			if (scale == 0.0) {
				e[i] = d[i - 1];
				for (int j = 0; j < i; j++) {
					d[j] = V[i - 1][j];
					V[i][j] = 0.0;
					V[j][i] = 0.0;
				}
			} else {
				// Generate Householder vector.
				for (int k = 0; k < i; k++) {
					d[k] /= scale;
					h += d[k] * d[k];
				}
				double f = d[i - 1];
				double g = FastMath.sqrt(h);
				if (f > 0) {
					g = -g;
				}
				e[i] = scale * g;
				h = h - f * g;
				d[i - 1] = f - g;
				for (int j = 0; j < i; j++) {
					e[j] = 0.0;
				}

				// Apply similarity transformation to remaining columns.
				for (int j = 0; j < i; j++) {
					f = d[j];
					V[j][i] = f;
					g = e[j] + V[j][j] * f;
					for (int k = j + 1; k <= i - 1; k++) {
						g += V[k][j] * d[k];
						e[k] += V[k][j] * f;
					}
					e[j] = g;
				}
				f = 0.0;
				for (int j = 0; j < i; j++) {
					e[j] /= h;
					f += e[j] * d[j];
				}
				double hh = f / (h + h);
				for (int j = 0; j < i; j++) {
					e[j] -= hh * d[j];
				}
				for (int j = 0; j < i; j++) {
					f = d[j];
					g = e[j];
					for (int k = j; k <= i - 1; k++) {
						V[k][j] -= (f * e[k] + g * d[k]);
					}
					d[j] = V[i - 1][j];
					V[i][j] = 0.0;
				}
			}
			d[i] = h;
		}

		// Accumulate transformations.
		for (int i = 0; i < n - 1; i++) {
			V[n - 1][i] = V[i][i];
			V[i][i] = 1.0;
			double h = d[i + 1];
			if (h != 0.0) {
				for (int k = 0; k <= i; k++) {
					d[k] = V[k][i + 1] / h;
				}
				for (int j = 0; j <= i; j++) {
					double g = 0.0;
					for (int k = 0; k <= i; k++) {
						g += V[k][i + 1] * V[k][j];
					}
					for (int k = 0; k <= i; k++) {
						V[k][j] -= g * d[k];
					}
				}
			}
			for (int k = 0; k <= i; k++) {
				V[k][i + 1] = 0.0;
			}
		}
		for (int j = 0; j < n; j++) {
			d[j] = V[n - 1][j];
			V[n - 1][j] = 0.0;
		}
		V[n - 1][n - 1] = 1.0;
		e[0] = 0.0;
	}

	public static void tql2(int n, double d[], double e[], double V[][]) {
		// This is derived from the Algol procedures tql2, by
		// Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
		// Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
		// Fortran subroutine in EISPACK.
		for (int i = 1; i < n; i++) {
			e[i - 1] = e[i];
		}
		e[n - 1] = 0.0;

		double f = 0.0;
		double tst1 = 0.0;
		double eps = FastMath.pow(2.0, -52.0);
		for (int l = 0; l < n; l++) {
			// Find small subdiagonal element
			tst1 = FastMath.max(tst1, FastMath.abs(d[l]) + FastMath.abs(e[l]));
			int m = l;
			while (m < n) {
				if (FastMath.abs(e[m]) <= eps * tst1) {
					break;
				}
				m++;
			}

			if (m > l) {
				int iter = 0;
				do {
					iter = iter + 1; // (Could check iteration count here.)

					// Compute implicit shift
					double g = d[l];
					double p = (d[l + 1] - g) / (2.0 * e[l]);
					double r = hypot(p, 1.0);
					if (p < 0) {
						r = -r;
					}
					d[l] = e[l] / (p + r);
					d[l + 1] = e[l] * (p + r);
					double dl1 = d[l + 1];
					double h = g - d[l];
					for (int i = l + 2; i < n; i++) {
						d[i] -= h;
					}
					f = f + h;

					// Implicit QL transformation.
					p = d[m];
					double c = 1.0;
					double c2 = c;
					double c3 = c;
					double el1 = e[l + 1];
					double s = 0.0;
					double s2 = 0.0;
					for (int i = m - 1; i >= l; i--) {
						c3 = c2;
						c2 = c;
						s2 = s;
						g = c * e[i];
						h = c * p;
						r = hypot(p, e[i]);
						e[i + 1] = s * r;
						s = e[i] / r;
						c = p / r;
						p = c * d[i] - s * g;
						d[i + 1] = h + s * (c * g + s * d[i]);

						// Accumulate transformation.
						for (int k = 0; k < n; k++) {
							h = V[k][i + 1];
							V[k][i + 1] = s * V[k][i] + c * h;
							V[k][i] = c * V[k][i] - s * h;
						}
					}
					p = -s * s2 * c3 * el1 * e[l] / dl1;
					e[l] = s * p;
					d[l] = c * p;

					// Check for convergence.
				} while (FastMath.abs(e[l]) > eps * tst1);
			}
			d[l] = d[l] + f;
			e[l] = 0.0;
		}

		// Sort eigenvalues and corresponding vectors.
		for (int i = 0; i < n - 1; i++) {
			int k = i;
			double p = d[i];
			for (int j = i + 1; j < n; j++) {
				if (d[j] < p) { // NH find smallest k>i
					k = j;
					p = d[j];
				}
			}
			if (k != i) {
				d[k] = d[i]; // swap k and i
				d[i] = p;
				for (int j = 0; j < n; j++) {
					p = V[j][i];
					V[j][i] = V[j][k];
					V[j][k] = p;
				}
			}
		}
	} // tql2

	/** sqrt(a^2 + b^2) without under/overflow. **/
	private static double hypot(double a, double b) {
		double r = 0;
		if (FastMath.abs(a) > FastMath.abs(b)) {
			r = b / a;
			r = FastMath.abs(a) * FastMath.sqrt(1 + r * r);
		} else if (b != 0) {
			r = a / b;
			r = FastMath.abs(b) * FastMath.sqrt(1 + r * r);
		}
		return r;
	}

	public static double polynomialMutation(double rnd, double x, double distributionIndex, Double lb, Double ub) {
		double dx = ub - lb;
		double val = distributionIndex + 1.0;

		if (rnd < 0.5) {
			double bl = (x - lb) / dx;
			double b = 2.0 * rnd + (1.0 - 2.0 * rnd) * FastMath.pow((1 - bl), val);
			val = FastMath.pow(b, (1.0 / val)) - 1.0;
		} else {
			double bu = (ub - x) / dx;
			double b = 2.0 * (1.0 - rnd) + 2.0 * (rnd - 0.5) * FastMath.pow((1 - bu), val);
			val = 1.0 - (FastMath.pow(b, (1.0 / val)));
		}
		x += val * dx;

		return x;
	}

	public static DoubleSolution getCentroid(Human[] humans) {
		double np = humans.length;
		int nVar = humans[0].getSolution().getNumberOfVariables();
		DoubleSolution centroid = new DoubleSolution(nVar, humans[0].getSolution().getOrder());

		for (int v = 0; v < nVar; v++) {
			centroid.setVariableValue(v, humans[0].getSolution().getVariableValue(v));
		}
		for (int h = 1; h < np; h++) {
			for (int v = 0; v < nVar; v++) {
				centroid.setVariableValue(v,
						centroid.getVariableValue(v) + humans[h].getSolution().getVariableValue(v));
			}
		}
		for (int v = 0; v < nVar; v++) {
			centroid.setVariableValue(v, centroid.getVariableValue(v) / np);
		}

		return centroid;
	}

	public static double getDistanceCentroid(Human[] humans) {
		double distancia = 0.0;
		double np = humans.length;
		DoubleSolution centroid = getCentroid(humans);

		for (int h = 0; h < np; h++) {
			distancia += getEuclideanDistance(centroid, humans[h].getSolution());
		}
		distancia /= np;

		return distancia;
	}

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public static String getStringVector(double[] vector) {
		String result = "[" + vector[0];
		for (int i = 1; i < vector.length; i++) {
			result += "," + vector[i];
		}
		result += "]";

		return result;
	} // getStringVector

	public static String getStringVectorClean(double[] vector) {
		String result = "" + vector[0];
		for (int i = 1; i < vector.length; i++) {
			result += " " + vector[i];
		}

		return result;
	} // getStringVector

	public static String getStringVectorClean(int[] vector) {
		String result = "" + vector[0];
		for (int i = 1; i < vector.length; i++) {
			result += " " + vector[i];
		}

		return result;
	} // getStringVector

	public static String getStringVectorClean(boolean[] vector) {
		String result = "" + Boolean.toString(vector[0]);
		for (int i = 1; i < vector.length; i++) {
			result += " " + Boolean.toString(vector[i]);
		}

		return result;
	} // getStringVector

	public static String getBestOfVector(double[] vector) {
		double best = vector[0];
		String result = "0";

		for (int i = 1; i < vector.length; i++) {
			if (vector[i] < best) {
				best = vector[i];
				result = Integer.toString(i);
			} else if (vector[i] == best) {
				result += "-" + i;
			}
		}

		return result;
	} // getBestOfVector
}
