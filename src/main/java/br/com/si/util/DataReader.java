/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * 
 * @author QIN
 */
public final class DataReader {
	private static double[] readDouble(Scanner input, int maxSize) {
		double[] ret = new double[maxSize];

		for (int i = 0; i < ret.length; i++) {
			ret[i] = input.nextDouble();
		}

		return ret;
	}

	private static double[] readDouble(Scanner input, int nx, int cf_num) {
		double[] ret = new double[nx * cf_num];

		for (int i = 0; i < cf_num - 1; i++) {
			for (int j = 0; j < nx; j++) {
				ret[i * nx + j] = input.nextDouble();
			}
			input.nextLine();
		}
		for (int j = 0; j < nx; j++) {
			ret[(cf_num - 1) * nx + j] = input.nextDouble();
		}

		return ret;
	}

	private static int[] readInt(Scanner input, int maxSize) {
		int[] ret = new int[maxSize];
		int actualSize = 0;

		try {
			// Scanner input = new Scanner(dataFile);
			for (int i = 0; i < ret.length; i++) {
				ret[i] = input.nextInt();
				actualSize++;
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

		if (actualSize != maxSize) {
			return Arrays.copyOf(ret, actualSize);
		} else {
			return ret;
		}
	}

	@SuppressWarnings("resource")
	public static int[] readShuffleData(int func_num, int nx, int cf_num) {
		int size = cf_num * nx;

		String fn = "input_data/shuffle_data_" + func_num + "_D" + nx + ".txt";
		// InputStream dataFile = DataReader.class.getResourceAsStream(fn);

		try {
			return readInt(new Scanner(new File(fn)).useLocale(Locale.ENGLISH), size);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("resource")
	public static double[] readRotation(int func_num, int nx, int cf_num) {
		int size = cf_num * nx * nx;
		String fn = "input_data/M_" + func_num + "_D" + nx + ".txt";
		// InputStream dataFile = DataReader.class.getResourceAsStream(fn);

		try {
			return readDouble(new Scanner(new File(fn)).useLocale(Locale.ENGLISH), size);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("resource")
	public static double[] readShiftData(int func_num, int nx, int cf_num) {
		int size = nx * cf_num;
		String fn = "input_data/shift_data_" + func_num + ".txt";
		// InputStream dataFile = DataReader.class.getResourceAsStream(fn);

		try {
			if (func_num < 9) {
				return readDouble(new Scanner(new File(fn)).useLocale(Locale.ENGLISH), size);
			} else {
				return readDouble(new Scanner(new File(fn)).useLocale(Locale.ENGLISH), nx, cf_num);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("resource")
	public static double[] readBiasValue(int func_num, int cf_num/*, boolean correct*/) {
		if (cf_num > 1) {
			int size = cf_num;
			String fn = "input_data/bias_" + func_num + ".txt";//(correct ? "_c.txt" : ".txt");
			// InputStream dataFile = DataReader.class.getResourceAsStream(fn);

			try {
				return readDouble(new Scanner(new File(fn)).useLocale(Locale.ENGLISH), size);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	@SuppressWarnings("resource")
	public static double[] readParams(String algorithm, int func_num, int dim_num, int size) {
		String fp = "input_data/results/PARAMS_" + algorithm.toUpperCase() + "_" + func_num + "_" + dim_num + ".txt";

		try {
			return readDouble(new Scanner(new File(fp)).useLocale(Locale.ENGLISH), size);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String saveResultsAlgorithmFunction(String name, String problem, int function, int dimension,
			List<BigDecimal[]> results) {
		String fileName;
		String result = null;

		if (function != 0) {
			fileName = "input_data/results/" + name.toUpperCase() + "_" + function + "_" + dimension + ".txt";
		} else {
			fileName = "input_data/results/" + name.toUpperCase() + "_" + problem.toUpperCase() + "_" + dimension
					+ ".txt";
		}
		String moments[] = { "0.0001*MaxFES", "0.001*MaxFES", "0.01*MaxFES", "0.02*MaxFES", "0.03*MaxFES",
				"0.04*MaxFES", "0.05*MaxFES", "0.1*MaxFES", "0.2*MaxFES", "0.3*MaxFES", "0.4*MaxFES", "0.5*MaxFES",
				"0.6*MaxFES", "0.7*MaxFES", "0.8*MaxFES", "0.9*MaxFES", "MaxFES" };

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		double[] lastResults = new double[results.size()];
		int index = 0;
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf("%20s | ", df.format(new Date()));
			for (int i = 0; i < results.size(); i++) {
				out.printf("%22s", "Run " + (i + 1));
			}
			out.println();

			for (int i = 0; i < moments.length; i++) {
				out.printf("%20s = ", moments[i]);
				for (BigDecimal[] r : results) {
					out.printf("%22s", r[i].setScale(8, RoundingMode.CEILING).doubleValue());

					if (i == (moments.length - 1)) {
						lastResults[index] = r[i].setScale(8, RoundingMode.CEILING).doubleValue();
						index++;
					}
				}
				out.println();
			}
			out.println();

			Arrays.sort(lastResults);

			out.printf("%22s%22s%22s%22s%22s", "Best", "Worst", "Median", "Mean", "Std");
			out.println();
			result = String.format("%22s%22s%22s%22s%22s", lastResults[0], lastResults[lastResults.length - 1],
					Utils.median(lastResults), Utils.mean(lastResults), Utils.standardDeviation(lastResults));
			out.println(result);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}

		if (function != 0) {
			return String.format("%23s %s\n", (function < 10 ? "0" : "") + function, result);
		} else {
			return String.format("%23s %s\n", problem.toUpperCase(), result);
		}
	}

	public static void saveResultsAlgorithms(List<String> algorithmNames, String[] resultsAlgorithms, int dimension) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fileName;
		String[] results;
		int index;

		for (index = 0; index < algorithmNames.size(); index++) {
			fileName = "input_data/results/Results_" + algorithmNames.get(index).toUpperCase() + "_" + dimension
					+ ".txt";
			try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
				out.println(df.format(new Date()));
				out.printf("Func.%18s %22s%22s%22s%22s%22s", "", "Best", "Worst", "Median", "Mean", "Std");
				out.println();

				results = resultsAlgorithms[index].split("\n");
				Arrays.sort(results);

				for (int i = 0; i < results.length; i++) {
					out.printf("%s", results[i]);
					out.println();
				}
				out.println();
			} catch (Exception e) {
				System.err.println(e);
			}
		}
	}

	public static void saveLog(Date begin, String name, int dimension, String info) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fileName = "input_data/results/log_" + name.toUpperCase() + "_" + dimension + ".txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.print(df.format(begin));
			out.print(" - ");
			out.println(df.format(new Date()));
			out.println(info);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveLog(String name, int dimension, String info) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fileName = "input_data/results/log_" + name.toUpperCase() + "_" + dimension + ".txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.println(df.format(new Date()));
			out.println(info);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveResultsExperiment(List<String> algorithmNames, List<String> problemNames,
			HashMap<String, Map<String, double[]>> results, int dimension, String info) {
		String fileName = "input_data/results/Results_Experiment_" + dimension + ".txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.println(info);
			out.printf("%17s\t", "Problem");
			// print the names of the problem
			for (String algName : algorithmNames) {
				out.printf("%24s | %10s\t", algName, "Iterations");
			}
			out.println();
			// print the results for each problem x algorithm
			for (String prbName : problemNames) {
				out.printf("%17s\t", prbName);

				List<String> orderAlg = Utils.sortMap(results.get(prbName));
				for (String algName : algorithmNames) {
					double[] data = results.get(prbName).get(algName);

					String evaluate = "(" + orderAlg.indexOf(algName) + ") " + String.format("%.8f", data[0]);
					String iterations = String.format("%.0f", data[1]);
					out.printf("%24s | %10s\t", evaluate, iterations);
				}

				out.println();
			}
			out.println();

			out.printf("%15s\t", "Algorithm");
			// print the names of the problem
			for (String prbName : problemNames) {
				out.printf("%24s | %10s\t", prbName, "Iterations");
			}
			out.println();
			// print the results for each algorithm x problem
			for (String algName : algorithmNames) {
				out.printf("%15s\t", algName);

				for (String prbName : problemNames) {
					Map<String, double[]> resultProblems = results.get(prbName);
					List<String> orderAlg = Utils.sortMap(results.get(prbName));

					double[] data = resultProblems.get(algName);
					String evaluate = "(" + orderAlg.indexOf(algName) + ") " + String.format("%.8f", data[0]);
					String iterations = String.format("%.0f", data[1]);
					out.printf("%24s | %10s\t", evaluate, iterations);
				}

				out.println();
			}
			out.println();

			out.printf("%15s | %10s", "Algorithm", "Average Rating");
			out.println();
			List<Double> data = new ArrayList<>();
			for (String prbName : problemNames) {
				List<String> orderAlg = Utils.sortMap(results.get(prbName));

				for (int i = 0; i < algorithmNames.size(); i++) {
					double value = 1.0 + orderAlg.indexOf(algorithmNames.get(i));
					if (data.size() != algorithmNames.size()) {
						data.add(value);
					} else {
						data.set(i, data.get(i) + value);
					}
				}
			}

			for (int i = 0; i < algorithmNames.size(); i++) {
				out.printf("%15s = %10s\t", algorithmNames.get(i), data.get(i) / problemNames.size());
				out.println();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	@SuppressWarnings({ "resource", "unused" })
	public static double getMeanOfFile(String fileName) {
		String result = "";
		try {
			FileInputStream in = new FileInputStream("input_data/results/" + fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String values[] = {};// br.lines().toArray()[20].toString().split("
									// ");
			int index = values.length - 2;
			do {
				if (values[index].length() > 0) {
					result = values[index];
				}
				index--;
			} while (result.length() == 0);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return Double.parseDouble(result);
	}

	public static void saveResultsSorted(Map<String, Double> results, int function, int dimension) {
		String fileName = "input_data/results/Results_Sorted.txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf("%10s | ", "F" + function + "-D" + dimension);
			int count = 1;
			for (Map.Entry<String, Double> entry : results.entrySet()) {
				out.print(count + "-" + entry.getKey() + "(" + entry.getValue() + ") ");
				count++;
			}
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveOverwriteData(String file, String algorithm, int function, int dimension, String data) {
		String fileName = "input_data/results/" + file.toUpperCase() + algorithm.toUpperCase() + "_" + function + "_"
				+ dimension + ".txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
			out.printf(data);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveOverwriteData(String fileName, String data) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)))) {
			out.printf(data);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static void saveData(String fileName, String data) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf(data);
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void saveMeanSorted(LinkedList results, int dimension) {
		String fileName = "input_data/results/Results_Sorted.txt";

		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)))) {
			out.printf("%10s | ", "Means-D" + dimension);
			for (Iterator it = results.iterator(); it.hasNext();) {
				Map.Entry entry = (Map.Entry) it.next();
				out.print(entry.getKey() + "(" + ((Double) entry.getValue() / (dimension > 2 ? 15.0 : 10.0)) + ") ");
			}
			out.println();
			out.println();
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	// double ellips_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Ellipsoidal */
	// double bent_cigar_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Bent_Cigar */
	// double discus_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Discus */
	// double rosenbrock_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Rosenbrock's */
	// double ackley_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Ackley's */
	// double rastrigin_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Rastrigin's */
	// double weierstrass_func (double[] , double , int , double[] ,double[]
	// ,int ,int) /* Weierstrass's */
	// double griewank_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Griewank's */
	// double schwefel_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Schwefel's */
	// double katsuura_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Katsuura */
	// double grie_rosen_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Griewank-Rosenbrock */
	// double escaffer6_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* Expanded Scaffer��s
	// double happycat_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* HappyCat */
	// double hgbat_func (double[] , double , int , double[] ,double[] ,int
	// ,int) /* HGBat */
	// double hf01 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 1 */
	// double hf02 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 2 */
	// double hf03 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 3 */
	// double hf04 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 4 */
	// double hf05 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 5 */
	// double hf06 (double[] , double[] , int , double[] ,double[] , int[] ,int
	// ,int) /* Composition Function 6 */
	// double cf01 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 1 */
	// double cf02 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 2 */
	// double cf03 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 3 */
	// double cf04 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 4 */
	// double cf05 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 5 */
	// double cf06 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 6 */
	// double cf07 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 7 */
	// double cf08 (double[] , double[] , int , double[] ,double[] ,int ) /*
	// Composition Function 8 */
	// void shiftfunc (double[] , double[] , int ,double[] )
	// void rotatefunc (double[] , double[] , int ,double[] )
	// void sr_func (double[] .double[] ,int ,double[] ,double[] ,double ,int
	// ,int )/* shift and rotate*/
	// void asyfunc (double[] , double[] , int , double )
	// void oszfunc (double[] , double[] , int )
	// double cf_cal(double[] , double , int , double[] ,double[] ,double[]
	// ,double[] , int )
}

// ~ Formatted by Jindent --- http://www.jindent.com
