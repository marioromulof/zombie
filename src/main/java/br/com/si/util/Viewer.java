package br.com.si.util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Viewer extends JFrame implements KeyListener {
	private static final long serialVersionUID = 198765456L;
	boolean keyPressed;

	public void drawImage(ImageComponent i) {
		getContentPane().add(new JLabel(new ImageIcon(i.getImage())));
		setVisible(true);
	}

	public Viewer(String name, int height, int width) {
		super(name);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(width + 50, height + 70);

		keyPressed = false;
		addKeyListener(this);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
			System.exit(0);
		keyPressed = true;
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	public boolean AnyKeyWasPressed() {
		boolean keyWasPressed = keyPressed;

		// every time this method is called, we restart
		// to check if new keys are pressed.
		keyPressed = false;

		return keyWasPressed;
	}
}
