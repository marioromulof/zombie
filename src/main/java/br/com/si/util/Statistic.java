package br.com.si.util;

import java.util.ArrayList;
import java.util.List;

public class Statistic {
	private List<Double> gauss;
	private List<Double> poly;
	private List<Double> eda;
	private List<Double> human;
	private List<Double> zombie;

	public Statistic() {
		gauss = new ArrayList<>();
		poly = new ArrayList<>();
		eda = new ArrayList<>();
		human = new ArrayList<>();
		zombie = new ArrayList<>();
	}

	public void addGauss(double value) {
		gauss.add(value);
	}

	public void addPoly(double value) {
		poly.add(value);
	}

	public void addEda(double value) {
		eda.add(value);
	}

	public void addHuman(double value) {
		human.add(value);
	}

	public void addZombie(double value) {
		zombie.add(value);
	}

	public double getVarianceGauss() {
		return variance(gauss);
	}

	public double getVariancePoly() {
		return variance(poly);
	}

	public double getVarianceEda() {
		return variance(eda);
	}

	public double getVarianceHuman() {
		return variance(human);
	}

	public double getVarianceZombie() {
		return variance(zombie);
	}

	public double getVariance() {
		List<Double> data = new ArrayList<>();
		data.addAll(gauss);
		data.addAll(poly);
		data.addAll(eda);

		return variance(data);
	}

	public double getMeanGauss() {
		return mean(gauss);
	}

	public double getMeanPoly() {
		return mean(poly);
	}

	public double getMeanEda() {
		return mean(eda);
	}

	public double getMeanHuman() {
		return mean(human);
	}

	public double getMeanZombie() {
		return mean(zombie);
	}

	public double getMean() {
		List<Double> data = new ArrayList<>();
		data.addAll(gauss);
		data.addAll(poly);
		data.addAll(eda);

		return mean(data);
	}

	public double getStDGauss() {
		return standardDeviation(gauss);
	}

	public double getStDPoly() {
		return standardDeviation(poly);
	}

	public double getStDEda() {
		return standardDeviation(eda);
	}

	public double getStDHuman() {
		return standardDeviation(human);
	}

	public double getStDZombie() {
		return standardDeviation(zombie);
	}

	public double getStD() {
		List<Double> data = new ArrayList<>();
		data.addAll(gauss);
		data.addAll(poly);
		data.addAll(eda);

		return standardDeviation(data);
	}

	public static double variance(List<Double> list) {
		double variance = 0.0;
		double mean = mean(list);

		double accum = 0.0;
		double dev = 0.0;
		double accum2 = 0.0;
		for (int i = 0; i < list.size(); i++) {
			dev = list.get(i) - mean;
			accum += dev * dev;
			accum2 += dev;
		}
		variance = (accum - (accum2 * accum2 / (double) list.size())) / (double) (list.size() - 1.0);

		return variance;
	}

	@SafeVarargs
	public static double varianceByGroups(List<Double>... groups) {
		double sumVar = 0.0;
		double sumNum = 0.0;

		for (List<Double> group : groups) {
			sumVar += variance(group) * ((double) group.size() - 1.0);
			sumNum += ((double) group.size() - 1.0);
		}

		return sumVar / sumNum;
	}

	@SafeVarargs
	public static double varianceByMean(List<Double>... groups) {
		double sumMea = 0.0;
		double sumNum = 0.0;
		double meanG = 0.0;

		for (List<Double> group : groups) {
			sumMea += mean(group) * ((double) group.size());
			sumNum += ((double) group.size());
		}
		meanG = sumMea / sumNum;

		sumMea = 0.0;
		for (List<Double> group : groups) {
			sumMea += (mean(group) - meanG) * ((double) group.size());
		}

		return sumMea / (double) groups.length;
	}

	public static double standardDeviation(List<Double> list) {
		return Math.sqrt(variance(list));
	}

	public static double mean(List<Double> list) {
		double sum = 0.0;
		for (double value : list) {
			sum += value;
		}
		return sum / (double) list.size();
	}
}
