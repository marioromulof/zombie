package br.com.si.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;

public class ImageComponent {
	private double _boundariesLeft;
	private double _boundariesRight;

	private double _boundariesTop;
	private double _boundariesBottom;

	private BufferedImage _image;
	private BufferedImage backGround;

	private int _height;
	private int _width;

	private String _imageToSearchSpaceX(int x) {
		double ratio_x = (double) x / (double) (_width);
		double search_space_x = ratio_x * (_boundariesRight - _boundariesLeft) + _boundariesLeft;

		return String.valueOf(search_space_x);
	}

	private String _imageToSearchSpaceY(int y) {
		double ratio_y = (double) y / (double) (_height);
		double search_space_y = ratio_y * (_boundariesTop - _boundariesBottom) + _boundariesBottom;

		return String.valueOf(search_space_y);
	}

	private int _searchSpaceToImageX(double x) {
		double ratio_x = (x - _boundariesLeft) / (_boundariesRight - _boundariesLeft);
		int img_x = (int) (ratio_x * _width);

		return img_x;
	}

	private int _searchSpaceToImageY(double y) {
		double ratio_y = (y - _boundariesBottom) / (_boundariesTop - _boundariesBottom);
		int img_y = (int) (ratio_y * _height);

		return img_y;
	}

	public void setImageBoundaries(double left, double bottom, double right, double top) {
		_boundariesLeft = left;
		_boundariesBottom = bottom;
		_boundariesRight = right;
		_boundariesTop = top;
	}

	public void drawFunction(Problem problem) {
		Double x, y;

		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;

		double mat[][] = new double[_height][_width];

		List<Double> trueBestPosition_x = new ArrayList<>();
		List<Double> trueBestPosition_y = new ArrayList<>();

		/**
		 * Evalute the objective function for all pixels, and store the max,
		 * min, and the positions of the mins
		 **/
		for (int i = 0; i < _height; i++) {
			// System.out.println(i);

			for (int j = 0; j < _width; j++) {
				y = Double.parseDouble(_imageToSearchSpaceY(i));
				x = Double.parseDouble(_imageToSearchSpaceX(j));
				DoubleSolution solution = new DoubleSolution(2);

				solution.setVariableValue(0, x);
				solution.setVariableValue(1, y);

				problem.evaluate(solution);

				mat[i][j] = solution.getObjective();

				if (solution.getObjective() > max)
					max = solution.getObjective();

				if (solution.getObjective() < min) {
					min = solution.getObjective();

					trueBestPosition_x.clear();
					trueBestPosition_y.clear();

					trueBestPosition_x.add(x);
					trueBestPosition_y.add(y);
				} else if (solution.getObjective() == min) {
					trueBestPosition_x.add(x);
					trueBestPosition_y.add(y);
				}
			}
		}

		/** Normalize to [0, 1] **/
		for (int i = 0; i < _height; i++)
			for (int j = 0; j < _width; j++)
				mat[i][j] = (mat[i][j] - min) / (max - min);

		/** Draw **/
		for (int i = 0; i < _height; i++)
			for (int j = 0; j < _width; j++)
				_image.setRGB(j, i, new Color((float) mat[i][j], (float) 0.0, (float) (1.0 - mat[i][j])).getRGB());

		/** draw the targets **/
		for (int i = 0; i < trueBestPosition_x.size(); i++) {
			drawSquare(trueBestPosition_x.get(i), trueBestPosition_y.get(i), 8, 0, 0.752941176f, 0);
			System.out.println(i + ": " + trueBestPosition_x.get(i) + ", " + trueBestPosition_y.get(i));
		}

		backGround = new BufferedImage(_width, _height, BufferedImage.TYPE_3BYTE_BGR);
		backGround.setData(_image.getRaster());
	}

	public void drawSquare(int x, int y, int size, float red, float green, float blue) {
		for (int i = y - (size / 2); i < y + (size / 2); i++)
			for (int j = x - (size / 2); j < x + (size / 2); j++)
				if (i >= 0 && j >= 0 && i < _height && j < _width)
					_image.setRGB(j, i, new Color(red, green, blue).getRGB());
	}

	public void drawSquare(double x, double y, int size, float red, float green, float blue) {
		int img_x = _searchSpaceToImageX(x);
		int img_y = _searchSpaceToImageY(y);

		for (int i = img_y - (size / 2); i < img_y + (size / 2); i++)
			for (int j = img_x - (size / 2); j < img_x + (size / 2); j++)
				if (i >= 0 && j >= 0 && i < _height && j < _width)
					_image.setRGB(j, i, new Color(red, green, blue).getRGB());
	}

	public BufferedImage getImage() {
		return _image;
	}

	public void updateBackGround() {
		this._image.setData(backGround.getRaster());
	}

	public ImageComponent(int height, int width) {
		_height = height;
		_width = width;

		_boundariesLeft = 0;
		_boundariesBottom = 0;
		_boundariesRight = width;
		_boundariesTop = height;

		_image = new BufferedImage(_width, _height, BufferedImage.TYPE_3BYTE_BGR);
	}
}
