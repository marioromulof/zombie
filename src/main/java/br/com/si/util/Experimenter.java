package br.com.si.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.si.algorithm.Algorithm;
import br.com.si.db.dao.ParameterDao;
import br.com.si.db.dao.ResultDao;
import br.com.si.db.entity.Parameter;
import br.com.si.db.entity.Result;
import br.com.si.solution.DoubleSolution;

public class Experimenter {
	private int _numRuns;

	private ArrayList<Algorithm> _algorithms;
	private HashMap<String, Map<String, double[]>> _results;

	private List<String> _algorithmNames;
	private List<String> _problemNames;
	private boolean saveResultsOnDataBase;
	private int numThreads;
	private boolean executeInThread;
	private String[] resultsAlgorithms;
	private int dimension;

	public Experimenter(int numRuns, int numThreads, boolean saveResultsOnDataBase) {
		_numRuns = numRuns;
		_algorithms = new ArrayList<Algorithm>();

		_results = new HashMap<String, Map<String, double[]>>();

		_algorithmNames = new ArrayList<String>();
		_problemNames = new ArrayList<String>();

		this.saveResultsOnDataBase = saveResultsOnDataBase;
		this.numThreads = numThreads;
		this.executeInThread = true;
		this.dimension = 0;
	}

	public void register(Algorithm algorithm) {
		if (algorithm.notExecuteInThread()) {
			executeInThread = false;
		}

		if (dimension == 0) {
			dimension = algorithm.getProblemDimension();
		}

		if (dimension == algorithm.getProblemDimension()) {
			_algorithms.add(algorithm);
		} else {
			System.err.println("Dimension of " + algorithm.getName() + " and Problem (" + algorithm.getProblemName()
					+ ") is incompatible.");
		}
	}

	public void run() {
		if (_algorithms.size() > 0) {
			Date inicio, fim;
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			inicio = new Date();

			initProcess();

			int i;
			int size = _algorithms.size();
			final Calendar date = Calendar.getInstance();

			if (executeInThread) {
				List<Thread> tList = new ArrayList<>();
				for (i = 0; i < size; i++) {
					tList.add(new Thread("" + i) {
						public void run() {
							System.out.println("Thread: " + getName() + " running");
							int index = Integer.parseInt(getName());
							executeAlgorithm(index, date);
							System.out.println("Thread: " + getName() + " finished");
						}
					});
				}

				List<Integer> alives = new ArrayList<>();
				size = numThreads < size ? numThreads : size;
				for (i = 0; i < size; i++) {
					alives.add(i);
					tList.get(i).start();
				}

				boolean alive;
				do {
					alive = false;
					try {
						Thread.sleep(10000); // 1000 milliseconds is one second.
						// System.out.println("Waited 10000");
					} catch (InterruptedException ex) {
						Thread.currentThread().interrupt();
					}

					if (alives.size() < size) {
						for (int index : alives) {
							try {
								tList.get(index).join();
							} catch (InterruptedException e) {
								System.err.println(e.getMessage());
							}
						}
						alives.clear();
					}

					int index;
					for (i = 0; i < alives.size(); i++) {
						if (tList.get(alives.get(i)).isAlive()) {
							alive = true;
						} else {
							tList.set(alives.get(i), null);
							index = alives.get(i) + 1;

							while (index < _algorithms.size() && (tList.get(index) == null || alives.contains(index))) {
								index++;
							}

							if (index < _algorithms.size()) {
								System.out.println("i: " + index);
								tList.get(index).start();
								alives.set(i, index);
								alive = true;
							} else {
								alives.remove(i);
							}
						}
					}
				} while (alive);

			} else {

				for (i = 0; i < size; i++) {
					executeAlgorithm(i, date);
				}
			}
			fim = new Date();

			DataReader.saveResultsAlgorithms(_algorithmNames, resultsAlgorithms, dimension);

			String info = "Inicio: " + df.format(inicio) + "\n" + "Fim: " + df.format(fim) + "\n" + "Duracao: "
					+ getDifference(inicio.getTime(), fim.getTime()) + "\n";
			System.out.println(info);

			printSummary(info);
		}
	}

	private void initProcess() {
		_algorithmNames.clear();
		_problemNames.clear();
		_results.clear();

		for (int i = 0; i < _algorithms.size(); i++) {
			String algName = _algorithms.get(i).getName();
			String prbName = _algorithms.get(i).getProblemName();

			if (!_algorithmNames.contains(algName)) {
				_algorithmNames.add(algName);
			}
			if (!_problemNames.contains(prbName)) {
				_problemNames.add(prbName);
			}
			if (!_results.containsKey(prbName)) {
				_results.put(prbName, new HashMap<String, double[]>());
			}
		}

		resultsAlgorithms = new String[_algorithmNames.size()];
		Arrays.fill(resultsAlgorithms, 0, resultsAlgorithms.length, "");
		// Utils.deleteFiles(dimension);
	}

	private void executeAlgorithm(int index, Calendar date) {
		BigDecimal averageResult = BigDecimal.ZERO;
		int averageIterarion = 0;
		int function, j;
		String algName, prbName, log;

		List<BigDecimal[]> results = new ArrayList<>();
		Algorithm algorithm = _algorithms.get(index);

		algName = algorithm.getName();
		prbName = algorithm.getProblemName();

		for (j = 0; j < _numRuns; j++) {
			Date begin = new Date();
			DoubleSolution solution = algorithm.search();
			averageResult = averageResult.add(new BigDecimal(solution.getObjective()));
			averageIterarion += algorithm.getIterations();
			results.add(algorithm.getResults().clone());

			if (saveResultsOnDataBase) {
				ParameterDao parDao = new ParameterDao();
				Parameter param = parDao.selectByAlgorithm(algName, algorithm.getParameter());
				if (param == null) {
					param = new Parameter();
					param.setAlgorithm(algName);
					param.setParameter(algorithm.getParameter());
					param.setDate(date);
					parDao.save(param);
				}

				ResultDao resDao = new ResultDao();
				Result result = new Result(null, algName, prbName, param, solution.getNumberOfVariables(),
						solution.getObjective().doubleValue(), date, solution.toString(),
						"Iterations: " + algorithm.getIterations());
				resDao.save(result);
			}

			if (prbName.contains("OptParam")) {
				log = solution.toString();
				String[] fd = prbName.split("-");
				DataReader.saveOverwriteData("params_", algName, Integer.parseInt(fd[1]), Integer.parseInt(fd[2]), log);
			}

			log = algName.concat(" - Solution: [ ".concat(solution.toString()
					.concat(" ]\n - Iteraction ".concat(prbName.concat(": ".concat(Integer.toString(j + 1)
							.concat(" / ".concat(Integer.toString(_numRuns).concat(algorithm.getSummary())))))))));
			DataReader.saveLog(begin, algName, algorithm.getProblemDimension(), log);
			System.out.println(log);
		}

		try {
			function = Integer.parseInt(prbName.split(":f")[1]);
		} catch (Exception e) {
			function = 0;
		}

		averageResult = averageResult.divide(new BigDecimal(_numRuns), 10, RoundingMode.HALF_UP);
		averageIterarion /= (double) _numRuns;

		j = _algorithmNames.indexOf(algName);
		resultsAlgorithms[j] += DataReader.saveResultsAlgorithmFunction(algName, prbName, function, dimension, results);
		_results.get(prbName).put(algName, new double[] { averageResult.doubleValue(), averageIterarion });
		_algorithms.set(index, null);

		results.clear();
	}

	public void printSummary(String info) {
		_printResultPxA();
		System.out.println("");
		_printResultAxP();
		System.out.println("");
		_printAverageRating();
		DataReader.saveResultsExperiment(_algorithmNames, _problemNames, _results, dimension, info);
	}

	private void _printResultPxA() {
		System.out.printf("%17s\t", "Problem");

		// print the names of the problem
		for (String algName : _algorithmNames) {
			System.out.printf("%24s | %10s\t", algName, "Iterations");
		}
		System.out.println("");

		// print the results for each problem x algorithm
		for (String prbName : _problemNames) {
			System.out.printf("%17s\t", prbName);

			List<String> orderAlg = Utils.sortMap(_results.get(prbName));
			for (String algName : _algorithmNames) {
				double[] data = _results.get(prbName).get(algName);

				String evaluate = "(" + orderAlg.indexOf(algName) + ") " + String.format("%.8f", data[0]);
				String iterations = String.format("%.0f", data[1]);
				System.out.printf("%24s | %10s\t", evaluate, iterations);
			}

			System.out.println("");
		}
	}

	private void _printResultAxP() {
		System.out.printf("%15s\t", "Algorithm");

		// print the names of the problem
		for (String prbName : _problemNames) {
			System.out.printf("%24s | %10s\t", prbName, "Iterations");
		}
		System.out.println("");

		// print the results for each algorithm x problem
		for (String algName : _algorithmNames) {
			System.out.printf("%15s\t", algName);

			for (String prbName : _problemNames) {
				Map<String, double[]> resultProblems = _results.get(prbName);
				List<String> orderAlg = Utils.sortMap(_results.get(prbName));

				double[] data = resultProblems.get(algName);
				String evaluate = "(" + orderAlg.indexOf(algName) + ") " + String.format("%.8f", data[0]);
				String iterations = String.format("%.0f", data[1]);
				System.out.printf("%24s | %10s\t", evaluate, iterations);
			}

			System.out.println("");
		}
	}

	private void _printAverageRating() {
		System.out.printf("%15s | %10s", "Algorithm", "Average Rating");
		System.out.println("");

		List<Double> data = new ArrayList<>();
		for (String prbName : _problemNames) {
			List<String> orderAlg = Utils.sortMap(_results.get(prbName));

			for (int i = 0; i < _algorithmNames.size(); i++) {
				double value = 1.0 + orderAlg.indexOf(_algorithmNames.get(i));
				if (data.size() != _algorithmNames.size()) {
					data.add(value);
				} else {
					data.set(i, data.get(i) + value);
				}
			}
		}

		for (int i = 0; i < _algorithmNames.size(); i++) {
			System.out.printf("%15s = %10s\t", _algorithmNames.get(i), data.get(i) / _problemNames.size());
			System.out.println("");
		}
	}

	private String getDifference(long inicio, long fim) {
		String result = "";
		// in milliseconds
		long diff = fim - inicio;

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		result += diffDays + " days, ";
		result += diffHours + " hours, ";
		result += diffMinutes + " minutes, ";
		result += diffSeconds + " seconds.";

		return result;
	}

}
