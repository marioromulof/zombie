package br.com.si;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import br.com.si.util.Statistic;

public class MainRandom {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Double[] knn2 = { 35.0,25.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0,30.0 };
		Double[] knn = { 80.0,90.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,95.0,90.0 };
		Double[] svm = { 85.0,90.0,85.0,90.0,90.0,90.0,90.0,90.0,95.0,95.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0,90.0 };
		
		double[] ex1 = { 15.7, 14.2, 16.1, 18.1, 18.4 };
		double mean;
		double std;
		double z;

//		 System.out.println("Média granja: "+variance(granja));
//		 System.out.println("Desvio padrão granja:
//		 "+standardDeviation(granja));
//		 System.out.println("Média caipira: "+mean(caipira));
//		 System.out.println("Desvio padrão caipira:
//		 "+standardDeviation(caipira));

		// System.out.println("Média ex1: " + mean(ex1));
		// System.out.println("Desvio padrão ex1: " + standardDeviation(ex1));
		 System.out.println("Variancia knn2: " + Statistic.variance(Arrays.asList(knn2)));
		 System.out.println("Variancia knn: " + Statistic.variance(Arrays.asList(knn)));
		 System.out.println("Variancia svm: " + Statistic.variance(Arrays.asList(svm)));
		 System.out.println("Variancia: " + Statistic.varianceByGroups(Arrays.asList(knn2), Arrays.asList(knn), Arrays.asList(svm)));
		 System.out.println("Variancia: " + Statistic.varianceByMean(Arrays.asList(knn2), Arrays.asList(knn), Arrays.asList(svm)));

		Random random = new Random();

		System.out.println(random.nextInt(5));
/*
		Map<String, Statistic> dados = new HashMap<>();
		String[] files = { "input_data/results/log_ZOMBIEEDA_10a.txt" };
		String[] data;
		for (String file : files) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					if (line.contains("ProblemsCEC15")) {
						data = line.split(":");
						Statistic func = dados.get(data[1].split(" ")[0]);

						if (func == null) {
							func = new Statistic();
							dados.put(data[1].split(" ")[0], func);
						}

						func.addZombie(Double.parseDouble(data[4].split(",")[0]));
						func.addHuman(Double.parseDouble(data[5].split(",")[0]));
						func.addGauss(Double.parseDouble(data[6].split(",")[0]));
						func.addPoly(Double.parseDouble(data[7].split(",")[0]));
						func.addEda(Double.parseDouble(data[8]));
					}
				}
			}

			double mZ = 0;
			double mH = 0;
			double mG = 0;
			double mP = 0;
			double mE = 0;
			double mT = 0;

			double sZ = 0;
			double sH = 0;
			double sG = 0;
			double sP = 0;
			double sE = 0;
			double sT = 0;

			double vZ = 0;
			double vH = 0;
			double vG = 0;
			double vP = 0;
			double vE = 0;
			double vT = 0;

			for (Entry<String, Statistic> entry : dados.entrySet()) {
				String key = entry.getKey();
				Statistic value = entry.getValue();

				mZ += value.getMeanZombie();
				mH += value.getMeanHuman();
				mG += value.getMeanGauss();
				mP += value.getMeanPoly();
				mE += value.getMeanEda();
				mT += value.getMean();
				System.out.println(key + ": Means(\t" + value.getMeanZombie() + " Z;\t" + value.getMeanHuman() + " H;\t"
						+ value.getMeanGauss() + " G;\t" + value.getMeanPoly() + " P;\t" + value.getMeanEda() + " E;\t"
						+ value.getMean() + " T);");

				sZ += value.getStDZombie();
				sH += value.getStDHuman();
				sG += value.getStDGauss();
				sP += value.getStDPoly();
				sE += value.getStDEda();
				sT += value.getStD();
				System.out.println(key + ": StD(\t" + value.getStDZombie() + " Z;\t" + value.getStDHuman() + " H;\t"
						+ value.getStDGauss() + " G;\t" + value.getStDPoly() + " P;\t" + value.getStDEda() + " E;\t"
						+ value.getStD() + " T);");

				vZ += value.getVarianceZombie();
				vH += value.getVarianceHuman();
				vG += value.getVarianceGauss();
				vP += value.getVariancePoly();
				vE += value.getVarianceEda();
				vT += value.getVariance();
				System.out.println(key + ": Var(\t" + value.getVarianceZombie() + " Z;\t" + value.getVarianceHuman()
						+ " H;\t" + value.getVarianceGauss() + " G;\t" + value.getVariancePoly() + " P;\t"
						+ value.getVarianceEda() + " E;\t" + value.getVariance() + " T);");
				System.out.println(" ");
			}

			System.out.println("Totais: Means(\t" + mZ/(double)dados.size() + " Z;\t" + mH/(double)dados.size() + " H;\t" + mG/(double)dados.size() + " G;\t" + mP /(double)dados.size()+ " P;\t" + mE/(double)dados.size()
					+ " E;\t" + mT/(double)dados.size() + " T);");
			System.out.println("Totais: StD(\t" + sZ/(double)dados.size() + " Z;\t" + sH/(double)dados.size() + " H;\t" + sG/(double)dados.size() + " G;\t" + sP/(double)dados.size() + " P;\t" + sE/(double)dados.size()
					+ " E;\t" + sT/(double)dados.size() + " T);");
			System.out.println("Totais: Var(\t" + vZ/(double)dados.size() + " Z;\t" + vH/(double)dados.size() + " H;\t" + vG/(double)dados.size() + " G;\t" + vP/(double)dados.size() + " P;\t" + vE/(double)dados.size()
					+ " E;\t" + vT/(double)dados.size() + " T);"); 
		}*/
	}

	public static double valueZ(double mean, double std, double x) {
		return (x - mean) / std;
	}

}
