package br.com.si.db.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import br.com.si.db.HibernateUtil;
import br.com.si.db.entity.Result;

public class ResultDao {

	private SessionFactory sessionFactory;

	public ResultDao() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	public void save(Result result) {
		// obtem uma sessao
		Session session = sessionFactory.openSession();
		Transaction tx = null; // permite transacao com o BD

		try {
			tx = session.beginTransaction();
			session.saveOrUpdate(result);
			tx.commit();// faz a transacao
		} catch (Exception e) {
			e.printStackTrace();
			// cancela a transcao em caso de falha
			tx.rollback();
		} finally {
			session.close();
		}
	}
}
