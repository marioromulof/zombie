package br.com.si.db.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import br.com.si.db.HibernateUtil;
import br.com.si.db.entity.Parameter;
import br.com.si.params.BatParams;
import br.com.si.params.CmaEsParams;
import br.com.si.params.EdaParams;
import br.com.si.params.GaParams;
import br.com.si.params.ProblemParams;
import br.com.si.params.PsoParams;
import br.com.si.params.ZombieParams;

public class ParameterDao {

	private SessionFactory sessionFactory;

	public ParameterDao() {
		sessionFactory = HibernateUtil.getSessionFactory();
	}

	public void save(Parameter parameter) {
		// obtem uma sessao
		Session session = sessionFactory.openSession();
		Transaction tx = null; // permite transacao com o BD

		try {
			tx = session.beginTransaction();
			parameter.setParameters(getParameters(parameter));
			session.saveOrUpdate(parameter);
			tx.commit();// faz a transacao
		} catch (Exception e) {
			e.printStackTrace();
			// cancela a transcao em caso de falha
			tx.rollback();
		} finally {
			session.close();
		}
	}

	private String getParameters(Parameter parameter) {
		String result = "";

		switch (parameter.getAlgorithm()) {
		case "BatAlgorithm":
			BatParams batP = (BatParams) parameter.getParameter();
			result = "Num Bats: " + batP._numBats + ", Alpha: "
					+ batP._alpha + ", Gamma: " + batP._gamma;
			break;

		case "CMAESAlgorithm":
			CmaEsParams cmaP = (CmaEsParams) parameter.getParameter();
			result = "Pop: " + cmaP._populationSize ;
			break;

		case "EdaAlgorithm":
			EdaParams edaP = (EdaParams) parameter.getParameter();
			result = "Soluts: " ;
			break;

		case "GaAlgorithm":
			GaParams gaP = (GaParams) parameter.getParameter();
			result = "Pop: " + gaP._populationSize + ", Tour: " + gaP._numTournaments + ", CrossProb: "
					+ gaP._crossoverProbability + ", Eps:" + gaP._eps;
			break;

		case "PsoAlgorithm":
			PsoParams psoP = (PsoParams) parameter.getParameter();
			result = "Swarm: " + psoP._swarmSize;
			break;

		case "ZombieSearch":
			ZombieParams zomP = (ZombieParams) parameter.getParameter();
			result = "PopHum: " + zomP.maxHumanPop + ", PopZom: " + zomP.zombiePopSize + ", Head: ";
					//+ zomP._humanHeadStart + ", HumSteps: " + zomP._numHumanStepsPerZombieStep + ", FearMax: "
					//+ zomP._maxFear + ", DistMax: " + zomP._maxDistance + ", DistPow: " + zomP._powDistance
					//+ ", ZomCov: " + zomP._covar;
			break;

		default:
			result = "";
			break;
		}

		return result;
	}

	public Parameter selectByAlgorithm(String algorithm, ProblemParams parameter) {
		// obtem uma sessao
		Parameter result = new Parameter();
		result.setAlgorithm(algorithm);
		result.setParameter(parameter);
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Parameter p where p.algorithm = :value and p.parameters = :value2");
		query.setParameter("value", algorithm);
		query.setParameter("value2", getParameters(result));

		result = (Parameter) query.uniqueResult();
		session.close();

		return result;
	}

}
