package br.com.si.db.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.si.params.ProblemParams;

@Entity
@Table(name = "parameter")
public class Parameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "st_algorithm", nullable = false)
	private String algorithm;

	@Column(name = "st_data", nullable = false)
	private String parameters;

	@Column(name = "dt_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar date;

	@Transient
	private ProblemParams parameter;

	public Parameter() {
	}

	public Parameter(Integer id, String algorithm, String parameters) {
		super();
		this.id = id;
		this.algorithm = algorithm;
		this.parameters = parameters;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public ProblemParams getParameter() {
		return parameter;
	}

	public void setParameter(ProblemParams parameter) {
		this.parameter = parameter;
	}

}
