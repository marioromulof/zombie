package br.com.si.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.io.Serializable;
import java.util.Calendar;

@Entity
@Table(name = "result")
public class Result implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "st_algorithm", nullable = false)
	private String algorithm;

	@Column(name = "st_problem", nullable = false)
	private String problem;

	@JoinColumn(name = "id_parameter", nullable = false)
	@OneToOne(targetEntity = Parameter.class)
	private Parameter parameter;

	@Column(name = "in_dimension", nullable = false)
	private Integer dimension;

	@Column(name = "re_evaluate", nullable = false)
	private Double evaluate;

	@Column(name = "dt_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar date;

	@Column(name = "st_solution", length = 2048)
	private String solution;

	@Column(name = "st_annotation")
	private String annotation;

	public Result() {
	}

	public Result(Integer id, String algorithm, String problem, Parameter parameter, Integer dimension, Double evaluate,
			Calendar date, String solution, String annotation) {
		super();
		this.id = id;
		this.algorithm = algorithm;
		this.problem = problem;
		this.parameter = parameter;
		this.dimension = dimension;
		this.evaluate = evaluate;
		this.date = date;
		this.solution = solution;
		this.annotation = annotation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public Parameter getParameter() {
		return parameter;
	}

	public void setParameter(Parameter parameter) {
		this.parameter = parameter;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public Double getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(Double evaluate) {
		this.evaluate = evaluate;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

}
