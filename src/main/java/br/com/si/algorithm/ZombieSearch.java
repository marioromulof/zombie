package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import br.com.si.actor.Human;
import br.com.si.actor.Zombie;
import br.com.si.params.ProblemParams;
import br.com.si.params.ZombieParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Ordering;
import br.com.si.util.Utils;

@SuppressWarnings({ "unchecked" })
public class ZombieSearch extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4168867188365723754L;

	public static final String name = "ZombieSearch";

	private double distribution;
	private double diffDist;
	private double covariance;
	private double diffCov;
	private int nDim;
	private int nCalls;
	private DoubleSolution bestMovement;
	private double probabilityMove;

	private ArrayList<Human> humans;
	private ArrayList<Zombie> zombies;

	private Random random;

	private boolean viewActive;
	private boolean stepView;
	private ZombieParams params;
	private int count;
	private double error;
	private double zero = Math.pow(10.0, -8);

	public ZombieSearch(Problem problem, ZombieParams params, boolean viewActive, boolean stepView) {
		super(problem);

		this.viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this.stepView = stepView && problem.getNumberOfVariables() == 2;
		this.params = params;
		this.notExecuteInThread = (this.viewActive || this.stepView);

		this.nDim = problem.getNumberOfVariables();

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		this.bestMovement = null;
		count = 0;

		// double numGenerations = (double) (params.humanPopSize -
		// (params.humanPopSize * 0.2)) / 2.0;
		// numGenerations = ((double) params.maxObjectiveFunctionCalls /
		// (double) params.humanPopSize)
		// / numGenerations;

		covariance = params.humanMaxMutIdxFactor;
		// covariance = params.humanMinMutIdxFactor;
		// diffCov = (params.humanMinMutIdxFactor / 20.0) -
		// (params.humanMaxMutIdxFactor / 20.0);
		// diffCov = (diffCov / numGenerations);

		// distribution = params.humanMaxMutIdxFactor;
		distribution = params.humanMinMutIdxFactor;
		// diffDist = params.humanMaxMutIdxFactor -
		// params.humanMinMutIdxFactor;
		// diffDist = (diffDist / numGenerations);

		this.nCalls = 0;
		this.probabilityMove = 1.0 / (double) this.nDim;

		this.humans = new ArrayList<Human>();
		for (int i = 0; i < params.maxHumanPop; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			getMeritEvaluation(solution);
			this.humans.add(new Human(solution, random.nextDouble(), params.humanMaxFear, random.nextDouble()));
		}

		this.zombies = new ArrayList<Zombie>();

		for (int i = 0; i < params.humanStepsPerZombieStep; i++) {
			this.moveHumans();
		}

		for (int i = 0; i < params.zombiePopSize; i++) {
			Zombie z = new Zombie(this.problem.getNumberOfVariables(), random.nextDouble());
			z.setSolution(DoubleSolution.createSolution(problem, random).getVariables());
			zombies.add(z);
		}
	}

	@Override
	public void updateProgress() {
		this.nCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return this.humans.isEmpty() || this.nCalls >= params._maxObjectiveFunctionCalls
				|| (nCalls > 0 && error == 0.0);
	}

	@Override
	public int getIterations() {
		return this.nCalls;
	}

	@Override
	public DoubleSolution search() {
		int idx;
		double candidateDistance, zombieDistance;

		initProgress();
		if (this.viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {

			if (viewActive) {
				List<DoubleSolution> humans = new ArrayList<>();
				List<DoubleSolution> zombies = new ArrayList<>();

				for (Human human : this.humans) {
					humans.add((DoubleSolution) human.getSolution());
				}

				for (Zombie zombie : this.zombies) {
					zombies.add((DoubleSolution) zombie.getSolution());
				}

				redraw(bestMovement, humans, zombies);

				if (stepView) {
					stepView();
				}
			}

			for (idx = 0; idx < params.humanStepsPerZombieStep; idx++) {
				this.moveHumans();
			}

			for (idx = 0; idx < this.zombies.size(); idx++) {
				Zombie zombie = this.zombies.get(idx);
				if (!zombie.isChaser()) {
					continue;
				}

				zombieDistance = Double.MAX_VALUE;
				for (Human human : this.humans) {
					candidateDistance = Utils.getEuclideanDistance(zombie.getSolution(), human.getSolution());

					if (candidateDistance < params.zombieDistanceAttack) {
						zombieDistance = 0.0;

						if // ((random.nextDouble() * human.getDefenderRate() <
							// random.nextDouble()
						/* * zombie.getHuntingRate()) && */ (humans.size() > params.minHumanPop) {// )
																									// {
							this.turnZombie(human);
						}

						break;
					} else if (candidateDistance < zombieDistance) {
						zombieDistance = candidateDistance;
					}
				}

				if (zombieDistance >= params.zombieDistanceAttack && zombieDistance < Double.MAX_VALUE) {
					List<Integer> tries = new ArrayList<>();
					for (idx = 0; idx < this.nDim; idx++) {
						tries.add(idx);
					}

					Collections.shuffle(tries);

					boolean foundBetter = false;
					for (idx = 0; (idx < this.nDim) && !foundBetter; idx++) {

						DoubleSolution zombieSolution = (DoubleSolution) zombie.getSolution().copy();
						for (int i = 0; (i < 5) && !foundBetter; i++) {

							// double change = random.nextGaussian() *
							// params.covar;
							// double change =
							// zombieSolution.getVariableValue(tries.get(idx)) +
							// (random.nextGaussian() *
							// params.humanMaxDistance);

							zombieSolution.setVariableValue(tries.get(idx),
									zombieSolution.getVariableValue(tries.get(idx))
											+ random.nextGaussian() * params.humanMaxDistance);

							for (Human human : this.humans) {
								candidateDistance = Utils.getEuclideanDistance(zombieSolution, human.getSolution());

								if (candidateDistance < zombieDistance) {
									zombieDistance = candidateDistance;
									foundBetter = true;
								}
							}

							if (foundBetter) {
								zombie.setSolution(zombieSolution.getVariables().clone());
							}
						}

						if (!foundBetter) {
							count++;
						}
					}
				}
			}

			// if (covariance > 0.5) {
			// covariance += diffCov;
			covariance = params.humanMaxMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMaxMutIdxFactor - params.humanMinMutIdxFactor));
			// }
			// distribution += diffDist;
			distribution = params.humanMinMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMinMutIdxFactor - params.humanMaxMutIdxFactor));
		}

		if (viewActive) {
			List<DoubleSolution> humans = new ArrayList<>();
			List<DoubleSolution> zombies = new ArrayList<>();

			for (Human human : this.humans) {
				humans.add((DoubleSolution) human.getSolution());
			}

			for (Zombie zombie : this.zombies) {
				zombies.add((DoubleSolution) zombie.getSolution());
			}

			redraw(bestMovement, humans, zombies);
			if (stepView) {
				stepView();
			}

			finishViewer();
		}
		System.out.println("Vezes ñ encontradas: " + count);

		System.out.println(problem.getName() + " - Z: " + this.zombies.size() + ", H: " + this.humans.size()
				+ ", Cov.: " + covariance + ", DiffCov.: " + diffCov + ", Dist.: " + distribution + ", DiffDist.: "
				+ diffDist + "\n");
		return this.bestMovement;
	}

	private void moveHumans() {
		double oldEvaluation, newEvaluation, oldDistance, newDistance, distance;
		boolean hasZombie;
		boolean hasMutation;
		double diff;

		for (Human human : this.humans) {
			hasZombie = false;
			hasMutation = false;
			oldDistance = 0;
			newDistance = 0;
			DoubleSolution humanSolution = (DoubleSolution) human.getSolution().copy();
			oldEvaluation = humanSolution.getObjective();

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					hasZombie = true;
					oldDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (hasZombie) {
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						diff = random.nextGaussian() * covariance;
						humanSolution.setVariableValue(var, humanSolution.getVariableValue(var) + diff);
						hasMutation = true;
					}
				}
			} else {
				// chosenAtt = random.nextInt(this.nDim);
				// oldCoord = humanSolution.getVariableValue(chosenAtt);
				//
				// diff = random.nextGaussian() * humanCovar;
				// value = (oldCoord + diff);
				//
				// humanSolution.setVariableValue(chosenAtt, value);

				// polynomial mutation
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						humanSolution.setVariableValue(var,
								Utils.polynomialMutation(random.nextDouble(), humanSolution.getVariableValue(var),
										distribution, problem.getLowerBound(var), problem.getUpperBound(var)));
						hasMutation = true;
					}
				}
			}

			problem.simpleBounds(humanSolution);

			if (hasMutation) {
				newEvaluation = getMeritEvaluation(humanSolution);
			} else {
				newEvaluation = humanSolution.getObjective();
			}

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					newDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (problem.getOrder() == Ordering.ASCENDING) {
				oldEvaluation += (human.getFear() * oldDistance);
				newEvaluation += (human.getFear() * newDistance);

				if (oldEvaluation > newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			} else {
				oldEvaluation -= (human.getFear() * oldDistance);
				newEvaluation -= (human.getFear() * newDistance);

				if (oldEvaluation < newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			}

		}
	}

	private double getMeritEvaluation(DoubleSolution solution) {
		this.problem.simpleBounds(solution);
		this.problem.evaluate(solution);

		updateProgress();

		if (Utils.compare(solution, this.bestMovement, problem.getOrder()) == -1) {
			this.bestMovement = (DoubleSolution) solution.copy();

			if (problem.getName().contains("ProblemsCEC15")) {
				this.error = solution.getObjective() - problem.getNumFunction() * 100;

				if (this.error < this.zero) {
					this.error = 0.0;
				}
			}
		}

		setResult(new BigDecimal(this.bestMovement.getObjective()), this.nCalls,
				this.params._maxObjectiveFunctionCalls);

		return solution.getObjective();
	}

	private void turnZombie(Human human) {
		Zombie newzombie = new Zombie(problem.getNumberOfVariables(), random.nextDouble());
		newzombie.setSolution(human.getSolution().getVariables().clone());

		this.zombies.add(newzombie);
		this.humans.remove(human);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this.params;
	}

	@Override
	public String getSummary() {
		return " - " + nCalls;
	}

}
