package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import br.com.si.actor.Human;
import br.com.si.actor.Zombie;
import br.com.si.params.ProblemParams;
import br.com.si.params.ZombieParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.DataReader;
import br.com.si.util.Ordering;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class ZombieCMAES extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3705539933904202875L;

	public static final String name = "ZombieCMAES";

	private double distribution;
	private double covariance;
	private int nDim;
	private int nCalls;
	private DoubleSolution bestMovement;
	private double probabilityMove;
	private int nGroups;
	private int nPoly;
	private int nGauss;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private ArrayList<Human> humans;
	private ArrayList<Zombie> zombies;

	private Random random;

	private boolean viewActive;
	private boolean stepView;
	private ZombieParams params;

	// CMA-ES
	private int lambda;
	private int mu;
	private int eigeneval;
	private double sigma;
	private double mueff;
	private double chiN;
	private double cc;
	private double cs;
	private double c1;
	private double cmu;
	private double damps;
	private double[] weights;
	private double[] diagD;
	private double[] xold;
	private double[] xmean;
	private double[] pc;
	private double[] ps;
	private double[][] B;
	private double[][] C;
	private double[][] arx;
	private double[][] invsqrtC;

	public ZombieCMAES(Problem problem, ZombieParams params, boolean viewActive, boolean stepView) {
		super(problem);

		this.viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this.stepView = stepView && problem.getNumberOfVariables() == 2;
		this.params = params;
		this.notExecuteInThread = (this.viewActive || this.stepView);

		this.nDim = problem.getNumberOfVariables();

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		String groupsHumanMoved;

		this.bestMovement = null;
		nGroups = 0;
		nPoly = 0;
		nGauss = 0;

		covariance = params.humanMaxMutIdxFactor;
		distribution = params.humanMinMutIdxFactor;

		this.nCalls = 0;
		this.probabilityMove = 1.0 / (double) this.nDim;

		this.humans = new ArrayList<Human>();
		for (int i = 0; i < params.maxHumanPop; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			getMeritEvaluation(solution);
			this.humans.add(new Human(solution, random.nextDouble(), params.humanMaxFear, random.nextDouble()));
		}

		this.zombies = new ArrayList<Zombie>();

		// move humans
		groupsHumanMoved = moveGroupHuman();
		for (int i = 0; i < params.humanStepsPerZombieStep; i++) {
			this.moveHumans(groupsHumanMoved);
		}

		for (int i = 0; i < params.zombiePopSize; i++) {
			Zombie z = new Zombie(this.problem.getNumberOfVariables(), random.nextDouble());
			z.setSolution(DoubleSolution.createSolution(problem, random).getVariables());
			zombies.add(z);
		}
	}

	private String moveGroupHuman() {
		double oldEvaluation, newEvaluation, oldDistance, newDistance, distance;
		boolean hasZombie;
		String groupHuman, idHuman, groupsHumanMoved;
		int j;

		groupsHumanMoved = ",";
		for (int i = 0; i < humans.size(); i++) {
			idHuman = ",".concat(String.valueOf(i).concat(","));
			if (groupsHumanMoved.contains(idHuman)) {
				continue;
			}

			Human human = humans.get(i);

			hasZombie = false;
			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(human.getSolution(), zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					hasZombie = true;
				}
			}

			if (!hasZombie) {
				List<Human> pop = new ArrayList<>();
				pop.add(human);

				groupHuman = ",".concat(String.valueOf(i).concat(","));
				for (j = 0; j < humans.size(); j++) {
					idHuman = ",".concat(String.valueOf(j).concat(","));
					if (!groupHuman.contains(idHuman) && !groupsHumanMoved.contains(idHuman)) {
						distance = Utils.getEuclideanDistance(human.getSolution(), humans.get(j).getSolution());

						if (distance < params.humanMaxDistance) {
							pop.add(humans.get(j));
							groupHuman = groupHuman.concat(String.valueOf(j).concat(","));
						}
					}
				}

				if (pop.size() > 1) {
					groupsHumanMoved = groupHuman.concat(groupsHumanMoved.substring(1));

					lambda = pop.size();

					oldEvaluation = Double.MAX_VALUE;
					for (int k = 0; k < pop.size(); k++) {
						if (pop.get(k).getSolution().getObjective() < oldEvaluation) {
							oldEvaluation = pop.get(k).getSolution().getObjective();
							j = k;
						}
					}
					initCMAES(pop.get(j).getSolution());

					List<DoubleSolution> nPop = new ArrayList<>();
					for (int idx = 0; idx < params.humanStepsPerZombieStep; idx++) {
						nGroups += pop.size();

						nPop.addAll(samplePopulation(lambda));
						Collections.sort(nPop);

						updateDistribution(nPop);
					}

					for (int idx = 0; idx < lambda; idx++) {
						oldEvaluation = pop.get(idx).getSolution().getObjective();
						oldDistance = 0.0;

						boolean idxFound = false;
						j = -1;
						while (!idxFound && j < nPop.size() - 1) {
							j++;
							DoubleSolution solution = nPop.get(j);
							newEvaluation = solution.getObjective();
							newDistance = 0.0;

							for (Zombie zombie : this.zombies) {
								distance = Utils.getEuclideanDistance(solution, zombie.getSolution());
								if (distance < params.humanMaxDistance) {
									newDistance += Math.pow(distance, params.powDistance);
								}
							}

							if (problem.getOrder() == Ordering.ASCENDING) {
								oldEvaluation += (pop.get(idx).getFear() * oldDistance);
								newEvaluation += (pop.get(idx).getFear() * newDistance);

								if (oldEvaluation > newEvaluation) {
									pop.get(idx).setSolution((DoubleSolution) solution.copy());
									nPop.remove(solution);
									idxFound = true;
								}
							} else {
								oldEvaluation -= (pop.get(idx).getFear() * oldDistance);
								newEvaluation -= (pop.get(idx).getFear() * newDistance);

								if (oldEvaluation < newEvaluation) {
									pop.get(idx).setSolution((DoubleSolution) solution.copy());
									nPop.remove(solution);
									idxFound = true;
								}
							}
						}
					}

					nPop.clear();
				}
			}
		}

		return groupsHumanMoved;
	}

	private void initCMAES(DoubleSolution best) {
		// number of objective variables/problem dimension
		int N = problem.getNumberOfVariables();

		// coordinate wise standard deviation (step size)
		sigma = 0.3;// 0.8297;// params.initStepSizeFactor;

		/* Strategy parameter setting: Selection */

		// number of parents/points for recombination
		mu = (int) Math.floor(lambda / 2);

		// muXone array for weighted recombination
		weights = new double[mu];
		double sum = 0;
		for (int i = 0; i < mu; i++) {
			weights[i] = (Math.log(mu + (1.0 / 2.0)) - Math.log(i + 1));
			sum += weights[i];
		}
		// normalize recombination weights array
		for (int i = 0; i < mu; i++) {
			weights[i] = weights[i] / sum;
		}

		// variance-effectiveness of sum wi xi
		double sum1 = 0;
		double sum2 = 0;
		for (int i = 0; i < mu; i++) {
			sum1 += weights[i];
			sum2 += weights[i] * weights[i];
		}
		mueff = sum1 * sum1 / sum2;

		/* Strategy parameter setting: Adaptation */

		// time constant for cumulation for C
		cc = (4.0 + mueff / (double) N) / (N + 4.0 + 2.0 * mueff / (double) N);

		// t-const for cumulation for sigma control
		cs = (mueff + 2.0) / (double) (N + mueff + 5.0);

		// learning rate for rank-one update of C
		c1 = 2.0 / ((N + 1.3) * (N + 1.3) + mueff);

		// learning rate for rank-mu update
		cmu = Math.min(1.0 - c1, 2 * (mueff - 2.0 + 1.0 / mueff) / ((N + 2.0) * (N + 2.0) + mueff));

		// damping for sigma, usually close to 1
		damps = 1.0 + 2.0 * Math.max(0.0, Math.sqrt((mueff - 1.0) / (N + 1.0)) - 1.0) + cs;

		/* Initialize dynamic (internal) strategy parameters and constants */

		// diagonal D defines the scaling
		diagD = new double[N];

		// evolution paths for C and sigma
		pc = new double[N];
		ps = new double[N];

		// B defines the coordinate system
		B = new double[N][N];
		// covariance matrix C
		C = new double[N][N];

		// C^-1/2
		invsqrtC = new double[N][N];

		for (int i = 0; i < N; i++) {
			pc[i] = 0.0;
			ps[i] = 0.0;
			diagD[i] = 1;
			for (int j = 0; j < N; j++) {
				B[i][j] = 0.0;
				invsqrtC[i][j] = 0.0;
			}
			for (int j = 0; j < i; j++) {
				C[i][j] = 0.0;
			}
			B[i][i] = 1.0;
			C[i][i] = diagD[i] * diagD[i];
			invsqrtC[i][i] = 1.0;
		}

		// objective variables initial point
		xmean = new double[N];
		for (int i = 0; i < N; i++) {
			// double offset = sigma * diagD[i];
			// double range = (problem.getUpperBound(i) -
			// problem.getLowerBound(i) - 2.0 * sigma * diagD[i]);
			//
			// if (offset > 0.4 * (problem.getUpperBound(i) -
			// problem.getLowerBound(i))) {
			// offset = 0.4 * (problem.getUpperBound(i) -
			// problem.getLowerBound(i));
			// range = 0.2 * (problem.getUpperBound(i) -
			// problem.getLowerBound(i));
			// }
			//
			// xmean[i] = problem.getLowerBound(i) + offset +
			// random.nextDouble() * range;
			xmean[i] = (best.getVariableValue(i) + bestMovement.getVariableValue(i)) / 2.0;
		}

		// track update of B and D
		eigeneval = 0;

		chiN = Math.sqrt(N) * (1.0 - 1.0 / (4.0 * N) + 1.0 / (21.0 * N * N));

		/* non-settable parameters */

		xold = new double[N];
		arx = new double[lambda][N];
	}

	private List<DoubleSolution> samplePopulation(int populationSize) {
		int N = problem.getNumberOfVariables();
		double[] artmp = new double[N];
		double sum;

		for (int iNk = 0; iNk < populationSize; iNk++) {
			for (int i = 0; i < N; i++) {
				artmp[i] = diagD[i] * random.nextGaussian();
			}
			for (int i = 0; i < N; i++) {
				sum = 0.0;
				for (int j = 0; j < N; j++) {
					sum += B[i][j] * artmp[j];
				}
				arx[iNk][i] = xmean[i] + sigma * sum;
			}
		}

		List<DoubleSolution> population = new ArrayList<>();
		for (int i = 0; i < populationSize; i++) {
			DoubleSolution solution = new DoubleSolution(N, problem.getOrder());

			for (int j = 0; j < N; j++) {
				solution.setVariableValue(j, arx[i][j]);
			}
			getMeritEvaluation(solution);

			population.add(solution);
		}

		return population;
	}

	private void updateDistribution(List<DoubleSolution> pop) {
		int N = problem.getNumberOfVariables();

		double[] arfitness = new double[lambda];
		int[] arindex = new int[lambda];

		/* Sort by fitness and compute weighted mean into xmean */

		// minimization
		for (int i = 0; i < lambda; i++) {
			arfitness[i] = pop.get(i).getObjective();
			arindex[i] = i;
		}
		Utils.minFastSort(arfitness, arindex, lambda);

		// calculate xmean and BDz~N(0,C)
		for (int i = 0; i < N; i++) {
			xold[i] = xmean[i];
			xmean[i] = 0.;
			for (int iNk = 0; iNk < mu; iNk++) {
				xmean[i] += weights[iNk] * arx[arindex[iNk]][i];
			}
			// BDz[i] = sqrt(sp->getMueff()) * (xmean[i] - xold[i]) / sigma;
		}

		/* Cumulation: Update evolution paths */

		double[] artmp = new double[N];
		for (int i = 0; i < N; i++) {
			artmp[i] = 0;
			// double value = (xmean[i] - xold[i]) / sigma;
			for (int j = 0; j < N; j++) {
				// artmp[i] += invsqrtC[i][j] * value;
				artmp[i] += invsqrtC[i][j] * (xmean[j] - xold[j]) / sigma;
			}
		}
		// cumulation for sigma (ps)
		for (int i = 0; i < N; i++) {
			ps[i] = (1. - cs) * ps[i] + Math.sqrt(cs * (2. - cs) * mueff) * artmp[i];
		}

		// calculate norm(ps)^2
		double psxps = 0.0;
		for (int i = 0; i < N; i++) {
			psxps += ps[i] * ps[i];
		}

		// cumulation for covariance matrix (pc)
		int hsig = 0;
		if ((Math.sqrt(psxps) / Math.sqrt(1. - Math.pow(1. - cs, 2. * nCalls / lambda)) / chiN) < (1.4
				+ 2. / (N + 1.))) {
			hsig = 1;
		}
		for (int i = 0; i < N; i++) {
			pc[i] = (1. - cc) * pc[i] + hsig * Math.sqrt(cc * (2. - cc) * mueff) * (xmean[i] - xold[i]) / sigma;
		}

		/* Adapt covariance matrix C */
		for (int i = 0; i < N; i++) {
			for (int j = 0; j <= i; j++) {
				C[i][j] = (1 - c1 - cmu) * C[i][j] + c1 * (pc[i] * pc[j] + (1 - hsig) * cc * (2. - cc) * C[i][j]);
				for (int k = 0; k < mu; k++) {
					C[i][j] += cmu * weights[k] * (arx[arindex[k]][i] - xold[i]) * (arx[arindex[k]][j] - xold[j])
							/ sigma / sigma;
				}
			}
		}

		/* Adapt step size sigma */
		sigma *= Math.exp((cs / damps) * (Math.sqrt(psxps) / chiN - 1));

		/* Decomposition of C into B*diag(D.^2)*B' (diagonalization) */
		if (nCalls - eigeneval > lambda / (c1 + cmu) / N / 10) {
			eigeneval = nCalls;

			// enforce symmetry
			for (int i = 0; i < N; i++) {
				for (int j = 0; j <= i; j++) {
					B[i][j] = B[j][i] = C[i][j];
				}
			}

			// eigen decomposition, B==normalized eigenvectors
			double[] offdiag = new double[N];
			Utils.tred2(N, B, diagD, offdiag);
			Utils.tql2(N, diagD, offdiag, B);

			for (int i = 0; i < N; i++) {
				if (diagD[i] < 0) { // numerical problem?
					System.err.println(
							"jmetal.metaheuristics.cmaes.CMAES.updateDistribution(): WARNING - an eigenvalue has become negative.");
				}
				diagD[i] = Math.sqrt(diagD[i]);
			}
			// diagD is a vector of standard deviations now

			// invsqrtC = B * diag(D.^-1) * B';
			double[][] artmp2 = new double[N][N];
			for (int i = 0; i < N; i++) {
				// double value = (xmean[i] - xold[i]) / sigma;
				for (int j = 0; j < N; j++) {
					artmp2[i][j] = B[i][j] * (1 / diagD[j]);
				}
			}
			for (int i = 0; i < N; i++) {
				// double value = (xmean[i] - xold[i]) / sigma;
				for (int j = 0; j < N; j++) {
					invsqrtC[i][j] = 0.0;
					for (int k = 0; k < N; k++) {
						invsqrtC[i][j] += artmp2[i][k] * B[j][k];
					}
				}
			}
		}
	}

	@Override
	public void updateProgress() {
		this.nCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return this.humans.isEmpty() || this.nCalls >= params._maxObjectiveFunctionCalls
				|| (nCalls > 0 && error == 0.0);
	}

	@Override
	public int getIterations() {
		return this.nCalls;
	}

	@Override
	public DoubleSolution search() {
		String groupsHumanMoved;
		int idx;
		double candidateDistance, zombieDistance;

		initProgress();
		if (this.viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {

			if (viewActive) {
				List<DoubleSolution> humans = new ArrayList<>();
				List<DoubleSolution> zombies = new ArrayList<>();

				for (Human human : this.humans) {
					humans.add((DoubleSolution) human.getSolution());
				}

				for (Zombie zombie : this.zombies) {
					zombies.add((DoubleSolution) zombie.getSolution());
				}

				redraw(bestMovement, humans, zombies);

				if (stepView) {
					stepView();
				}
			}

			// move humans
			groupsHumanMoved = moveGroupHuman();
			for (idx = 0; idx < params.humanStepsPerZombieStep; idx++) {
				this.moveHumans(groupsHumanMoved);
			}

			for (idx = 0; idx < this.zombies.size(); idx++) {
				Zombie zombie = this.zombies.get(idx);
				if (!zombie.isChaser()) {
					continue;
				}

				zombieDistance = Double.MAX_VALUE;
				for (Human human : this.humans) {
					candidateDistance = Utils.getEuclideanDistance(zombie.getSolution(), human.getSolution());

					if (candidateDistance < params.zombieDistanceAttack) {
						zombieDistance = 0.0;

						double defense = random.nextDouble() * human.getDefenderRate();
						double attack = random.nextDouble() * zombie.getHuntingRate();
						if ((defense < attack) && (humans.size() > params.minHumanPop)) {
							this.turnZombie(human);
						}

						break;
					} else if (candidateDistance < zombieDistance) {
						zombieDistance = candidateDistance;
					}
				}

				if (zombieDistance >= params.zombieDistanceAttack && zombieDistance < Double.MAX_VALUE) {
					List<Integer> tries = new ArrayList<>();
					for (idx = 0; idx < this.nDim; idx++) {
						tries.add(idx);
					}

					Collections.shuffle(tries);

					boolean foundBetter = false;
					for (idx = 0; (idx < this.nDim) && !foundBetter; idx++) {

						DoubleSolution zombieSolution = (DoubleSolution) zombie.getSolution().copy();
						for (int i = 0; (i < 5) && !foundBetter; i++) {
							zombieSolution.setVariableValue(tries.get(idx),
									zombieSolution.getVariableValue(tries.get(idx))
											+ random.nextGaussian() * params.humanMaxDistance);

							for (Human human : this.humans) {
								candidateDistance = Utils.getEuclideanDistance(zombieSolution, human.getSolution());

								if (candidateDistance < zombieDistance) {
									zombieDistance = candidateDistance;
									foundBetter = true;
								}
							}

							if (foundBetter) {
								zombie.setSolution(zombieSolution.getVariables().clone());
							}
						}
					}
				}
			}

			covariance = params.humanMaxMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMaxMutIdxFactor - params.humanMinMutIdxFactor));
			distribution = params.humanMinMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMinMutIdxFactor - params.humanMaxMutIdxFactor));
		}

		if (viewActive) {
			List<DoubleSolution> humans = new ArrayList<>();
			List<DoubleSolution> zombies = new ArrayList<>();

			for (Human human : this.humans) {
				humans.add((DoubleSolution) human.getSolution());
			}

			for (Zombie zombie : this.zombies) {
				zombies.add((DoubleSolution) zombie.getSolution());
			}

			redraw(bestMovement, humans, zombies);
			if (stepView) {
				stepView();
			}

			finishViewer();
		}

		String result = problem.getName() + " - Z: " + this.zombies.size() + ", H: " + this.humans.size()
				+ ", Num. Gauss: " + nGauss + ", Num. Poly: " + nPoly + ", Num. Groups: " + nGroups;
		DataReader.saveLog(getName(), problem.getNumberOfVariables(), result);
		System.out.println(result + "\n");

		return this.bestMovement;
	}

	private void moveHumans(String groupsHumanMoved) {
		double oldEvaluation, newEvaluation, oldDistance, newDistance, distance;
		boolean hasZombie;
		boolean hasMutation;
		double diff;

		// for (Human human : this.humans) {
		for (int i = 0; i < this.humans.size(); i++) {
			if (groupsHumanMoved.contains(",".concat(String.valueOf(i).concat(",")))) {
				continue;
			}

			Human human = humans.get(i);
			hasZombie = false;
			hasMutation = false;
			oldDistance = 0;
			newDistance = 0;
			DoubleSolution humanSolution = (DoubleSolution) human.getSolution().copy();
			oldEvaluation = humanSolution.getObjective();

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					hasZombie = true;
					oldDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (hasZombie) {
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						diff = random.nextGaussian() * covariance;
						humanSolution.setVariableValue(var, humanSolution.getVariableValue(var) + diff);
						hasMutation = true;
					}
				}
				if (hasMutation) {
					nGauss++;
				}
			} else {
				// polynomial mutation
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						humanSolution.setVariableValue(var,
								Utils.polynomialMutation(random.nextDouble(), humanSolution.getVariableValue(var),
										distribution, problem.getLowerBound(var), problem.getUpperBound(var)));
						hasMutation = true;
					}
				}
				if (hasMutation) {
					nPoly++;
				}
			}

			problem.simpleBounds(humanSolution);

			if (hasMutation) {
				newEvaluation = getMeritEvaluation(humanSolution);
			} else {
				newEvaluation = humanSolution.getObjective();
			}

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					newDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (problem.getOrder() == Ordering.ASCENDING) {
				oldEvaluation += (human.getFear() * oldDistance);
				newEvaluation += (human.getFear() * newDistance);

				if (oldEvaluation > newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			} else {
				oldEvaluation -= (human.getFear() * oldDistance);
				newEvaluation -= (human.getFear() * newDistance);

				if (oldEvaluation < newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			}
		}
	}

	private double getMeritEvaluation(DoubleSolution solution) {
		this.problem.simpleBounds(solution);
		this.problem.evaluate(solution);

		updateProgress();

		if (Utils.compare(solution, this.bestMovement, problem.getOrder()) == -1) {
			this.bestMovement = (DoubleSolution) solution.copy();

			if (problem.getName().contains("ProblemsCEC15")) {
				this.error = solution.getObjective() - problem.getNumFunction() * 100;

				if (this.error < this.zero) {
					this.error = 0.0;
				}
			}
		}

		setResult(new BigDecimal(this.bestMovement.getObjective()), this.nCalls,
				this.params._maxObjectiveFunctionCalls);

		return solution.getObjective();
	}

	private void turnZombie(Human human) {
		Zombie newzombie = new Zombie(problem.getNumberOfVariables(), random.nextDouble());
		newzombie.setSolution(human.getSolution().getVariables().clone());

		this.zombies.add(newzombie);
		this.humans.remove(human);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this.params;
	}

	@Override
	public String getSummary() {
		return " - " + nCalls;
	}

}
