package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import br.com.si.params.GaParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class GA extends Algorithm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4012761989982068157L;

	public static final String name = "GaAlgorithm";

	/*
	 * private int currentIteration;
	 * 
	 * private Random random;
	 * 
	 * private boolean _viewActive; private boolean _stepView; private GaParams
	 * _params;
	 * 
	 * private double bestCost;
	 * 
	 * public GA(Problem problem, GaParams params, boolean viewActive, boolean
	 * stepView) { super(problem);
	 * 
	 * this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
	 * this._stepView = stepView && problem.getNumberOfVariables() == 2;
	 * this._params = params; this.notExecuteInThread = (this._viewActive ||
	 * this._stepView);
	 * 
	 * this.random = new Random(); }
	 * 
	 * @Override public void initProgress() { super.initProgress();
	 * 
	 * currentIteration = 0; bestCost = Double.MAX_VALUE; }
	 * 
	 * @Override public void updateProgress() { currentIteration++; }
	 * 
	 * @Override public void setSeed(long seed) { random.setSeed(seed); }
	 * 
	 * @Override public boolean isStoppingConditionReached() { return
	 * currentIteration >= _params._maxObjectiveFunctionCalls; }
	 * 
	 * @Override public DoubleSolution search() { List<DoubleSolution>
	 * offspringPopulation; List<DoubleSolution> matingPopulation;
	 * List<DoubleSolution> population;
	 * 
	 * initProgress(); population = createInitialPopulation();
	 * evaluatePopulation(population);
	 * 
	 * if (this._viewActive) { initViewer(); }
	 * 
	 * while (!isStoppingConditionReached()) { if (_viewActive) {
	 * redraw(population.get(0), population);
	 * 
	 * if (_stepView) { stepView(); } }
	 * 
	 * matingPopulation = selection(population); offspringPopulation =
	 * reproduction(matingPopulation); evaluatePopulation(offspringPopulation);
	 * population = replacement(population, offspringPopulation); }
	 * 
	 * if (_viewActive) { redraw(population.get(0), population);
	 * 
	 * if (_stepView) { stepView(); }
	 * 
	 * finishViewer(); }
	 * 
	 * return population.get(0); }
	 * 
	 * private List<DoubleSolution> replacement(List<DoubleSolution>
	 * population2, List<DoubleSolution> offspringPopulation) {
	 * Collections.sort(population2);
	 * offspringPopulation.add(population2.get(0));
	 * offspringPopulation.add(population2.get(1));
	 * Collections.sort(offspringPopulation);
	 * offspringPopulation.remove(offspringPopulation.size() - 1);
	 * offspringPopulation.remove(offspringPopulation.size() - 1);
	 * 
	 * return offspringPopulation; }
	 * 
	 * private List<DoubleSolution> reproduction(List<DoubleSolution>
	 * matingPopulation) { List<DoubleSolution> offspringPopulation = new
	 * ArrayList<>(matingPopulation.size() + 2); for (int i = 0; i <
	 * _params._populationSize; i += 2) {
	 * 
	 * List<DoubleSolution> offspring =
	 * doCrossover(_params._crossoverProbability, matingPopulation.get(i),
	 * matingPopulation.get(i + 1));
	 * 
	 * doMutation(offspring.get(0)); doMutation(offspring.get(1));
	 * 
	 * offspringPopulation.add(offspring.get(0));
	 * offspringPopulation.add(offspring.get(1)); } return offspringPopulation;
	 * }
	 * 
	 * private void doMutation(DoubleSolution solution) { double y, yl, yu,
	 * deltaq;
	 * 
	 * for (int i = 0; i < solution.getNumberOfVariables(); i++) { if
	 * (random.nextDouble() <= _params._mutationProbability) { y =
	 * solution.getVariableValue(i); yl = problem.getLowerBound(i); yu =
	 * problem.getUpperBound(i);
	 * 
	 * deltaq = 0.0; for (int j = 0; j < 20; j++) { if (random.nextDouble() <=
	 * 0.05) { deltaq += Math.pow(2, -j); } }
	 * 
	 * if (random.nextDouble() < 0.5) { y += (yu - yl) * deltaq; } else { y -=
	 * (yu - yl) * deltaq; }
	 * 
	 * solution.setVariableValue(i, y); } } problem.simpleBounds(solution);
	 * 
	 * }
	 * 
	 * private List<DoubleSolution> doCrossover(double probability,
	 * DoubleSolution parent1, DoubleSolution parent2) { List<DoubleSolution>
	 * offspring = new ArrayList<>(2);
	 * 
	 * offspring.add(new DoubleSolution(problem.getNumberOfVariables()));
	 * offspring.add(new DoubleSolution(problem.getNumberOfVariables()));
	 * 
	 * int index; double alpha, valueX1, valueX2;
	 * 
	 * for (index = 0; index < problem.getNumberOfVariables(); index++) {
	 * valueX1 = parent1.getVariableValue(index); valueX2 =
	 * parent2.getVariableValue(index);
	 * 
	 * if (random.nextDouble() <= probability) { alpha = random.nextDouble() *
	 * 1.5; alpha -= 0.25;
	 * 
	 * valueX1 += alpha * (valueX2 - valueX1); valueX2 += alpha * (valueX1 -
	 * valueX2); }
	 * 
	 * offspring.get(0).setVariableValue(index, valueX1);
	 * offspring.get(1).setVariableValue(index, valueX2); }
	 * offspring.get(0).setOrder(problem.getOrder());
	 * offspring.get(1).setOrder(problem.getOrder());
	 * 
	 * return offspring; }
	 * 
	 * private List<DoubleSolution> selection(List<DoubleSolution> population) {
	 * List<DoubleSolution> matingPopulation = new
	 * ArrayList<>(population.size());
	 * 
	 * for (int i = 0; i < _params._populationSize; i++) { DoubleSolution
	 * solution = tournamentSelection(population);
	 * matingPopulation.add((DoubleSolution) solution.copy()); }
	 * 
	 * return matingPopulation; }
	 * 
	 * private DoubleSolution tournamentSelection(List<DoubleSolution>
	 * population) { DoubleSolution result;
	 * 
	 * if (population.size() == 1) result = population.get(0); else { result =
	 * population.get(random.nextInt(population.size())); int count = 1;
	 * 
	 * do { DoubleSolution candidate =
	 * population.get(random.nextInt(population.size())); result =
	 * (DoubleSolution) Utils.getBestSolution(result, candidate,
	 * problem.getOrder(), random); } while (++count < _params._numTournaments);
	 * }
	 * 
	 * return result; }
	 * 
	 * private void evaluatePopulation(List<DoubleSolution> population) { for
	 * (DoubleSolution solution : population) { problem.evaluate(solution);
	 * updateProgress();
	 * 
	 * if (solution.getObjective() < this.bestCost) { this.bestCost =
	 * solution.getObjective(); }
	 * 
	 * setResult(this.bestCost, this.currentIteration,
	 * this._params._maxObjectiveFunctionCalls); } }
	 * 
	 * private List<DoubleSolution> createInitialPopulation() {
	 * List<DoubleSolution> population = new
	 * ArrayList<>(_params._populationSize); for (int i = 0; i <
	 * _params._populationSize; i++) { DoubleSolution newIndividual =
	 * problem.createSolution();
	 * 
	 * population.add(newIndividual); } return population; }
	 */

	private double mutationProbability;
	private int currentIteration;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private Random random;

	private boolean _viewActive;
	private boolean _stepView;
	private GaParams _params;

	private DoubleSolution bestSolution;
	private double time;

	public GA(Problem problem, GaParams params, boolean viewActive, boolean stepView) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);

		// this.maxIterations = problem.getNumberOfVariables() *
		// Parameters.MAX_FES;
		// this.minError = Parameters.MIN_ERROR;
		// this.populationSize = Parameters.GA_POP_SIZE;
		// this.numberOfTournaments = Parameters.GA_TOURNAMENTS;
		// this.distributionIndex = Parameters.GA_DISTRIBUTION_INDEX_CROSSOVER;
		// this.crossoverProbability = Parameters.GA_CROSSOVER_PROBABILITY;

		this.mutationProbability = 1.0 / (double) problem.getNumberOfVariables();

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		currentIteration = 0;
		time = 0;
		bestSolution = null;
	}

	@Override
	public void updateProgress() {
		currentIteration++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return currentIteration >= _params._maxObjectiveFunctionCalls || (currentIteration > 0 && error == 0.0);
	}

	@Override
	public DoubleSolution search() {
		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		List<DoubleSolution> offspringPopulation;
		List<DoubleSolution> matingPopulation;
		List<DoubleSolution> population;

		initProgress();
		population = createInitialPopulation();
		evaluatePopulation(population);

		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {
			if (_viewActive) {
				redraw(bestSolution, population);

				if (_stepView) {
					stepView();
				}
			}

			matingPopulation = selection(population);
			offspringPopulation = reproduction(matingPopulation);
			evaluatePopulation(offspringPopulation);
			population = replacement(population, offspringPopulation);
		}

		if (_viewActive) {
			redraw(bestSolution, population);

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}

		return population.get(0);
	}

	private List<DoubleSolution> replacement(List<DoubleSolution> population2,
			List<DoubleSolution> offspringPopulation) {
		Collections.sort(population2);
		offspringPopulation.add(population2.get(0));
		offspringPopulation.add(population2.get(1));
		Collections.sort(offspringPopulation);
		offspringPopulation.remove(offspringPopulation.size() - 1);
		offspringPopulation.remove(offspringPopulation.size() - 1);

		return offspringPopulation;
	}

	private List<DoubleSolution> reproduction(List<DoubleSolution> matingPopulation) {
		List<DoubleSolution> offspringPopulation = new ArrayList<>(matingPopulation.size() + 2);
		for (int i = 0; i < _params._populationSize; i += 2) {

			List<DoubleSolution> offspring = doCrossover(_params._crossoverProbability, matingPopulation.get(i),
					matingPopulation.get(i + 1));

			doMutation(offspring.get(0));
			doMutation(offspring.get(1));

			offspringPopulation.add(offspring.get(0));
			offspringPopulation.add(offspring.get(1));
		}
		return offspringPopulation;
	}

	private void doMutation(DoubleSolution solution) {
		Double rnd, delta1, delta2, mutPow, deltaq;
		double y, yl, yu, val, xy;

		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			if (random.nextDouble() <= mutationProbability) {
				y = solution.getVariableValue(i).doubleValue();
				yl = problem.getLowerBound(i);
				yu = problem.getUpperBound(i);

				if (yl == yu) {
					y = yl;
				} else {
					delta1 = (y - yl) / (yu - yl);
					delta2 = (yu - y) / (yu - yl);
					rnd = random.nextDouble();
					mutPow = 1.0 / (_params._distributionMutation + 1.0);
					if (rnd <= 0.5) {
						xy = 1.0 - delta1;
						val = 2.0 * rnd + (1.0 - 2.0 * rnd) * (Math.pow(xy, _params._distributionMutation + 1.0));
						deltaq = Math.pow(val, mutPow) - 1.0;
					} else {
						xy = 1.0 - delta2;
						val = 2.0 * (1.0 - rnd)
								+ 2.0 * (rnd - 0.5) * (Math.pow(xy, _params._distributionMutation + 1.0));
						deltaq = 1.0 - Math.pow(val, mutPow);
					}
					y = y + deltaq * (yu - yl);
				}

				// y = Math.round(y * 100.0);
				// y /= 100.0;

				solution.setVariableValue(i, y);
			}
		}
		problem.simpleBounds(solution);
	}

	private List<DoubleSolution> doCrossover(double probability, DoubleSolution parent1, DoubleSolution parent2) {
		List<DoubleSolution> offspring = new ArrayList<>(2);

		offspring.add((DoubleSolution) parent1.copy());
		offspring.add((DoubleSolution) parent2.copy());

		int i;
		double rand;
		double y1, y2, yL, yu;
		double c1, c2;
		double alpha, beta, betaq;
		double valueX1, valueX2;

		if (random.nextDouble() <= probability) {
			for (i = 0; i < parent1.getNumberOfVariables(); i++) {
				valueX1 = parent1.getVariableValue(i).doubleValue();
				valueX2 = parent2.getVariableValue(i).doubleValue();

				if (random.nextDouble() <= 0.5) {
					if (Math.abs(valueX1 - valueX2) > _params._eps) {
						if (valueX1 < valueX2) {
							y1 = valueX1;
							y2 = valueX2;
						} else {
							y1 = valueX2;
							y2 = valueX1;
						}

						yL = problem.getLowerBound(i);
						yu = problem.getUpperBound(i);

						rand = random.nextDouble();
						beta = 1.0 + (2.0 * (y1 - yL) / (y2 - y1));
						alpha = 2.0 - Math.pow(beta, -(_params._distributionCrossover + 1.0));

						if (rand <= (1.0 / alpha)) {
							betaq = Math.pow((rand * alpha), (1.0 / (_params._distributionCrossover + 1.0)));
						} else {
							betaq = Math.pow(1.0 / (2.0 - rand * alpha), 1.0 / (_params._distributionCrossover + 1.0));
						}

						c1 = 0.5 * ((y1 + y2) - betaq * (y2 - y1));
						beta = 1.0 + (2.0 * (yu - y2) / (y2 - y1));
						alpha = 2.0 - Math.pow(beta, -(_params._distributionCrossover + 1.0));

						if (rand <= (1.0 / alpha)) {
							betaq = Math.pow((rand * alpha), (1.0 / (_params._distributionCrossover + 1.0)));
						} else {
							betaq = Math.pow(1.0 / (2.0 - rand * alpha), 1.0 / (_params._distributionCrossover + 1.0));
						}

						c2 = 0.5 * (y1 + y2 + betaq * (y2 - y1));

						// c1 = Math.round(c1 * 100.0);
						// c1 /= 100.0;
						// c2 = Math.round(c2 * 100.0);
						// c2 /= 100.0;

						if (c1 < yL) {
							c1 = yL;
						}

						if (c2 < yL) {
							c2 = yL;
						}

						if (c1 > yu) {
							c1 = yu;
						}

						if (c2 > yu) {
							c2 = yu;
						}

						if (random.nextDouble() <= 0.5) {
							offspring.get(0).setVariableValue(i, c2);
							offspring.get(1).setVariableValue(i, c1);
						} else {
							offspring.get(0).setVariableValue(i, c1);
							offspring.get(1).setVariableValue(i, c2);
						}
					} else {
						offspring.get(0).setVariableValue(i, valueX1);
						offspring.get(1).setVariableValue(i, valueX2);
					}
				} else {
					offspring.get(0).setVariableValue(i, valueX2);
					offspring.get(1).setVariableValue(i, valueX1);
				}
			}
		}

		return offspring;
	}

	private List<DoubleSolution> selection(List<DoubleSolution> population) {
		List<DoubleSolution> matingPopulation = new ArrayList<>(population.size());

		for (int i = 0; i < _params._populationSize; i++) {
			DoubleSolution solution = tournamentSelection(population);
			matingPopulation.add((DoubleSolution) solution.copy());
		}

		return matingPopulation;
	}

	private DoubleSolution tournamentSelection(List<DoubleSolution> population) {
		DoubleSolution result;

		if (population.size() == 1)
			result = population.get(0);
		else {
			result = population.get(random.nextInt(population.size()));
			int count = 1;

			do {
				DoubleSolution candidate = population.get(random.nextInt(population.size()));
				result = (DoubleSolution) Utils.getBestSolution(result, candidate, problem.getOrder(), random);
			} while (++count < _params._numTournaments);
		}

		return result;
	}

	private void evaluatePopulation(List<DoubleSolution> population) {
		Date inicio, fim;
		for (DoubleSolution solution : population) {
			inicio = new Date(); 
			problem.evaluate(solution);
			fim = new Date();
			updateProgress();
			if (currentIteration < _params._maxObjectiveFunctionCalls+1) {
				time += fim.getTime() - inicio.getTime();
			}

			if (Utils.compare(solution, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}

			setResult(new BigDecimal(bestSolution.getObjective()), this.currentIteration,
					this._params._maxObjectiveFunctionCalls);
		}
	}

	private List<DoubleSolution> createInitialPopulation() {
		List<DoubleSolution> population = new ArrayList<>(_params._populationSize);
		for (int i = 0; i < _params._populationSize; i++) {
			DoubleSolution newIndividual = DoubleSolution.createSolution(problem, random);

			population.add(newIndividual);
		}
		return population;
	}

	@Override
	public int getIterations() {
		return currentIteration;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	@Override
	public String getSummary() {
		return " - Iter.: " + currentIteration + " - Best: " + bestSolution.getObjective() + " - Parameters: "
				+ getStringVector(bestSolution.getVariables()) + " - Iter.: " + currentIteration;
	}

	private String getStringVector(Double[] vector) {
		String result = "[" + vector[0];
		for (int i = 1; i < vector.length; i++) {
			result += "," + vector[i];
		}
		result += "]";

		return result;
	} // getStringVector
	
	public double getTime() {
		return time;
	}
}