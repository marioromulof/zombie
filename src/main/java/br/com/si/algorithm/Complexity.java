package br.com.si.algorithm;

import org.apache.commons.math3.util.FastMath;

import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;

public class Complexity extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5040503265125976822L;

	public Complexity(Problem inputProblem) {
		super(inputProblem);
	}

	@Override
	public void updateProgress() {
	}

	@Override
	public void setSeed(long seed) {
	}

	@Override
	public boolean isStoppingConditionReached() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getIterations() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		return "Complexity";
	}

	@Override
	public ProblemParams getParameter() {
		return null;
	}

	@Override
	public DoubleSolution search() {
		double x;
		for (int i = 1; i < 1000001; i++) {
			x = 0.55 + (double) i;
			x = x + x;
			x = x / 2;
			x = x * x;
			x = FastMath.sqrt(x);
			x = FastMath.log(x);
			x = FastMath.exp(x);
			x = x / (x + 2);
		}
		return null;
	}

	@Override
	public String getSummary() {
		return null;
	}

}
