package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import br.com.si.params.CmaEsParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings({ "unused", "unchecked" })
public class CMAES extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -321825678600820523L;

	public static final String name = "CMAESAlgorithm";

	private static final Logger logger = LogManager.getLogger(CMAES.class);

	private int counteval;
	private double error;
	private double zero = Math.pow(10.0, -8);
	private int nDim;
	private int mu;
	private double mueff;
	private double cc;
	private double cs;
	private double c1;
	private double cmu;
	private double damps;
	private int eigeneval;
	private double chiN;
	private double sigma;
	private DoubleSolution bestSolutionEver;

	private double[] arFuncValueHist;
	private double[] weights;
	private double[] xmean;
	private double[] xold;
	private double[] pc;
	private double[] ps;
	private double[][] b;
	private double[] diagD;
	private double[][] c;
	private double[][] invsqrtC;
	private double[][] arx;

	private List<DoubleSolution> population;

	private Random random;

	private boolean _viewActive;
	private boolean _stepView;

	private CmaEsParams _params;

	public CMAES(Problem problem, CmaEsParams params, boolean viewActive, boolean stepView) {

		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);

		this.nDim = problem.getNumberOfVariables();

		this.random = new Random();
		super.initProgress();
	}

	@Override
	public void initProgress() {
		// super.initProgress();
		if (this._params._populationSize > 0) {
			this._params._populationSize = (int) (this._params._populationSize * this._params._iPOPFactor);
		} else {
			this._params._populationSize = 4 + (int) (_params._popSizeFactor * Math.log((double) this.nDim));
		}

		this.bestSolutionEver = null;
		counteval = 0;
		// sigma = _params._sigma;
		double trace = 0.0;
		for (int i = 0; i < nDim; ++i) {
			trace += _params._initStepSizeFactor * _params._initStepSizeFactor;
		}
		sigma = Math.sqrt(trace / nDim); /*
											 * t->sp.mueff/(0.2*t->sp.mueff+sqrt
											 * (N) ) * sqrt(trace/N);
											 */

		xmean = new double[nDim];
		for (int i = 0; i < nDim; i++) {
			xmean[i] = random.nextDouble();
		}

		// int lambda = (int) (4.0 + 3.0 * Math.log(nDim));
		int lambda = (int) _params._populationSize;
		mu = (int) Math.floor(lambda / 2);

		weights = new double[mu];
		double sum = 0;
		for (int i = 0; i < mu; i++) {
			weights[i] = (Math.log(mu + 1.0) - Math.log(i + 1));
			sum += weights[i];
		}

		for (int i = 0; i < mu; i++) {
			weights[i] /= sum;
		}

		double sum1 = 0;
		double sum2 = 0;
		for (int i = 0; i < mu; i++) {
			sum1 += weights[i];
			sum2 += weights[i] * weights[i];
		}
		mueff = sum1 * sum1 / sum2;

		cc = (4 + mueff / nDim) / (nDim + 4 + 2 * mueff / nDim);
		cs = (mueff + 2) / (nDim + mueff + 5);
		c1 = 2 / ((nDim + 1.3) * (nDim + 1.3) + mueff);
		cmu = Math.min(1 - c1, 2 * (mueff - 2 + 1 / mueff) / ((nDim + 2) * (nDim + 2) + mueff));
		damps = 1 + 2 * Math.max(0, Math.sqrt((mueff - 1) / (nDim + 1)) - 1) + cs;

		diagD = new double[nDim];
		pc = new double[nDim];
		ps = new double[nDim];
		b = new double[nDim][nDim];
		c = new double[nDim][nDim];
		invsqrtC = new double[nDim][nDim];

		for (int i = 0; i < nDim; i++) {
			pc[i] = 0;
			ps[i] = 0;
			diagD[i] = 1;
			for (int j = 0; j < nDim; j++) {
				b[i][j] = 0;
				invsqrtC[i][j] = 0;
			}
			for (int j = 0; j < i; j++) {
				c[i][j] = 0;
			}
			b[i][i] = 1;
			c[i][i] = diagD[i] * diagD[i];
			invsqrtC[i][i] = 1;
		}

		eigeneval = 0;
		chiN = Math.sqrt(nDim) * (1.0 - 1.0 / (4.0 * nDim) + 1.0 / (21.0 * nDim * nDim));
		xold = new double[nDim];
		arx = new double[lambda][nDim];

		if (arFuncValueHist == null) {
			arFuncValueHist = new double[10 + (int) Math.ceil(3. * 10. * nDim / lambda) + 1];
		} else {
			arFuncValueHist = new double[(int) (arFuncValueHist.length * _params._iPOPFactor)];
		}
	}

	@Override
	public void updateProgress() {
		counteval++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return counteval >= _params._maxObjectiveFunctionCalls || (counteval > 0 && error == 0.0);
	}

	public boolean isStoppingConditionRestart() {
		double range;
		boolean result;

		if (population != null && population.size() > 0) {
			/* TOLFUN */
			double[] history = arFuncValueHist.clone();
			Arrays.sort(history);
			System.out.println("F: " + history[0] + " - L: " + history[history.length - 1]);
			range = Math.max(history[history.length - 1],
					population.get(population.size() - 1).getObjective().doubleValue())
					- Math.min(history[0], population.get(0).getObjective().doubleValue());
			result = range <= _params._stopTolFun.doubleValue();
			// if ((countiter > 1 || state >= 3) &&
			// Math.max(math.max(fit.history),
			// fit.fitness[fit.fitness.length - 1].val)
			// - Math.min(math.min(fit.history), fit.fitness[0].val) <=
			// options.stopTolFun)
			// appendMessage("TolFun: function value changes below stopTolFun="
			// +
			// options.stopTolFun);

			System.out.println("TOLFUN: " + result);
			/* TOLFUNHIST */
			range = history[history.length - 1] - history[0];
			// result = result || range <= _params._stopTolFunHist;
			// if (options.stopTolFunHist >= 0 && countiter >
			// fit.history.length) {
			// if (math.max(fit.history) - math.min(fit.history) <=
			// options.stopTolFunHist)
			// appendMessage(
			// "TolFunHist: history of function value changes below
			// stopTolFunHist="
			// + options.stopTolFunHist);
			// }
			System.out.println("TOLFUNHIST: " + (range <= _params._stopTolFunHist.doubleValue()));

		} else {
			result = false;
		}
		/* TOLX */
		int cTemp = 0;
		for (int i = 0; i < nDim; ++i) {
			cTemp += (sigma * Math.sqrt(c[i][i]) < _params._stopTolX.doubleValue()) ? 1 : 0;
			cTemp += (sigma * pc[i] < _params._stopTolX.doubleValue()) ? 1 : 0;
		}
		result = result || cTemp != 2 * nDim;

		System.out.println("TOLX: " + (cTemp != 2 * nDim));

		return result;
	}

	@Override
	public DoubleSolution search() {
		initProgress();
		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {

			population = samplePopulation();

			for (DoubleSolution solution : population) {
				problem.evaluate(solution);
				updateProgress();

				if (Utils.compare(solution, bestSolutionEver, problem.getOrder()) == -1) {
					bestSolutionEver = (DoubleSolution) solution.copy();

					if (problem.getName().contains("ProblemsCEC15")) {
						this.error = solution.getObjective() - problem.getNumFunction() * 100;

						if (this.error < this.zero) {
							this.error = 0.0;
						}
					}
				}

				setResult(new BigDecimal(bestSolutionEver.getObjective()), this.counteval,
						this._params._maxObjectiveFunctionCalls);
			}

			if (_viewActive) {
				redraw(bestSolutionEver, population);
				// System.out.println("Num calls: " + counteval);

				if (_stepView) {
					stepView();
				}
			}

			updateDistribution();

		}

		if (_viewActive) {
			redraw(bestSolutionEver, population);

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}
		return bestSolutionEver;
	}

	public void updateDistribution() {
		int lambda = (int) _params._populationSize;

		double[] arfitness = new double[lambda];
		double[] BDz = new double[nDim];
		int[] arindex = new int[lambda];

		for (int i = 0; i < lambda; i++) {
			arFuncValueHist[i] = arfitness[i] = population.get(i).getObjective().doubleValue();
			arindex[i] = i;
		}
		minFastSort(arfitness, arindex, lambda);

		/* Test if function values are identical, escape flat fitness */
		if (arFuncValueHist[arindex[0]] == arFuncValueHist[arindex[(int) lambda / 2]]) {
			System.out.println("Entrei -> Putz!");
			sigma *= Math.exp(0.2 + cs / damps);
			// ERRORMESSAGE("Warning: sigma increased due to equal function
			// values\n",
			// " Reconsider the formulation of the objective function",0,0);
		}

		/* update fitness history */
		for (int i = arFuncValueHist.length - 1; i > 0; i--) {
			arFuncValueHist[i] = arFuncValueHist[i - 1];
		}
		arFuncValueHist[0] = arfitness[arindex[0]];

		// calculate xmean and BDz~N(0,C)
		for (int i = 0; i < nDim; i++) {
			xold[i] = xmean[i];
			xmean[i] = 0.;
			for (int iNk = 0; iNk < mu; iNk++) {
				xmean[i] += weights[iNk] * arx[arindex[iNk]][i];
			}
			BDz[i] = Math.sqrt(mueff) * (xmean[i] - xold[i]) / sigma;
		}

		double[] artmp = new double[nDim];
		for (int i = 0; i < nDim; i++) {
			artmp[i] = 0;
			for (int j = 0; j < nDim; j++) {
				artmp[i] += invsqrtC[i][j] * (xmean[j] - xold[j]) / sigma;
			}
		}

		for (int i = 0; i < nDim; i++) {
			ps[i] = (1. - cs) * ps[i] + Math.sqrt(cs * (2. - cs) * mueff) * artmp[i];
		}

		double psxps = 0.0;
		for (int i = 0; i < nDim; i++) {
			psxps += ps[i] * ps[i];
		}

		int hsig = 0;
		if ((Math.sqrt(psxps) / Math.sqrt(1. - Math.pow(1. - cs, 2. * counteval / lambda)) / chiN) < (1.4
				+ 2. / (nDim + 1.))) {
			hsig = 1;
		}
		for (int i = 0; i < nDim; i++) {
			pc[i] = (1. - cc) * pc[i] + hsig * Math.sqrt(cc * (2. - cc) * mueff) * (xmean[i] - xold[i]) / sigma;
		}

		for (int i = 0; i < nDim; i++) {
			for (int j = 0; j <= i; j++) {
				c[i][j] = (1 - c1 - cmu) * c[i][j] + c1 * (pc[i] * pc[j] + (1 - hsig) * cc * (2. - cc) * c[i][j]);
				for (int k = 0; k < mu; k++) {
					c[i][j] += cmu * weights[k] * (arx[arindex[k]][i] - xold[i]) * (arx[arindex[k]][j] - xold[j])
							/ sigma / sigma;
				}
			}
		}

		sigma *= Math.exp((cs / damps) * (Math.sqrt(psxps) / chiN - 1));

		if ((counteval - eigeneval) > (lambda / (c1 + cmu) / nDim / 10)) {
			eigeneval = counteval;

			for (int i = 0; i < nDim; i++) {
				for (int j = 0; j <= i; j++) {
					b[i][j] = b[j][i] = c[i][j];
				}
			}

			double[] offdiag = new double[nDim];
			tred2(nDim, b, diagD, offdiag);
			tql2(nDim, diagD, offdiag, b);

			if (checkEigenSystem(nDim, c, diagD, b) > 0) {
				counteval = _params._maxObjectiveFunctionCalls;
			}

			for (int i = 0; i < nDim; i++) {
				if (diagD[i] < 0) {
					// logger.info("updateDistribution(): WARNING - an
					// eigenvalue has become negative.");
					counteval = _params._maxObjectiveFunctionCalls;
				}
				diagD[i] = Math.sqrt(diagD[i]);
			}

			double[][] artmp2 = new double[nDim][nDim];
			for (int i = 0; i < nDim; i++) {
				for (int j = 0; j < nDim; j++) {
					artmp2[i][j] = b[i][j] * (1 / diagD[j]);
				}
			}
			for (int i = 0; i < nDim; i++) {
				for (int j = 0; j < nDim; j++) {
					invsqrtC[i][j] = 0.0;
					for (int k = 0; k < nDim; k++) {
						invsqrtC[i][j] += artmp2[i][k] * b[j][k];
					}
				}
			}
		}
	}

	private int checkEigenSystem(int n, double[][] c, double[] diag, double[][] q) {
		int i, j, k, res = 0;
		double cc, dd;
		String s;

		for (i = 0; i < n; ++i) {
			for (j = 0; j < n; ++j) {
				for (cc = 0., dd = 0., k = 0; k < n; ++k) {
					cc += diag[k] * q[i][k] * q[j][k];
					dd += q[i][k] * q[j][k];
				}

				if (Math.abs(cc - c[i > j ? i : j][i > j ? j : i]) / Math.sqrt(c[i][i] * c[j][j]) > 1e-10
						&& Math.abs(cc - c[i > j ? i : j][i > j ? j : i]) > 1e-9) {
					s = " " + i + " " + j + " " + cc + " " + c[i > j ? i : j][i > j ? j : i] + " "
							+ (cc - c[i > j ? i : j][i > j ? j : i]);
					// logger.info("checkEigenSystem(): WARNING - imprecise
					// result detected " + s);
					++res;
				}
				if (Math.abs(dd - (i == j ? 1 : 0)) > 1e-10) {
					s = i + " " + j + " " + dd;
					// logger.info("checkEigenSystem(): WARNING - imprecise
					// result detected (Q not orthog.) " + s);
					++res;
				}
			}
		}
		return res;
	}

	private void tql2(int n, double[] d, double[] e, double[][] v) {
		System.arraycopy(e, 1, e, 0, n - 1);
		e[n - 1] = 0.0;

		double f = 0.0;
		double tst1 = 0.0;
		double eps = Math.pow(2.0, -52.0);
		for (int l = 0; l < n; l++) {

			// Find small subdiagonal element
			tst1 = Math.max(tst1, Math.abs(d[l]) + Math.abs(e[l]));
			int m = l;
			while (m < n) {
				if (Math.abs(e[m]) <= eps * tst1) {
					break;
				}
				m++;
			}

			// If m == l, d[l] is an eigenvalue,
			// otherwise, iterate.
			if (m > l && m < nDim) {
				int iter = 0;
				do {
					iter = iter + 1; // (Could check iteration count here.)
					// Compute implicit shift
					f += specificShift(l, n, d, e);
					// Implicit QL transformation.
					implicitQLTransformation(l, m, n, v, d, e);
					// Check for convergence.
				} while (Math.abs(e[l]) > eps * tst1);
			}
			d[l] = d[l] + f;
			e[l] = 0.0;
		}

		// Sort eigenvalues and corresponding vectors.
		sortEigenValues(n, d, v);
	}

	private void sortEigenValues(int n, double[] d, double[][] v) {
		for (int i = 0; i < n - 1; i++) {
			int k = i;
			double p = d[i];
			for (int j = i + 1; j < n; j++) {
				if (d[j] < p) { // NH find smallest k>i
					k = j;
					p = d[j];
				}
			}
			if (k != i) {
				d[k] = d[i]; // swap k and i
				d[i] = p;
				for (int j = 0; j < n; j++) {
					p = v[j][i];
					v[j][i] = v[j][k];
					v[j][k] = p;
				}
			}
		}
	}

	private void implicitQLTransformation(int l, int m, int n, double[][] v, double[] d, double[] e) {
		double dl1 = d[l + 1];
		double p = d[m];
		double c = 1.0;
		double c2 = c;
		double c3 = c;
		double el1 = e[l + 1];
		double s = 0.0;
		double s2 = 0.0;
		for (int i = m - 1; i >= l; i--) {
			c3 = c2;
			c2 = c;
			s2 = s;
			double g = c * e[i];
			double h = c * p;
			double r = hypot(p, e[i]);
			e[i + 1] = s * r;
			s = e[i] / r;
			c = p / r;
			p = c * d[i] - s * g;
			d[i + 1] = h + s * (c * g + s * d[i]);

			// Accumulate transformation.
			for (int k = 0; k < n; k++) {
				h = v[k][i + 1];
				v[k][i + 1] = s * v[k][i] + c * h;
				v[k][i] = c * v[k][i] - s * h;
			}
		}
		p = -s * s2 * c3 * el1 * e[l] / dl1;
		e[l] = s * p;
		d[l] = c * p;
	}

	private double specificShift(int idx, int n, double[] d, double[] e) {
		double g = d[idx];
		double p = (d[idx + 1] - g) / (2.0 * e[idx]);
		double r = hypot(p, 1.0);
		if (p < 0) {
			r = -r;
		}
		d[idx] = e[idx] / (p + r);
		d[idx + 1] = e[idx] * (p + r);

		double h = g - d[idx];
		for (int i = idx + 2; i < n; i++) {
			d[i] -= h;
		}
		return h;
	}

	private double hypot(double a, double b) {
		double r = 0.0;
		if (Math.abs(a) > Math.abs(b)) {
			r = b / a;
			r = Math.abs(a) * Math.sqrt(1 + r * r);
		} else if (b != 0.0) {
			r = a / b;
			r = Math.abs(b) * Math.sqrt(1 + r * r);
		}
		return r;
	}

	private void tred2(int n, double[][] v, double[] d, double[] e) {
		System.arraycopy(v[n - 1], 0, d, 0, n);

		// Householder reduction to tridiagonal form.
		for (int i = n - 1; i > 0; i--) {

			// Scale to avoid under/overflow.
			double scale = 0.0;
			double h = 0.0;
			for (int k = 0; k < i; k++) {
				scale = scale + Math.abs(d[k]);
			}
			if (scale == 0.0) {
				e[i] = d[i - 1];
				for (int j = 0; j < i; j++) {
					d[j] = v[i - 1][j];
					v[i][j] = 0.0;
					v[j][i] = 0.0;
				}
			} else {
				h = householderIteration(i, scale, v, d, e);
			}
			d[i] = h;
		}

		// Accumulate transformations.
		accumulateTransformations(n, v, d);

		e[0] = 0.0;
	}

	private void accumulateTransformations(int n, double[][] v, double[] d) {
		for (int i = 0; i < n - 1; i++) {
			v[n - 1][i] = v[i][i];
			v[i][i] = 1.0;
			double h = d[i + 1];
			if (h != 0.0) {
				for (int k = 0; k <= i; k++) {
					d[k] = v[k][i + 1] / h;
				}
				for (int j = 0; j <= i; j++) {
					double g = 0.0;
					for (int k = 0; k <= i; k++) {
						g += v[k][i + 1] * v[k][j];
					}
					for (int k = 0; k <= i; k++) {
						v[k][j] -= g * d[k];
					}
				}
			}
			for (int k = 0; k <= i; k++) {
				v[k][i + 1] = 0.0;
			}
		}
		for (int j = 0; j < n; j++) {
			d[j] = v[n - 1][j];
			v[n - 1][j] = 0.0;
		}
		v[n - 1][n - 1] = 1.0;
	}

	private double householderIteration(int index, double scale, double[][] v, double[] d, double[] e) {
		double h = 0.0;

		// Generate Householder vector.
		for (int k = 0; k < index; k++) {
			d[k] /= scale;
			h += d[k] * d[k];
		}
		double f = d[index - 1];
		double g = Math.sqrt(h);
		if (f > 0) {
			g = -g;
		}
		e[index] = scale * g;
		h = h - f * g;
		d[index - 1] = f - g;
		for (int j = 0; j < index; j++) {
			e[j] = 0.0;
		}

		// Apply similarity transformation to remaining columns.
		for (int j = 0; j < index; j++) {
			f = d[j];
			v[j][index] = f;
			g = e[j] + v[j][j] * f;
			for (int k = j + 1; k <= index - 1; k++) {
				g += v[k][j] * d[k];
				e[k] += v[k][j] * f;
			}
			e[j] = g;
		}
		f = 0.0;
		for (int j = 0; j < index; j++) {
			e[j] /= h;
			f += e[j] * d[j];
		}
		double hh = f / (h + h);
		for (int j = 0; j < index; j++) {
			e[j] -= hh * d[j];
		}
		for (int j = 0; j < index; j++) {
			f = d[j];
			g = e[j];
			for (int k = j; k <= index - 1; k++) {
				v[k][j] -= (f * e[k] + g * d[k]);
			}
			d[j] = v[index - 1][j];
			v[index][j] = 0.0;
		}

		return h;
	}

	private void minFastSort(double[] x, int[] idx, int size) {
		for (int i = 0; i < size; i++) {
			for (int j = i + 1; j < size; j++) {
				if (x[i] > x[j]) {
					double temp = x[i];
					int tempIdx = idx[i];
					x[i] = x[j];
					x[j] = temp;
					idx[i] = idx[j];
					idx[j] = tempIdx;
				} else if (x[i] == x[j]) {
					if (idx[i] > idx[j]) {
						double temp = x[i];
						int tempIdx = idx[i];
						x[i] = x[j];
						x[j] = temp;
						idx[i] = idx[j];
						idx[j] = tempIdx;
					}
				}
			}
		}
	}

	public List<DoubleSolution> samplePopulation() {
		double[] artmp = new double[nDim];
		double sum;

		for (int iNk = 0; iNk < _params._populationSize; iNk++) {

			for (int i = 0; i < nDim; i++) {
				artmp[i] = diagD[i] * random.nextGaussian();
			}
			for (int i = 0; i < nDim; i++) {
				sum = 0.0;
				for (int j = 0; j < nDim; j++) {
					sum += b[i][j] * artmp[j];
				}
				arx[iNk][i] = xmean[i] + sigma * sum;
			}
		}

		return genoPhenoTransformation(arx);
	}

	private List<DoubleSolution> genoPhenoTransformation(double[][] popx) {
		List<DoubleSolution> population_ = new ArrayList<>(_params._populationSize);
		for (int i = 0; i < _params._populationSize; i++) {
			DoubleSolution solution = new DoubleSolution(nDim);
			for (int j = 0; j < nDim; j++) {
				solution.setVariableValue(j, popx[i][j]);
			}
			solution.setOrder(problem.getOrder());
			problem.simpleBounds(solution);

			population_.add(solution);
		}
		return population_;
	}

	@Override
	public int getIterations() {
		return counteval;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	public void setPopulation(List<DoubleSolution> pop) {
		this.population = pop;
	}

	@Override
	public String getSummary() {
		return " - " + counteval;
	}

}
