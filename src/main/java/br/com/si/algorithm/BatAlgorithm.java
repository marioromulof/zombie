package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import br.com.si.params.BatParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class BatAlgorithm extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1241754012788732702L;

	public static final String name = "BatAlgorithm";

	private int currentIteration;
	private int iteration;
	private int nDim;
	private double error;
	private double zero = Math.pow(10.0, -8);
	private double a[], r[], q[];
	private double v[][];
	private List<DoubleSolution> solutions;
	private DoubleSolution bestMovement;

	private Random random;
	private boolean _viewActive;
	private boolean _stepView;
	private boolean isComplexity;
	private BatParams _params;

	public BatAlgorithm(Problem problem, BatParams params, boolean viewActive, boolean stepView, boolean isComplexity) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);
		this.nDim = problem.getNumberOfVariables();
		this.isComplexity = isComplexity;

		random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		currentIteration = 0;
		iteration = 0;
		a = new double[this._params._numBats];
		r = new double[this._params._numBats];
		q = new double[this._params._numBats];
		v = new double[this._params._numBats][this.nDim];

		this.bestMovement = null;

		for (int i = 0; i < this._params._numBats; i++) {
			q[i] = 0;
			a[i] = random.nextDouble() + 1.0; // loudness ∈ [1,2]
			r[i] = random.nextDouble(); // rates ∈ [0,1]

			for (int j = 0; j < this.nDim; j++) {
				v[i][j] = 0.0;
			}
		}

		solutions = new ArrayList<>(this._params._numBats);
		for (int i = 0; i < this._params._numBats; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);

			problem.evaluate(solution);
			updateProgress();

			if (Utils.compare(solution, bestMovement, problem.getOrder()) == -1) {
				bestMovement = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}

			setResult(new BigDecimal(bestMovement.getObjective()), currentIteration,
					_params._maxObjectiveFunctionCalls);

			solutions.add(solution);
		}

	}

	@Override
	public void updateProgress() {
		currentIteration++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return currentIteration >= _params._maxObjectiveFunctionCalls
				|| (!isComplexity && currentIteration > 0 && error == 0.0);
	}

	@Override
	public DoubleSolution search() {
		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		double average_loudness;
		DoubleSolution newPosition, position;
		initProgress();
		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {

			if (_viewActive) {
				redraw(bestMovement, solutions);

				if (_stepView) {
					stepView();
				}
			}

			updateBats();
			average_loudness = Utils.mean(a);

			for (int i = 0; i < this._params._numBats; i++) {
				newPosition = new DoubleSolution(problem.getNumberOfVariables());
				newPosition.setOrder(problem.getOrder());

				if (random.nextDouble() > r[i]) {
					// Generate a local solution around the best solution
					for (int j = 0; j < problem.getNumberOfVariables(); j++) {
						newPosition.setVariableValue(j,
								bestMovement.getVariableValue(j) + (random.nextGaussian() * average_loudness));
					}
				}

				// Generate a new solution by flying randomly
				boolean hasValue = newPosition.getVariableValue(0) != null;
				for (int j = 0; j < problem.getNumberOfVariables(); j++) {
					newPosition.setVariableValue(j, (hasValue ? newPosition.getVariableValue(j)
							: (problem.getLowerBound(j)
									+ random.nextDouble() * (problem.getUpperBound(j) - problem.getLowerBound(j))))
							+ random.nextGaussian());
				}

				position = solutions.get(i);
				problem.simpleBounds(position);
				problem.evaluate(position);
				updateProgress();

				if (Utils.compare(position, bestMovement, problem.getOrder()) == -1) {
					bestMovement = (DoubleSolution) position.copy();

					if (problem.getName().contains("ProblemsCEC15")) {
						this.error = position.getObjective() - problem.getNumFunction() * 100;

						if (this.error < this.zero) {
							this.error = 0.0;
						}
					}
				}
				setResult(new BigDecimal(bestMovement.getObjective()), currentIteration,
						_params._maxObjectiveFunctionCalls);

				problem.simpleBounds(newPosition);
				problem.evaluate(newPosition);
				updateProgress();

				if (Utils.compare(newPosition, bestMovement, problem.getOrder()) == -1) {
					bestMovement = (DoubleSolution) newPosition.copy();

					if (problem.getName().contains("ProblemsCEC15")) {
						this.error = newPosition.getObjective() - problem.getNumFunction() * 100;

						if (this.error < this.zero) {
							this.error = 0.0;
						}
					}
				}
				setResult(new BigDecimal(bestMovement.getObjective()), currentIteration,
						_params._maxObjectiveFunctionCalls);

				if (newPosition.getObjective().compareTo(position.getObjective()) == -1 && random.nextDouble() < a[i]) {
					solutions.set(i, newPosition);
					a[i] *= _params._alpha;
					r[i] *= (1 - Math.exp(-_params._gamma * iteration));
				}
			}
			iteration++;

			// for (int i = 0; i < this._params._numBats; i++) {
			// // q[i] = _params._qMin + (_params._qMin - _params._qMax) *
			// // random.nextDouble();
			// q[i] = problem.getLowerBound(0)
			// + (problem.getUpperBound(0) - problem.getLowerBound(0)) *
			// random.nextDouble();
			//
			// DoubleSolution solution = (DoubleSolution)
			// solutions.get(i).copy();
			// for (int j = 0; j < nDim; j++) {
			// v[i][j] = v[i][j] + (solution.getVariableValue(j) -
			// bestMovement.getVariableValue(j)) * q[i];
			// double value = solution.getVariableValue(j) + v[i][j];
			//
			// // value = Math.round(value * 100.0);
			// // value /= 100.0;
			//
			// solution.setVariableValue(j, value);
			// }
			//
			// if (random.nextDouble() > r[i]) {
			// for (int j = 0; j < nDim; j++) {
			// double alpha = problem.getLowerBound(j)
			// + (problem.getUpperBound(j) - problem.getLowerBound(j)/* + 1 */)
			// * random.nextDouble();
			// double value = bestMovement.getVariableValue(j) + (0.01 * alpha);
			//
			// // value = Math.round(value * 100.0);
			// // value /= 100.0;
			//
			// solution.setVariableValue(j, value);
			// }
			// }
			//
			// problem.simpleBounds(solution);
			// problem.evaluate(solution);
			// updateProgress();
			//
			// if ((Utils.compare(solution, solutions.get(i),
			// problem.getOrder()) == -1)
			// && random.nextDouble() < a[i]) {
			// solutions.set(i, solution);
			//
			// a[i] = this._params._alpha * a[i];
			// r[i] = 1 * (1 - Math.exp(-this._params._gamma *
			// currentIteration));
			//
			// if (Utils.compare(solution, bestMovement, problem.getOrder()) ==
			// -1) {
			// bestMovement = (DoubleSolution) solution.copy();
			// }
			// }
			//
			// setResult(this.bestMovement.getObjective(),
			// this.currentIteration,
			// this._params._maxObjectiveFunctionCalls);
			// }
		}

		if (_viewActive) {
			redraw(bestMovement, solutions);

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}
		return bestMovement;
	}

	private void updateBats() {
		for (int i = 0; i < this._params._numBats; i++) {
			// Update Frequency
			q[i] = problem.getLowerBound(0)
					+ (problem.getUpperBound(0) - problem.getLowerBound(0)) * random.nextDouble();

			DoubleSolution bat = solutions.get(i);
			for (int j = 0; j < nDim; j++) {
				// Update velocity
				v[i][j] += (bat.getVariableValue(j) - bestMovement.getVariableValue(j)) * q[i];

				// Update Position
				bat.setVariableValue(j, bat.getVariableValue(j) + v[i][j]);
			}
		}
	}

	@Override
	public int getIterations() {
		return currentIteration;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	@Override
	public String getSummary() {
		return " - " + currentIteration;
	}

}