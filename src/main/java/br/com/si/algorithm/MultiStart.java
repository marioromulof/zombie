package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import br.com.si.params.MultiStartParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings({ "unchecked" })
public class MultiStart extends Algorithm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2157709462621832556L;

	public static final String name = "ContinuousGRASP";

	private int numFunctionCalls;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private DoubleSolution bestSolution;

	private Random random;

	private boolean _viewActive;
	private boolean _stepView;
	private boolean isComplexity;
	private MultiStartParams _params;

	public MultiStart(Problem problem, MultiStartParams params, boolean viewActive, boolean stepView,
			boolean isComplexity) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);
		this.isComplexity = isComplexity;

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();
		bestSolution = null;
		numFunctionCalls = 0;
	}

	@Override
	public void updateProgress() {
		numFunctionCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return numFunctionCalls >= _params._maxObjectiveFunctionCalls
				|| (!isComplexity && numFunctionCalls > 0 && error == 0.0);
	}

	@Override
	public DoubleSolution search() {
		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		double h;
		boolean imprC, imprL;

		initProgress();

		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			h = _params._hs;

			problem.evaluate(solution);
			updateProgress();
			if (Utils.compare(solution, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}
			setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCalls,
					_params._maxObjectiveFunctionCalls);

			while (h >= _params._he && !isStoppingConditionReached()) {
				imprC = constructGreedyRandomized(solution, h);
				imprL = localImprovement(solution, h);

				if (this._viewActive) {
					List<DoubleSolution> sols = new ArrayList<>();
					sols.add(solution);
					redraw(bestSolution, sols);
					// System.out.println("Num calls: " + numFunctionCalls);

					if (_stepView) {
						stepView();
					}
				}

				if (!imprC && !imprL) {
					h /= 2.0;
				}
			}
		}

		return bestSolution;
	}

	private boolean localImprovement(DoubleSolution x, double h) {
		double numGridPoints, norm;
		Double bestValue;
		int maxPointsToExamine, numPointsExamined;
		boolean ok, imprL;

		DoubleSolution z = new DoubleSolution(problem.getNumberOfVariables());
		DoubleSolution y = new DoubleSolution(problem.getNumberOfVariables());
		z.setOrder(problem.getOrder());
		y.setOrder(problem.getOrder());

		bestValue = x.getObjective();
		imprL = false;

		numGridPoints = 1;
		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			numGridPoints *= Math.ceil((problem.getUpperBound(i) - problem.getLowerBound(i)) / h);
		}
		maxPointsToExamine = (int) Math.ceil(_params._plo * numGridPoints);
		numPointsExamined = 0;

		while (numPointsExamined <= maxPointsToExamine && !isStoppingConditionReached()) {
			numPointsExamined += 1;
			ok = false;

			do {
				norm = 0.0;
				for (int i = 0; i < problem.getNumberOfVariables(); i++) {
					double value;
					if (random.nextDouble() <= 0.5) {
						value = (problem.getUpperBound(i) - x.getVariableValue(i)) / h;

						if (value != 0.0) {
							z.setVariableValue(i, random.nextDouble() % value);
						} else {
							z.setVariableValue(i, value);
						}

						// Math.ceil((double) (problem.getUpperBound(i) -
						// x.getVariableValue(i).doubleValue()) / h))));
					} else {
						value = (x.getVariableValue(i) - problem.getLowerBound(i)) / h;

						if (value != 0.0) {
							z.setVariableValue(i, -1.0 * (random.nextDouble() % value));
						} else {
							z.setVariableValue(i, value);
						}

						// Math.ceil(((x.getVariableValue(i).doubleValue() -
						// problem.getLowerBound(i)) / h))))));
					}

					if (z.getVariableValue(i) != 0) {
						ok = true;
					}
					norm += Math.pow(z.getVariableValue(i) * h, 2);
				}

			} while (!ok);

			norm = Math.sqrt(norm);

			for (int i = 0; i < problem.getNumberOfVariables(); i++) {
				y.setVariableValue(i, x.getVariableValue(i) + ((1.0 / norm) * h * z.getVariableValue(i) * h));
			}

			problem.simpleBounds(y);
			problem.evaluate(y);
			updateProgress();

			if (Utils.compare(y, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) y.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = y.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}
			setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCalls,
					_params._maxObjectiveFunctionCalls);

			if (y.getObjective().compareTo(bestValue) == -1) {
				for (int i = 0; i < problem.getNumberOfVariables(); i++) {
					x.setVariableValue(i, y.getVariableValue(i));
				}
				x.setObjective(y.getObjective());
				bestValue = y.getObjective();
				numPointsExamined = 0;
				imprL = true;
			}
		}

		return imprL;
	}

	private boolean constructGreedyRandomized(DoubleSolution x, double h) {
		double alpha, threshold;
		Double min, max;
		boolean reuse, imprC;
		int j;
		DoubleSolution[] zi = new DoubleSolution[problem.getNumberOfVariables()];
		boolean[] unFixed = new boolean[problem.getNumberOfVariables()];
		boolean[] rcl = new boolean[problem.getNumberOfVariables()];
		Double[] gi = new Double[problem.getNumberOfVariables()];

		alpha = random.nextDouble();
		reuse = false;
		imprC = false;
		min = Double.MAX_VALUE;
		max = Double.MIN_VALUE;

		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			gi[i] = x.getObjective();
			zi[i] = (DoubleSolution) x.copy();
		}

		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			unFixed[i] = true;
		}

		while (hasTrue(unFixed)) {
			for (int i = 0; i < problem.getNumberOfVariables(); i++) {
				if (unFixed[i]) {
					if (!reuse) {
						zi[i] = lineSearch((DoubleSolution) x.copy(), h, i);
						gi[i] = zi[i].getObjective();

						if (isStoppingConditionReached()) {
							return imprC;
						}
					}

					if (gi[i].compareTo(min) == -1) {
						min = gi[i];
					}
					if (gi[i].compareTo(max) == 1) {
						max = gi[i];
					}
				}
			}

			for (int i = 0; i < problem.getNumberOfVariables(); i++) {
				rcl[i] = false;
			}

			threshold = min + alpha * (max - min);
			for (int i = 0; i < problem.getNumberOfVariables(); i++) {
				if (unFixed[i] && gi[i].doubleValue() <= threshold) {
					rcl[i] = true;
				}
			}

			if (hasTrue(rcl)) {
				j = random.nextInt(rcl.length);
				while (!rcl[j]) {
					j = random.nextInt(rcl.length);
				}

				if (x.getVariableValue(j) == zi[j].getVariableValue(j)) {
					reuse = true;
				} else {
					x.setVariableValue(j, zi[j].getVariableValue(j));
					reuse = false;
					imprC = true;
				}

				unFixed[j] = false;
			}
		}

		return imprC;
	}

	private boolean hasTrue(boolean[] unFixed) {
		for (boolean item : unFixed) {
			if (item) {
				return item;
			}
		}

		return false;
	}

	private DoubleSolution lineSearch(DoubleSolution x, double h, int i) {
		double minF, ti, zi;

		minF = x.getObjective();
		zi = x.getVariableValue(i).doubleValue();

		ti = problem.getLowerBound(i);
		while (ti <= problem.getUpperBound(i)) {
			x.setVariableValue(i, ti);
			problem.evaluate(x);
			updateProgress();

			if (Utils.compare(x, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) x.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = x.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}
			setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCalls,
					_params._maxObjectiveFunctionCalls);

			if (x.getObjective().compareTo(minF) < 0) {
				minF = x.getObjective();
				zi = x.getVariableValue(i).doubleValue();
			}

			ti += h;
		}

		x.setVariableValue(i, problem.getUpperBound(i));
		problem.evaluate(x);
		updateProgress();

		if (x.getObjective().compareTo(minF) < 0) {
			minF = x.getObjective();
			zi = x.getVariableValue(i).doubleValue();
		}

		if (x.getVariableValue(i).doubleValue() != zi) {
			x.setObjective(minF);
			x.setVariableValue(i, zi);
		}

		if (this._viewActive) {
			List<DoubleSolution> sols = new ArrayList<>();
			sols.add(x);
			redraw(bestSolution, sols);
			// System.out.println("Num calls: " + numFunctionCalls);

			if (_stepView) {
				stepView();
			}
		}

		return x;

	}

	@Override
	public int getIterations() {
		return numFunctionCalls;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	@Override
	public String getSummary() {
		return " - " + numFunctionCalls;
	}

}
