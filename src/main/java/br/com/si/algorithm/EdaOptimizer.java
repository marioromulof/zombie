package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import br.com.si.params.EdaParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class EdaOptimizer extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6938581450578145584L;

	public static final String name = "EDAmvg";

	private int numFunctionCall;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private DoubleSolution bestSolution;
	private List<DoubleSolution> solutions;

	private Random random;

	private boolean _viewActive;
	private boolean _stepView;
	private boolean isComplexity;
	private EdaParams _params;

	public EdaOptimizer(Problem problem, EdaParams params, boolean viewActive, boolean stepView, boolean isComplexity) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);
		this.isComplexity = isComplexity;

		// this.numSolutions = Parameters.EDA_NUM_SOLUTIONS;
		// this.h = Parameters.EDA_NUM_SUBINTERVALS;
		// this.maxFunctionCalls = problem.getNumberOfVariables() *
		// Parameters.MAX_FES;
		// this.minError = Parameters.MIN_ERROR;
		// this.m = Parameters.EDA_NUM_PARTICLES;

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		solutions = new ArrayList<>(_params._populationSize);
		numFunctionCall = 0;
		bestSolution = null;

		for (int i = 0; i < _params._populationSize; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			problem.evaluate(solution);
			updateProgress();

			if (Utils.compare(solution, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}

			setResult(new BigDecimal(bestSolution.getObjective()), this.numFunctionCall,
					this._params._maxObjectiveFunctionCalls);

			solutions.add(solution);
		}
	}

	@Override
	public void updateProgress() {
		numFunctionCall++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return numFunctionCall >= _params._maxObjectiveFunctionCalls
				|| (!isComplexity && numFunctionCall > 0 && error == 0.0);
	}

	@Override
	public DoubleSolution search() {
		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		List<Integer> selections = new ArrayList<>();
		initProgress();
		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {
			if (_viewActive) {
				redraw(bestSolution, solutions);

				if (_stepView) {
					stepView();
				}
			}

			// Select some individuals Psel from P.
			selections.clear();
			for (int i = 0; i < _params._populationSize; i++) {
				if (random.nextDouble() < _params._selectionRatio) {
					selections.add(i);
				}
			}

			// Generate matrix of some individuals Psel from P.
			double[][] dataset = new double[selections.size()][problem.getNumberOfVariables()];
			for (int i = 0; i < selections.size(); i++) {
				DoubleSolution solution = solutions.get(selections.get(i));
				for (int j = 0; j < problem.getNumberOfVariables(); j++) {
					dataset[i][j] = solution.getVariableValue(j).doubleValue();
				}
			}

			samplePopulation(dataset);

			Collections.sort(solutions);

			while (solutions.size() > _params._populationSize) {
				solutions.remove(solutions.size() - 1);
			}
		}

		if (_viewActive) {
			redraw(bestSolution, solutions);

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}
		return bestSolution;
	}

	@Override
	public int getIterations() {
		return numFunctionCall;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	private void samplePopulation(double[][] observations) {
		double[] means;
		double[][] cov, chol, samples;

		// Estimate the mean(μ) and covariance(∑) of Psel.
		means = mean(observations, 0);
		cov = covariance(observations, means, observations.length - 1, 0);

		// To maintain the diversity, a simple approach is to amplify the
		// covariance matrix
		cov = Utils.times(_params._maxAmplification, cov);

		// The Cholesky decomposition
		chol = choleskyDecomposition(cov);

		// generate Z an N-by-M matrix with random elements
		samples = new double[observations[0].length][observations.length];
		for (int i = 0; i < observations[0].length; i++) {
			for (int j = 0; j < observations.length; j++) {
				samples[i][j] = random.nextGaussian();
			}
		}

		// generate new X
		samples = Utils.transpose(Utils.matrixTimes(chol, samples));

		for (int i = 0; i < samples.length; i++) {
			DoubleSolution solution = new DoubleSolution(problem.getNumberOfVariables(), problem.getOrder());

			for (int j = 0; j < problem.getNumberOfVariables(); j++) {
				// solution.setVariableValue(j, (((1 -
				// _params._learningRate) * means[j] +
				// _params._learningRate *
				// bestSolution.getVariableValue(j)) + samples[i][j]));
				solution.setVariableValue(j, bestSolution.getVariableValue(j) * _params._learningRate
						+ ((1 - _params._learningRate) * means[j]) + samples[i][j]);
			}

			problem.simpleBounds(solution);
			problem.evaluate(solution);
			updateProgress();

			if (Utils.compare(solution, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}

			setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCall, _params._maxObjectiveFunctionCalls);

			solutions.add(solution);
		}
	}

	private double[] mean(double[][] matrix, int dimension) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		double[] mean;

		if (dimension == 0) {
			mean = new double[cols];
			double N = rows;

			// for each column
			for (int j = 0; j < cols; j++) {
				// for each row
				for (int i = 0; i < rows; i++) {
					mean[j] += matrix[i][j];
				}

				mean[j] /= N;
			}
		} else if (dimension == 1) {
			mean = new double[rows];
			double N = cols;

			// for each row
			for (int j = 0; j < rows; j++) {
				// for each column
				for (int i = 0; i < cols; i++) {
					mean[j] += matrix[j][i];
				}

				mean[j] /= N;
			}
		} else {
			throw new IllegalArgumentException("Invalid dimension");
		}

		return mean;
	}

	private double[][] covariance(double[][] matrix, double[] means, double divisor, int dimension) {
		int rows = matrix.length;
		if (rows == 0) {
			return new double[0][0];
		}
		int cols = matrix[0].length;

		double[][] cov;

		if (dimension == 0) {
			if (means.length != cols) {
				throw new IllegalArgumentException("Length of the mean vector should equal the number of columns");
			}

			cov = new double[cols][cols];
			for (int i = 0; i < cols; i++) {
				for (int j = i; j < cols; j++) {
					double s = 0.0;
					for (int k = 0; k < rows; k++) {
						s += (matrix[k][j] - means[j]) * (matrix[k][i] - means[i]);
					}
					s /= divisor;
					cov[i][j] = s;
					cov[j][i] = s;
				}
			}
		} else if (dimension == 1) {
			if (means.length != rows) {
				throw new IllegalArgumentException("Length of the mean vector should equal the number of rows");
			}

			cov = new double[rows][rows];
			for (int i = 0; i < rows; i++) {
				for (int j = i; j < rows; j++) {
					double s = 0.0;
					for (int k = 0; k < cols; k++) {
						s += (matrix[j][k] - means[j]) * (matrix[i][k] - means[i]);
					}
					s /= divisor;
					cov[i][j] = s;
					cov[j][i] = s;
				}
			}
		} else {
			throw new IllegalArgumentException("Invalid dimension");
		}

		return cov;
	}

	private double[][] choleskyDecomposition(double[][] A) {
		// Initialize.
		int n = A.length;
		double[][] L = new double[n][n];
		// boolean isspd = (A[0].length == n);

		// Main loop.
		for (int j = 0; j < n; j++) {
			double[] Lrowj = L[j];
			double d = 0.0;
			for (int k = 0; k < j; k++) {
				double[] Lrowk = L[k];
				double s = 0.0;
				for (int i = 0; i < k; i++) {
					s += Lrowk[i] * Lrowj[i];
				}
				Lrowj[k] = s = (A[j][k] - s) / L[k][k];
				d = d + s * s;
				// isspd = isspd & (A[k][j] == A[j][k]);
			}
			d = A[j][j] - d;
			// isspd = isspd & (d > 0.0);
			L[j][j] = Math.sqrt(Math.max(d, 0.0));
			for (int k = j + 1; k < n; k++) {
				L[j][k] = 0.0;
			}
		}

		return L;
	}

	@Override
	public String getSummary() {
		return " - " + numFunctionCall;
	}

}
