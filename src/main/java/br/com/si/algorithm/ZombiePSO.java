package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import br.com.si.actor.Human;
import br.com.si.actor.Zombie;
import br.com.si.params.ProblemParams;
import br.com.si.params.ZombieParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.DataReader;
import br.com.si.util.Ordering;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class ZombiePSO extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4236292021334424008L;

	public static final String name = "ZombiePSO";

	private double distribution;
	private double covariance;
	private int nDim;
	private int nCalls;
	private DoubleSolution bestMovement;
	private double probabilityMove;
	private int nGroups;
	private int nPoly;
	private int nGauss;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private ArrayList<Human> humans;
	private ArrayList<Zombie> zombies;

	private Random random;

	private boolean viewActive;
	private boolean stepView;
	private ZombieParams params;

	private DoubleSolution[] localBest;
	private double[][] speed;
	private int[][] matrixL;

	public ZombiePSO(Problem problem, ZombieParams params, boolean viewActive, boolean stepView) {
		super(problem);

		this.viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this.stepView = stepView && problem.getNumberOfVariables() == 2;
		this.params = params;
		this.notExecuteInThread = (this.viewActive || this.stepView);

		this.nDim = problem.getNumberOfVariables();

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		String groupsHumanMoved;
		super.initProgress();

		this.bestMovement = null;
		nGroups = 0;
		nPoly = 0;
		nGauss = 0;

		// double numGenerations = (double) (params.humanPopSize -
		// (params.humanPopSize * 0.2)) / 2.0;
		// numGenerations = ((double) params.maxObjectiveFunctionCalls /
		// (double) params.humanPopSize)
		// / numGenerations;

		covariance = params.humanMaxMutIdxFactor;
		// covariance = params.humanMinMutIdxFactor;
		// diffCov = (params.humanMinMutIdxFactor / 20.0) -
		// (params.humanMaxDistribution / 20.0);
		// diffCov = (diffCov / numGenerations);

		// distribution = params.humanMaxDistribution;
		distribution = params.humanMinMutIdxFactor;
		// diffDist = params.humanMaxDistribution -
		// params.humanMinMutIdxFactor;
		// diffDist = (diffDist / numGenerations);

		this.nCalls = 0;
		this.probabilityMove = 1.0 / (double) this.nDim;

		this.humans = new ArrayList<Human>();
		for (int i = 0; i < params.maxHumanPop; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			getMeritEvaluation(solution);
			this.humans.add(new Human(solution, random.nextDouble(), params.humanMaxFear, random.nextDouble()));
		}

		this.zombies = new ArrayList<Zombie>();

		groupsHumanMoved = moveGroupHuman();
		for (int i = 0; i < params.humanStepsPerZombieStep; i++) {
			this.moveHumans(groupsHumanMoved);
		}

		for (int i = 0; i < params.zombiePopSize; i++) {
			Zombie z = new Zombie(this.problem.getNumberOfVariables(), random.nextDouble());
			z.setSolution(DoubleSolution.createSolution(problem, random).getVariables());
			zombies.add(z);
		}
	}

	@Override
	public void updateProgress() {
		this.nCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return this.humans.isEmpty() || this.nCalls >= params._maxObjectiveFunctionCalls
				|| (nCalls > 0 && error == 0.0);
	}

	@Override
	public int getIterations() {
		return this.nCalls;
	}

	@Override
	public DoubleSolution search() {
		int idx;
		double candidateDistance, zombieDistance;
		String groupsHumanMoved;

		initProgress();
		if (this.viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {

			if (viewActive) {
				List<DoubleSolution> humans = new ArrayList<>();
				List<DoubleSolution> zombies = new ArrayList<>();

				for (Human human : this.humans) {
					humans.add((DoubleSolution) human.getSolution());
				}

				for (Zombie zombie : this.zombies) {
					zombies.add((DoubleSolution) zombie.getSolution());
				}

				redraw(bestMovement, humans, zombies);

				if (stepView) {
					stepView();
				}
			}

			// move humans
			groupsHumanMoved = moveGroupHuman();
			for (idx = 0; idx < params.humanStepsPerZombieStep; idx++) {
				this.moveHumans(groupsHumanMoved);
			}

			for (idx = 0; idx < this.zombies.size(); idx++) {
				Zombie zombie = this.zombies.get(idx);
				if (!zombie.isChaser()) {
					continue;
				}

				zombieDistance = Double.MAX_VALUE;
				for (Human human : this.humans) {
					candidateDistance = Utils.getEuclideanDistance(zombie.getSolution(), human.getSolution());

					if (candidateDistance < params.zombieDistanceAttack) {
						zombieDistance = 0.0;

						double defense = random.nextDouble() * human.getDefenderRate();
						double attack = random.nextDouble() * zombie.getHuntingRate();
						if ((defense < attack) && (humans.size() > params.minHumanPop)) {
							this.turnZombie(human);
						}

						break;
					} else if (candidateDistance < zombieDistance) {
						zombieDistance = candidateDistance;
					}
				}

				if (zombieDistance >= params.zombieDistanceAttack && zombieDistance < Double.MAX_VALUE) {
					List<Integer> tries = new ArrayList<>();
					for (idx = 0; idx < this.nDim; idx++) {
						tries.add(idx);
					}

					Collections.shuffle(tries);

					boolean foundBetter = false;
					for (idx = 0; (idx < this.nDim) && !foundBetter; idx++) {

						DoubleSolution zombieSolution = (DoubleSolution) zombie.getSolution().copy();
						for (int i = 0; (i < 5) && !foundBetter; i++) {

							// double change = random.nextGaussian() *
							// params.covar;
							// double change =
							// zombieSolution.getVariableValue(tries.get(idx)) +
							// (random.nextGaussian() *
							// params.humanMaxDistance);

							zombieSolution.setVariableValue(tries.get(idx),
									zombieSolution.getVariableValue(tries.get(idx))
											+ random.nextGaussian() * params.humanMaxDistance);

							for (Human human : this.humans) {
								candidateDistance = Utils.getEuclideanDistance(zombieSolution, human.getSolution());

								if (candidateDistance < zombieDistance) {
									zombieDistance = candidateDistance;
									foundBetter = true;
								}
							}

							if (foundBetter) {
								zombie.setSolution(zombieSolution.getVariables().clone());
							}
						}
					}
				}
			}

			// if (covariance > 0.5) {
			// covariance += diffCov;
			covariance = params.humanMaxMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMaxMutIdxFactor - params.humanMinMutIdxFactor));
			// }
			// distribution += diffDist;
			distribution = params.humanMinMutIdxFactor - ((this.nCalls / params._maxObjectiveFunctionCalls)
					* (params.humanMinMutIdxFactor - params.humanMaxMutIdxFactor));
		}

		if (viewActive) {
			List<DoubleSolution> humans = new ArrayList<>();
			List<DoubleSolution> zombies = new ArrayList<>();

			for (Human human : this.humans) {
				humans.add((DoubleSolution) human.getSolution());
			}

			for (Zombie zombie : this.zombies) {
				zombies.add((DoubleSolution) zombie.getSolution());
			}

			redraw(bestMovement, humans, zombies);
			if (stepView) {
				stepView();
			}

			finishViewer();
		}

		String result = problem.getName() + " - Z: " + this.zombies.size() + ", H: " + this.humans.size()
				+ ", Num. Gauss: " + nGauss + ", Num. Poly: " + nPoly + ", Num. Groups: " + nGroups;
		DataReader.saveLog(getName(), problem.getNumberOfVariables(), result);
		System.out.println(result + "\n");

		return this.bestMovement;
	}

	private void initializeVelocityAndParticlesMemory(List<DoubleSolution> swarm) {
		double lb, ub;
		localBest = new DoubleSolution[swarm.size()];
		speed = new double[swarm.size()][problem.getNumberOfVariables()];
		matrixL = new int[swarm.size()][swarm.size()];

		for (int i = 0; i < swarm.size(); i++) {
			matrixL[i][i] = 1;
			localBest[i] = (DoubleSolution) swarm.get(i).copy();
			for (int j = 0; j < problem.getNumberOfVariables(); j++) {
				speed[i][j] = 0.0;
				lb = problem.getLowerBound(j) - swarm.get(i).getVariableValue(j);
				ub = problem.getUpperBound(j) - swarm.get(i).getVariableValue(j);
				speed[i][j] = lb + this.random.nextDouble() * (ub - lb);
			}
		}
	}

	private void updateParticlesMemory(List<DoubleSolution> swarm, int position) {
		int flag = Utils.compare(swarm.get(position), localBest[position], problem.getOrder());
		if (flag == -1) {
			localBest[position] = (DoubleSolution) swarm.get(position).copy();
		}
	}

	private String moveGroupHuman() {
		double oldEvaluation, newEvaluation, oldDistance, newDistance, distance;
		boolean hasZombie;
		String groupHuman, idHuman, groupsHumanMoved;

		groupsHumanMoved = ",";
		for (int i = 0; i < humans.size(); i++) {
			idHuman = ",".concat(String.valueOf(i).concat(","));
			if (groupsHumanMoved.contains(idHuman)) {
				continue;
			}

			Human human = humans.get(i);

			hasZombie = false;
			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(human.getSolution(), zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					hasZombie = true;
				}
			}

			if (!hasZombie) {
				List<Human> pop = new ArrayList<>();
				pop.add(human);

				groupHuman = ",".concat(String.valueOf(i).concat(","));
				for (int j = 0; j < humans.size(); j++) {
					idHuman = ",".concat(String.valueOf(j).concat(","));
					if (!groupHuman.contains(idHuman) && !groupsHumanMoved.contains(idHuman)) {
						distance = Utils.getEuclideanDistance(human.getSolution(), humans.get(j).getSolution());

						if (distance < params.humanMaxDistance) {
							pop.add(humans.get(j));
							groupHuman = groupHuman.concat(String.valueOf(j).concat(","));
						}
					}
				}

				if (pop.size() > 1) {
					groupsHumanMoved = groupHuman.concat(groupsHumanMoved.substring(1));

					int s, m, g;
					oldDistance = 0;
					newDistance = 0;
					double value;
					double[] px = new double[problem.getNumberOfVariables()];
					double[] pxl = new double[problem.getNumberOfVariables()];
					DoubleSolution gi = new DoubleSolution(problem.getNumberOfVariables(), problem.getOrder());
					DoubleSolution particle;
					double c = 0.5 + Math.log(2.0);
					double w = 1.0 / (2.0 * Math.log(2.0));

					List<DoubleSolution> solutions = new ArrayList<>();
					for (int j = 0; j < pop.size(); j++) {
						solutions.add((DoubleSolution) pop.get(j).getSolution().copy());
					}

					initializeVelocityAndParticlesMemory(solutions);
					for (int idx = 0; idx < params.humanStepsPerZombieStep; idx++) {
						nGroups += pop.size();
						// Loop on particles
						for (s = 0; s < solutions.size(); s++) {
							oldDistance = 0;
							oldEvaluation = solutions.get(s).getObjective();

							for (Zombie zombie : this.zombies) {
								distance = Utils.getEuclideanDistance(solutions.get(s), zombie.getSolution());
								if (distance < params.humanMaxDistance) {
									hasZombie = true;
									oldDistance += Math.pow(distance, params.powDistance);
								}
							}

							// ... find the first informant
							int s1 = 0;
							while (matrixL[s1][s] == 0 && s1 < solutions.size()) {
								s1++;
							}

							g = s1;
							for (m = s1; m < solutions.size(); m++) {
								if (matrixL[m][s] == 1
										&& localBest[m].getObjective().compareTo(localBest[g].getObjective()) == -1) {
									g = m;
								}
							}

							particle = (DoubleSolution) solutions.get(s).copy();
							for (int d = 0; d < problem.getNumberOfVariables(); d++) {
								// define a point p' on x-p, beyond p
								value = particle.getVariableValue(d)
										+ c * (localBest[s].getVariableValue(d) - particle.getVariableValue(d));
								px[d] = value;

								// ... define a point g' on x-g, beyond g
								value = particle.getVariableValue(d)
										+ c * (localBest[g].getVariableValue(d) - particle.getVariableValue(d));
								pxl[d] = value;
							}

							// If the best informant is the particle itself,
							// define the
							// gravity center G as the middle of x-p'

							if (g == s) {
								for (int d = 0; d < problem.getNumberOfVariables(); d++) {
									gi.setVariableValue(d, (particle.getVariableValue(d) + px[d]) * 0.5);
								}
							} else { // % Usual way to define G
								for (int d = 0; d < problem.getNumberOfVariables(); d++) {
									gi.setVariableValue(d, particle.getVariableValue(d) + (px[d] + pxl[d]) / 3.0);
								}

							}

							value = Utils.getEuclideanDistance(particle, gi);
							Utils.getRandomPoint(gi, value, random);

							// New "velocity" and update position
							for (int d = 0; d < problem.getNumberOfVariables(); d++) {
								value = particle.getVariableValue(d);
								speed[s][d] = w * speed[s][d] + gi.getVariableValue(d) - value;

								value += speed[s][d];

								if (value < problem.getLowerBound(d)) {
									value = problem.getLowerBound(d);
									speed[s][d] *= -0.5;
								} else if (value > problem.getUpperBound(d)) {
									value = problem.getUpperBound(d);
									speed[s][d] *= -0.5;
								}

								particle.setVariableValue(d, value);
							}

							getMeritEvaluation(particle);

							newEvaluation = particle.getObjective();
							newDistance = 0;
							for (Zombie zombie : this.zombies) {
								distance = Utils.getEuclideanDistance(particle, zombie.getSolution());
								if (distance < params.humanMaxDistance) {
									newDistance += Math.pow(distance, params.powDistance);
								}
							}

							if (problem.getOrder() == Ordering.ASCENDING) {
								oldEvaluation += (pop.get(s).getFear() * oldDistance);
								newEvaluation += (pop.get(s).getFear() * newDistance);

								if (oldEvaluation > newEvaluation) {
									solutions.set(s, (DoubleSolution) particle.copy());
									updateParticlesMemory(solutions, s);
									pop.get(s).setSolution((DoubleSolution) particle.copy());
								}
							} else {
								oldEvaluation -= (pop.get(s).getFear() * oldDistance);
								newEvaluation -= (pop.get(s).getFear() * newDistance);

								if (oldEvaluation < newEvaluation) {
									solutions.set(s, (DoubleSolution) particle.copy());
									updateParticlesMemory(solutions, s);
									pop.get(s).setSolution((DoubleSolution) particle.copy());
								}
							}

						}
					}
				}
			}
		}

		return groupsHumanMoved;
	}

	private void moveHumans(String groupsHumanMoved) {
		double oldEvaluation, newEvaluation, oldDistance, newDistance, distance;
		boolean hasZombie;
		boolean hasMutation;
		double diff;

		// for (Human human : this.humans) {
		for (int i = 0; i < this.humans.size(); i++) {
			if (groupsHumanMoved.contains(",".concat(String.valueOf(i).concat(",")))) {
				continue;
			}

			Human human = humans.get(i);
			hasZombie = false;
			hasMutation = false;
			oldDistance = 0;
			newDistance = 0;
			DoubleSolution humanSolution = (DoubleSolution) human.getSolution().copy();
			oldEvaluation = humanSolution.getObjective();

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					hasZombie = true;
					oldDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (hasZombie) {
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						diff = random.nextGaussian() * covariance;
						humanSolution.setVariableValue(var, humanSolution.getVariableValue(var) + diff);
						hasMutation = true;
					}
				}
				if (hasMutation) {
					nGauss++;
				}
			} else {
				// polynomial mutation
				for (int var = 0; var < problem.getNumberOfVariables(); var++) {
					if (random.nextDouble() <= probabilityMove) {
						humanSolution.setVariableValue(var,
								Utils.polynomialMutation(random.nextDouble(), humanSolution.getVariableValue(var),
										distribution, problem.getLowerBound(var), problem.getUpperBound(var)));
						hasMutation = true;
					}
				}
				if (hasMutation) {
					nPoly++;
				}
			}

			problem.simpleBounds(humanSolution);

			if (hasMutation) {
				newEvaluation = getMeritEvaluation(humanSolution);
			} else {
				newEvaluation = humanSolution.getObjective();
			}

			for (Zombie zombie : this.zombies) {
				distance = Utils.getEuclideanDistance(humanSolution, zombie.getSolution());
				if (distance < params.humanMaxDistance) {
					newDistance += Math.pow(distance, params.powDistance);
				}
			}

			if (problem.getOrder() == Ordering.ASCENDING) {
				oldEvaluation += (human.getFear() * oldDistance);
				newEvaluation += (human.getFear() * newDistance);

				if (oldEvaluation > newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			} else {
				oldEvaluation -= (human.getFear() * oldDistance);
				newEvaluation -= (human.getFear() * newDistance);

				if (oldEvaluation < newEvaluation) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			}

		}
	}

	private double getMeritEvaluation(DoubleSolution solution) {
		this.problem.simpleBounds(solution);
		this.problem.evaluate(solution);

		updateProgress();

		if (Utils.compare(solution, this.bestMovement, problem.getOrder()) == -1) {
			this.bestMovement = (DoubleSolution) solution.copy();

			if (problem.getName().contains("ProblemsCEC15")) {
				this.error = solution.getObjective() - problem.getNumFunction() * 100;

				if (this.error < this.zero) {
					this.error = 0.0;
				}
			}
		}

		setResult(new BigDecimal(this.bestMovement.getObjective()), this.nCalls,
				this.params._maxObjectiveFunctionCalls);

		return solution.getObjective();
	}

	private void turnZombie(Human human) {
		Zombie newzombie = new Zombie(problem.getNumberOfVariables(), random.nextDouble());
		newzombie.setSolution(human.getSolution().getVariables().clone());

		this.zombies.add(newzombie);
		this.humans.remove(human);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this.params;
	}

	@Override
	public String getSummary() {
		return " - " + nCalls;
	}

}
