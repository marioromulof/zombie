package br.com.si.algorithm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.problem.math.Constants;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.ImageComponent;
import br.com.si.util.Viewer;

public abstract class Algorithm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5217786504688087650L;

	protected Problem problem;
	protected boolean notExecuteInThread;
	private Viewer viewer;
	private ImageComponent imageComponent;
	private BigDecimal[] results;
	private int indexResults;
	private double[] momentsFES;

	public Algorithm(Problem inputProblem) {
		problem = inputProblem;
		momentsFES = new double[17];
		results = new BigDecimal[17];

		momentsFES[0] = 0.0001;
		momentsFES[1] = 0.001;
		momentsFES[2] = 0.01;
		momentsFES[3] = 0.02;
		momentsFES[4] = 0.03;
		momentsFES[5] = 0.04;
		momentsFES[6] = 0.05;
		momentsFES[7] = 0.1;
		momentsFES[8] = 0.2;
		momentsFES[9] = 0.3;
		momentsFES[10] = 0.4;
		momentsFES[11] = 0.5;
		momentsFES[12] = 0.6;
		momentsFES[13] = 0.7;
		momentsFES[14] = 0.8;
		momentsFES[15] = 0.9;
		momentsFES[16] = 1.0;
	}

	public void initProgress() {
		indexResults = 0;
	}

	public abstract void updateProgress();

	public abstract void setSeed(long seed);

	public abstract boolean isStoppingConditionReached();

	public abstract int getIterations();

	public abstract String getName();

	public abstract ProblemParams getParameter();

	public boolean notExecuteInThread() {
		return notExecuteInThread;
	}

	public String getProblemName() {
		return problem.getName();
	}

	public int getProblemDimension() {
		return problem.getNumberOfVariables();
	}

	public BigDecimal[] getResults() {
		return results;
	}

	public void setResult(BigDecimal result, int moment, int maxFES) {
		BigDecimal sub;
		if (problem.getNumFunction() != null) {
			sub = new BigDecimal(problem.getNumFunction()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
		} else {
			sub = BigDecimal.ZERO;
		}

		result = result.subtract(sub);
		if (result.compareTo(Constants.SMALLER_VALUE) == -1) {
			result = BigDecimal.ZERO;

			while (indexResults < 17) {
				results[indexResults] = result;
				indexResults++;
			}
		}

		if (indexResults < 17 && moment == momentsFES[indexResults] * maxFES) {
			results[indexResults] = result;
			indexResults++;
		}
	}

	public abstract DoubleSolution search();

	public void initViewer() {
		viewer = new Viewer(getName() + " " + problem.getName(), 600, 600);
		imageComponent = new ImageComponent(600, 600);
		imageComponent.setImageBoundaries(problem.getLowerBound(0), problem.getLowerBound(1), problem.getUpperBound(0),
				problem.getUpperBound(1));
		imageComponent.drawFunction(problem);
	}

	@SuppressWarnings("unchecked")
	public void redraw(DoubleSolution bestMovement, List<DoubleSolution>... lists) {
		double coordx, coordy;

		imageComponent.updateBackGround();

		for (int i = 0; i < lists.length; i++) {
			for (DoubleSolution solution : lists[i]) {
				coordx = solution.getVariableValue(0).doubleValue();
				coordy = solution.getVariableValue(1).doubleValue();

				imageComponent.drawSquare(coordx, coordy, 7, i, i, i);
			}
		}

		coordx = bestMovement.getVariableValue(0).doubleValue();
		coordy = bestMovement.getVariableValue(1).doubleValue();

		imageComponent.drawSquare(coordx, coordy, 6, 1f, 0.501960784f, 0f);

		viewer.drawImage(imageComponent);
	}

	public void redraw(DoubleSolution bestMovement, DoubleSolution[]... lists) {
		double coordx, coordy;

		imageComponent.updateBackGround();

		for (int i = 0; i < lists.length; i++) {
			boolean color = i == 0 || i > 2;
			for (DoubleSolution solution : lists[i]) {
				coordx = solution.getVariableValue(0).doubleValue();
				coordy = solution.getVariableValue(1).doubleValue();

				if (!color) {
					imageComponent.drawSquare(coordx, coordy, 6, i - 1, i - 1, i - 1);
				} else if (i == 0) {
					imageComponent.drawSquare(coordx, coordy, 6, (float) 0.0, (float) 0.0, (float) 0.0);
					imageComponent.drawSquare(coordx, coordy, 4, (float) 0.3, (float) 0.3, (float) 0.3);
				} else {
					imageComponent.drawSquare(coordx, coordy, 6, (float) 1.0, (float) 1.0, (float) 1.0);
					imageComponent.drawSquare(coordx, coordy, 4, (float) 0.0, (float) 0.3, (float) 0.0);
				}
			}
		}

		coordx = bestMovement.getVariableValue(0).doubleValue();
		coordy = bestMovement.getVariableValue(1).doubleValue();

		imageComponent.drawSquare(coordx, coordy, 5, 1f, 0.501960784f, 0f);

		viewer.drawImage(imageComponent);
	}

	public void finishViewer() {
		viewer.dispose();
	}

	public void stepView() {
		while (!viewer.AnyKeyWasPressed()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public abstract String getSummary();

}
