package br.com.si.algorithm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;

import br.com.si.params.CmaEsParams;
import br.com.si.params.ProblemParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings({ "unchecked" })
public class ICMAES extends Algorithm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4881359862902157640L;

	public static final String name = "iCMAES";

	/**
	 * An initial search point to start searching from, or {@code null} if no
	 * initial search point is specified.
	 */
	private final double[] initialSearchPoint;

	/**
	 * If {@code true}, perform consistency checks to ensure CMA-ES remains
	 * numerically stable.
	 */
	private final boolean checkConsistency;

	/**
	 * Secondary comparison criteria for comparing population individuals with
	 * the same rank. If {@code null}, the default crowding distance metric is
	 * used.
	 */
	// private final FitnessEvaluator fitnessEvaluator; --> Variable Problem

	/**
	 * The number of iterations already performed.
	 */
	private int iteration;

	/**
	 * The number of iterations in which only the covariance diagonal is used.
	 * This enhancement helps speed up the algorithm when there are many
	 * decision variables. Set to {@code 0} to always use the full covariance
	 * matrix.
	 */
	private int diagonalIterations;

	/**
	 * Number of offspring selected for recombination.
	 */
	private int mu;

	/**
	 * Overall standard deviation.
	 */
	private double sigma;

	/**
	 * Variance-effectiveness.
	 */
	private double mueff;

	/**
	 * Learning rate.
	 */
	private double ccov;

	/**
	 * Learning rate when diagonal mode is active.
	 */
	private double ccovsep;

	/**
	 * Expectation of ||N(0, I)||.
	 */
	private double chiN;

	/**
	 * Step size cumulation parameter.
	 */
	private double cs;

	/**
	 * Cumulation parameter.
	 */
	private double cc;

	/**
	 * Damping for step size.
	 */
	private double damps;

	/**
	 * Last iteration were the eigenvalue decomposition was calculated.
	 */
	private int lastEigenupdate;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private DoubleSolution bestSolutionEver;
	private int counteval;
	private int state = -1;
	private boolean _viewActive;
	private boolean _stepView;
	private CmaEsParams _params;
	private int tolfunhist;
	private int tolfun;
	private int tolx;
	private int maxtol;
	private boolean isComplexity;

	private Random random;

	public ICMAES(Problem problem, CmaEsParams params, boolean viewActive, boolean stepView,
			double[] initialSearchPoint, boolean checkConsistency, boolean isComplexity) {
		super(problem);

		this.initialSearchPoint = initialSearchPoint;
		this.checkConsistency = checkConsistency;
		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);
		this.isComplexity = isComplexity;

		this.random = new Random();
	}

	@Override
	public void updateProgress() {
		counteval++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return counteval >= _params._maxObjectiveFunctionCalls || (!isComplexity && counteval > 0 && error == 0.0);
	}

	@Override
	public int getIterations() {
		return counteval;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	public void initProgress(int lambda, boolean xmeanInit, double[] xmean, double[] weights, double[] diagD,
			double[] pc, double[] ps, double[][] B, double[][] C) {
		int N = problem.getNumberOfVariables();
		// DoubleSolution prototypeSolution = problem.createSolution();
		// preInitChecks(prototypeSolution);

		// initialization
		if (sigma < 0) {
			sigma = _params._initStepSizeFactor;
		}

		if (diagonalIterations < 0) {
			diagonalIterations = 150 * N / lambda;
		}

		for (int i = 0; i < N; i++) {
			pc[i] = 0;
			ps[i] = 0;
			diagD[i] = 1;

			for (int j = 0; j < N; j++) {
				B[i][j] = 0;
			}

			for (int j = 0; j < i; j++) {
				C[i][j] = 0;
			}

			B[i][i] = 1;
			C[i][i] = diagD[i] * diagD[i];
		}

		// initialization of xmean
		if (xmeanInit) {
			if (initialSearchPoint == null) {
				for (int i = 0; i < N; i++) {
					double offset = sigma * diagD[i];
					double range = (problem.getUpperBound(i) - problem.getLowerBound(i) - 2 * sigma * diagD[i]);

					if (offset > 0.4 * (problem.getUpperBound(i) - problem.getLowerBound(i))) {
						offset = 0.4 * (problem.getUpperBound(i) - problem.getLowerBound(i));
						range = 0.2 * (problem.getUpperBound(i) - problem.getLowerBound(i));
					}

					xmean[i] = problem.getLowerBound(i) + offset + random.nextDouble() * range;
				}
			} else {
				for (int i = 0; i < N; i++) {
					xmean[i] = initialSearchPoint[i] + sigma * diagD[i] * random.nextGaussian();
				}
			}
		}

		// initialization of other parameters
		chiN = Math.sqrt(N) * (1.0 - 1.0 / (4.0 * N) + 1.0 / (21.0 * N * N));

		for (int i = 0; i < mu; i++) {
			weights[i] = Math.log(mu + 1) - Math.log(i + 1);
		}

		double sum = 0;
		for (int i = 0; i < weights.length; i++) {
			sum += weights[i];
		}

		for (int i = 0; i < mu; i++) {
			weights[i] /= sum;
		}

		double sumSq = 0;
		for (int i = 0; i < weights.length; i++) {
			sumSq += weights[i] * weights[i];
		}

		mueff = 1.0 / sumSq; // also called mucov

		if (cs < 0) {
			cs = (mueff + 2) / (N + mueff + 3);
		}

		if (damps < 0) {
			damps = (1 + 2 * Math.max(0, Math.sqrt((mueff - 1.0) / (N + 1)) - 1)) + cs;
		}

		if (cc < 0) {
			cc = 4.0 / (N + 4.0);
		}

		if (ccov < 0) {
			ccov = (1/mueff) * (2.0 / (N + 1.41) * (N + 1.41))
					+ (1 - (1.0 / mueff)) * Math.min(1, (2 * mueff - 1) / (mueff + (N + 2) * (N + 2)));
		}

		if (ccovsep < 0) {
			ccovsep = Math.min(1, ccov * (N + 1.5) / 3.0);
		}

		// postInitChecks();
		state = 0;
	}

	@Override
	public void initProgress() {
		super.initProgress();
		maxtol = 5;
		tolfunhist = 0;
		tolfun = 0;
		tolx = 0;
		bestSolutionEver = null;

		sigma = -1;
		diagonalIterations = -1;
		cs = -1;
		damps = -1;
		cc = -1;
		ccov = -1;
		ccovsep = -1;

		counteval = 0;
		iteration = 0;
	};

	@Override
	public DoubleSolution search() {
		double[] xmean, diagD, pc, ps, artmp, BDz, offdiag, archive, weights;
		double[][] B, C;
		DoubleSolution[] population;
		int newMu, lambda;
		int N = problem.getNumberOfVariables();
		boolean xmeanInit = true;

		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		initProgress();

		diagD = new double[N];
		pc = new double[N];
		ps = new double[N];
		B = new double[N][N];
		C = new double[N][N];
		artmp = new double[N];
		BDz = new double[N];
		offdiag = new double[N];
		xmean = new double[N];

		lambda = (int) (4.0 + Math.floor(_params._popSizeFactor * Math.log(N)));
		mu = (int) Math.floor(lambda / _params._parentSizeFactor);
		archive = new double[10 + (int) Math.ceil(3.0 * 10.0 * N / (double) lambda)];
		population = new DoubleSolution[lambda];
		weights = new double[mu];

		if (this._viewActive) {
			initViewer();
		}

		while (!isStoppingConditionReached()) {
			newMu = (int) Math.floor(lambda / _params._parentSizeFactor);
			if (newMu != mu) {
				// System.out.println("Change weights! it: " + counteval);
				mu = newMu;
				weights = null;
				weights = new double[mu];
			}

			initProgress(lambda, xmeanInit, xmean, weights, diagD, pc, ps, B, C);
			xmeanInit = false;

			while (!isStoppingConditionRestarts(archive, pc, C, population)) {
				if (population.length != lambda) {
					// System.out.println("Change lambda! it: " + counteval);
					population = null;
					population = new DoubleSolution[lambda];
				}
				samplePopulation(lambda, xmean, diagD, pc, offdiag, artmp, B, C, population);

				for (DoubleSolution solution : population) {
					problem.evaluate(solution);
					updateProgress();

					if (Utils.compare(solution, bestSolutionEver, problem.getOrder()) == -1) {
						bestSolutionEver = (DoubleSolution) solution.copy();

						if (problem.getName().contains("ProblemsCEC15")) {
							this.error = solution.getObjective() - problem.getNumFunction() * 100;

							if (this.error < this.zero) {
								this.error = 0.0;
							}
						}
					}

					setResult(new BigDecimal(bestSolutionEver.getObjective()), this.counteval,
							this._params._maxObjectiveFunctionCalls);
				}
				updateDistribution(xmean, weights, BDz, artmp, ps, diagD, pc, B, C, population);

				for (int i = archive.length - 1; i > 0; i--) {
					archive[i] = archive[i - 1];
				}
				archive[0] = population[0].getObjective();

				if (_viewActive) {
					redraw(bestSolutionEver, Arrays.asList(population));
					// System.out.println("Num calls: " + counteval);

					if (_stepView) {
						stepView();
					}
				}
			}
			// usedMB = Utils.bytesToMegabytes(rt.totalMemory() -
			// rt.freeMemory());
			// System.out.println("*" + usedMB + "MB -> it: " + iteration + ",
			// Best: " + bestSolutionEver.getObjective());

			lambda *= _params._iPOPFactor; /* needed for the restart */
		}

		if (_viewActive) {
			redraw(bestSolutionEver, Arrays.asList(population));

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}
		// usedMB = Utils.bytesToMegabytes(rt.totalMemory() - rt.freeMemory());
		// System.out.println("End -> Men: " + usedMB + "MB");

		return bestSolutionEver;
	}

	boolean isStoppingConditionRestarts(double[] archive, double[] pc, double[][] C, DoubleSolution[] population) {
		/* TOLFUN */
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		double value;
		int limit;
		BigDecimal range;

		if (iteration > 0 && state > 0) {
			limit = Math.min(iteration, archive.length);
			for (int i = 0; i < limit; i++) {
				value = archive[i];
				if (value > max) {
					max = value;
				}
				if (value < min) {
					min = value;
				}
			}

			for (int i = 0; i < population.length; i++) {
				value = population[i].getObjective();
				if (value > max) {
					max = value;
				}
				if (value < min) {
					min = value;
				}
			}

			range = new BigDecimal(Double.toString(max - min)).setScale(20, RoundingMode.HALF_UP);

			if (range.compareTo(_params._stopTolFun) < 1 && tolfun < maxtol) {
				tolfun++;
				// System.out.println("TOLFUN it: " + iteration);
				return true;
			}
		}

		/* TOLFUNHIST */
		max = Double.MIN_VALUE;
		min = Double.MAX_VALUE;
		if (iteration > archive.length - 1 && state > 0) {
			for (int i = 0; i < archive.length; i++) {
				value = archive[i];
				if (value > max) {
					max = value;
				}
				if (value < min) {
					min = value;
				}
			}

			range = new BigDecimal(Double.toString(max - min)).setScale(10, RoundingMode.HALF_UP);

			if (range.compareTo(_params._stopTolFunHist) < 1 && tolfunhist < maxtol) {
				tolfunhist++;
				// System.out.println("TOLFUNHIST it: " + iteration);
				return true;
			}
		}

		/* TOLX */
		max = 0;
		for (int i = 0; i < problem.getNumberOfVariables(); i++) {
			max += (_params._stopTolX.compareTo(new BigDecimal(Double.toString(sigma * Math.sqrt(C[i][i])))) > 0) ? 1
					: 0;
			max += (_params._stopTolX.compareTo(new BigDecimal(Double.toString(sigma * pc[i]))) > 0) ? 1 : 0;
		}
		if (Double.compare(max, 2.0 * problem.getNumberOfVariables()) == 0 && tolx < maxtol) {
			tolx++;
			// System.out.println("TOLX it: " + iteration);
			return true;
		}

		// System.out.println("Iteraction: " + counteval + "; " + (counteval >=
		// _params._maxObjectiveFunctionCalls));

		return counteval >= _params._maxObjectiveFunctionCalls;
	}

	/**
	 * Samples a new population.
	 */
	private void samplePopulation(int lambda, double[] xmean, double[] diagD, double[] pc, double[] offdiag,
			double[] artmp, double[][] B, double[][] C, DoubleSolution[] population) {
		boolean feasible = true;
		int N = problem.getNumberOfVariables();

		if ((iteration - lastEigenupdate) > 1.0 / ccov / N / 5.0) {
			eigendecomposition(diagD, offdiag, B, C);
		}

		if (checkConsistency) {
			testAndCorrectNumerics(lambda, diagD, pc, C, population);
		}

		// sample the distribution
		for (int i = 0; i < lambda; i++) {
			DoubleSolution solution = new DoubleSolution(problem.getNumberOfVariables(), problem.getOrder());

			if (diagonalIterations >= iteration) {
				// loop until a feasible solution is generated
				int trying = N * 10;
				do {
					feasible = true;

					for (int j = 0; j < N; j++) {
						double value = xmean[j] + sigma * diagD[j] * random.nextGaussian();

						if (trying < 0) {
							if (value < problem.getLowerBound(j)) {
								value = problem.getLowerBound(j);
							} else if (value > problem.getUpperBound(j)) {
								value = problem.getUpperBound(j);
							}
						}

						if (value < problem.getLowerBound(j) || value > problem.getUpperBound(j)) {
							trying--;
							feasible = false;
							break;
						}

						solution.setVariableValue(j, value);
					}
				} while (!feasible);
			} else {
				// loop until a feasible solution is generated
				int trying = N * 10;
				do {
					feasible = true;

					for (int j = 0; j < N; j++) {
						artmp[j] = diagD[j] * random.nextGaussian();
					}

					// add mutation (sigma * B * (D*z))
					for (int j = 0; j < N; j++) {
						double sum = 0.0;

						for (int k = 0; k < N; k++) {
							sum += B[j][k] * artmp[k];
						}

						double value = xmean[j] + sigma * sum;

						if (trying < 0) {
							if (value < problem.getLowerBound(j)) {
								value = problem.getLowerBound(j);
							} else if (value > problem.getUpperBound(j)) {
								value = problem.getUpperBound(j);
							}
						}

						if (value < problem.getLowerBound(j) || value > problem.getUpperBound(j)) {
							trying--;
							feasible = false;
							break;
						}

						solution.setVariableValue(j, value);
					}
				} while (!feasible);
			}

			population[i] = solution;
		}

		iteration++;
		state = 1;
	}

	/**
	 * Updates the internal parameters given the evaluated population.
	 */
	private void updateDistribution(double[] xmean, double[] weights, double[] BDz, double[] artmp, double[] ps,
			double[] diagD, double[] pc, double[][] B, double[][] C, DoubleSolution[] population) {
		int N = problem.getNumberOfVariables();
		double[] xold = Arrays.copyOf(xmean, xmean.length);

		// sort function values
		Arrays.sort(population);

		// calculate xmean and BDz
		for (int i = 0; i < N; i++) {
			xmean[i] = 0;

			for (int j = 0; j < mu; j++) {
				xmean[i] += weights[j] * population[j].getVariableValue(i).doubleValue();
			}

			BDz[i] = Math.sqrt(mueff) * (xmean[i] - xold[i]) / sigma;
		}

		// cumulation for sigma (ps) using B*z
		if (diagonalIterations >= iteration) {
			// given B=I we have B*z = z = D^-1 BDz
			for (int i = 0; i < N; i++) {
				ps[i] = (1.0 - cs) * ps[i] + Math.sqrt(cs * (2.0 - cs)) * BDz[i] / diagD[i];
			}
		} else {
			for (int i = 0; i < N; i++) {
				double sum = 0.0;

				for (int j = 0; j < N; j++) {
					sum += B[j][i] * BDz[j];
				}

				artmp[i] = sum / diagD[i];
			}

			for (int i = 0; i < N; i++) {
				double sum = 0.0;

				for (int j = 0; j < N; j++) {
					sum += B[i][j] * artmp[j];
				}

				ps[i] = (1.0 - cs) * ps[i] + Math.sqrt(cs * (2.0 - cs)) * sum;
			}
		}

		// calculate norm(ps)^2
		double psxps = 0;

		for (int i = 0; i < N; i++) {
			psxps += ps[i] * ps[i];
		}

		// cumulation for covariance matrix (pc) using B*D*z
		int hsig = 0;

		if (Math.sqrt(psxps) / Math.sqrt(1.0 - Math.pow(1.0 - cs, 2.0 * iteration)) / chiN < 1.4 + 2.0 / (N + 1)) {
			hsig = 1;
		}

		for (int i = 0; i < N; i++) {
			pc[i] = (1.0 - cc) * pc[i] + hsig * Math.sqrt(cc * (2.0 - cc)) * BDz[i];
		}

		// update of C
		for (int i = 0; i < N; i++) {
			for (int j = (diagonalIterations >= iteration ? i : 0); j <= i; j++) {
				C[i][j] = (1.0 - (diagonalIterations >= iteration ? ccovsep : ccov)) * C[i][j]
						+ ccov * (1.0 / mueff) * (pc[i] * pc[j] + (1 - hsig) * cc * (2.0 - cc) * C[i][j]);

				for (int k = 0; k < mu; k++) {
					C[i][j] += ccov * (1 - 1.0 / mueff) * weights[k]
							* (population[k].getVariableValue(i).doubleValue() - xold[i])
							* (population[k].getVariableValue(j).doubleValue() - xold[j]) / sigma / sigma;
				}
			}
		}

		// update of sigma
		sigma *= Math.exp(((Math.sqrt(psxps) / chiN) - 1) * cs / damps);

		state = 3;
	}

	/**
	 * Performs eigenvalue decomposition to update B and diagD.
	 */
	private void eigendecomposition(double[] diagD, double[] offdiag, double[][] B, double[][] C) {
		int N = problem.getNumberOfVariables();

		lastEigenupdate = iteration;

		if (diagonalIterations >= iteration) {
			for (int i = 0; i < N; i++) {
				diagD[i] = Math.sqrt(C[i][i]);
			}
		} else {
			// set B <- C
			for (int i = 0; i < N; i++) {
				for (int j = 0; j <= i; j++) {
					B[i][j] = B[j][i] = C[i][j];
				}
			}

			// eigenvalue decomposition
			if (offdiag == null) {
				offdiag = new double[N];
			} else {
				for (int i = 0; i < N; i++) {
					offdiag[i] = 0;
				}
			}
			Utils.tred2(N, B, diagD, offdiag);
			Utils.tql2(N, diagD, offdiag, B);

			if (checkConsistency) {
				checkEigenSystem(N, C, diagD, B);
			}

			// assign diagD to eigenvalue square roots
			for (int i = 0; i < N; i++) {
				if (diagD[i] < 0) { // numerical problem?
					System.err.println("an eigenvalue has become negative");
					diagD[i] = 0;
				}

				diagD[i] = Math.sqrt(diagD[i]);
			}
		}
	}

	/**
	 * Test and correct any numerical issues.
	 */
	private void testAndCorrectNumerics(int lambda, double[] diagD, double[] pc, double[][] C,
			DoubleSolution[] population) {
		// flat fitness, test is function values are identical
		if (population.length > 0) {
			Arrays.sort(population);

			if (population[0].getObjective()
					.compareTo(population[Math.min(lambda - 1, lambda / 2 + 1) - 1].getObjective()) == 0) {
				System.err.println("flat fitness landscape, consider reformulation of fitness, step size increased");
				sigma *= Math.exp(0.2 + cs / damps);
			}
		}

		// align (renormalize) scale C (and consequently sigma)
		double fac = 1.0;
		double max = Double.MIN_VALUE;
		double min = Double.MAX_VALUE;
		for (int i = 0; i < diagD.length; i++) {
			if (diagD[i] > max) {
				max = diagD[i];
			}
			if (diagD[i] < min) {
				min = diagD[i];
			}
		}

		if (max < 1e-6) {
			fac = 1.0 / max;
		} else if (min > 1e4) {
			fac = 1.0 / min;
		}

		if (fac != 1.0) {
			sigma /= fac;

			for (int i = 0; i < problem.getNumberOfVariables(); i++) {
				pc[i] *= fac;
				diagD[i] *= fac;

				for (int j = 0; j <= i; j++) {
					C[i][j] *= fac * fac;
				}
			}
		}
	}

	/**
	 * Exhaustive test of the output of the eigendecomposition. Needs O(n^3)
	 * operations.
	 * 
	 * @return the number of detected inaccuracies
	 */
	private static int checkEigenSystem(int N, double[][] C, double[] diag, double[][] Q) {
		/* compute Q diag Q^T and Q Q^T to check */
		int i;
		int j;
		int k;
		int res = 0;
		double cc;
		double dd;

		for (i = 0; i < N; ++i) {
			for (j = 0; j < N; ++j) {
				for (cc = 0., dd = 0., k = 0; k < N; ++k) {
					cc += diag[k] * Q[i][k] * Q[j][k];
					dd += Q[i][k] * Q[j][k];
				}
				/* check here, is the normalization the right one? */
				if (Math.abs(cc - C[i > j ? i : j][i > j ? j : i]) / Math.sqrt(C[i][i] * C[j][j]) > 1e-10
						&& Math.abs(cc - C[i > j ? i : j][i > j ? j
								: i]) > 1e-9) { /* quite large */
					System.err.println("imprecise result detected " + i + " " + j + " " + cc + " "
							+ C[i > j ? i : j][i > j ? j : i] + " " + (cc - C[i > j ? i : j][i > j ? j : i]));
					++res;
				}
				if (Math.abs(dd - (i == j ? 1 : 0)) > 1e-10) {
					System.err.println("imprecise result detected (Q not orthog.) " + i + " " + j + " " + dd);
					++res;
				}
			}
		}
		return res;
	}

	@Override
	public String getSummary() {
		return " - " + counteval;
	}
}
