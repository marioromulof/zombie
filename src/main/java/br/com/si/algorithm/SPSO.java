package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import br.com.si.params.ProblemParams;
import br.com.si.params.PsoParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings("unchecked")
public class SPSO extends Algorithm {

	// private double c1Max;
	// private double c1Min;
	// private double c2Max;
	// private double c2Min;
	// private double r1Max;
	// private double r1Min;
	// private double r2Max;
	// private double r2Min;
	// private double weight;
	// private double speedMax;
	// private double speedMin;
	// private int swarmSize;
	// private int maxIterations;
	// private double minError;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5672160135036757985L;

	public static final String name = "PsoAlgorithm";

	private int currentIteration;
	// private int indexBest;
	private int[][] matrixL;
	private boolean initLinks;
	private double errorPrev;
	private double error;
	private double errorE;
	private double zero = Math.pow(10.0, -8);

	private DoubleSolution[] localBest;
	private DoubleSolution globalBest;

	private Random random;

	private double[][] speed;

	private boolean _viewActive;
	private boolean _stepView;
	private boolean isComplexity;
	private PsoParams _params;

	public SPSO(Problem problem, PsoParams params, boolean viewActive, boolean stepView, boolean isComplexity) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);
		this.isComplexity = isComplexity;

		// this.swarmSize = Parameters.PSO_SWARM_SIZE;
		// this.maxIterations = problem.getNumberOfVariables() *
		// Parameters.MAX_FES;
		// this.minError = Parameters.MIN_ERROR;
		//
		// this.r1Max = Parameters.PSO_R1_MAX;
		// this.r1Min = Parameters.PSO_R1_MIN;
		// this.r2Max = Parameters.PSO_R2_MAX;
		// this.r2Min = Parameters.PSO_R2_MIN;
		// this.c1Max = Parameters.PSO_C1_MAX;
		// this.c1Min = Parameters.PSO_C1_MIN;
		// this.c2Max = Parameters.PSO_C2_MAX;
		// this.c2Min = Parameters.PSO_C2_MIN;
		// this.weight = Parameters.PSO_WEIGHT;
		// this.speedMax = Parameters.PSO_SPEED_MAX;
		// this.speedMin = Parameters.PSO_SPEED_MIN;

		random = new Random();

		this.localBest = new DoubleSolution[_params._swarmSize];
		this.speed = new double[_params._swarmSize][problem.getNumberOfVariables()];
		this.matrixL = new int[_params._swarmSize][_params._swarmSize];
	}

	@Override
	public void initProgress() {
		super.initProgress();

		globalBest = null;
		currentIteration = 0;
		initLinks = true;
	}

	@Override
	public void updateProgress() {
		currentIteration++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return currentIteration >= _params._maxObjectiveFunctionCalls
				|| (!isComplexity && currentIteration > 0 && errorE == 0.0);
	}

	@Override
	public DoubleSolution search() {
		// Generate Seed
		setSeed(Calendar.getInstance().getTimeInMillis());

		int s, m, g;
		double value;
		double[] px = new double[problem.getNumberOfVariables()];
		double[] pxl = new double[problem.getNumberOfVariables()];
		DoubleSolution gi = new DoubleSolution(problem.getNumberOfVariables());

		initProgress();
		List<DoubleSolution> swarm;
		DoubleSolution particle;

		swarm = createInitialSwarm();
		initializeVelocity(swarm);

		swarm = evaluateSwarm(swarm);
		initializeParticlesMemory(swarm);

		errorPrev = globalBest.getObjective() - 100 * problem.getNumFunction();

		if (this._viewActive) {
			initViewer();
		}

		for (s = 0; s < _params._swarmSize; s++) {
			matrixL[s][s] = 1;
		}
		while (!isStoppingConditionReached()) {
			if (initLinks) {
				for (s = 0; s < _params._swarmSize; s++) {
					for (m = 0; m < _params._swarmSize; m++) {
						if (m == s) {
							continue;
						}

						if (random.nextDouble() < _params._p) {
							matrixL[m][s] = 1;
						} else {
							matrixL[m][s] = 0;
						}
					}
				}
			}

			// Loop on particles
			for (s = 0; s < _params._swarmSize; s++) {
				if (_viewActive) {
					redraw(globalBest, swarm);

					if (_stepView) {
						stepView();
					}
				}

				// ... find the first informant
				int s1 = 0;
				while (matrixL[s1][s] == 0 && s1 < _params._swarmSize) {
					s1++;
				}

				g = s1;
				for (m = s1; m < _params._swarmSize; m++) {
					if (matrixL[m][s] == 1
							&& localBest[m].getObjective().compareTo(localBest[g].getObjective()) == -1) {
						g = m;
					}
				}

				particle = (DoubleSolution) swarm.get(s).copy();
				for (int d = 0; d < problem.getNumberOfVariables(); d++) {
					// define a point p' on x-p, beyond p
					value = particle.getVariableValue(d)
							+ _params._c * (localBest[s].getVariableValue(d) - particle.getVariableValue(d));
					px[d] = value;

					// ... define a point g' on x-g, beyond g
					value = particle.getVariableValue(d)
							+ _params._c * (localBest[g].getVariableValue(d) - particle.getVariableValue(d));
					pxl[d] = value;
				}

				// If the best informant is the particle itself, define the
				// gravity center G as the middle of x-p'

				if (g == s) {
					for (int d = 0; d < problem.getNumberOfVariables(); d++) {
						gi.setVariableValue(d, (particle.getVariableValue(d) + px[d]) * 0.5);
					}
				} else { // % Usual way to define G
					for (int d = 0; d < problem.getNumberOfVariables(); d++) {
						gi.setVariableValue(d, (particle.getVariableValue(d) + px[d] + pxl[d]) / 3.0);
					}

				}

				value = Utils.getEuclideanDistance(particle, gi);
				Utils.getRandomPoint(gi, value, random);

				// New "velocity" and update position
				for (int d = 0; d < problem.getNumberOfVariables(); d++) {
					value = particle.getVariableValue(d);
					speed[s][d] = _params._w * speed[s][d] + gi.getVariableValue(d) - value;

					value += speed[s][d];

					if (value < problem.getLowerBound(d)) {
						value = problem.getLowerBound(d);
						speed[s][d] *= -0.5;
					} else if (value > problem.getUpperBound(d)) {
						value = problem.getUpperBound(d);
						speed[s][d] *= -0.5;
					}

					particle.setVariableValue(d, value);
				}

				problem.evaluate(particle);
				swarm.set(s, (DoubleSolution) particle.copy());
				updateParticlesMemory(swarm, s);

				// Check if finished
				error = globalBest.getObjective() - 100 * problem.getNumFunction();

				if (error < errorPrev) { // Improvement of the global best
					initLinks = false;
				} else { // No global improvement
					initLinks = true; // Information links will be reinitialized
				}

				errorPrev = error;
			}
			// updateVelocity(swarm);
			// updatePosition(swarm);
			// swarm = evaluateSwarm(swarm);
			// updateParticlesMemory(swarm);
		}

		if (_viewActive) {
			redraw(globalBest, swarm);

			if (_stepView) {
				stepView();
			}

			finishViewer();
		}

		return globalBest;
	}

	private void updateParticlesMemory(List<DoubleSolution> swarm, int position) {
		updateProgress();

		int flag = Utils.compare(swarm.get(position), localBest[position], problem.getOrder());
		if (flag == -1) {
			localBest[position] = (DoubleSolution) swarm.get(position).copy();
			if (Utils.compare(localBest[position], globalBest, problem.getOrder()) == -1) {
				// indexBest = position;
				globalBest = (DoubleSolution) localBest[position].copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.errorE = localBest[position].getObjective() - problem.getNumFunction() * 100;

					if (this.errorE < this.zero) {
						this.errorE = 0.0;
					}
				}
			}
		}

		setResult(new BigDecimal(globalBest.getObjective()), this.currentIteration,
				this._params._maxObjectiveFunctionCalls);
	}

	// private void updatePosition(List<DoubleSolution> swarm) {
	// for (int i = 0; i < _params._swarmSize; i++) {
	// DoubleSolution particle = swarm.get(i);
	// for (int j = 0; j < particle.getNumberOfVariables(); j++) {
	// double value = particle.getVariableValue(j) + speed[i][j];
	// // double value = Math.round((
	// // particle.getVariableValue(j) + speed[i][j]) * 100.0);
	// // value /= 100.0;
	//
	// particle.setVariableValue(j, value);
	// }
	// problem.simpleBounds(particle);
	// }
	// }

	// private void updateVelocity(List<DoubleSolution> swarm) {
	// double r1, r2, c1, c2;
	//
	// for (int i = 0; i < swarm.size(); i++) {
	// DoubleSolution particle = (DoubleSolution) swarm.get(i).copy();
	// DoubleSolution bestParticle = (DoubleSolution) localBest[i].copy();
	//
	// r1 = _params._R1Min + random.nextDouble() * (_params._R1Max -
	// _params._R1Min);
	// r2 = _params._R2Min + random.nextDouble() * (_params._R2Max -
	// _params._R2Min);
	// c1 = _params._C1Min + random.nextDouble() * (_params._C1Max -
	// _params._C1Min);
	// c2 = _params._C2Min + random.nextDouble() * (_params._C2Max -
	// _params._C2Min);
	//
	// double value1, value2;
	// for (int var = 0; var < particle.getNumberOfVariables(); var++) {
	// value1 = bestParticle.getVariableValue(var) -
	// particle.getVariableValue(var);
	// value2 = globalBest.getVariableValue(var) -
	// particle.getVariableValue(var);
	//
	// speed[i][var] = (_params._weight * speed[i][var]) + (c1 * r1 * value1) +
	// (c2 * r2 * value2);
	//
	// // speed[i][var] = clamp(speed[i][var], _params._speedMin,
	// // _params._speedMax);
	// }
	// }
	// }

	// private double clamp(double value, double min, double max) {
	// value = Math.max(value, min);
	// value = Math.min(value, max);
	// return value;
	// }

	private void initializeVelocity(List<DoubleSolution> swarm) {
		double lb, ub;
		for (int i = 0; i < swarm.size(); i++) {
			for (int j = 0; j < problem.getNumberOfVariables(); j++) {
				speed[i][j] = 0.0;
				lb = problem.getLowerBound(j) - swarm.get(i).getVariableValue(j);
				ub = problem.getUpperBound(j) - swarm.get(i).getVariableValue(j);
				speed[i][j] = lb + this.random.nextDouble() * (ub - lb);
			}
		}
	}

	private void initializeParticlesMemory(List<DoubleSolution> swarm) {
		for (int i = 0; i < swarm.size(); i++) {
			updateProgress();

			localBest[i] = (DoubleSolution) swarm.get(i).copy();
			if (Utils.compare(localBest[i], globalBest, problem.getOrder()) == -1) {
				// indexBest = i;
				globalBest = (DoubleSolution) localBest[i].copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.errorE = localBest[i].getObjective() - problem.getNumFunction() * 100;

					if (this.errorE < this.zero) {
						this.errorE = 0.0;
					}
				}
			}

			setResult(new BigDecimal(this.globalBest.getObjective()), this.currentIteration,
					this._params._maxObjectiveFunctionCalls);
		}
	}

	private List<DoubleSolution> evaluateSwarm(List<DoubleSolution> swarm) {
		for (DoubleSolution solution : swarm) {
			problem.evaluate(solution);
		}

		return swarm;
	}

	private List<DoubleSolution> createInitialSwarm() {
		List<DoubleSolution> swarm = new ArrayList<>(_params._swarmSize);

		for (int i = 0; i < _params._swarmSize; i++) {
			swarm.add(DoubleSolution.createSolution(problem, random));
		}

		return swarm;
	}

	@Override
	public int getIterations() {
		return currentIteration;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this._params;
	}

	@Override
	public String getSummary() {
		return " - " + currentIteration;
	}

}
