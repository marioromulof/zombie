package br.com.si.algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.math3.util.FastMath;

import br.com.si.actor.Human;
import br.com.si.actor.Zombie;
import br.com.si.params.ProblemParams;
import br.com.si.params.ZombieParams;
import br.com.si.problem.OptimizeParameters;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.DataReader;
import br.com.si.util.Ordering;
import br.com.si.util.Utils;

public class ZombieEDA extends Algorithm {

	private static final long serialVersionUID = 3375689358592926614L;

	public static final String name = "ZombieEDA";

	private int nEsc;
	private int nAlone;
	private int nGroup;

	private int nDim;
	private int nCalls;
	private DoubleSolution bestMovement;
	private int indexUpdateH;
	private int indexUpdateZ;
	private boolean earlyStop;
	private boolean noiseHandling;
	private int countstagnation;
	private boolean failedIteration;
	private int sizeZombies;
	private int sizeHumans;
	private double error;
	private double zero = FastMath.pow(10.0, -8);
	private double[] memoryHc;
	private double[] memoryHn;
	private double[] memoryZc;
	private double[] memoryZn;
	private int[] qiGH;
	private int[] siGH;
	private double[] quGH;
	private int[] qiGZ;
	private int[] siGZ;
	private double[] quGZ;
	private int sizeMemoryH;
	private int sizeMemoryZ;
	private int indexMemoryHc;
	private int indexMemoryHn;
	private int indexMemoryZc;
	private int indexMemoryZn;

	private Random random;

	private boolean viewActive;
	private boolean stepView;
	private boolean isComplexity;
	private ZombieParams params;
	private Human[] humans;

	private String fileName;
	private long seed;
	private boolean fileExists;
	private int countStep = 0;
	private int idxHuman = 0;
	private int fase;

	public ZombieEDA(Problem problem, ZombieParams params, boolean viewActive, boolean stepView, boolean earlyStop,
			boolean noiseHandling, boolean isComplexity) {
		super(problem);

		this.nDim = problem.getNumberOfVariables();

		this.viewActive = viewActive && nDim == 2;
		this.stepView = stepView && nDim == 2;
		this.params = params;
		this.notExecuteInThread = (this.viewActive || this.stepView);
		this.earlyStop = earlyStop;
		this.noiseHandling = noiseHandling;
		this.isComplexity = isComplexity;

		if (problem.getName().contains("OptParam")) {
			this.fileName = "input_data/results/PARAMS_" + name.concat("_opt").toUpperCase() + "_"
					+ problem.getNumFunction() + "_" + ((OptimizeParameters) problem).getNumDim() + ".txt";
			this.fileExists = Files.exists(Paths.get(fileName));
		}

		this.random = new Random();
	} // ZombieEDA

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return this.params;
	}

	@Override
	public void initProgress() {
		super.initProgress();

		error = Double.MAX_VALUE;
		nEsc = 0;
		nAlone = 0;
		nGroup = 0;
		this.bestMovement = null;

		this.nCalls = 0;

		sizeMemoryH = params.maxHumanPop;
		sizeMemoryZ = params.zombiePopSize;
		memoryHc = new double[sizeMemoryH];
		memoryHn = new double[sizeMemoryH];
		memoryZc = new double[sizeMemoryZ];
		memoryZn = new double[sizeMemoryZ];

		for (int i = 0; i < sizeMemoryH; i++) {
			memoryHc[i] = params.hCinit;
			memoryHn[i] = params.hNinit;
		}

		for (int i = 0; i < sizeMemoryZ; i++) {
			memoryZc[i] = params.zCinit;
			memoryZn[i] = params.zNinit;
		}

		indexMemoryHc = 0;
		indexMemoryHn = 0;
		indexMemoryZc = 0;
		indexMemoryZn = 0;

		qiGH = new int[sizeMemoryH];
		siGH = new int[sizeMemoryH];
		quGH = new double[2];
		qiGZ = new int[sizeMemoryZ];
		siGZ = new int[sizeMemoryZ];
		quGZ = new double[2];

		indexUpdateH = 1;
		indexUpdateZ = 1;
	} // initProgress

	@Override
	public void updateProgress() {
		this.nCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		boolean outofmaxfunevals = this.nCalls >= params._maxObjectiveFunctionCalls;

		if (earlyStop) {
			Arrays.sort(humans);
			boolean reachftarget = humans[0].getSolution().getObjective() <= Double.NEGATIVE_INFINITY;

			Double[][] x = new Double[humans.length][];
			double[] f = new double[humans.length];
			for (int h = 0; h < humans.length; h++) {
				x[h] = humans[h].getSolution().getVariables();
				f[h] = humans[h].getSolution().getObjective();
			}
			double[] tolX = Utils.mean(x);
			double[] stdX = Utils.standardDeviation(x);
			for (int h = 0; h < nDim; h++) {
				tolX[h] = 10.0 * Math.ulp(tolX[h]);
			}
			boolean solutionconvergence = stdX[0] <= tolX[0];
			for (int h = 1; h < nDim; h++) {
				solutionconvergence = solutionconvergence && stdX[h] <= tolX[h];
			}

			double tolF = 10.0 * Math.ulp(Utils.mean(f));
			double stdF = Utils.standardDeviation(f);
			boolean functionvalueconvergence = stdF <= tolF;
			boolean stagnation = countstagnation >= Integer.MAX_VALUE;

			return outofmaxfunevals || reachftarget || solutionconvergence || functionvalueconvergence || stagnation;
		}
		boolean result = (!isComplexity && nCalls > 0 && error == 0.0);
		return outofmaxfunevals || result;
	} // isStoppingConditionReached

	@Override
	public int getIterations() {
		return this.nCalls;
	}

	@Override
	public DoubleSolution search() {
		int idx;
		humans = new Human[params.maxHumanPop];
		Zombie[] zombies;

		initProgress();
		if (fileExists) {
			try {
				Scanner scan = new Scanner(new File(fileName));
				scan.useDelimiter(System.getProperty("line.separator"));

				updateCalls(scan.nextLine().split(" "));

				int size = updateIndexs(scan.nextLine().split(" "));
				setSeed(seed);

				updateVect(scan.nextLine().split(" "), memoryHc);
				updateVect(scan.nextLine().split(" "), memoryHn);
				updateVect(scan.nextLine().split(" "), memoryZc);
				updateVect(scan.nextLine().split(" "), memoryZn);
				updateVect(scan.nextLine().split(" "), qiGH);
				updateVect(scan.nextLine().split(" "), siGH);
				updateVect(scan.nextLine().split(" "), quGH);
				updateVect(scan.nextLine().split(" "), qiGZ);
				updateVect(scan.nextLine().split(" "), siGZ);
				updateVect(scan.nextLine().split(" "), quGZ);

				bestMovement = new DoubleSolution(problem.getNumberOfVariables());
				updateSolution(scan.nextLine().split(" "), bestMovement, 0);

				for (int i = 0; i < humans.length; i++) {
					humans[i] = new Human(new DoubleSolution(problem.getNumberOfVariables()), 0, 0);
					updateHuman(scan.nextLine().split(" "), humans[i]);
				}

				if (fase == 1) {
					for (idx = countStep; idx < params.humanStepsPerZombieStep
							&& !isStoppingConditionReached(); idx++) {
						moveHumans(null, idx);
						indexUpdateH = getIndexUnsuccessful(siGH, qiGH, humans.length, quGH, params.hQ, indexUpdateH);

						if (idx + 1 < params.humanStepsPerZombieStep) {
							saveDataOpt(null, (idx + 1), 0);
						}
					}
					countStep = 0;
				}

				zombies = new Zombie[size];
				if (scan.hasNextLine()) {
					for (int i = 0; i < size; i++) {
						zombies[i] = new Zombie(problem.getNumberOfVariables(), 0, problem.getOrder());
						updateZombie(scan.nextLine().split(" "), zombies[i]);
					}
				} else {
					// initialize zombie pop
					for (idx = 0; idx < params.zombiePopSize; idx++) {
						Zombie z = new Zombie(nDim, 1, problem.getOrder());
						z.setSolution(DoubleSolution.createSolution(problem, random).getVariables());
						zombies[idx] = z;

						qiGZ[idx] = 0;
						siGZ[idx] = 0;
					}
				}
				scan.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			// Generate Seed
			seed = Calendar.getInstance().getTimeInMillis();
			setSeed(seed);

			fase = 1;
			zombies = new Zombie[params.zombiePopSize];
			if (this.viewActive) {
				initViewer();
			}

			// initialize human pop
			for (idx = 0; idx < params.maxHumanPop; idx++) {
				DoubleSolution solution = DoubleSolution.createSolution(problem, random);
				getMeritEvaluation(solution);
				humans[idx] = new Human(solution, random.nextDouble(), params.humanMaxFear);
				qiGH[idx] = 0;
				siGH[idx] = 0;
			}

			for (idx = 0; idx < params.humanStepsPerZombieStep && !isStoppingConditionReached(); idx++) {
				if (viewActive) {
					DoubleSolution[] humanSolutions = new DoubleSolution[humans.length];
					DoubleSolution[] human2Solutions = new DoubleSolution[humans.length];
					DoubleSolution[] zombieSolutions = new DoubleSolution[0];
					DoubleSolution[] zombie2Solutions = new DoubleSolution[0];

					int i = 0;
					int z = 0;
					for (Human human : humans) {
						if (human.isAdventure()) {
							human2Solutions[z] = human.getSolution();
							z++;
						} else {
							humanSolutions[i] = human.getSolution();
							i++;
						}
					}
					humanSolutions = Arrays.copyOf(humanSolutions, i);
					human2Solutions = Arrays.copyOf(human2Solutions, z);

					redraw(bestMovement, zombie2Solutions, zombieSolutions, humanSolutions, human2Solutions);

					if (stepView) {
						stepView();
					}
				}
				moveHumans(null, idx);
				indexUpdateH = getIndexUnsuccessful(siGH, qiGH, humans.length, quGH, params.hQ, indexUpdateH);

				if (noiseHandling && (idx + 1) < params.humanStepsPerZombieStep) {
					saveDataOpt(null, (idx + 1), 0);
				}
			}

			// initialize zombie pop
			for (idx = 0; idx < params.zombiePopSize; idx++) {
				Zombie z = new Zombie(nDim, 1, problem.getOrder());
				z.setSolution(DoubleSolution.createSolution(problem, random).getVariables());
				zombies[idx] = z;

				qiGZ[idx] = 0;
				siGZ[idx] = 0;
			}
		}

		fase = 2;
		if (noiseHandling) {
			saveDataOpt(zombies, 0, 0);
		}

		while (!isStoppingConditionReached()) {
			for (idx = countStep; idx < params.humanStepsPerZombieStep; idx++) {
				if (viewActive) {
					DoubleSolution[] humanSolutions = new DoubleSolution[humans.length];
					DoubleSolution[] human2Solutions = new DoubleSolution[humans.length];
					DoubleSolution[] zombieSolutions = new DoubleSolution[params.zombiePopSize];
					DoubleSolution[] zombie2Solutions = new DoubleSolution[zombies.length - params.zombiePopSize];

					int i = 0;
					int z = 0;
					for (Human human : humans) {
						if (human.isAdventure()) {
							human2Solutions[z] = human.getSolution();
							z++;
						} else {
							humanSolutions[i] = human.getSolution();
							i++;
						}
					}
					humanSolutions = Arrays.copyOf(humanSolutions, i);
					human2Solutions = Arrays.copyOf(human2Solutions, z);

					z = 0;
					int hz = 0;
					for (i = 0; i < zombies.length; i++) {
						if (zombies[i].isChaser()) {
							zombieSolutions[z] = zombies[i].getSolution();
							z++;
						} else {
							zombie2Solutions[hz] = zombies[i].getSolution();
							hz++;
						}
					}

					redraw(bestMovement, zombie2Solutions, zombieSolutions, humanSolutions, human2Solutions);

					if (stepView) {
						stepView();
					}
				}

				moveHumans(zombies, idx);
				indexUpdateH = getIndexUnsuccessful(siGH, qiGH, humans.length, quGH, params.hQ, indexUpdateH);

				if (noiseHandling && idx + 1 < params.humanStepsPerZombieStep) {
					saveDataOpt(zombies, (idx + 1), 0);
				}
			}
			countStep = 0;

			List<Zombie> newZombies = moveZombies(zombies);
			if (newZombies.size() > 0) {
				idx = zombies.length + newZombies.size();
				zombies = Arrays.copyOf(zombies, idx);

				idx = zombies.length - newZombies.size();
				for (Zombie z : newZombies) {
					zombies[idx++] = z;
				}
			}

			indexUpdateZ = getIndexUnsuccessful(siGZ, qiGZ, params.zombiePopSize, quGZ, params.zQ, indexUpdateZ);

			idx = 0;
			for (Human h : humans) {
				if (h != null) {
					humans[idx++] = h;
				}
			}
			if (idx != humans.length) {
				for (int i = idx; i < humans.length; i++) {
					DoubleSolution solution = DoubleSolution.createSolution(problem, random);
					getMeritEvaluation(solution);
					humans[i] = new Human(solution, random.nextDouble(), params.humanMaxFear);
				}
			}
			Arrays.sort(humans);

			sizeZombies = zombies.length;
			sizeHumans = humans.length;

			if (noiseHandling) {
				saveDataOpt(zombies, 0, 0);
			}
		}

		if (viewActive) {
			DoubleSolution[] humanSolutions = new DoubleSolution[humans.length];
			DoubleSolution[] zombieSolutions = new DoubleSolution[zombies.length];

			int i = 0;
			for (Human human : humans) {
				humanSolutions[i] = human.getSolution();
				i++;
			}

			i = 0;
			for (Zombie zombie : zombies) {
				zombieSolutions[i] = zombie.getSolution();
				i++;
			}

			redraw(bestMovement, zombieSolutions, humanSolutions);

			if (stepView) {
				stepView();
			}

			finishViewer();
		}

		return this.bestMovement;
	} // search

	private double getMeritEvaluation(DoubleSolution solution) {
		this.problem.evaluate(solution);
		updateProgress();

		if (Utils.compare(solution, this.bestMovement, problem.getOrder()) == -1) {
			this.bestMovement = (DoubleSolution) solution.copy();
			this.error = getZero(problem.getError(solution.getObjective()));
		}

		setResult(BigDecimal.valueOf(this.bestMovement.getObjective()), this.nCalls,
				this.params._maxObjectiveFunctionCalls);

		return solution.getObjective();
	} // getMeritEvaluation

	private double getZero(double erro) {
		if (FastMath.abs(erro) < zero) {
			return 0.0;
		}
		return erro;
	} // getZero

	private void moveHumans(Zombie[] zombies, int step) {
		double oldEvaluation, newEvaluation, oldDistance, newDistance;
		boolean escape, alone, lowSensitivity;
		int selected;
		int[] idx = null;
		boolean[] groupsHumanMoved = new boolean[humans.length];
		DoubleSolution humanSolution;
		double[] hC = new double[humans.length];
		double[] hN = new double[humans.length];
		double[] diff = new double[humans.length];
		double[] shC = new double[humans.length];
		double[] shN = new double[humans.length];
		String fileName2 = null;
		if (noiseHandling) {
			fileName2 = fileName.substring(0, fileName.indexOf('.')) + ".2.txt";
		}

		failedIteration = true;
		if (idxHuman == 0) {
			Arrays.sort(humans);
			updateFactorsCN(humans.length, memoryHc, params.hCstd, memoryHn, params.hNstd, hC, hN, diff, shC, shN);
		} else {
			try {
				Scanner scan = new Scanner(new File(fileName2));
				scan.useDelimiter(System.getProperty("line.separator"));

				updateVect(scan.nextLine().split(" "), groupsHumanMoved);
				updateVect(scan.nextLine().split(" "), hC);
				updateVect(scan.nextLine().split(" "), hN);
				updateVect(scan.nextLine().split(" "), diff);
				updateVect(scan.nextLine().split(" "), shC);
				updateVect(scan.nextLine().split(" "), shN);

				scan.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		for (int i = idxHuman; i < humans.length; i++) {
			if (groupsHumanMoved[i]) {
				continue;
			}

			alone = false;
			escape = false;
			Human human = humans[i];
			humanSolution = (DoubleSolution) human.getSolution().copy();

			lowSensitivity = random.nextDouble() > human.getFear();
			oldEvaluation = humanSolution.getObjective();
			oldDistance = getMinDistanceToZombie(humanSolution, human.getFear(), hN[i], zombies, lowSensitivity);

			if (oldDistance < Double.MAX_VALUE) {
				escape = true;
				groupsHumanMoved[i] = true;
				getEscapeMovement(humanSolution, human.getSolution(), hC[i]);

			} else {
				double maxDistance = params.maxDistance;
				if (lowSensitivity) {
					maxDistance *= (FastMath.log(indexUpdateH) + hN[i] + (1 - human.getFear()));
				} else {
					maxDistance *= (FastMath.log(indexUpdateH) + hN[i]);
				}
				idx = getHumansNearby(i, groupsHumanMoved, maxDistance);

				if (idx.length == 1) {
					alone = true;
					groupsHumanMoved[i] = true;
					getAloneMovement(i, humanSolution, human.getSolution(), hC[i]);

				} else {
					Human[] pop = new Human[idx.length];
					selected = 0;
					for (int id : idx) {
						pop[selected++] = humans[id];
						groupsHumanMoved[id] = true;
					}

					Arrays.sort(pop);
					reOrderIdxArray(idx, pop, humans);
					selected = random.nextInt(idx.length);

					DoubleSolution best = pop[0].getSolution();
					boolean bestChoose = false;
					if (Utils.getEuclideanDistance(best, bestMovement) < maxDistance) {
						best = bestMovement;
						bestChoose = true;
					}

					getGroupMovement(pop, idx, zombies, best, hC, hN, diff, shC, shN, humans.length, bestChoose);
				}
			}

			if (escape || alone) {
				if (!hasMovement(humanSolution, human.getSolution())) {
					newEvaluation = human.getSolution().getObjective();
				} else {
					newEvaluation = getMeritEvaluation(humanSolution);
				}

				if (noiseHandling && qiGH[i] > params.hQ) {
					oldEvaluation = getMeritEvaluation(human.getSolution());
					oldDistance = getMinDistanceToZombie(humanSolution, human.getFear(), hN[i], zombies,
							lowSensitivity);
				}

				boolean flag = newEvaluation <= oldEvaluation;

				newDistance = getMinDistanceToZombie(humanSolution, human.getFear(), hN[i], zombies, lowSensitivity);

				oldEvaluation += human.getFear() > 0 ? (1.0 / human.getFear() / oldDistance) : (1.0 / oldDistance);
				newEvaluation += human.getFear() > 0 ? (1.0 / human.getFear() / newDistance) : (1.0 / newDistance);

				flag = evaluateQualityLocation(i, oldEvaluation, newEvaluation, flag, !lowSensitivity && escape, shC,
						hC[i], shN, hN[i], diff);

				if (flag) {
					human.setSolution((DoubleSolution) humanSolution.copy());
				}
			}

			if (noiseHandling && (i + 1) < humans.length) {
				saveDataOpt(zombies, step, i + 1);

				DataReader.saveOverwriteData(fileName2, Utils.getStringVectorClean(groupsHumanMoved));
				DataReader.saveData(fileName2, Utils.getStringVectorClean(hC));
				DataReader.saveData(fileName2, Utils.getStringVectorClean(hN));
				DataReader.saveData(fileName2, Utils.getStringVectorClean(diff));
				DataReader.saveData(fileName2, Utils.getStringVectorClean(shC));
				DataReader.saveData(fileName2, Utils.getStringVectorClean(shN));
			}
		}

		if (failedIteration) {
			countstagnation++;
		} else {
			countstagnation = 0;
		}

		// update memory S and L
		if (diff[0] != -1.0) {
			updateMemory(diff, shC, memoryHc, indexMemoryHc);

			indexMemoryHc++;
			if (indexMemoryHc >= sizeMemoryH) {
				indexMemoryHc = 0;
			}

			updateMemory(diff, shN, memoryHn, indexMemoryHn);

			indexMemoryHn++;
			if (indexMemoryHn >= sizeMemoryH) {
				indexMemoryHn = 0;
			}
		}

		idxHuman = 0;
	} // moveHumans

	private void updateFactorsCN(int size, double[] memoryC, double stdC, double[] memoryN, double stdN, double[] c,
			double[] n, double[] diff, double[] sC, double[] sN) {
		int selected;
		for (int i = 0; i < size; i++) {
			selected = random.nextInt(memoryC.length);
			c[i] = Utils.cauchyG(memoryC[selected], stdC, random, 0.0, Double.POSITIVE_INFINITY);

			if (c[i] > 1) {
				c[i] = 1.0;
			}

			if (n != null && sN != null) {
				selected = random.nextInt(memoryN.length);
				n[i] = Utils.gauss(memoryN[selected], stdN, random);

				if (n[i] > 1.0) {
					n[i] = 1.0;
				} else if (n[i] <= 0.0) {
					n[i] = 0.00000001;
				}

				sN[i] = -1.0;
			}

			diff[i] = -1.0;
			sC[i] = -1.0;
		}
	} // updateFactorsSL

	private void getEscapeMovement(DoubleSolution escape, DoubleSolution last, double hC) {
		for (int var = 0; var < nDim; var++) {
			double value = Utils.polynomialMutation(random.nextDouble(), escape.getVariableValue(var).doubleValue(),
					(FastMath.log(indexUpdateH) + hC), problem.getLowerBound(var), problem.getUpperBound(var));

			if (value >= problem.getLowerBound(var) && value <= problem.getUpperBound(var)) {
				escape.setVariableValue(var, value);
			}

			if (var == nDim - 1 && !hasMovement(escape, last)) {
				var = -1;
			}
		}
		nEsc++;
	} // getEscapeMovement

	private boolean hasMovement(DoubleSolution humanSolution, DoubleSolution solution) {
		for (int i = 0; i < nDim; i++) {
			if (humanSolution.getVariableValue(i).compareTo(solution.getVariableValue(i)) != 0) {
				return true;
			}
		}
		return false;
	} // hasMovement

	private void countUnsuccessfulFactors(int index, boolean successful, int[] siG, int[] qiG) {
		if (qiG[index] == 0) {
			siG[index] = 1;
		} else {
			siG[index] = 0;
		}

		if (successful) {
			qiG[index] = 0;
		} else {
			qiG[index] += 1;
		}
	} // updateUnsuccessfulFactors

	private double getMinDistanceToZombie(DoubleSolution solution, double fear, double hN, Zombie[] zombies,
			boolean flagSlowFear) {
		double result = Double.MAX_VALUE;
		double distance, maxDistance;

		maxDistance = params.maxDistance;
		if (zombies != null) {
			if (flagSlowFear) {
				maxDistance *= (double) (FastMath.log(indexUpdateH) + hN);
			} else {
				maxDistance *= (double) (FastMath.log(indexUpdateH) + hN + fear);
			}

			for (Zombie zombie : zombies) {
				distance = Utils.getEuclideanDistance(solution, zombie.getSolution());
				if (distance < result) {
					result = distance;
				}
			}
		}

		if (result < maxDistance) {
			return FastMath.pow(result, params.powDistance);
		}
		return Double.MAX_VALUE;
	} // getMinDistanceToZombie

	private int[] getHumansNearby(int idx, boolean[] groupsHumanMoved, double maxDistance) {
		String data = "";
		for (int i = 0; i < humans.length; i++) {
			if (!groupsHumanMoved[i] && i != idx
					&& Utils.getEuclideanDistance(humans[idx].getSolution(), humans[i].getSolution()) < maxDistance) {
				data += i + ",";
			}
		}
		data += idx;

		String[] temp = data.split(",");
		int[] result = new int[temp.length];
		for (int i = 0; i < temp.length; i++) {
			result[i] = Integer.parseInt(temp[i]);
		}

		return result;
	} // getHumansNearby

	private void getAloneMovement(int index, DoubleSolution alone, DoubleSolution last, double hC) {
		double[] median = new double[nDim];

		boolean odd = humans.length % 2 == 1;
		int middle = humans.length / 2;
		for (int var = 0; var < nDim; var++) {
			if (odd) {
				median[var] = humans[middle].getSolution().getVariableValue(var);
			} else {
				median[var] = (humans[middle - 1].getSolution().getVariableValue(var)
						+ humans[middle].getSolution().getVariableValue(var)) / 2.0;
			}
		}

		for (int i = 0; i < nDim; i++) {
			double range1 = alone.getVariableValue(i) + (Utils.cauchyG(median[i], FastMath.log(indexUpdateH + 1),
					random, problem.getLowerBound(i), problem.getUpperBound(i)) - alone.getVariableValue(i)) * hC;

			alone.setVariableValue(i, range1);

			if (alone.getVariableValue(i) < problem.getLowerBound(i)
					|| alone.getVariableValue(i) > problem.getUpperBound(i)) {
				alone.setVariableValue(i, last.getVariableValue(i));
			}
		}

		if (hasMovement(alone, last)) {
			nAlone++;
		} 
	} // getAloneMovement

	private void reOrderIdxArray(int[] idx, Human[] pop, Human[] humans) {
		int i;
		int[] ni = new int[idx.length];
		for (int j = 0; j < idx.length; j++) {
			i = 0;
			DoubleSolution sol = humans[idx[j]].getSolution();
			while (hasMovement(pop[i].getSolution(), sol)) {
				i++;
			}
			ni[i] = idx[j];
		}

		for (i = 0; i < idx.length; i++) {
			idx[i] = ni[i];
		}
	} // reOrderIdxArray

	private void getGroupMovement(Human[] pop, int[] idx, Zombie[] zombies, DoubleSolution best, double[] hC,
			double[] hN, double[] diff, double[] shC, double[] shN, int popSize, boolean bestChoose) {
		int j, h;
		double oldEvaluation, newEvaluation, oldDistance, newDistance;
		boolean lowSensitivity;

		// Generate matrix of some humans.
		DoubleSolution[] solutions = new DoubleSolution[pop.length * 2];
		Double[][] dataset = new Double[pop.length][nDim];
		for (j = 0; j < pop.length; j++) {
			solutions[pop.length + j] = pop[j].getSolution();
			dataset[j] = pop[j].getSolution().getVariables();
		}

		samplePopulation(solutions, best, dataset, pop.length, idx, hC, hN);
		Arrays.sort(solutions);

		h = 0;
		for (Human human : pop) {
			lowSensitivity = random.nextDouble() > human.getFear();
			oldDistance = getMinDistanceToZombie(human.getSolution(), human.getFear(), hN[idx[h]], zombies,
					lowSensitivity);

			if (noiseHandling && qiGH[idx[h]] > params.hQ) {
				oldEvaluation = getMeritEvaluation(human.getSolution());
			} else {
				oldEvaluation = human.getSolution().getObjective();
			}

			j = 0;
			boolean found = false;
			do {
				while (solutions[j] == null && j < (solutions.length - 1)) {
					j++;
				}

				if (solutions[j] == null) {
					j++;
				} else {
					newEvaluation = solutions[j].getObjective();
					newDistance = getMinDistanceToZombie(solutions[j], human.getFear(), hN[idx[h]], zombies,
							lowSensitivity);

					found = newEvaluation <= oldEvaluation;

					oldEvaluation += human.getFear() > 0 ? (1.0 / human.getFear() / oldDistance) : (1.0 / oldDistance);
					newEvaluation += human.getFear() > 0 ? (1.0 / human.getFear() / newDistance) : (1.0 / newDistance);

					found = evaluateQualityLocation(idx[h], oldEvaluation, newEvaluation, found, false, shC, hC[idx[h]],
							shN, hN[idx[h]], diff);

					if (found) {
						human.setSolution((DoubleSolution) solutions[j].copy());
					} else {
						j++;
					}
				}
			} while (j < solutions.length && !found);

			if (found) {
				solutions[j] = null;
			}
			h++;
		}
	} // getGroupMovement

	private boolean evaluateQualityLocation(int idx, double oldEvaluation, double newEvaluation, boolean unsuccessful,
			boolean sensitivity, double[] shC, double hC, double[] shN, double hN, double[] diff) {
		boolean found = false;

		countUnsuccessfulFactors(idx, unsuccessful, siGH, qiGH);

		if (newEvaluation < oldEvaluation || newEvaluation > oldEvaluation && sensitivity) {
			if (newEvaluation < oldEvaluation) {
				failedIteration = false;
				int d = 0;
				while (d < diff.length && diff[d] != -1.0) {
					d++;
				}
				diff[d] = FastMath.abs(oldEvaluation - newEvaluation);
				shC[d] = hC;
				shN[d] = hN;
			}

			found = true;
		}

		return found;
	}

	private void samplePopulation(DoubleSolution[] solutions, DoubleSolution best, Double[][] observations,
			int popLength, int[] idx, double[] hC, double[] hN) {
		double[] means;
		double[][] cov, chol, samples;

		// Estimate the mean and covariance of Psel.
		means = Utils.mean(observations);
		cov = Utils.covariance(observations, means, observations.length - 1);

		// To maintain the diversity, a simple approach is to amplify the
		// covariance matrix
		double mc = 0.0;
		for (int i : idx) {
			mc += hC[i];
		}
		mc /= idx.length;
		cov = Utils.times(FastMath.log(indexUpdateH) + mc, cov);

		// The Cholesky decomposition
		chol = Utils.choleskyDecomposition(cov);

		// generate Z an N-by-M matrix with random elements
		samples = new double[observations[0].length][observations.length];
		for (int i = 0; i < observations[0].length; i++) {
			for (int j = 0; j < observations.length; j++) {
				samples[i][j] = random.nextGaussian();
			}
		}

		// generate new X
		samples = Utils.transpose(Utils.matrixTimes(chol, samples));

		for (int i = 0; i < samples.length; i++) {
			DoubleSolution solution = new DoubleSolution(nDim, problem.getOrder());

			for (int j = 0; j < nDim; j++) {
				solution.setVariableValue(j,
						((best.getVariableValue(j) * hN[idx[i]]) + ((1.0 - hN[idx[i]]) * means[j]) + samples[i][j]));

				if (solution.getVariableValue(j) < problem.getLowerBound(j)) {
					solution.setVariableValue(j, solutions[popLength + i].getVariableValue(j));
				} else if (solution.getVariableValue(j) > problem.getUpperBound(j)) {
					solution.setVariableValue(j, solutions[popLength + i].getVariableValue(j));
				}
			}

			if (!hasMovement(solution, solutions[popLength + i])) {
				solution.setObjective(solutions[popLength + i].getObjective());
			} else {
				nGroup++;
				getMeritEvaluation(solution);
			}
			solutions[i] = solution;
		}
	} // samplePopulation

	private void updateMemory(double[] diff, double[] sf, double[] memory, int index) {
		double sum = 0.0;
		for (int i = 0; i < diff.length && diff[i] != -1.0; i++) {
			sum += diff[i];
		}

		// weighted mean for F
		double sum1 = 0.0;
		double sum2 = 0.0;
		for (int i = 0; i < diff.length && diff[i] != -1.0; i++) {
			double wk = diff[i] / sum;

			sum1 += sf[i] * sf[i] * wk;
			sum2 += sf[i] * wk;
		}

		if (Double.isNaN(sum1) || Double.isNaN(sum2)) {
			memory[index] = 0.00000001;
		} else {
			memory[index] = sum1 / sum2;
		}
	} // updateMemory

	private int getIndexUnsuccessful(int[] siG, int[] qiG, int popSize, double[] quG, double limit, int index) {
		double sumS = 0;
		double sumQ = 0;
		for (int i = 0; i < popSize; i++) {
			sumS += siG[i];
			sumQ += qiG[i];
		}

		quG[1] = quG[0];
		quG[0] = (sumQ / sumS);
		if (quG[0] == 0) {
			quG[0] = quG[1];
		}

		if (quG[0] > limit) {
			index += 1;
		} else {
			return 1;
		}

		return index;
	} // getIndexUnsuccessful

	private List<Zombie> moveZombies(Zombie[] zombies) {
		boolean foundBetter = false;
		int idx, size;
		double zombieDistance, distanceMove, distanceAttack;
		List<Zombie> newZombies = new LinkedList<>();

		size = params.zombiePopSize;
		double[] zC = new double[size];
		double[] zN = new double[size];
		double[] diff = new double[size];
		double[] szC = new double[size];
		double[] szN = new double[size];

		updateFactorsCN(size, memoryZc, params.zCstd, memoryZn, params.zNstd, zC, zN, diff, szC, szN);

		int k = -1;
		for (idx = 0; idx < zombies.length; idx++) {
			Zombie zombie = zombies[idx];
			if (!zombie.isChaser()) {
				continue;
			}
			k++;

			distanceMove = (FastMath.log(indexUpdateZ) + zC[k]) * params.maxDistance;
			distanceAttack = zN[k] * params.maxDistance;

			zombieDistance = tryToAttackHumans(zombie, distanceAttack, zC[k], zN[k], diff, szC, szN, newZombies);

			if (zombieDistance == Double.MAX_VALUE) {
				setZombieRandomMovement(zombie, distanceMove);

			} else if (zombieDistance >= distanceAttack) {
				List<Integer> tries = new LinkedList<>();
				for (int i = 0; i < nDim; i++) {
					tries.add(i);
				}
				Collections.shuffle(tries);

				foundBetter = false;
				for (int i = 0; (i < this.nDim) && !foundBetter; i++) {
					Double[] zombieSolution = zombie.getSolution().getVariables().clone();
					int index = tries.get(i);
					for (int j = 0; (j < params.humanStepsPerZombieStep) && !foundBetter; j++) {
						zombieSolution[index] = getRandomPosition(zombie.getSolution().getVariableValue(index),
								distanceMove, index);

						Object[] result = checkImprovement(zombieSolution, zombieDistance, zC[k], distanceAttack);
						foundBetter = (boolean) result[0];

						if (foundBetter) {
							int d = 0;
							while (d < diff.length && diff[d] != -1.0) {
								d++;
							}
							diff[d] = FastMath.abs(zombieDistance - (double) result[1]);
							szC[d] = zC[k];
							szN[d] = zN[k];

							zombie.setSolution(zombieSolution);
						}
					}
				}
			}

			countUnsuccessfulFactors(k, foundBetter, siGZ, qiGZ);
		}

		// update memory Z
		if (diff[0] != -1.0) {
			updateMemory(diff, szC, memoryZc, indexMemoryZc);

			indexMemoryZc++;
			if (indexMemoryZc >= sizeMemoryZ) {
				indexMemoryZc = 0;
			}

			updateMemory(diff, szN, memoryZn, indexMemoryZn);

			indexMemoryZn++;
			if (indexMemoryZn >= sizeMemoryZ) {
				indexMemoryZn = 0;
			}
		}

		return newZombies;
	} // moveZombies

	private double tryToAttackHumans(Zombie zombie, double distanceAttack, double zC, double zN, double[] diff,
			double[] szC, double[] szN, List<Zombie> newZombies) {
		double zombieDistance, candidateDistance;

		zombieDistance = Double.MAX_VALUE;
		for (int h = 0; h < humans.length; h++) {
			Human human = humans[h];
			if (human != null) {
				boolean notAttack = zC * Utils.getEuclideanDistance(human.getSolution(), bestMovement) < distanceAttack;

				if (!notAttack) {
					candidateDistance = Utils.getEuclideanDistance(zombie.getSolution(), human.getSolution());

					if (candidateDistance < zombieDistance) {
						zombieDistance = candidateDistance;

						if (zombieDistance < distanceAttack) {
							int z = 0;
							while (z < diff.length && diff[z] != -1.0) {
								z++;
							}
							diff[z] = FastMath.abs(zombieDistance - distanceAttack);
							szC[z] = zC;
							szN[z] = zN;

							newZombies.add(turnZombie(h));
							break;
						}
					}
				}
			}
		}

		return zombieDistance;
	} // tryToAttackHumans

	private Zombie turnZombie(int human) {
		Zombie newzombie = new Zombie(nDim, 0, problem.getOrder());
		newzombie.setSolution(humans[human].getSolution().getVariables().clone());
		humans[human] = null;

		return newzombie;
	} // turnZombie

	private void setZombieRandomMovement(Zombie zombie, double distanceMove) {
		double probabilityMove = indexUpdateZ / (double) nDim;
		DoubleSolution mov = (DoubleSolution) zombie.getSolution().copy();
		for (int i = 0; i < nDim; i++) {
			if (random.nextDouble() <= probabilityMove) {
				mov.setVariableValue(i, getRandomPosition(mov.getVariableValue(i), distanceMove, i));
			}
		}

		zombie.setSolution(mov.getVariables());
	} // setZombieRandomMovement

	private double getRandomPosition(double position, double distanceMove, int index) {
		double result = position;

		while (result == position) {
			result = result + (random.nextGaussian() * distanceMove);

			if (result < problem.getLowerBound(index) || result > problem.getUpperBound(index)) {
				result = position;
			}
		}

		return result;
	} // getRandomPosition

	private Object[] checkImprovement(Double[] zombieSolution, double zombieDistance, double zC,
			double distanceAttack) {
		boolean foundBetter = false;
		double candidateDistance;

		for (Human human : humans) {
			if (human != null) {
				boolean notAttack = zC * Utils.getEuclideanDistance(human.getSolution(), bestMovement) < distanceAttack;

				if (!notAttack || indexUpdateZ > indexUpdateH) {
					candidateDistance = Utils.getEuclideanDistance(zombieSolution, human.getSolution().getVariables());

					if (candidateDistance < zombieDistance) {
						zombieDistance = candidateDistance;
						foundBetter = true;
					}
				}
			}
		}

		return new Object[] { foundBetter, zombieDistance };
	} // checkImprovement

	@Override
	public String getSummary() {
		String result = " - Error: " + error + " - Z: " + sizeZombies + ", H: " + sizeHumans + " - UpDist: ["
				+ indexUpdateH + "," + indexUpdateZ + "]"
				+ " - Calls: " + nCalls;
		result += "\nMemorys: ";
		result += "\n\tmHC " + Utils.getStringVector(memoryHc);
		result += "\n\tmHN " + Utils.getStringVector(memoryHn);
		result += "\n\tmZC " + Utils.getStringVector(memoryZc);
		result += "\n\tmZN " + Utils.getStringVector(memoryZn) + "\n";

		if (noiseHandling) {
			result += "Solutions Top 10: ";
			for (int i = 0; i < humans.length && i < 10; i++) {
				result += "\n\t(" + i + "): " + humans[i].getSolution().getObjective() + " - [ "
						+ humans[i].getSolution().toString() + " ]";
			}
			result += "\n";
		}

		return result;
	} // getSummary

	public double getPunishment() {
		return FastMath.cbrt(params._maxObjectiveFunctionCalls) + FastMath.cbrt(nEsc + nAlone - nGroup);
	}

	private void saveDataOpt(Zombie[] zombies, int step, int idxH) {
		// + " " +
		DataReader.saveOverwriteData(fileName, nCalls + " " + params.toStringClean());
		DataReader.saveData(fileName,
				indexUpdateH + " " + indexUpdateZ + " " + (zombies != null ? zombies.length : params.zombiePopSize)
						+ " " + indexMemoryHc + " " + indexMemoryHn + " " + indexMemoryZc + " " + indexMemoryZn + " "
						+ nEsc + " " + nAlone + " " + nGroup + " " + seed + " " + fase + " " + step + " " + idxH);
		DataReader.saveData(fileName, Utils.getStringVectorClean(memoryHc));
		DataReader.saveData(fileName, Utils.getStringVectorClean(memoryHn));
		DataReader.saveData(fileName, Utils.getStringVectorClean(memoryZc));
		DataReader.saveData(fileName, Utils.getStringVectorClean(memoryZn));
		DataReader.saveData(fileName, Utils.getStringVectorClean(qiGH));
		DataReader.saveData(fileName, Utils.getStringVectorClean(siGH));
		DataReader.saveData(fileName, Utils.getStringVectorClean(quGH));
		DataReader.saveData(fileName, Utils.getStringVectorClean(qiGZ));
		DataReader.saveData(fileName, Utils.getStringVectorClean(siGZ));
		DataReader.saveData(fileName, Utils.getStringVectorClean(quGZ));
		DataReader.saveData(fileName, bestMovement.toString());
		for (Human h : humans) {
			DataReader.saveData(fileName, h.toString());
		}
		if (zombies != null) {
			for (Zombie z : zombies) {
				DataReader.saveData(fileName, z.toString());
			}
		}
	}

	private void updateCalls(String[] data) {
		nCalls = Integer.parseInt(data[0]);
	}

	private int updateIndexs(String[] data) {
		indexUpdateH = Integer.parseInt(data[0]);
		indexUpdateZ = Integer.parseInt(data[1]);
		indexMemoryHc = Integer.parseInt(data[3]);
		indexMemoryHn = Integer.parseInt(data[4]);
		indexMemoryZc = Integer.parseInt(data[5]);
		indexMemoryZn = Integer.parseInt(data[6]);
		nEsc = Integer.parseInt(data[7]);
		nAlone = Integer.parseInt(data[8]);
		nGroup = Integer.parseInt(data[9]);
		seed = Long.parseLong(data[10]);
		fase = Integer.parseInt(data[11]);
		countStep = Integer.parseInt(data[12]);
		try {
			idxHuman = Integer.parseInt(data[13]);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Integer.parseInt(data[2]);
	}

	private void updateVect(String[] data, double[] vect) {
		for (int i = 0; i < vect.length; i++) {
			vect[i] = Double.parseDouble(data[i]);
		}
	}

	private void updateVect(String[] data, int[] vect) {
		for (int i = 0; i < vect.length; i++) {
			vect[i] = Integer.parseInt(data[i]);
		}
	}

	private void updateVect(String[] data, boolean[] vect) {
		for (int i = 0; i < vect.length; i++) {
			vect[i] = Boolean.parseBoolean(data[i]);
		}
	}

	private void updateSolution(String[] data, DoubleSolution solution, int init) {
		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			solution.setVariableValue(i, Double.parseDouble(data[i + init]));
		}
		solution.setObjective(Double.parseDouble(data[solution.getNumberOfVariables() + init]));
		solution.setOrder(Ordering.values()[Integer.parseInt(data[solution.getNumberOfVariables() + 1 + init])]);
	}

	private void updateHuman(String[] data, Human human) {
		human.setFear(Double.parseDouble(data[0]));
		updateSolution(data, human.getSolution(), 1);
	}

	private void updateZombie(String[] data, Zombie zombie) {
		zombie.setChaser(Double.parseDouble(data[0]));
		updateSolution(data, zombie.getSolution(), 1);
	}
}
