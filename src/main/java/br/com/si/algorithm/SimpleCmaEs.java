package br.com.si.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.analysis.function.Abs;
import org.omg.CORBA._PolicyStub;

import br.com.si.params.CmaEsParams;
import br.com.si.params.EdaParams;
import br.com.si.params.MultiStartParams;
import br.com.si.params.ProblemParams;
import br.com.si.params.SimpleCmaEsParams;
import br.com.si.problem.Problem;
import br.com.si.solution.DoubleSolution;
import br.com.si.util.Utils;

@SuppressWarnings({ "unused", "unchecked" })
public class SimpleCmaEs extends Algorithm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5977562986524645665L;

	public static final String name = "SimpleCmaEs";

	private int numFunctionCalls;
	private double error;
	private double zero = Math.pow(10.0, -8);

	private DoubleSolution bestSolution;

	private Random random;

	private boolean _viewActive;
	private boolean _stepView;
	private SimpleCmaEsParams _params;

	public SimpleCmaEs(Problem problem, SimpleCmaEsParams params, boolean viewActive, boolean stepView) {
		super(problem);

		this._viewActive = viewActive && problem.getNumberOfVariables() == 2;
		this._stepView = stepView && problem.getNumberOfVariables() == 2;
		this._params = params;
		this.notExecuteInThread = (this._viewActive || this._stepView);

		this.random = new Random();
	}

	@Override
	public void initProgress() {
		super.initProgress();

		List<DoubleSolution> solutions;
		solutions = new ArrayList<>(_params._numSolutions);

		bestSolution = DoubleSolution.createSolution(problem, random);
		numFunctionCalls = 0;

		for (int i = 0; i < _params._numSolutions; i++) {
			DoubleSolution solution = DoubleSolution.createSolution(problem, random);
			problem.evaluate(solution);
			updateProgress();

			if (Utils.compare(solution, bestSolution, problem.getOrder()) == -1) {
				bestSolution = (DoubleSolution) solution.copy();

				if (problem.getName().contains("ProblemsCEC15")) {
					this.error = solution.getObjective() - problem.getNumFunction() * 100;

					if (this.error < this.zero) {
						this.error = 0.0;
					}
				}
			}

			setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCalls,
					_params._maxObjectiveFunctionCalls);

			solutions.add(solution);
		}

		if (_viewActive) {
			redraw(bestSolution, solutions);

			if (_stepView)
				stepView();
		}

	}

	@Override
	public void updateProgress() {
		numFunctionCalls++;
	}

	@Override
	public void setSeed(long seed) {
		random.setSeed(seed);
	}

	@Override
	public boolean isStoppingConditionReached() {
		return numFunctionCalls >= _params._maxObjectiveFunctionCalls || (numFunctionCalls > 0 && error == 0.0);
	}

	@Override
	public DoubleSolution search() {
		double numGenerations = ((double) _params._maxObjectiveFunctionCalls / (double) _params._numSolutions);

		double initialCovar = _params._initialCovar;
		double finalCovar = _params._finalCovar;

		double covarReductionRate = (finalCovar - initialCovar) / numGenerations;
		double covar = initialCovar;

		if (this._viewActive)
			initViewer();

		initProgress();

		int generation = 0;

		while (!isStoppingConditionReached()) {
			List<DoubleSolution> solutions = new ArrayList<>(_params._numSolutions);

			for (int i = 0; i < _params._numSolutions; i++) {
				DoubleSolution newSolution = (DoubleSolution) bestSolution.copy();

				for (int k = 0; k < problem.getNumberOfVariables(); k++) {
					double attributeSearchSpaceSize = (problem.getUpperBound(k) - problem.getLowerBound(k));

					do {
						if (i < (_params._numSolutions - _params._numParticlesToIntensificate)) {
							newSolution.setVariableValue(k, bestSolution.getVariableValue(k)
									+ random.nextGaussian() * covar * attributeSearchSpaceSize);
						} else {
							newSolution.setVariableValue(k, bestSolution.getVariableValue(k)
									+ random.nextGaussian() * _params._intensificationCovar * attributeSearchSpaceSize);
						}
					} while (newSolution.getVariableValue(k).doubleValue() > problem.getUpperBound(k)
							|| newSolution.getVariableValue(k).doubleValue() < problem.getLowerBound(k));
				}

				problem.evaluate(newSolution);
				updateProgress();

				solutions.add(newSolution);

				if (Utils.compare(newSolution, bestSolution, problem.getOrder()) == -1) {
					bestSolution = (DoubleSolution) newSolution.copy();

					if (problem.getName().contains("ProblemsCEC15")) {
						this.error = newSolution.getObjective() - problem.getNumFunction() * 100;

						if (this.error < this.zero) {
							this.error = 0.0;
						}
					}
				}

				setResult(new BigDecimal(bestSolution.getObjective()), numFunctionCalls,
						_params._maxObjectiveFunctionCalls);
			}

			if (_viewActive) {
				redraw(bestSolution, solutions);

				if (_stepView)
					stepView();
			}

			covar += covarReductionRate;
			generation++;

		}

		if (_viewActive)
			finishViewer();

		return bestSolution;
	}

	@Override
	public int getIterations() {
		return numFunctionCalls;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProblemParams getParameter() {
		return _params;
	}

	@Override
	public String getSummary() {
		return " - " + numFunctionCalls;
	}

}
