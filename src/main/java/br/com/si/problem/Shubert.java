package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Shubert extends Problem {

	private boolean restricted;

	public Shubert(int numberOfVariables, Ordering order, String typeSolution, boolean restricted) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		this.restricted = restricted;

		if (this.restricted) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.12);
				this.upperLimit.add(+5.12);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-10.0);
				this.upperLimit.add(+10.0);
			}
		}
	}

	@Override
	public String getName() {
		if (restricted) {
			return "Shubert-Restricted";
		} else {
			return "Shubert";
		}
		// Global Min -> f(x*) = -186.7309
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double sum1 = 0.0;
		double sum2 = 0.0;

		for (int i = 1; i < 6; i++) {
			sum1 += (double) i * Math.cos((1.0 + i) * solution.getVariableValue(0) + i);
			sum2 += (double) i * Math.cos((1.0 + i) * solution.getVariableValue(1) + i);
		}
		sum1 *= sum2;

		solution.setObjective(sum1);
	}

	@Override
	public double getError(double result) {
		return result + 186.7309;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal sum1 = BigDecimal.ZERO;
		BigDecimal sum2 = BigDecimal.ZERO;

		for (int i = 1; i < 6; i++) {
			sum1 = sum1.add(BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(Math.cos(BigDecimal.valueOf(i + 1)
					.multiply(solution.getVariableValue(0)).add(BigDecimal.valueOf(i)).doubleValue()))));
			sum2 = sum2.add(BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(Math.cos(BigDecimal.valueOf(i + 1)
					.multiply(solution.getVariableValue(1)).add(BigDecimal.valueOf(i)).doubleValue()))));
		}

		solution.setObjective(sum1.multiply(sum2).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
