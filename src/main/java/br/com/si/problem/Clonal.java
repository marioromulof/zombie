package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Clonal extends Problem {
	private double granularity = 500.0;

	public Clonal(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-1.0 * granularity);
			this.upperLimit.add(+2.0 * granularity);
		}
	}

	@Override
	public String getName() {
		return "Clonal";
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value = 0;
		double axis1 = 0;
		double axis2 = 0;
		double numberOfVariables = (double) (getNumberOfVariables() - 1);

		for (int i = 0; i < numberOfVariables; i++) {
			axis1 = solution.getVariableValue(i) / granularity;
			axis2 = solution.getVariableValue(i + 1) / granularity;
			value += axis1 * Math.sin(4.0 * Math.PI * axis1);
			value -= axis2 * Math.sin((4.0 * Math.PI * axis2) + Math.PI);
			value += 1;
		}
		value /= numberOfVariables;

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double axis1 = 0;
		double axis2 = 0;
		double numberOfVariables = (double) (getNumberOfVariables() - 1);

		for (int i = 0; i < numberOfVariables; i++) {
			axis1 = solution.getVariableValue(i).doubleValue() / granularity;
			axis2 = solution.getVariableValue(i + 1).doubleValue() / granularity;
			value += axis1 * Math.sin(4.0 * Math.PI * axis1);
			value -= axis2 * Math.sin((4.0 * Math.PI * axis2) + Math.PI);
			value += 1;
		}
		value /= numberOfVariables;

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
