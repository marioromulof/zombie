package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class StyblinskiTang extends Problem {

	public StyblinskiTang(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-5.0);
			// this.upperLimit.add(+5.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Styblinski-Tang";
		// Global Min -> f(x*) = -39.16599d, at x* = (-2.903534, ..., -2.903534)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 0; i < nVar; i++) {
			value += solution.getVariableValue(i) * solution.getVariableValue(i) * solution.getVariableValue(i)
					* solution.getVariableValue(i)
					- (16.0 * solution.getVariableValue(i) * solution.getVariableValue(i)
							+ 5.0 * solution.getVariableValue(i));
		}
		value /= 2.0;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result + 39.16599 * (double) getNumberOfVariables();
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value = BigDecimal.ZERO;
		int nVar = getNumberOfVariables();

		for (int i = 0; i < nVar; i++) {
			value = value.add(solution.getVariableValue(i).pow(4)
					.subtract(solution.getVariableValue(i).pow(2).multiply(new BigDecimal("16.0"))
							.add(solution.getVariableValue(i).multiply(new BigDecimal("5.0")))));
		}

		solution.setObjective(value.divide(new BigDecimal("2.0")).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
