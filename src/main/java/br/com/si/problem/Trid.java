package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Trid extends Problem {

	public Trid(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-Math.pow((double) numberOfVariables, 2.0));
			this.upperLimit.add(+Math.pow((double) numberOfVariables, 2.0));
		}
	}

	@Override
	public String getName() {
		return "Trid";
		// Global Min -> f(x*) = -50.0, at d = 6
		// Global Min -> f(x*) = -210.0, at d = 10
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double sum1 = 0.0;
		double sum2 = 0.0;

		sum1 += (solution.getVariableValue(0) - 1) * (solution.getVariableValue(0) - 1);
		for (int i = 1; i < nVar; i++) {
			sum1 += (solution.getVariableValue(i) - 1) * (solution.getVariableValue(i) - 1);
			sum2 += solution.getVariableValue(i) * solution.getVariableValue(i - 1);
		}

		solution.setObjective(sum1 - sum2);
	}

	@Override
	public double getError(double result) {
		double diff = 2.0;
		double value = 2.0;
		for (int i = 3; i <= getNumberOfVariables(); i++) {
			value += i;
			diff += value;
		}
		return result + diff;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal sum1 = BigDecimal.ZERO;
		BigDecimal sum2 = BigDecimal.ZERO;

		sum1 = sum1.add(solution.getVariableValue(0).subtract(BigDecimal.ONE).pow(2));
		for (int i = 1; i < nVar; i++) {
			sum1 = sum1.add(solution.getVariableValue(i).subtract(BigDecimal.ONE).pow(2));
			sum2 = sum2.add(solution.getVariableValue(i).multiply(solution.getVariableValue(i - 1)));
		}

		solution.setObjective(sum1.subtract(sum2).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
