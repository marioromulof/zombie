package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Levy extends Problem {

	public Levy(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Levy";
		// Global Min -> f(x*) = 0.0, at x* = (1, ..., 1)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables() - 1;
		double value, w, aux;

		value = Math.sin(Math.PI * getW(solution.getVariableValue(0)));
		value *= value;

		for (int i = 0; i < nVar; i++) {
			w = getW(solution.getVariableValue(i));
			aux = Math.sin(Math.PI * w + 1.0);
			aux *= aux * 10.0;
			value += (w - 1.0) * (w - 1.0) * (1.0 + aux);
		}

		w = getW(solution.getVariableValue(nVar));
		aux = Math.sin(2.0 * Math.PI * w);
		aux *= aux;
		value += (w - 1.0) * (w - 1.0) * (1.0 + aux);

		solution.setObjective(value);
	}

	private double getW(double axis) {
		return 1.0 + ((axis - 1.0) / 4.0);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables() - 1;
		BigDecimal value, w;

		w = BigDecimal.ONE.add(solution.getVariableValue(0).subtract(BigDecimal.ONE).divide(BigDecimal.valueOf(4.0)));
		value = BigDecimal.valueOf(Math.sin(Math.PI * w.doubleValue())).pow(2);

		for (int i = 0; i < nVar; i++) {
			w = BigDecimal.ONE
					.add(solution.getVariableValue(i).subtract(BigDecimal.ONE).divide(BigDecimal.valueOf(4.0)));

			value = value.add(w.subtract(BigDecimal.ONE).pow(2).multiply(BigDecimal.ONE.add(BigDecimal
					.valueOf(Math.sin(Math.PI * w.doubleValue() + 1)).pow(2).multiply(BigDecimal.valueOf(10.0)))));
		}
		w = BigDecimal.ONE
				.add(solution.getVariableValue(nVar).subtract(BigDecimal.ONE).divide(BigDecimal.valueOf(4.0)));
		value = value.add(w.subtract(BigDecimal.ONE).pow(2)
				.multiply(BigDecimal.ONE.add(BigDecimal.valueOf(Math.sin(Math.PI * w.doubleValue() * 2.0)).pow(2))));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
