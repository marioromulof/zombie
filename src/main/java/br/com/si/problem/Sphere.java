package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Sphere extends Problem {

	private boolean restricted;

	public Sphere(int numberOfVariables, Ordering order, String typeSolution, boolean restricted) {
		super(numberOfVariables, order, typeSolution);

		this.restricted = restricted;

		if (this.restricted) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-100.0);
				this.upperLimit.add(+100.0);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.12);
				this.upperLimit.add(+5.12);
			}
		}
	}

	@Override
	public String getName() {
		if (restricted) {
			return "Sphere-Restricted";
		} else {
			return "Sphere";
		}
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double result = 0.0;

		for (int i = 0; i < nVar; i++) {
			result += solution.getVariableValue(i) * solution.getVariableValue(i);
		}

		solution.setObjective(result);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal result = BigDecimal.ZERO;

		for (int i = 0; i < nVar; i++) {
			result = result.add(solution.getVariableValue(i).pow(2));
		}

		solution.setObjective(result.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
