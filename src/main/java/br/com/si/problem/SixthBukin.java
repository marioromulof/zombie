package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class SixthBukin extends Problem {

	public SixthBukin(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		// this.lowerLimit.add(-15.0);
		// this.upperLimit.add(-5.0);

		// this.lowerLimit.add(-3.0);
		// this.upperLimit.add(+3.0);
		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Sixth Bukin";
		// Global Min -> f(x*) = 0.0, at x* = (-10, 1)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value;

		value = 100.0
				* Math.sqrt(Math.abs(solution.getVariableValue(1)
						- 0.01 * solution.getVariableValue(0) * solution.getVariableValue(0)))
				+ Math.abs(0.01 * (solution.getVariableValue(0) + 10.0));

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal.valueOf(100.0).multiply(BigDecimal.valueOf(Math.sqrt(solution.getVariableValue(1)
				.subtract(BigDecimal.valueOf(0.01).multiply(solution.getVariableValue(0).pow(2))).abs().doubleValue())))
				.add(BigDecimal.valueOf(0.01)
						.multiply(solution.getVariableValue(0).add(BigDecimal.valueOf(10.0)).abs()));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
