package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class DeJongN5 extends Problem {
	private static double[] a1 = { -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32, -32, -16, 0, 16, 32,
			-32, -16, 0, 16, 32 };
	private static double[] a2 = { -32, -32, -32, -32, -32, -16, -16, -16, -16, -16, 0, 0, 0, 0, 0, 16, 16, 16, 16, 16,
			32, 32, 32, 32, 32 };

	public DeJongN5(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-65.536);
			this.upperLimit.add(+65.536);
		}
	}

	@Override
	public String getName() {
		return "De Jong N. 5";
		// Global Min -> f(x*) = 1.0, at x* = (-31.89418666666667,
		// -31.89418666666667)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double aux1 = solution.getVariableValue(0);
		double aux2 = solution.getVariableValue(1);
		double value = 0.002;

		for (int i = 0; i < 25; i++) {
			value += 1.0 / (i + 1.0 + Math.pow(aux1 - a1[i], 6.0) + Math.pow(aux2 - a2[i], 6.0));
		}

		solution.setObjective(1.0 / value);
	}

	@Override
	public double getError(double result) {
		return result - 1.0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double aux1 = solution.getVariableValue(0).doubleValue();
		double aux2 = solution.getVariableValue(1).doubleValue();
		double value = 0.002;

		for (int i = 0; i < 25; i++) {
			value += 1 / (i + 1 + Math.pow(aux1 - a1[i], 6) + Math.pow(aux2 - a2[i], 6));
		}

		solution.setObjective(1 / value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
