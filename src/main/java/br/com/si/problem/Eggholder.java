package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Eggholder extends Problem {

	public Eggholder(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-512.0);
			this.upperLimit.add(+512.0);
		}
	}

	@Override
	public String getName() {
		return "Eggholder";
		// Global Min -> f(x*) = -959.6407, at x* = (512, 404.2319)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value;

		value = -1.0 * (solution.getVariableValue(1) + 47.0) * Math
				.sin(Math.sqrt(Math.abs(solution.getVariableValue(1) + solution.getVariableValue(0) / 2.0 + 47.0)));
		value -= solution.getVariableValue(0)
				* Math.sin(Math.sqrt(Math.abs(solution.getVariableValue(0) - (solution.getVariableValue(1) + 47.0))));

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result + 959.6407;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;
		value = solution.getVariableValue(1).add(BigDecimal.valueOf(47.0))
				.multiply(BigDecimal.valueOf(Math.sin(Math.sqrt(
						solution.getVariableValue(1).add(solution.getVariableValue(0).divide(BigDecimal.valueOf(2.0)))
								.add(BigDecimal.valueOf(47.0)).abs().doubleValue()))))
				.negate();
		value = value.subtract(
				solution.getVariableValue(0).multiply(BigDecimal.valueOf(Math.sin(Math.sqrt(solution.getVariableValue(0)
						.subtract(solution.getVariableValue(1).add(BigDecimal.valueOf(47.0))).abs().doubleValue())))));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
