package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Cigar extends Problem {

	public Cigar(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Cigar";
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value = 0;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 1; i < numberOfVariables; i++) {
			value += solution.getVariableValue(i) * solution.getVariableValue(i);
		}
		value *= 10.0 * 10.0 * 10.0 * 10.0 * 10.0 * 10.0;
		value += solution.getVariableValue(0) * solution.getVariableValue(0);

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		// TODO Auto-generated method stub
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 1; i < numberOfVariables; i++) {
			value += Math.pow(solution.getVariableValue(i).doubleValue(), 2.0);
		}
		value *= Math.pow(10.0, 6.0);
		value += Math.pow(solution.getVariableValue(0).doubleValue(), 2.0);

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}
}
