package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Beale extends Problem {

	public Beale(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-4.5);
			// this.upperLimit.add(+4.5);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Beale";
		// Global Min -> f(x*) = 0.0, at x* = (3.0, 0.5)
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal.valueOf(1.5).subtract(solution.getVariableValue(0))
				.add(solution.getVariableValue(0).multiply(solution.getVariableValue(1))).pow(2);

		value = value.add(BigDecimal.valueOf(2.25).subtract(solution.getVariableValue(0))
				.add(solution.getVariableValue(0).multiply(solution.getVariableValue(1).pow(2))).pow(2));

		value = value.add(BigDecimal.valueOf(2.625).subtract(solution.getVariableValue(0))
				.add(solution.getVariableValue(0).multiply(solution.getVariableValue(1).pow(3))).pow(2));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value1, value2, value3, mult;

		mult = solution.getVariableValue(0) * solution.getVariableValue(1);

		value1 = 1.5 - solution.getVariableValue(0) + mult;
		value1 *= value1;

		value2 = 2.25 - solution.getVariableValue(0) + mult * mult;
		value2 *= value2;

		value3 = 2.625 - solution.getVariableValue(0) + mult * mult * mult;
		value3 *= value3;

		solution.setObjective(value1 + value2 + value3);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
