package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class PermDB extends Problem {

	public PermDB(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add((double) -numberOfVariables);
			this.upperLimit.add((double) +numberOfVariables);
		}
	}

	@Override
	public String getName() {
		return "Perm Function d, β";
		// Global Min -> f(x*) = 0.0, at x* = (1, 1/2, ..., 1/d)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double result = 0.0;

		for (int i = 0; i < nVar; i++) {
			double value = 0.0;
			for (int j = 0; j < nVar; j++) {
				value += Math.pow((1.0 + j), (1.0 + i))
						+ 0.5 * (Math.pow((solution.getVariableValue(j) / (1.0 + j)), (1.0 + i)) - 1.0);
			}
			result += value * value;
		}

		solution.setObjective(result);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal result = BigDecimal.ZERO;

		for (int i = 0; i < nVar; i++) {
			BigDecimal value = BigDecimal.ZERO;
			for (int j = 0; j < nVar; j++) {
				value = value.add(BigDecimal.valueOf(j + 1).pow(i + 1).add(BigDecimal.valueOf(0.5)).multiply(solution
						.getVariableValue(j).divide(BigDecimal.valueOf(j + 1)).pow(i + 1).subtract(BigDecimal.ONE)));
			}
			result = result.add(value.pow(2));
		}

		solution.setObjective(result.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
