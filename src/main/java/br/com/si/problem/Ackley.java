package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Ackley extends Problem {

	private boolean restricted;

	public Ackley(int numberOfVariables, Ordering order, String typeSolution, boolean restricted) {
		super(numberOfVariables, order, typeSolution);

		this.restricted = restricted;

		if (this.restricted) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.0);
				this.upperLimit.add(+5.0);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				// this.lowerLimit.add(-32.768);
				// this.upperLimit.add(+32.768);
				this.lowerLimit.add(-100.0);
				this.upperLimit.add(+100.0);
			}
		}
	}

	@Override
	public String getName() {
		if (restricted) {
			return "Ackley-Restricted";
		} else {
			return "Ackley";
		}
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value = 0;
		double cos = 0;
		double axis;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i);
			value += axis * axis;
			cos += Math.cos(2.0 * Math.PI * axis);
		}
		value = -20.0 * Math.exp(-0.2 * Math.sqrt(value / numberOfVariables));
		value -= Math.exp(cos / numberOfVariables);
		value += Math.exp(1.0) + 20.0;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double cos = 0;
		double axis;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i).doubleValue();
			value += axis * axis;
			cos += Math.cos(2.0 * Math.PI * axis);
		}
		value = -20.0 * Math.exp(-0.2 * Math.sqrt(value / numberOfVariables));
		value -= Math.exp(cos / numberOfVariables);
		value += Math.exp(1.0) + 20.0;

		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}
}
