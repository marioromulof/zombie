package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class SumSquares extends Problem {

	private boolean restricted;

	public SumSquares(int numberOfVariables, Ordering order, String typeSolution, boolean restricted) {
		super(numberOfVariables, order, typeSolution);

		this.restricted = restricted;

		if (this.restricted) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.12);
				this.upperLimit.add(+5.12);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				// this.lowerLimit.add(-10.0);
				// this.upperLimit.add(+10.0);
				this.lowerLimit.add(-100.0);
				this.upperLimit.add(+100.0);
			}
		}
	}

	@Override
	public String getName() {
		if (restricted) {
			return "Sum Squares-Restricted";
		} else {
			return "Sum Squares";
		}
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 0; i < nVar; i++) {
			value += solution.getVariableValue(i) * solution.getVariableValue(i) * (1.0 + i);
		}

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal value = BigDecimal.ZERO;

		for (int i = 0; i < nVar; i++) {
			value = value.add(solution.getVariableValue(i).pow(2).multiply(BigDecimal.valueOf(i + 1)));
		}

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
