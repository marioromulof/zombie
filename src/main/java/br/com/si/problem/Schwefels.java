package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Schwefels extends Problem {

	public Schwefels(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-500.0);
			this.upperLimit.add(+500.0);
		}
	}

	@Override
	public String getName() {
		return "Schwefels";
		// Global Min -> f(x*) = 0.0, at x* = (420.9687, ..., 420.9687)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value;
		double numberOfVariables = (double) getNumberOfVariables();

		value = 0.0;
		for (int i = 0; i < numberOfVariables; i++) {
			value += solution.getVariableValue(i) * Math.sin(Math.sqrt(Math.abs(solution.getVariableValue(i))));
		}
		value = (418.9829 * numberOfVariables) - value;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value;
		double numberOfVariables = (double) getNumberOfVariables();

		value = 0.0;
		for (int i = 0; i < numberOfVariables; i++) {
			value += solution.getVariableValue(i).doubleValue()
					* Math.sin(Math.sqrt(solution.getVariableValue(i).abs().doubleValue()));
		}
		value = (418.9829 * numberOfVariables) - value;

		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
