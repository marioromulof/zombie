package br.com.si.problem;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public abstract class Problem {
	protected List<Double> lowerLimit;
	protected List<Double> upperLimit;
	protected String typeSolution;

	protected int numberOfVariables;
	protected Ordering order;

	public Problem(int numberOfVariables, Ordering order, String typeSolution) {
		super();
		this.numberOfVariables = numberOfVariables;
		this.order = order;
		this.lowerLimit = new ArrayList<>(numberOfVariables);
		this.upperLimit = new ArrayList<>(numberOfVariables);
		this.typeSolution = typeSolution.toLowerCase();
	}

	public Double getUpperBound(int index) {
		return upperLimit.get(index);
	}

	public Double getLowerBound(int index) {
		return lowerLimit.get(index);
	}

	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	public Ordering getOrder() {
		return order;
	}

	public void simpleBounds(DoubleSolution solution) {
		for (int i = 0; i < getNumberOfVariables(); i++) {
			if (solution.getVariableValue(i) < getLowerBound(i)) {
				solution.setVariableValue(i, getLowerBound(i));
			} else if (solution.getVariableValue(i) > getUpperBound(i)) {
				solution.setVariableValue(i, getUpperBound(i));
			}
		}
	}

	public boolean plotProblem(String filename) throws Exception {
		PrintWriter writer;

		try {
			writer = new PrintWriter(filename, "UTF-8");
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}

		writer.printf("%d %d\n", this.lowerLimit.get(0), this.lowerLimit.get(0));
		DoubleSolution solution = new DoubleSolution(numberOfVariables);

		for (double y = this.lowerLimit.get(1); y < this.upperLimit.get(1); y += 1) {
			StringBuilder sb = new StringBuilder();
			solution.setVariableValue(1, y);

			for (double x = this.lowerLimit.get(0); x < this.upperLimit.get(0) - 1; x += 1) {
				solution.setVariableValue(0, x);
				evaluate(solution);
				sb.append(solution.getObjective() + " ");
			}

			solution.setVariableValue(0, this.upperLimit.get(0));
			evaluate(solution);
			sb.append(solution.getObjective());
			writer.println(sb.toString());
		}

		writer.close();
		return true;
	}

	public void setNumFunction(int numFunction) {
	}

	public Integer getNumFunction() {
		return null;
	}

	public abstract String getName();

	public abstract void evaluate(BigDecimalSolution solution);

	public abstract void evaluate(DoubleSolution solution);

	public abstract void evaluate(IntegerSolution solution);

	public abstract double getError(double result);
}
