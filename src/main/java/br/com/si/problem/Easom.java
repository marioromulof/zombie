package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Easom extends Problem {

	public Easom(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Easom";
		// Global Min -> f(x*) = -1.0, at x* = (Math.PI, Math.PI)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double aux1 = solution.getVariableValue(0);
		double aux2 = solution.getVariableValue(1);

		double fact1 = -Math.cos(aux1) * Math.cos(aux2);
		double fact2 = Math.exp(-Math.pow(aux1 - Math.PI, 2) - Math.pow(aux2 - Math.PI, 2));

		solution.setObjective(fact1 * fact2);
	}

	@Override
	public double getError(double result) {
		return result + 1.0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double aux1 = solution.getVariableValue(0).doubleValue();
		double aux2 = solution.getVariableValue(1).doubleValue();

		double fact1 = -Math.cos(aux1) * Math.cos(aux2);
		double fact2 = Math.exp(-Math.pow(aux1 - Math.PI, 2) - Math.pow(aux2 - Math.PI, 2));

		solution.setObjective(fact1 * fact2);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
