package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Michalewicz extends Problem {

	public Michalewicz(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(0.0);
			this.upperLimit.add(+Math.PI);
		}
	}

	@Override
	public String getName() {
		return "Michalewicz";
		// Global Min -> f(x*) = -1.8013, at x* = (2.20, 1.57), at d = 2
		// Global Min -> f(x*) = -4.687658, at d = 5
		// Global Min -> f(x*) = -9.6605, at d = 10
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 0; i < nVar; i++) {
			value += Math.sin(solution.getVariableValue(i)) * Math.pow(
					Math.sin((1.0 + i) * solution.getVariableValue(i) * solution.getVariableValue(i) / Math.PI), 20.0);
		}
		value *= -1.0;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		if (getNumberOfVariables() == 2) {
			return result + 1.8013;
		} else if (getNumberOfVariables() == 5) {
			return result + 4.687658;
		} else {
			return result + 9.6605;
		}
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 0; i < nVar; i++) {
			value += Math.sin(solution.getVariableValue(i).doubleValue())
					* Math.pow(Math.sin((i + 1) * solution.getVariableValue(i).pow(2).doubleValue() / Math.PI), 20.0);
		}

		solution.setObjective(-value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
