package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Katsuura extends Problem {

	public Katsuura(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Katsuura";
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value, p, axis;
		double numberOfVariables = (double) getNumberOfVariables();

		p = 1;
		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i);
			double sum = 0;
			for (int j = 1; j <= 32; j++) {
				double term = Math.pow(2.0, (double) j) * axis;
				sum += Math.abs(term - Math.round(term)) / Math.pow(2, j);
			}
			p *= Math.pow(1 + ((i + 1) * sum), 10.0 / Math.pow(numberOfVariables, 1.2));
		}
		value = (10.0 / numberOfVariables * numberOfVariables) * p - (10.0 / numberOfVariables * numberOfVariables);

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value, p, axis;
		double numberOfVariables = (double) getNumberOfVariables();

		p = 1;
		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i).doubleValue();
			double sum = 0;
			for (int j = 1; j <= 32; j++) {
				double term = Math.pow(2.0, (double) j) * axis;
				sum += Math.abs(term - Math.round(term)) / Math.pow(2, j);
			}
			p *= Math.pow(1 + ((i + 1) * sum), 10.0 / Math.pow(numberOfVariables, 1.2));
		}
		value = (10.0 / numberOfVariables * numberOfVariables) * p - (10.0 / numberOfVariables * numberOfVariables);

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
