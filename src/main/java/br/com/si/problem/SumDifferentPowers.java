package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class SumDifferentPowers extends Problem {

	public SumDifferentPowers(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-1.0);
			// this.upperLimit.add(+1.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Sum of Different Powers";
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 0; i < nVar; i++) {
			value += Math.pow(Math.abs(solution.getVariableValue(i)), (i + 2));
		}

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal value = BigDecimal.ZERO;

		for (int i = 0; i < nVar; i++) {
			value = value.add(solution.getVariableValue(i).abs().pow(i + 2));
		}

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
