package br.com.si.problem;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Griewanks extends Problem {

	private int type;

	public Griewanks(int numberOfVariables, Ordering order, String typeSolution, int type) {
		super(numberOfVariables, order, typeSolution);

		this.type = type;

		if (this.type == 1) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-600.0);
				this.upperLimit.add(+600.0);
			}
		} else if (this.type == 2) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-50.0);
				this.upperLimit.add(+50.0);
			}
		} else if (this.type == 3) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-10.0);
				this.upperLimit.add(+10.0);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.12);
				this.upperLimit.add(+5.12);
			}
		}

	}

	@Override
	public String getName() {
		if (type == 1) {
			return "Griewanks600";
		} else if (type == 2) {
			return "Griewanks50";
		} else if (type == 3) {
			return "Griewanks10";
		} else {
			return "Griewanks5";
		}
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double numberOfVariables = (double) getNumberOfVariables();
		double sum = 0.0;
		double prod = 1.0;

		for (int i = 0; i < numberOfVariables; i++) {
			sum += solution.getVariableValue(i) * solution.getVariableValue(i) / 4000.0;
			prod *= Math.cos(solution.getVariableValue(i) / Math.sqrt((double) (1.0 + i)));
		}

		solution.setObjective(sum - prod + 1.0);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double numberOfVariables = (double) getNumberOfVariables();
		BigDecimal sum = BigDecimal.ZERO;
		BigDecimal prod = BigDecimal.ONE;

		for (int i = 0; i < numberOfVariables; i++) {
			sum = sum.add(solution.getVariableValue(i).pow(2).divide(BigDecimal.valueOf(4000.0)));
			prod = prod.multiply(BigDecimal.valueOf(Math.cos(solution.getVariableValue(i)
					.divide(BigDecimal.valueOf(Math.sqrt((double) (i + 1))), RoundingMode.CEILING).doubleValue())));
		}

		solution.setObjective(sum.subtract(prod).add(BigDecimal.ONE).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
