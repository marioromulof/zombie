package br.com.si.problem;

import java.util.Random;

import org.apache.commons.math3.util.FastMath;

import br.com.si.algorithm.ZombieEDA;
import br.com.si.params.ZombieParams;
import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class OptimizeParameters extends Problem {

	private int numFunction;
	private int numDim;

	public OptimizeParameters(int numberOfVariables, Ordering order, String typeSolution, Integer numFunction,
			Integer dim) {
		super(numberOfVariables, order, typeSolution);
		this.numFunction = numFunction;
		this.numDim = dim;

		// (int) maxHumanPop
		this.lowerLimit.add(2.0);
		this.upperLimit.add(30.0 * dim);
		// (int) humanStepsPerZombieStep
		this.lowerLimit.add(1.0);
		this.upperLimit.add(100.0);
		// (double) hNinit
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) hNstd
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) hCinit
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) hCstd
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (int) hQ
		this.lowerLimit.add(1.0);
		this.upperLimit.add(5.0 * dim);
		// (int) zombiePopSize
		this.lowerLimit.add(1.0);
		this.upperLimit.add(30.0 * dim);
		// (double) zNinit
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) zNstd
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) zCinit
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (double) zCstd
		this.lowerLimit.add(0.00001);
		this.upperLimit.add(1.0);
		// (int) zQ
		this.lowerLimit.add(0.0);
		this.upperLimit.add(5.0 * dim);
		// (double) maxDistance
		this.lowerLimit.add(0.0);
		double max = 40000 * dim;
		this.upperLimit.add(FastMath.sqrt(max));
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int iterations = 10000 * numDim;

		while (solution.getVariableValue(7).intValue() > solution.getVariableValue(0).intValue()) {
			solution.setVariableValue(7, (solution.getVariableValue(7) + this.getLowerBound(7)) * 0.5);
		}

		ZombieParams zp = new ZombieParams(iterations, solution.getVariableValue(0).intValue(),
				solution.getVariableValue(1).intValue(), solution.getVariableValue(2), solution.getVariableValue(3),
				solution.getVariableValue(4), solution.getVariableValue(5), solution.getVariableValue(6).intValue(),
				solution.getVariableValue(7).intValue(), solution.getVariableValue(8), solution.getVariableValue(9),
				solution.getVariableValue(10), solution.getVariableValue(11), solution.getVariableValue(12).intValue(),
				solution.getVariableValue(13), -1);

		Problem problem = new ProblemsCEC15(numDim, order, "double", numFunction);

		ZombieEDA z = new ZombieEDA(problem, zp, false, false, true, false, false);
		DoubleSolution result = z.search();

		solution.setObjective(getError(result.getObjective() + z.getPunishment()));
	}

	@Override
	public String getName() {
		return "OptParam-" + getNumFunction() + "-" + getNumDim();
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		// TODO Auto-generated method stub

	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getError(double result) {
		return result - (double) numFunction * 100;
	}

	public DoubleSolution createSolution(Random random) {
		DoubleSolution solution = new DoubleSolution(getNumberOfVariables(), getOrder());

		for (int i = 0; i < getNumberOfVariables(); i++) {
			if (i == 7) {
				solution.setVariableValue(i,
						getLowerBound(i) + random.nextDouble() * (solution.getVariableValue(0) - getLowerBound(i)));
			} else {
				solution.setVariableValue(i,
						getLowerBound(i) + random.nextDouble() * (getUpperBound(i) - getLowerBound(i)));
			}
		}

		return solution;
	}

	@Override
	public Integer getNumFunction() {
		return numFunction;
	}

	public Integer getNumDim() {
		return numDim;
	}
}
