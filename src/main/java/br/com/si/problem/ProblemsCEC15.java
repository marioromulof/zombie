
/*
CEC14 Test Function Suite for Single Objective Optimization
BO Zheng (email: zheng.b1988@gmail.com)
Dec. 19th 2013
Modified in Aug. 8th 2014
 */
package br.com.si.problem;

import br.com.si.problem.math.Functions;
import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.DataReader;
import br.com.si.util.Ordering;

public class ProblemsCEC15 extends Problem {
	// -------------- 0_ 1_ 2_ 3_ 4_ 5_ 6_ 7_ 8_ 9 10 11 12 13 14_ 15
	int[] cf_nums = { 0, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 5, 5, 5, 7, 10 };
	int[] bShuffle = { 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0 };

	private double[] OShift, M, bias;
	private int[] SS;
	private int numFunction;

	public ProblemsCEC15(int numberOfVariables, Ordering order, String typeSolution,
			Integer numFunction/* , boolean correct */) {
		super(numberOfVariables, order, typeSolution);
		initialization(numberOfVariables, null, numFunction/* , correct */);
	}

	public void initialization(Integer numberOfVariables, Ordering order,
			Integer numFunction/* , boolean correct */) {
		if (numberOfVariables != null) {
			this.numberOfVariables = numberOfVariables;

			this.lowerLimit.clear();
			this.upperLimit.clear();
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-100.0);
				this.upperLimit.add(+100.0);
			}
		}

		if (order != null) {
			this.order = order;
		}

		if (numFunction != null) {
			this.numFunction = numFunction;
			int cf_num = cf_nums[this.numFunction];

			/*
			 * Load Matrix M****************************************************
			 */
			M = DataReader.readRotation(this.numFunction, this.numberOfVariables, cf_num);

			/*
			 * Load Bias_value bias
			 *************************************************/
			bias = DataReader.readBiasValue(this.numFunction,
					cf_num/* , correct */);

			/*
			 * Load shift_data**************************************************
			 */
			OShift = DataReader.readShiftData(this.numFunction, this.numberOfVariables, cf_num);

			/* Load Shuffle_data****************************************** */
			if (bShuffle[this.numFunction] == 1) {
				SS = DataReader.readShuffleData(this.numFunction, this.numberOfVariables, cf_num);
			}
		}
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double[] f = new double[1];
		double[] x = new double[numberOfVariables];
		for (int i = 0; i < numberOfVariables; i++) {
			x[i] = solution.getVariableValue(i);
		}

		switch (numFunction) {
		case 1:
			f[0] = Functions.ellips_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 100.0;
			break;

		case 2:
			f[0] = Functions.bent_cigar_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 200.0;
			break;

		case 3:
			f[0] = Functions.ackley_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 300.0;
			break;

		case 4:
			f[0] = Functions.rastrigin_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 400.0;
			break;

		case 5:
			f[0] = Functions.schwefel_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 500.0;
			break;

		case 6:
			f[0] = Functions.hf01(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 600.0;
			break;

		case 7:
			f[0] = Functions.hf02(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 700.0;
			break;

		case 8:
			f[0] = Functions.hf03(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 800.0;
			break;

		case 9:
			f[0] = Functions.cf01(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 900.0;
			break;

		case 10:
			f[0] = Functions.cf02(x, numberOfVariables, OShift, M, SS, bias, 1);
			f[0] += 1000.0;
			break;

		case 11:
			f[0] = Functions.cf03(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1100.0;
			break;

		case 12:
			f[0] = Functions.cf04(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1200.0;
			break;

		case 13:
			f[0] = Functions.cf05(x, numberOfVariables, OShift, M, SS, bias, 1);
			f[0] += 1300.0;
			break;

		case 14:
			f[0] = Functions.cf06(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1400.0;
			break;

		case 15:
			f[0] = Functions.cf07(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1500.0;
			break;

		default:
			break;
		}

		// solution.setObjective(new BigDecimal(String.valueOf(f[0])));
		solution.setObjective(f[0]);
	}

	@Override
	public String getName() {
		return "ProblemsCEC15:f" + numFunction;
	}

	@Override
	public Integer getNumFunction() {
		return numFunction;
	}

	@Override
	public double getError(double result) {
		return result - (double) numFunction * 100;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double[] f = new double[1];
		double[] x = new double[numberOfVariables];
		for (int i = 0; i < numberOfVariables; i++) {
			x[i] = solution.getVariableValue(i).doubleValue();
		}

		switch (numFunction) {
		case 1:
			f[0] = Functions.ellips_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 100.0;
			break;

		case 2:
			f[0] = Functions.bent_cigar_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 200.0;
			break;

		case 3:
			f[0] = Functions.ackley_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 300.0;
			break;

		case 4:
			f[0] = Functions.rastrigin_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 400.0;
			break;

		case 5:
			f[0] = Functions.schwefel_func(x, numberOfVariables, OShift, M, 1, 1);
			f[0] += 500.0;
			break;

		case 6:
			f[0] = Functions.hf01(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 600.0;
			break;

		case 7:
			f[0] = Functions.hf02(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 700.0;
			break;

		case 8:
			f[0] = Functions.hf03(x, numberOfVariables, OShift, M, SS, 1, 1);
			f[0] += 800.0;
			break;

		case 9:
			f[0] = Functions.cf01(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 900.0;
			break;

		case 10:
			f[0] = Functions.cf02(x, numberOfVariables, OShift, M, SS, bias, 1);
			f[0] += 1000.0;
			break;

		case 11:
			f[0] = Functions.cf03(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1100.0;
			break;

		case 12:
			f[0] = Functions.cf04(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1200.0;
			break;

		case 13:
			f[0] = Functions.cf05(x, numberOfVariables, OShift, M, SS, bias, 1);
			f[0] += 1300.0;
			break;

		case 14:
			f[0] = Functions.cf06(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1400.0;
			break;

		case 15:
			f[0] = Functions.cf07(x, numberOfVariables, OShift, M, bias, 1);
			f[0] += 1500.0;
			break;

		default:
			break;
		}

		// solution.setObjective(new BigDecimal(String.valueOf(f[0])));
		solution.setObjective(f[0]);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}
}
