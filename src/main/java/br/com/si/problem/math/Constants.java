/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.si.problem.math;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 
 * @author QIN
 */
public final class Constants {
	public final static double INF = 1.0e99;
	public final static double EPS = 1.0e-14;
	public final static double E = 2.7182818284590452353602874713526625;
	public final static double PI = 3.1415926535897932384626433832795029;
	public final static BigDecimal SMALLER_VALUE = BigDecimal.ONE.divide(BigDecimal.TEN.pow(8), 10,
			RoundingMode.HALF_UP); // 10e-8
}

// ~ Formatted by Jindent --- http://www.jindent.com
