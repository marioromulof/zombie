package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Matyas extends Problem {

	public Matyas(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-10.0);
			// this.upperLimit.add(+10.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Matyas";
		// Global Min -> f(x*) = 0.0, at x* = (0, 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value;

		value = 0.26 * (solution.getVariableValue(0) * solution.getVariableValue(0)
				+ solution.getVariableValue(1) * solution.getVariableValue(1));
		value -= 0.48 * solution.getVariableValue(0) * solution.getVariableValue(1);

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal.valueOf(0.26)
				.multiply(solution.getVariableValue(0).pow(2).add(solution.getVariableValue(1).pow(2)));
		value = value.add(BigDecimal.valueOf(-0.48).multiply(solution.getVariableValue(0))
				.multiply(solution.getVariableValue(1)));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
