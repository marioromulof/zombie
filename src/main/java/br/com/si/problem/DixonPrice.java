package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class DixonPrice extends Problem {

	public DixonPrice(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-10.0);
			// this.upperLimit.add(+10.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Dixon-Price";
		// Global Min -> f(x*) = 0.0
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double value = 0.0;

		for (int i = 1; i < nVar; i++) {
			double aux = 2.0 * solution.getVariableValue(i) * solution.getVariableValue(i)
					- solution.getVariableValue(i - 1);
			aux *= aux;
			value += (1.0 + i) * aux;
		}
		value += (solution.getVariableValue(0) - 1.0) * (solution.getVariableValue(0) - 1.0);

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal value = BigDecimal.ZERO;

		for (int i = 1; i < nVar; i++) {
			value = value.add(solution.getVariableValue(i).pow(2).multiply(BigDecimal.valueOf(2.0))
					.subtract(solution.getVariableValue(i - 1)).pow(2).multiply(BigDecimal.valueOf(i + 1)));
		}
		value = value.add(solution.getVariableValue(0).subtract(BigDecimal.ONE).pow(2));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
