package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Rastrigin extends Problem {
	public Rastrigin(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-5.12);
			// this.upperLimit.add(+5.12);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Rastrigin";
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double numberOfVariables = (double) this.getNumberOfVariables();
		double value = 0;

		for (int i = 0; i < numberOfVariables; i++) {
			double axis = solution.getVariableValue(i);
			value += axis * axis - (10.0 * Math.cos(2.0 * Math.PI * axis)) + 10.0;
		}

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double axis;
		double numberOfVariables = (double) this.getNumberOfVariables();
		double value = 0;

		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i).doubleValue();
			value += Math.pow(axis, 2.0) - (10.0 * Math.cos(2.0 * Math.PI * axis)) + 10.0;
		}

		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
