package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Zakharov extends Problem {

	public Zakharov(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-5.0);
			// this.upperLimit.add(+10.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Zakharov";
		// Global Min -> f(x*) = 0.0, at x* = (0, ..., 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		int nVar = getNumberOfVariables();
		double sum1 = 0.0;
		double sum2 = 0.0;

		for (int i = 0; i < nVar; i++) {
			sum1 += solution.getVariableValue(i) * solution.getVariableValue(i);
			sum2 += solution.getVariableValue(i) * 0.5 * (1.0 + i);
		}

		sum1 += sum2 * sum2 + sum2 * sum2 * sum2 * sum2;
		solution.setObjective(sum1);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		int nVar = getNumberOfVariables();
		BigDecimal sum1 = BigDecimal.ZERO;
		BigDecimal sum2 = BigDecimal.ZERO;

		for (int i = 0; i < nVar; i++) {
			sum1 = sum1.add(solution.getVariableValue(i).pow(2));
			sum2 = sum2.add(
					solution.getVariableValue(i).multiply(BigDecimal.valueOf(0.5)).multiply(BigDecimal.valueOf(i + 1)));
		}

		solution.setObjective(sum1.add(sum2.pow(2)).add(sum2.pow(4)).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
