package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Booth extends Problem {

	public Booth(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(-10.0);
			// this.upperLimit.add(+10.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Booth";
		// Global Min -> f(x*) = 0.0, at x* = (1, 3)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value1, value2;

		value1 = solution.getVariableValue(0) + 2 * solution.getVariableValue(1) - 7;
		value1 *= value1;

		value2 = 2 * solution.getVariableValue(0) + solution.getVariableValue(1) - 5;
		value2 *= value2;

		solution.setObjective(value1 + value2);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = solution.getVariableValue(0).add(BigDecimal.valueOf(2).multiply(solution.getVariableValue(1)))
				.subtract(BigDecimal.valueOf(7)).pow(2);
		value = value.add(BigDecimal.valueOf(2).multiply(solution.getVariableValue(0)).add(solution.getVariableValue(1))
				.subtract(BigDecimal.valueOf(5)).pow(2));

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
