package br.com.si.problem;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class DropWave extends Problem {

	private boolean smaller;

	public DropWave(int numberOfVariables, Ordering order, String typeSolution, boolean smaller) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		this.smaller = smaller;

		if (this.smaller) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-2.0);
				this.upperLimit.add(+2.0);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-5.12);
				this.upperLimit.add(+5.12);
			}
		}

	}

	@Override
	public String getName() {
		if (smaller) {
			return "Drop-Wave-Smaller";
		} else {
			return "Drop-Wave";
		}
		// Global Min -> f(x*) = -1.0, at x* = (0, 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value, aux;

		aux = solution.getVariableValue(0) * solution.getVariableValue(0)
				+ solution.getVariableValue(1) * solution.getVariableValue(1);
		value = -1.0 * (1.0 + Math.cos(12.0 * Math.sqrt(aux))) / (0.5 * aux + 2.0);

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result + 1.0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal.ONE
				.add(BigDecimal.valueOf(Math.cos(12.0 * Math.sqrt(
						solution.getVariableValue(0).pow(2).add(solution.getVariableValue(1).pow(2)).doubleValue()))))
				.divide(BigDecimal.valueOf(0.5)
						.multiply(solution.getVariableValue(0).pow(2).add(solution.getVariableValue(1).pow(2)))
						.add(BigDecimal.valueOf(2.0)), RoundingMode.CEILING)
				.negate();

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
