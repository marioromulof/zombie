package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Weierstrass extends Problem {

	public Weierstrass(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Weierstrass";
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value = 0;
		double sum, sub;
		double axis;
		double a = 0.5;
		double b = 3.0;
		int kmax = 20;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i);
			sum = 0;
			sub = 0;
			for (int k = 0; k < kmax; k++) {
				sum += a * Math.cos(2.0 * Math.PI * b * (axis + 0.5));
				sub += a * Math.cos(2.0 * Math.PI * b * 0.5);
			}
			value += (sum - (numberOfVariables * sub));
		}

		//solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double sum, sub;
		double axis;
		double a = 0.5;
		double b = 3.0;
		int kmax = 20;
		double numberOfVariables = (double) getNumberOfVariables();

		for (int i = 0; i < numberOfVariables; i++) {
			axis = solution.getVariableValue(i).doubleValue();
			sum = 0;
			sub = 0;
			for (int k = 0; k < kmax; k++) {
				sum += a * Math.cos(2.0 * Math.PI * b * (axis + 0.5));
				sub += a * Math.cos(2.0 * Math.PI * b * 0.5);
			}
			value += (sum - (numberOfVariables * sub));
		}

		//solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub
		
	}

}
