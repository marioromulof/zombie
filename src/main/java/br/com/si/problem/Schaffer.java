package br.com.si.problem;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Schaffer extends Problem {

	public Schaffer(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Schaffer";
		// Global Min -> f(x*) = 0.0, at x* = (0, 0)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value, value2;

		value = Math.sin(solution.getVariableValue(0) * solution.getVariableValue(0)
				- solution.getVariableValue(1) * solution.getVariableValue(0));
		value *= value;
		value -= 0.5;

		value2 = 1.0 + 0.001 * (solution.getVariableValue(0) * solution.getVariableValue(0)
				+ solution.getVariableValue(1) * solution.getVariableValue(1));
		value2 *= value2;

		value /= value2;
		value += 0.5;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal.valueOf(Math
				.sin(solution.getVariableValue(0).pow(2).subtract(solution.getVariableValue(1).pow(2)).doubleValue()))
				.pow(2).subtract(BigDecimal.valueOf(0.5));
		value = value
				.divide(BigDecimal.ONE
						.add(BigDecimal.valueOf(0.001)
								.multiply(solution.getVariableValue(0).pow(2).add(solution.getVariableValue(1).pow(2))))
						.pow(2), RoundingMode.CEILING);

		solution.setObjective(value.add(BigDecimal.valueOf(0.5)).doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}
}