package br.com.si.problem;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Elliptic extends Problem {

	public Elliptic(int numberOfVariables, Ordering order, String typeSolution) {
		super(numberOfVariables, order, typeSolution);

		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Elliptic";
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value = 0;
		double numberOfVariables = (double) (getNumberOfVariables() - 1);

		for (int i = 0; i <= numberOfVariables; i++) {
			value += Math.pow(Math.pow(10.0, 6.0), ((double) i / numberOfVariables)) * solution.getVariableValue(i)
					* solution.getVariableValue(i);
		}

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double numberOfVariables = (double) (getNumberOfVariables() - 1);

		for (int i = 0; i <= numberOfVariables; i++) {
			value += Math.pow(Math.pow(10.0, 6.0), ((double) i / numberOfVariables))
					* Math.pow(solution.getVariableValue(i).doubleValue(), 2.0);
		}

		// solution.setObjective(new BigDecimal(String.valueOf(value)));
		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
