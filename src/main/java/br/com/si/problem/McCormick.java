package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class McCormick extends Problem {

	public McCormick(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		// this.lowerLimit.add(-1.5);
		// this.lowerLimit.add(-3.0);
		// this.upperLimit.add(+4.0);
		// this.upperLimit.add(+4.0);
		for (int i = 0; i < numberOfVariables; i++) {
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "McCormick";
		// Global Min -> f(x*) = -1.9133, at x* = (-0.54719, -1.54719)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value;

		value = Math.sin(solution.getVariableValue(0) + solution.getVariableValue(1));
		value += (solution.getVariableValue(0) - solution.getVariableValue(1))
				* (solution.getVariableValue(0) - solution.getVariableValue(1));
		value -= 1.5 * solution.getVariableValue(0);
		value += 2.5 * solution.getVariableValue(1) + 1.0;

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result + 1.9133;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal value;

		value = BigDecimal
				.valueOf(Math.sin(solution.getVariableValue(0).add(solution.getVariableValue(1)).doubleValue()));
		value = value.add(solution.getVariableValue(0).subtract(solution.getVariableValue(1)).pow(2));
		value = value.add(BigDecimal.valueOf(-1.5).multiply(solution.getVariableValue(0)));
		value = value.add(BigDecimal.valueOf(2.5).multiply(solution.getVariableValue(1)));
		value = value.add(BigDecimal.ONE);

		solution.setObjective(value.doubleValue());
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
