package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class Rosenbrocks extends Problem {

	private boolean restricted;

	public Rosenbrocks(int numberOfVariables, Ordering order, String typeSolution, boolean restricted) {
		super(numberOfVariables, order, typeSolution);
		this.restricted = restricted;

		if (this.restricted) {
			for (int i = 0; i < numberOfVariables; i++) {
				this.lowerLimit.add(-2.048);
				this.upperLimit.add(+2.048);
			}
		} else {
			for (int i = 0; i < numberOfVariables; i++) {
				// this.lowerLimit.add(-5.0);
				// this.upperLimit.add(+10.0);
				this.lowerLimit.add(-100.0);
				this.upperLimit.add(+100.0);
			}
		}
	}

	@Override
	public String getName() {
		if (this.restricted) {
			return "Rosenbrocks-Restricted";
		} else {
			return "Rosenbrocks";
		}
		// Global Min -> f(x*) = 0.0
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double value2, value = 0.0;
		double numberOfVariables = (double) getNumberOfVariables() - 1;

		if (getNumberOfVariables() != 2) {
			for (int i = 0; i < numberOfVariables; i++) {
				value2 = solution.getVariableValue(i + 1) - solution.getVariableValue(i) * solution.getVariableValue(i);
				value2 *= value2;
				value += 100 * value2;

				value2 = solution.getVariableValue(i) - 1.0;
				value2 *= value2;
				value += value2;
			}
		} else {
			value2 = solution.getVariableValue(0) * solution.getVariableValue(0) - solution.getVariableValue(1);
			value2 *= value2;
			value += 100 * value2;

			value2 = solution.getVariableValue(0) - 1.0;
			value2 *= value2;
			value += value2;
		}

		solution.setObjective(value);
	}

	@Override
	public double getError(double result) {
		return result - 0;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		double value = 0;
		double numberOfVariables = (double) getNumberOfVariables() - 1;

		if (getNumberOfVariables() != 2) {
			for (int i = 0; i < numberOfVariables; i++) {
				value += 100 * solution.getVariableValue(i + 1).subtract(solution.getVariableValue(i).pow(2)).pow(2)
						.doubleValue();
				value += solution.getVariableValue(i).subtract(BigDecimal.ONE).pow(2).doubleValue();
			}
		} else {
			value += 100
					* solution.getVariableValue(0).pow(2).subtract(solution.getVariableValue(1)).pow(2).doubleValue();
			value += solution.getVariableValue(0).subtract(BigDecimal.ONE).pow(2).doubleValue();
		}

		solution.setObjective(value);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
