package br.com.si.problem;

import java.math.BigDecimal;

import br.com.si.solution.BigDecimalSolution;
import br.com.si.solution.DoubleSolution;
import br.com.si.solution.IntegerSolution;
import br.com.si.util.Ordering;

public class GoldsteinPriceScaled extends Problem {

	public GoldsteinPriceScaled(int numberOfVariables, Ordering order, String typeSolution) throws Exception {
		super(numberOfVariables, order, typeSolution);

		if (numberOfVariables != 2) {
			throw new Exception("Quantidade de dimensões não suportada: " + numberOfVariables
					+ ", a quantidade permitida deve ser igual a 2 (dois).");
		}

		for (int i = 0; i < numberOfVariables; i++) {
			// this.lowerLimit.add(0.0);
			// this.upperLimit.add(+1.0);
			this.lowerLimit.add(-100.0);
			this.upperLimit.add(+100.0);
		}
	}

	@Override
	public String getName() {
		return "Goldstein-Price Scaled";
		// Global Min -> f(x*) = -3.129125550610585, at x* = (0.5, 0.25)
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		double factA, factB, fact1, fact2, x1, x2;

		x1 = 4.0 * solution.getVariableValue(0) - 2.0;
		x2 = 4.0 * solution.getVariableValue(1) - 2.0;

		factA = (x1 + x2 + 1) * (x1 + x2 + 1);
		factB = 19.0 - 14.0 * x1 + 3.0 * x1 * x1 - 14.0 * x2 + 3.0 * x2 * x2 + 6.0 * x1 * x2;
		fact1 = 1.0 + factA * factB;

		factA = 2.0 * x1 - 3.0 * x2;
		factA *= factA;
		factB = 18.0 - 32.0 * x1 + 12.0 * x1 * x1 + 48.0 * x2 + 27.0 * x2 * x2 - 36.0 * x1 * x2;
		fact2 = 30.0 + factA * factB;

		factA = (Math.log(fact1 * fact2) - 8.693) / 2.427;

		solution.setObjective(factA);
	}

	@Override
	public double getError(double result) {
		return result + 3.129125550610585;
	}

	@Override
	public void evaluate(BigDecimalSolution solution) {
		BigDecimal factA, factB, fact1, fact2, x1, x2;

		x1 = BigDecimal.valueOf(4).multiply(solution.getVariableValue(0)).subtract(BigDecimal.valueOf(2));
		x2 = BigDecimal.valueOf(4).multiply(solution.getVariableValue(1)).subtract(BigDecimal.valueOf(2));

		factA = x1.add(x2).add(BigDecimal.ONE).pow(2);
		factB = BigDecimal.valueOf(19).subtract(BigDecimal.valueOf(14).multiply(x1))
				.add(BigDecimal.valueOf(3).multiply(x1.pow(2))).subtract(BigDecimal.valueOf(14).multiply(x2))
				.add(BigDecimal.valueOf(6).multiply(x1).multiply(x2)).add(BigDecimal.valueOf(3).multiply(x2.pow(2)));
		fact1 = BigDecimal.ONE.add(factA.multiply(factB));

		factA = BigDecimal.valueOf(2).multiply(x1).subtract(BigDecimal.valueOf(3).multiply(x2)).pow(2);
		factB = BigDecimal.valueOf(18).subtract(BigDecimal.valueOf(32).multiply(x1))
				.add(BigDecimal.valueOf(12).multiply(x1.pow(2))).add(BigDecimal.valueOf(48).multiply(x2))
				.subtract(BigDecimal.valueOf(36).multiply(x1).multiply(x2))
				.add(BigDecimal.valueOf(27).multiply(x2.pow(2)));
		fact2 = BigDecimal.valueOf(30).add(factA.multiply(factB));

		solution.setObjective((Math.log(fact1.multiply(fact2).doubleValue()) - 8.693) / 2.427);
	}

	@Override
	public void evaluate(IntegerSolution solution) {
		// TODO Auto-generated method stub

	}

}
